/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Config
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 *
 * TODO: Deprecate this.
 */

/* -- NAMESPACES -- */

/** @namespace api.v1 */
/** @namespace api.v1.core */
/** @namespace api.v1.core.get */
/** @namespace api.v1.core.post */
/** @namespace api.v1.core.delete */

/** @namespace api.v1.pdb */
/** @namespace api.v1.pdb.get */
/** @namespace api.v1.pdb.post */
/** @namespace api.v1.pdb.delete */

/** @namespace api.v1.cdn */
/** @namespace api.v1.cdn.get */
/** @namespace api.v1.cdn.post */
/** @namespace api.v1.cdn.delete */

/** @namespace api.v1.crypto */
/** @namespace api.v1.crypto.get */
/** @namespace api.v1.crypto.post */
/** @namespace api.v1.crypto.delete */

/** @namespace api.v1.voice */
/** @namespace api.v1.voice.get */
/** @namespace api.v1.voice.post */
/** @namespace api.v1.voice.delete */

/** @namespace api.v1.wallet */
/** @namespace api.v1.wallet.get */
/** @namespace api.v1.wallet.post */
/** @namespace api.v1.wallet.delete */

/** @namespace middleware.v1 */

/*
* DEFINING CONSOLE LOGGING INDICATORS
* -----------------------------------
* Note: Options are base 3. (2^3 - 1) = 7 different logging options.
*
* Code    | Fatal    | Warning    | Good
* ------------------------------------------
* 000 (0) | False    | False      | False
* 001 (1) | False    | False      | True
* 010 (2) | False    | True       | False
* 011 (3) | False    | True       | True
* 100 (4) | True     | False      | False
* 101 (5) | True     | False      | True
* 110 (6) | True     | True       | False
* 111 (7) | True     | True       | True
* */

/* -- CONFIG CONSTANTS -- */

/**
 * Determine which Express Response Statuses to be recorded by TIM.
 * @type {Number[]}
 */
export const GLOBAL_RESPONSE_STATUSES_TO_LOG = [
  401, // Unauthorized
  403, // Forbidden
  404, // Not Found
  406, // Not acceptable
  407, // Proxy Authentication Required
  429, // Too Many Requests
  511 // Network Authentication Required
]

/**
 * Leveraged by each node to determine their networking configuration.
 * @type {Object}
 */
export const NETWORK_CONFIG =
{

  CORE: {
    IP: '192.168.1.202',
    PORTS: [
      9000,
      9001
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  VOICE: {
    IP: '127.0.0.1',
    PORTS: [
      9100,
      9101
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  PDB: {
    IP: '192.168.1.202',
    PORTS: [
      9200,
      9201
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  CRYPTO: {
    IP: '192.168.1.202',
    PORTS: [
      9300,
      9301
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  WALLET: {
    IP: '192.168.1.202',
    PORTS: [
      9400,
      9401
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  CDN: {
    IP: '192.168.1.202',
    PORTS: [
      9500,
      9501
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  },

  BUBBLE: {
    IP: '192.168.1.202',
    PORTS: [
      9600,
      9601
    ],
    LOG: {
      os: false,
      network: false,
      api_status_codes: [
        ...GLOBAL_RESPONSE_STATUSES_TO_LOG
      ]
    }
  }

}

/**
 * Endpoint Strings to simplify node referencing for the CORE.
 * @type {Object}
 */
export const ENDPOINT_STRINGS = {

  CORE: {
    endpoint: `${process.env.HTTP_STATUS}://${NETWORK_CONFIG.CORE.IP}:${NETWORK_CONFIG.CORE.PORTS[0]}/api/${process.env.API_VERSION}/core/`,
    resources: [
      'FILL THIS IN'
    ]
  },

  VOICE: {
    endpoint: `${process.env.HTTP_STATUS}://${NETWORK_CONFIG.VOICE.IP}:${NETWORK_CONFIG.VOICE.PORTS[0]}/api/${process.env.API_VERSION}/voice/`,
    resources: [
      {
        2000: {
          resource: 'add/bubble',
          parameters: [],
          status: 'TBD',
          deprecated: false
        },
        2100: {
          resource: 'remove/bubble',
          parameters: [],
          status: 'TBD',
          deprecated: false
        },
        2200: {
          resource: 'query/heartbeat',
          parameters: [],
          status: 'verify',
          deprecated: false
        }
      }
    ]
  },

  PDB: {
    endpoint: `${process.env.HTTP_STATUS}://${NETWORK_CONFIG.PDB.IP}:${NETWORK_CONFIG.PDB.PORTS[0]}/api/${process.env.API_VERSION}/pdb/`,
    resources: {
      3000: {
        resource: 'create/bubble',
        parameters: [],
        status: '',
        deprecated: false
      },
      3001: {
        resource: 'create/user',
        parameters: [],
        status: '',
        deprecated: false
      },
      3100: {
        resource: 'retrieve/bubble',
        parameters: ['bubble_id'],
        status: '',
        deprecated: false
      },
      3101: {
        resource: 'retrieve/bubbles',
        parameters: ['bubble_status', 'bubble_category'],
        status: '',
        deprecated: false
      },
      3102: {
        resource: 'retrieve/user_jwt',
        parameters: [''],
        status: 'WIP',
        deprecated: false
      },
      3200: {
        resource: 'query/self',
        parameters: ['access_jwt'],
        status: '',
        deprecated: false
      },
      3201: {
        resource: 'query/user',
        parameters: [],
        status: 'WIP',
        deprecated: false
      },
      3202: {
        resource: 'query/heartbeat',
        parameters: [],
        status: 'verify',
        deprecated: false
      },
      3300: {
        resource: 'update/user',
        parameters: [],
        status: 'TBD',
        deprecated: false
      },
      3301: {
        resource: 'update/bubble',
        parameters: [],
        status: 'TBD',
        deprecated: false
      },
      3400: {
        resource: 'delete/bubble',
        parameters: [],
        status: 'TBD',
        deprecated: false
      },
      3401: {
        resource: 'delete/user',
        parameters: [],
        status: 'TBD',
        deprecated: false
      }
    }
  },

  CRYPTO: {
    endpoint: `${process.env.HTTP_STATUS}://${NETWORK_CONFIG.CRYPTO.IP}:${NETWORK_CONFIG.CRYPTO.PORTS[0]}/api/${process.env.API_VERSION}/crypto/`,
    resources: {
      4000: {
        resource: 'generate/user_jwt',
        parameters: [],
        status: 'review',
        deprecated: false
      },
      4001: {
        resource: 'generate/access_jwt',
        parameters: ['user_jwt'],
        status: 'review',
        deprecated: false
      },
      4002: {
        resource: 'generate/snowflake_id',
        parameters: [],
        status: 'review',
        deprecated: false
      },
      4100: {
        resource: 'verify/jwt',
        parameters: ['jwt'],
        status: 'review',
        deprecated: false
      },
      4200: {
        resource: 'query/heartbeat',
        parameters: [],
        status: 'review',
        deprecated: false
      }
    }
  },

  BUBBLE: {
    endpoint: `${process.env.HTTP_STATUS}://${NETWORK_CONFIG.BUBBLE.IP}:${NETWORK_CONFIG.BUBBLE.PORTS[0]}/api/${process.env.API_VERSION}/bubble/`,
    serve: {
      uri: 'http://localhost:9500/static-content/',
      directories: ['bubble-images', 'profile-images', 'attachments']
    },
    resources: [
      {
        5000: {
          resource: 'upload/image',
          parameters: [],
          body: ['bubbleImageBuffer', 'bubbleID'],
          status: 'review',
          deprecated: false
        }
      }
    ]
  }

}

/**
 * Generalized MongoDB Connection String to be leveraged by the DBA.
 * @type {String}
 */
export const MONGO_CONNECTION_URI = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_ADDR}:${process.env.DB_PORT}/psdb`
