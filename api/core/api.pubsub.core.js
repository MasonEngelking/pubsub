/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name PUBSUB.CORE.API
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */

/* EXPRESS DEP */
import express  from 'express'
import cors     from 'cors'

/* OS DEP */
import os      from 'os'
import cluster from 'cluster'

/* NETWORK DEP */
import fileUpload from 'express-fileupload'
import {Server}   from 'socket.io'
import http       from 'http'

/* CACHE DEP */
import redis    from 'redis'

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

/* NODE MIDDLEWARE */
import {
    healthCheckAllNodes,
    handleQuery,
    handleDeleteViaDatabase,
    handleDeleteViaCDN,
    handleCreateViaDatabase,
    handleCreateViaCDN,
    handleUpdateViaDatabase,
    handleUpdateViaCDN,
    preventWithActiveSuspension,
    preventIncorrectAuthTokenAJWT,
    preventInvalidAuthToken,
    preventMalformedHCaptcha,
    preventMissingAuthToken,
    verifyHCaptchaToken,
    filenameParser,
    appendFilenameExtensionInfoToRes,
    configParser
} from './middleware.js'

/* GLOBAL MIDDLEWARE */
import {
    handleAppListen,
    logLevelPresentCheck,
} from "./global.middleware.js"

/* CACHE CONFIG */
// TODO: Implement redis into requests for efficiency
const redisClient = redis.createClient({
    host: 'redis',
    port: 6379
})

/* EXPRESS-APP CONFIG */
const app= express()
app.use(cors())
app.use(express.json())
app.use(fileUpload({
    limits: { fileSize: 5 * 1024 * 1024 },
    abortOnLimit: true
}))
app.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin',  '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    next()
})

/* Creating Server for Sockets */
const server= http.createServer(app)
const io= new Server(server)

const { 
    API_VERSION,
    API_CORE_IP_OR_DNS,
    API_VOICE_IP_OR_DNS,
    API_DB_IP_OR_DNS,
    API_CRYPTO_IP_OR_DNS,
    API_WALLET_IP_OR_DNS,
    API_CDN_IP_OR_DNS,
    API_SESSIONS_IP_OR_DNS,
    API_CORE_PORT,
    API_VOICE_PORT,
    API_DB_PORT,
    API_CRYPTO_PORT,
    API_WALLET_PORT,
    API_CDN_PORT,
    API_SESSIONS_PORT,
} = process.env

/* ~~~ MASTER PROCESS ~~~ */
if (cluster.isMaster) {
    console.log(`Master process <${process.pid}> is running.`)

    /* For Workers */
    for(let i = 0; i < os.cpus().length; i++)
        cluster.fork()

    cluster.on('exit', (worker, code, signal) => {
        console.log(`Worker ${worker.process.pid} died.`)
    })
}

/* WORKER PROCESSES */
else {

    app.get('/api/v1/core/healthcheck/all',
        /* Security */
        /* Prevention */
        preventMissingAuthToken,
        preventInvalidAuthToken,
        preventIncorrectAuthTokenAJWT,
        preventWithActiveSuspension,   // TODO: FR#75 - https://gitlab.com/rsoh/pubsub/-/issues/75
        /* Processing */
        healthCheckAllNodes)

    app.post(`/api/v1/core/create`,
        /* Security */
        preventMalformedHCaptcha,
        verifyHCaptchaToken,
        /* Security */
        /* Prevention */
        preventMissingAuthToken,
        preventInvalidAuthToken,
        preventIncorrectAuthTokenAJWT,
        preventWithActiveSuspension,   // TODO: FR#75 - https://gitlab.com/rsoh/pubsub/-/issues/75
        /* Processing */
        filenameParser,
        appendFilenameExtensionInfoToRes,
        configParser,
        handleCreateViaCDN,            // TODO: FR#30 - https://gitlab.com/rsoh/pubsub/-/issues/30
        handleCreateViaDatabase)       // TODO: FR#22 - https://gitlab.com/rsoh/pubsub/-/issues/22

    app.post(`/api/v1/core/delete`,
        /* Security */
        /* Prevention */
        preventMissingAuthToken,
        preventInvalidAuthToken,     
        preventIncorrectAuthTokenAJWT,
        preventWithActiveSuspension,   // TODO: FR#75 - https://gitlab.com/rsoh/pubsub/-/issues/75
        /* Processing */
        handleDeleteViaDatabase,       // TODO: FR#31 - https://gitlab.com/rsoh/pubsub/-/issues/31
        handleDeleteViaCDN)            // TODO: FR#32 - https://gitlab.com/rsoh/pubsub/-/issues/32

    app.post('/api/v1/core/query',
        /* Security */
        /* Prevention */
        preventMissingAuthToken,
        preventInvalidAuthToken,     
        preventIncorrectAuthTokenAJWT,
        preventWithActiveSuspension,   // TODO: FR#75 - https://gitlab.com/rsoh/pubsub/-/issues/75
        /* Processing */
        handleQuery)

    app.post('/api/v1/core/update',
        /* Security */
        /* Prevention */
        preventMissingAuthToken,
        preventInvalidAuthToken,     
        preventIncorrectAuthTokenAJWT,
        preventWithActiveSuspension,   // TODO: FR#75 - https://gitlab.com/rsoh/pubsub/-/issues/75
        /* Processing */
        handleUpdateViaDatabase,       // TODO: FR#36 - https://gitlab.com/rsoh/pubsub/-/issues/36
        handleUpdateViaCDN)            // TODO: FR#37 - https://gitlab.com/rsoh/pubsub/-/issues/37

    server.listen(API_CORE_PORT, handleAppListen)
}