import { render, screen } from '@testing-library/react'
import App from '../App'

/* Redux Imports */
import React from 'react'
import { Provider } from 'react-redux'
import store from '../redux/store'

// eslint-disable-next-line no-undef
test('renders the landing page', () => {
  <Provider store={store}>
    <App/>
  </Provider>
})
