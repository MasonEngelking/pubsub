/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/* EXPRESS IMPORTS */
import express   from 'express'
import cors      from 'cors'
import rateLimit from 'express-rate-limit'

/* OS IMPORTS */
import os      from 'os'
import cluster from 'cluster'

/* Middleware */
import {handleBubbleCreate, handleBubbleDelete, handleBubbleQuery, PurgeExpiredBubblesHandler} from "./middleware"
import {handleHeathCheck, isTrustedSource} from "./global.middleware"

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

import {Server} from "socket.io"
import * as http from "http"

/* Rate Limitations */
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100,                 // Limit each IP to 100 requests per `window` (here, per 15 minutes)
    standardHeaders: true,    // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false,     // Disable the `X-RateLimit-*` headers
})

/* EXPRESS-APP CONFIGURATION */
const app = express()
app.use(cors())
app.use(express.json())
app.use(limiter)
app.use((req, res, next) =>
{
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    next()
})

/* Creating Server for Sockets */
const server = http.createServer(app)
const io= new Server(server)

/* ~~ API Endpoints ~~ */

/**
 * @description Used to verify that the node is responding.
 * @status      Operational
 */
app.get('/api/v1/session/healthcheck',
    /* Security */
    isTrustedSource,
    /* Processing */
    handleHeathCheck)

/**
 * @description Used to verify that the node is responding.
 * @status      Needs to be implemented.
 */
app.get('/api/v1/session/create',
    /* Security */
    isTrustedSource,
    /* Processing */
    handleBubbleCreate)

/**
 * @description Used to verify that the node is responding.
 * @status      Needs to be implemented.
 */
app.get('/api/v1/session/delete',
    /* Security */
    isTrustedSource,
    /* Processing */
    handleBubbleDelete)

/**
 * @description Used to verify that the node is responding.
 * @status      Needs to be implemented.
 */
app.get('/api/v1/session/query',
    /* Security */
    isTrustedSource,
    /* Processing */
    handleBubbleQuery)

/**
 * @description Perpetual handler which removed expired bubbles
 * @status      Needs to be implemented.
 */
PurgeExpiredBubblesHandler()

/* ~~ Socket Information ~~ */

io.on('connection', socket => {
    console.log(`Socket ${socket.id} established a connection.`)

    /* Expecting an access token to use this server - Valid AJWT */
    let token = socket.handshake.query.accessToken

    socket.on('disconnect', () => console.log(`Socket ${socket.id} disconnect.`))
})

io.on('connection-error', err => console.log(err))