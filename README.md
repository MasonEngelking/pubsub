<p align="center">
  <a href="https://gitlab.com/rsoh/pubsub">
    <img src="client/src/assets/svgs/logo_rep_emblem.svg" alt="Logo" width="80" height="80">
  </a>

  <h1 align="center">PubSub</h1>

  <p align="center">
    The World's First Completely Clonable Social Media App. Forever open-source. Continuously growing. Readily adaptable.
    <br />
    <a href="https://gitlab.com/rsoh/pubsub/-/boards/4940175"><strong>Explore Specification Docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/rsoh/pubsub">View Demo</a>
    ·
    <a href="https://gitlab.com/rsoh/pubsub/-/boards/4956625">Report Bug</a>
    ·
    <a href="https://gitlab.com/rsoh/pubsub/-/boards/4956628">Request Feature</a>
  </p>
</p>

---

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about)
  * [Mission](#mission)
    * [Education](#education)
    * [Expression](#expression)
    * [Entrepreneurship](#entrepreneurship)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Microservices](#microservices) 
    * [Client](#client)
    * [Proxy](#proxy-reverse-proxy)
    * [Core](#core)
    * [Crypto](#crypto)
    * [CDN](#cdn)
    * [Database](#database)
    * [Wallet](#wallet)
    * [Session](#session)
    * [Voice](#voice)
    * [Chat](#chat)
  * [Dependency Installation](#dependency-installation)
  * [Prep Environment Files](#prep-environment-files)
  * [Launch App](#launching-app)
* [Deploy (coming soon)]()
  * [Docker Files]()
  * [Docker Compose]()
  * [AWS]()
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

---

<!-- ABOUT THE PROJECT -->
## About

Please read this section carefully, it will provide insights into the reasoning behind why this project exists and how 
to best use it moving forward. Thank you!

### Mission
PubSub is a multifaceted gem of a project. Although, this project serves to be a testament to the engineering abilities
of the contributors, it's mission goes far beyond that. PubSub will go down in history as the project that fosters: 
education, expression, and entrepreneurship.

#### Education
PubSub serves to be a learning experience for:
- System Engineers
- Software Engineers
- Software Designers
- UI/UX Designers
- Network Engineers
- Cyber-Security Specialists
- Entrepreneurs
- And much more...    

Given the completely open-source nature of the project, individuals in a wide-range of professions with a wide-range of
experience can use this project as a means of skill refinement. With this, we hope that by offering this opportunity to
others, professionals in their respective careers can contribute back by providing insights and open merge requests with
improvements to the overall system.

#### Expression
Nothing is more important than the ability to express yourself freely. With this freedom comes great responsibility. Given
the nature of PubSub, with it being open-source, we expect nothing but the best from our community. Although, it is not 
legally binding, we would like for our community to come to a mutual agreement that is with our applications:
- We will promote free and fare discussion
- We will promote the idea of shared growth and development
- We will encourage others to join in our endeavors to learn and build this project together
- We will condemn any and all forms of discriminatory behavior

#### Entrepreneurship
One of the driving factors of modern day is commercialism. Granted, the founders of this project are demotivated by
financial incentives, we fully understand that is a form of motivation for others. That is why we encourage aspiring
entrepreneurs to use this project as a means of growth for their local communities. The long term vision for this project
is to have integrated monetization methods for steady community growth and support.

### Built With
Note:  
Over the long term, we plan to walk through each dependency
one-by-one and build our own versions to prevent dependency deprecation.

Libraries that made this project possible:  
[React](https://reactjs.org/), [React-Redux](https://react-redux.js.org/), [Mongoose](https://mongoosejs.com/), [Node](https://nodejs.org/en/),
[Express](https://expressjs.com/), [SocketIO](https://socket.io/), [MaterialUI (MUI)](https://mui.com/), [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS),
[Express Fileupload](https://www.npmjs.com/package/express-fileupload), [Sharp](https://www.npmjs.com/package/sharp),
[AWS SDK](https://www.npmjs.com/package/aws-sdk), [JSON Web-Tokens](https://www.npmjs.com/package/jsonwebtoken),
[Axios](https://www.npmjs.com/package/axios), [Express Rate-Limit](https://www.npmjs.com/package/express-rate-limit),
[Flake ID-Gen](https://www.npmjs.com/package/flake-idgen), [Big UInt Format](https://www.npmjs.com/package/biguint-format),
[Bcrypt](https://www.npmjs.com/package/bcrypt), [Crypto](https://www.npmjs.com/package/crypto), [WebRTC](https://www.npmjs.com/package/wrtc),
[Babel](https://babeljs.io/docs/en/babel-node), [Emotion](https://emotion.sh/docs/introduction), [React Google Maps](https://www.npmjs.com/package/react-google-maps),
[Popper](https://www.npmjs.com/package/@popperjs/core), [Acorn](https://www.npmjs.com/package/acorn), [Acorn JSX](https://www.npmjs.com/package/acorn-jsx),
[Acorn walk](https://www.npmjs.com/package/acorn-walk), [Ajv](https://www.npmjs.com/package/ajv), [Ansi Keywords](https://www.npmjs.com/package/ajv-keywords), 
[Ansi Escapes](https://www.npmjs.com/package/ansi-escapes), [Ansi Regex](https://www.npmjs.com/package/ansi-regex),
[Ansi Styles](https://www.npmjs.com/package/ansi-styles), [Anymatch](https://www.npmjs.com/package/anymatch), 
[Array Union](https://www.npmjs.com/package/array-union), [Attr Accept](https://www.npmjs.com/package/attr-accept),
[Auto Prefixer](https://www.npmjs.com/package/autoprefixer), [Body Parser](https://www.npmjs.com/package/body-parser),
[Browsers List](https://www.npmjs.com/package/browserslist), [Builtin Modules](https://www.npmjs.com/package/builtin-modules),
[Callsites](https://www.npmjs.com/package/callsites), [Camelcase](https://www.npmjs.com/package/camelcase),
[Chalk](https://www.npmjs.com/package/chalk), [Char Regex](https://www.npmjs.com/package/char-regex), 
[CJS Module Lexer](https://www.npmjs.com/package/cjs-module-lexer), [Class Names](https://www.npmjs.com/package/classnames),
[CLSX](https://www.npmjs.com/package/clsx), [Collect V8 Coverage](https://www.npmjs.com/package/collect-v8-coverage),
[Colord](https://www.npmjs.com/package/colord), [Colorette](https://www.npmjs.com/package/colerette),
[Commander](https://www.npmjs.com/package/commander), [Common Path Prefix](https://www.npmjs.com/package/common-path-prefix),
[Cosmic Config](https://www.npmjs.com/package/cosmicconfig), [Crypto Random String](https://www.npmjs.com/package/crypto-random-string),
[CSS Declaration Sorter](https://www.npmjs.com/package/css-declaration-sorter), [CSS Minimizer WebPack Plugin](https://www.npmjs.com/package/css-minimizer-webpack-plugin),
[CSS Nano](https://www.npmjs.com/package/cssnano), [Deep Merge](https://www.npmjs.com/package/deepmerge),
[Detect Newline](https://www.npmjs.com/package/detect-newline), [Diff Sequences](https://www.npmjs.com/package/diff-sequences), 
[DOM Accessibility API](https://www.npmjs.com/package/dom-accessibility-api), [Dotenv Webpack](https://www.npmjs.com/package/dotenv-webpack), 
[Emittery](https://www.npmjs.com/package/emittery), [Engine IO](https://www.npmjs.com/package/engine.io), 
[Engine IO Client](https://www.npmjs.com/package/engine.io-client), [Engine IO Parser](https://www.npmjs.com/package/engine.io-parser), 
[Entities](https://www.npmjs.com/package/entities), [Error Stack Parser](https://www.npmjs.com/package/error-stack-parser),
[Escalade](https://www.npmjs.com/package/escalade), [Eslint Config Standard](https://www.npmjs.com/package/eslint-config-standard), 
..., and many more. Please refer to the ``node_modules`` directory of each node for additional information.

---


<!-- GETTING STARTED -->
## Getting Started

PAUSE.  
We'd like to take a moment to tell you that there is no rush to learning the entirety of this project. We fully
understand the scope of it may be massive BUT we are doing our best to make it digestible bit-by-bit. As the old adage
goes, Rome was not built in a day... and we expect nobody to learn about it in the same amount of time either.

### Microservices
This project leverages the concept of [microservices](https://en.wikipedia.org/wiki/Microservices). That is, we have a 
number of nodes that run independently of one another yet communicate to complete their clearly established purposes. 
The overall node network diagram can be found [here](https://gitlab.com/rsoh/pubsub/-/issues/69) which is issue #69 
(nice) on the documentation board.

#### Client
First and foremost comes the Client node. The Client node rests behind the Reverse Proxy node, as with all nodes
in the network, and is a powered by [NGINX](https://www.nginx.com/). The compiled application code which is stored on the 
NGINX server is powered by [React](https://reactjs.org/). During development however, you can use the command ``npm start``
while in the client directory to launch a local development server to view client code in real-time.

#### Proxy (Reverse Proxy)
Ah, the arbiter of routing, the [Reverse Proxy](https://en.wikipedia.org/wiki/Reverse_proxy)! Unfortunately, no clever
node naming with this one... This node, also being powered by [NGINX](https://www.nginx.com/), simply defines the way in 
which a client can interact with the node network. Additionally, it provides configurations for things such as rate-limiting
to protect the network from malicious behavior.

#### Core
Core is effectively the root node of the network... hence the name "Core". Again, we were not very original with the 
naming here... This actually holds true for all nodes... Continuing. Once a particular request is sent to Core, it will
communicate with all sub-nodes to complete and construct the request to be returned to a client. Ideally, this would not be
needed for a microservice network, but we have decided for the time being that it was best for Core to exist and additionally
handle pre-flight checks for intercommunication between nodes. This can be thought of as an additional node relay.

#### Crypto
It's not what you think! The Crypto node handles all things [cryptography](https://en.wikipedia.org/wiki/Cryptography) 
(NOT including cryptocurrency). As shown in the [sequence diagrams](https://gitlab.com/rsoh/pubsub/-/issues/57), you 
will see that Crypto handles requests to not only generate and decrypt a range of encryption algorithms, but also generate 
[JSON Web-Tokens](https://jwt.io/) and [Snowflake IDs](https://en.wikipedia.org/wiki/Snowflake_ID) for client-side
security and unique identification between objects in the database.

#### CDN
The Content Delivery Network node, CDN in short, is responsible for the processing of static assets and deploying them to either
local storage or via [Amazons Web Services (AWS)](https://aws.amazon.com/)'s 
[Simple Storage Service (S3)](https://aws.amazon.com/s3/?nc2=h_ql_prod_st_s3). Storing the static assets is only part of
the process of creating your very own content delivery network. For it to be a proper CDN, your static assets will need
to be propagated to servers across the world which can be achieved via [AWS's CloudFront](https://aws.amazon.com/cloudfront/?nc2=type_a).
This will be covered more extensively in the optional [Deploy](#deploy) section of this readme.

#### Database
The Database node, with its infinitely unique name, does one thing and one thing very well... Receives database operation 
requests and handles them accordingly. Even though the endpoints have been tremendously generalized for the sake of being
easy to follow, they have not been generalized at the expense of security. Specific implementation details can be found
in documentation and the code, so I'll summarize the behavior here: Via JWT + Operation Aggregation, we ensure that the 
"secured" sub-document of the User schema is never exposed to anyone other than the person who owns the document (account). 
Much more details similar to the above are explained in the Database nodes README.md.

#### Wallet
This is yet to be implemented! Wallet will be responsible for all transaction processing. This includes but is not limited
to: [PayPal](https://www.paypal.com/us/home), [Stripe](https://stripe.com/), [Square](https://squareup.com/), 
[Coinbase](https://www.coinbase.com/), etc.

#### Session
The Session node keeps track of all application updates and propagates that to the Client, Voice, and Chat nodes. Session,
as with the Voice and Chat nodes, leverage Web-Socket technology. Upon establishing a connection with the Session node,
the connection will handle authentication, then handle "messages" accordingly. More specifically, Session provides user
and Bubble information to the Client in real-time.

#### Voice
The Voice node is responsible for the processing and handling of voice/audio streams. This is in large part powered by 
cutting-edge [Web Real-time Communication (WebRTC)](https://webrtc.org/) technology. Given our networks topological
structure, we opted for a [Selective Forwarding Unit (SFU)](https://bloggeek.me/webrtcglossary/sfu/) approach to balance
performance, security, and ease of implementation. The Voice node establishes a connection to the Client node on Bubble
join.

Side Note:  
In the future, we may take an approach similar to [ION SFU](https://github.com/pion/ion-sfu) which is a pure 
[GO](https://en.wikipedia.org/wiki/Go_(programming_language)) implementation. We took a purely Javascript approach which 
may be easier to interpret for beginners but may produce not as efficient results affecting scalability. The most likely
outcome as that we will develop multiple language renditions of the Voice node and allow for our community to select on
preference.

#### Chat
As aforementioned, the Chat node being a WebSocket powered node, keeps track of all things messaging. As an example, upon 
joining the staging area of a Bubble (viewing the contents of the channel but not connecting your stream to it), you can 
interact with the channels' members via the messaging box.


<br>


### Dependency Installation

#### Ensure your Node Package Manager is up-to-date
```sh
npm install npm@latest -g
```

#### Clone (download) the latest version of PubSub
```sh
git clone https://gitlab.com/rsoh/pubsub.git
```
#### Install all nodes dependencies
- Navigate to the projects root directory (../pubsub/)
- Copy and paste the commands into the terminal
```sh
cd api/core && npm i && cd ../
cd cdn      && npm i && cd ../
cd database && npm i && cd ../
cd crypto   && npm i && cd ../
cd wallet   && npm i && cd ../
cd chat     && npm i && cd ../
cd session  && npm i && cd ../
cd voice    && npm i && cd ../../
cd client   && npm i && cd ../
```

### Prep Environment Files

#### Duplicate the .template.env then rename to .env (keep both for reference)
- Navigate to the projects root directory (../pubsub/)
- Copy and paste the commands into the terminal
```sh
cd api/core && cp .template.env .env && cd ../
cd cdn      && cp .template.env .env && cd ../
cd database && cp .template.env .env && cd ../
cd crypto   && cp .template.env .env && cd ../
cd wallet   && cp .template.env .env && cd ../
cd chat     && cp .template.env .env && cd ../
cd session  && cp .template.env .env && cd ../
cd voice    && cp .template.env .env && cd ../../
cd client   && cp .template.env .env && cd ../
```
#### Update all .env files credentials
- [ WARNING ] : Keep your credentials secured at all times
- Once all of the above steps are completed, you should be ready for LOCAL development and deployment of PubSub

### Launching App
- Some nodes are capable of running independently: Core, Crypto, etc...
- If you would like to launch all nodes simultaneously, use the below script
- Navigate to the projects root directory
- HEADS UP: The below script will run all nodes as background processes. To end the processes you will need to:
- Windows: Open the Task Manager and close all node processes.
- Linux: Use the ``ps`` command to list running processes then use ``sudo kill -9 <pid>`` to end the processes.
- Suggestion: Launch the nodes one by one not as background processes to both view the terminal output and easily close via ``ctrl + c``
```sh
cd api/core && (node api.pubsub.core.js &)     && cd ../
cd cdn      && (node api.pubsub.cdn.s3.js &)   && cd ../
cd database && (node api.pubsub.database.js &) && cd ../
cd crypto   && (node api.pubsub.crypto.js &)   && cd ../
cd wallet   && (node api.pubsub.wallet.js &)   && cd ../
cd chat     && (node api.pubsub.chat.js &)     && cd ../
cd session  && (node api.pubsub.session.js &)  && cd ../
cd voice    && (node api.pubsub.voice.js &)    && cd ../../
cd client   && npm start                       && cd ../
```

---

<!-- USAGE EXAMPLES -->
## Usage
- Clone PubSub
- Either:
  - Make it your own social media app with PubSub foundations  
    or
  - Help the core project by opening merge requests with improvements
- ???
- Profit

---

<!-- ROADMAP -->
## Roadmap
See the [open issues](https://gitlab.com/rsoh/pubsub/issues) for a list of proposed features (and known issues).

---

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

---

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

---

<!-- CONTACT -->
## Contact
Discord 🌐 [PubSub](https://discord.gg/trhbUFM6Km)  

Chris 🐤 [Twitter](https://twitter.com/tldr_chris)  
Chris 💻 [Gitlab](https://gitlab.com/christianlarcomb)  

---

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Best Readme Template](https://gitlab.unige.ch/Joakim.Tutt/Best-README-Template)
* [Shields IO](https://shields.io/)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-url]: https://linkedin.com/in/christianlarcomb

---

## A Note From Chris

Hi there,

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In today's world, a finite number of private entities have done a wonderful job bringing people together... hence lies
the problem. We currently have an over-dependency problem on an extremely finite number of private entities which is
where we are failing spectacularly. If they fail, so do we all. In other words, do not put all of your eggs in one basket.
Just as we do not like over-dependency on specific packages for our software projects, we should hold the same notion for
those of whom we trust with our communities. PubSub provides the much needed counter-balance to the issue allowing for
communities to not only build their own social media applications, but build, foster, and protect their very own communities.
Again, with this comes great responsibility, we must be more proactive in protecting communication and be held to a higher
standard. This means holding our community responsible by condemning egregious behaviors. With this, I hope we can
collectively define what it means to be the best that we can be and continue this project together.

Onward,  
Chris
