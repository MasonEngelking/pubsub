/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
import sharp  from 'sharp'
import AWS    from 'aws-sdk'
import dotenv from 'dotenv/config'
import { logLevelPresentCheck } from './global.middleware.js'

/* AWS CONFIG */
AWS.config.update({
  region: process.env.API_AWS_S3_REGION
})
let S3 = new AWS.S3({
  accessKeyId:     process.env.API_AWS_S3_ACCESS_KEY_ID,
  secretAccessKey: process.env.API_AWS_S3_SECRET_ACCESS_KEY,
  region:          process.env.API_AWS_S3_REGION,
  apiVersion:      process.env.API_AWS_S3_VERSION
})

/**
 * TODO: Add JSDOC here!
 */
 function handleCheck(focalPoint, permittedContent, typeOfCheck, res, next) {
  let check = permittedContent.filter(type => type === focalPoint).length > 0

  if(check)
    return next()

  res.status(400).send({
    'PubSub Response': {
      header: {
        status:  400,
        message: `Request config \'${typeOfCheck}\' is invalid.`,
        worker:  process.pid
      }
    }
  })
}

/**
* TODO: Add JSDOC here!
*/
export function getUniqueDate()
{
  let now     = new Date()
  let start   = new Date(now.getFullYear(), 0, 0)
  let diff    = (now - start) + ((start.getTimezoneOffset() - now.getTimezoneOffset()) * 60 * 1000)
  let oneDay  = 1000 * 60 * 60 * 24
  let day     = Math.floor(diff / oneDay).toString()
  let hour    = now.getHours().toString()
  let minutes = now.getMinutes().toString()
  let seconds = now.getSeconds().toString()
  let millisecond = now.getMilliseconds().toString()
  return day+hour+minutes+seconds+millisecond
}

/**
* TODO: Add JSDOC here!
*/
export async function filenameParser(req, res, next)
{
  let file = req.files?.imageFile
  if (!file)
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Invalid or no image provided.',
          pid:     process.pid
        }
      }
    })

  let filenameRe = /([^\\]*)\./
  let extRe      = /(?:\.([^.]+))?$/
  let filename   = filenameRe.exec(file?.name)[1]
  let extension  = extRe.exec(file?.name)[1]

  if(filename !== null || extension !== null){
    if(logLevelPresentCheck('info'))
      console.log({
        'PubSub Logging': {
          header: {
            status:  'Healthy',
            message: 'Successfully parsed the image filename and extension.',
            pid:     process.pid
          }
        }
      })

    res.locals.filename  = filename
    res.locals.extension = extension

    return next()
  }

  if(logLevelPresentCheck('err'))
    console.log({
      'PubSub Logging': {
        header: {
          status: 'Severe',
          message: 'Failed to parse the image filename or extension.',
          pid: process.pid
        }
      }
    })

  return res.status(400).send({
    'PubSub Response': {
      header: {
        status:  400,
        message: 'Failed to parse the image filename or extension.',
        pid:     process.pid
      }
    }
  })

}

/**
* TODO: Add JSDOC here!
*/
export function appendFilenameExtensionInfoToRes(req, res, next)
{
  res.locals.imageFileEnhanced = {
    ...req.files?.imageFile,
    filename: res.locals?.filename,
    extension: res.locals?.extension
  }

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export async function configParser(req, res, next)
{
  let configBuffer = req.files?.imageConfig?.data
  let configObject = JSON.parse(configBuffer.toString())

  if(configObject === undefined)
    return req.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Failed to parse the config document.',
          pid:     process.pid
        }
      }
    })

  if(logLevelPresentCheck('info'))
    console.log({
      'PubSub Logging': {
        header: {
          status:  'Success',
          message: 'Config JSON file successfully parsed and appended to req.files.configObject.',
          pid:     process.pid
        }
      }
    })

  res.locals.configObject = configObject

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export function preventInvalidContext(req, res, next)
{
  let { context=undefined } = req.headers, permittedContent = ['user','bubble']
  handleCheck(context, permittedContent, 'context type', res, next)
}

/**
* TODO: Add JSDOC here!
*/
export function preventInvalidFileExtension(req,res,next)
{
  let { extension=undefined } = res.locals, permittedContent = ['jpg','gif','png','webp']
  handleCheck(extension, permittedContent, 'file extension', res, next)
}

/**
* TODO: Add JSDOC here!
*/
export function preventInvalidContentType(req,res,next)
{
  let { context=undefined } = req.headers
  let { type=undefined } = res.locals?.configObject
  let permittedContent

  if (context === 'user')
    permittedContent = ['avatar', 'banner']
  
  if (context === 'bubble')
    permittedContent = ['icon', 'attachment']

  handleCheck(type, permittedContent, 'content type', res, next)
}

/**
* TODO: Add JSDOC here!
*/
export function prepDirectoryString(req,res,next)
{
  const { context } = req.headers
  const { xuid, type } = res.locals.configObject

  res.locals.preppedDirectory = `${context}/${xuid}/${type}`

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export async function processAttachmentAssets(req,res,next){
  const { type } = res.locals.configObject

  if (type !== 'attachment')
    return next()

  const file = res.locals?.imageFileEnhanced

  const generatedAssets = [
    {
      prepend: '',
      buffer:  file.data
    }
  ]

  const bufferChecks = [
    generatedAssets[0].buffer === null
  ]

  if (bufferChecks.includes(true))
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  "Error",
          code:    400,
          message: "Failed to generate the image assets. Aborting.",
          pid:     process.pid
        }
      }
    })

  res.locals.assetsToUpload = generatedAssets

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export async function processBannerAssets(req,res,next){
  const { type } = res.locals.configObject

  if (type !== 'banner')
    return next()

  const file = res.locals?.imageFileEnhanced

  const generatedAssets = [
    {
      prepend: '',
      buffer:  await sharp(file.data).resize(426, 240).toBuffer()
    }
  ]

  const bufferChecks = [
    generatedAssets[0].buffer === null
  ]

  if (bufferChecks.includes(true))
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  "Error",
          code:    400,
          message: "Failed to generate the image assets. Aborting.",
          pid:     process.pid
        }
      }
    })

  res.locals.assetsToUpload = generatedAssets

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export async function processIconAvatarAssets(req,res,next)
{
  const { type } = res.locals.configObject

  if (type !== 'icon' && type !== 'avatar')
    return next()

  const file = res.locals?.imageFileEnhanced

  const generatedAssets = [
    {
      prepend: '.1x@',
      buffer: await sharp(file.data).resize(50, 50).toBuffer()
    },
    {
      prepend: '.25x@',
      buffer: await sharp(file.data).resize(125, 125).toBuffer()
    },
    {
      prepend: '.5x@',
      buffer: await sharp(file.data).resize(250, 250).toBuffer()
    },
    {
      prepend: '',
      buffer: await sharp(file.data).resize(500, 500).toBuffer()
    }
  ]

  const bufferChecks = [
    generatedAssets[0].buffer === null,
    generatedAssets[1].buffer === null,
    generatedAssets[2].buffer === null,
    generatedAssets[3].buffer === null,
  ]

  if (bufferChecks.includes(true))
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  "Error",
          code:    400,
          message: "Failed to generate the image assets. Aborting.",
          pid:     process.pid
        }
      }
    })

  res.locals.assetsToUpload = generatedAssets

  return next()
}

/**
* TODO: Add JSDOC here!
*/
export async function S3Upload(req,res,next)
{
  let imageURLs       = []
  let generationDate  = getUniqueDate()
  let { assetsToUpload, preppedDirectory } = res.locals

  for (const asset of assetsToUpload){
    let key = `${preppedDirectory}/${asset.prepend}${generationDate}.${res.locals.extension}`
    let url = process.env.API_AWS_S3_BUCKET_NAME + '.pub/' + key

    let params = {
      Bucket:   process.env.API_AWS_S3_BUCKET_NAME,
      Key:      key,
      Body:     asset.buffer,
      Metadata: {},
    }

    let options = {partSize: 10 * 1024 * 1024, queueSize: 1}

    const outcome = await S3.upload(params, options).promise()

    if (outcome) imageURLs.push(url)
  }

  if(imageURLs.length !== 0){
    if (logLevelPresentCheck('info'))
      console.log({
        'PubSub Logging': {
          header: {
            status:  'Healthy',
            message: "Upload to S3 Success!",
            pid:     process.pid
          },
          data: { imageURLs: imageURLs }
        }
      })
    return res.status(200).send({
      'PubSub Response': {
        header: {
          status: 200,
          message: "Upload to S3 Success!",
          pid: process.pid
        },
        data: { imageURLs: imageURLs }
      }
    })
  }
  
  if (logLevelPresentCheck('info'))
    console.log({
      'PubSub Response': {
        header: {
          status: 'Error',
          code: 400,
          message: "Failed to upload assets to S3!",
          pid: process.pid
        }
      }
    })
  return res.status(400).send({
    'PubSub Response': {
      header: {
        status: 'Error',
        code: 400,
        message: "Failed to upload assets to S3!",
        pid: process.pid
      }
    }
  })

}