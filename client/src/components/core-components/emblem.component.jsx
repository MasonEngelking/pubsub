/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Emblem
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import { Outlet } from 'react-router-dom'
import styled from 'styled-components'
import { SECTION_COLORS } from '../../constants/themes.constants.js'
import { ReactComponent as TemplateSVG } from '../../assets/svgs/add.svg'
import PropTypes from 'prop-types'

const EmblemContainer = styled.div`
  color:   white;
  width:   auto;
  margin:  ${props => props.margin};
  padding: 0;

  -webkit-touch-callout: none;
  -webkit-user-select:   none;
  -khtml-user-select:    none;
  -moz-user-select:      none;
  -ms-user-select:       none;
  user-select:           none;
  
  /* Span Across Multiple Columns? */
  grid-column: ${props => props.gridColumn}
`

const EmblemWrapper = styled.div`
  display:         flex;
  justify-content: center;
  align-items:     center;
  position:        relative;
  border-radius:   100%;
  background:      radial-gradient(${props => props.colorOne}, ${props => props.colorTwo});
  height:          ${props => props.size};
  width:           ${props => props.size};
  margin:          0 auto;
`

const TextWrapper = styled.div`
  text-align: center;
  color:      white;
  max-width:  160px;
  margin:     0 auto;
`
const TitleWrapper = styled(TextWrapper)`
  font-size: ${props => props.titleSize};
`
const DescriptionWrapper = styled(TextWrapper)`
  font-size: ${props => props.descriptionSize};
  color:     gray;
`

const TemplateSVGStyled = styled(TemplateSVG)`
  height: 35px;
  fill:   white;
`

export default
function Emblem ({
  margin,
  gridColumn,
  colorOne,
  colorTwo,
  size,
  icon,
  gap,
  title,
  titleSize,
  description,
  descriptionSize
}) {
  const handleTemplateIconRender = () => {
    if (typeof icon === 'undefined') { return <TemplateSVGStyled/> }
  }

  return (
    <EmblemContainer
      margin={margin}
      gridColumn={gridColumn}
    >
      {/* Emblem Container */}
      <EmblemWrapper
        colorOne={colorOne}
        colorTwo={colorTwo}
        size ={size}
      >
        { handleTemplateIconRender() }
        { icon }
      </EmblemWrapper>

      <div style={{ height: gap }}/>

      <TitleWrapper
        titleSize={titleSize}
      >
        {title}
      </TitleWrapper>

      <div style={{ height: gap }}/>

      <DescriptionWrapper
        descriptionSize={descriptionSize}
      >
        {description}
      </DescriptionWrapper>

    </EmblemContainer>
  )
}

Emblem.displayName = 'Emblem'
Emblem.propTypes = {
  margin: PropTypes.string,
  gridColumn: PropTypes.number,
  colorOne: PropTypes.string,
  colorTwo: PropTypes.string,
  size: PropTypes.number,
  icon: PropTypes.element,
  gap: PropTypes.number,
  title: PropTypes.string,
  titleSize: PropTypes.number,
  descriptionSize: PropTypes.number,
  description: PropTypes.string
}
Emblem.defaultProps = {
  colorOne: '#888888',
  colorTwo: '#666666',
  title: 'Emblem...',
  description: undefined,
  margin: '5px 0',
  gridColumn: undefined,
  icon: undefined,
  size: '70px',
  titleSize: '20px',
  descriptionSize: '14px',
  gap: 3
}
