/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Tooltip
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import { useRef, useState } from 'react'
import styled from 'styled-components'

const TooltipWrapper = styled.div`
  position: relative;
  display:  inline-flex;
`

const TooltipTarget = styled.div`
  border:     none;
  background: inherit;
  padding:    5px;
  margin:     -1px;
  font-size:  inherit;
  color:      inherit;
  cursor:     inherit;
  display:    flex;
`

const CenterContainer = styled.div`
  position:    absolute;
  width:       200px;
  margin-left: -100px;
  display:     flex;
  justify-content: center;
  align-items: center;
  left:        50%;
  bottom:      calc(100% + 5px);
  pointer-events: none;
  
  ${props => {
      switch (props.position) {
        case 'bottom':
          return `
          bottom: unset !important;
          top:    calc(100% + 5px);
        `
        case 'left':
          return `
          margin-right: 0;
          width:        100%;
          left:         unset;
          top:          50%;
          right:        calc(100% + 5px);
          width:        max-content;
        `
        case 'right':
          return `
          margin-left: 0;
          width:       100%;
          top:         50%;
          left:        calc(100% + 5px);
          width:       max-content;
        `
        default:
          return `
          bottom: calc(100% + 5px);
        `
      }
  }}
`

const TooltipBox = styled.div`
  position:         relative;
  background-color: #${props => props.background};
  color:            #fff;
  text-align:       center;
  border-radius:    5px;
  padding:          10px 8px;
  font-size:        1.25rem;
  box-shadow:       0 4px 14px rgba(0, 0, 0, 0.15), 0 4px 8px rgba(0, 0, 0, 0.2);

  ${props => {
    switch (props.position) {
      case 'right':
        return `
          color: #000;
        `
      default:
        return ''
    }
  }}

  &:after {
    content: "";
    position: absolute;
    width: 1px;
    height: 1px;
    border-width: 5px;
    border-style: solid;
    border-color: #${(props) => props.background} transparent transparent transparent;
    left: calc(50% - 4.5px);
    top: 100%;
  }

  ${props => {
    switch (props.position) {
      case 'bottom':
        return `
          &:after {
            border-color: transparent transparent #${props => props.background} transparent;
            top: unset;
            width: 1px;
            bottom: 100%;
            left: calc(50% - 5px);
          }
        `
      case 'left':
        return `
          &:after {
            border-color: transparent transparent transparent #${props => props.background};
            left: 100%;
            top: calc(50% - 5px);
          }
        `
      case 'right':
        return `
          &:after {
            border-color: transparent #${props => props.background} transparent
              transparent;
            right: 100%;
            left: unset;
            top: calc(50% - 5px);
          }
        `
      default:
        return ''
    }
  }}
`

function Tooltip ({ position, text, children, background, styleMe = true }) {
  const [isHovered, setIsHovered] = useState(false)
  const [isFocused, setIsFocused] = useState(false)
  const targetRef = useRef(null)
  const showTooltip = isHovered || isFocused

  return (
        <TooltipWrapper>

            <TooltipTarget
                onMouseEnter={() => setIsHovered(true)}
                onMouseLeave={() => setIsHovered(false)}
                onFocus ={() => setIsFocused(true)}
                onBlur ={() => setIsFocused(false)}
                ref ={targetRef}
                styleMe ={styleMe}
                highlightOnHover={isHovered}
                showOnFocus ={isFocused}
            >
                {children}
            </TooltipTarget>

            {showTooltip && (
                <CenterContainer position={position}>
                    <TooltipBox background={background} position={position}>
                        {text}
                    </TooltipBox>
                </CenterContainer>
            )}

        </TooltipWrapper>
  )
}

export default Tooltip
