/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
const Bubbles = [
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#234f23'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#25b625'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#26bda5'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#23404f'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#d2ea20'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#5a2020'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#8b178d'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#3425a9'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#0f4b4f'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#141350'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#47e247'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#d1ff39'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#2ab9cc'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#5a189b'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#00ff00'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ffd000'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ff0095'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ff0000'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#b728ff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ff5622'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ffffff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#441919'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#6590c9'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#2936e8'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#e78735'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#84aaff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ad2f2f'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#2f234f'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#1cfd1c'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#3a8c3a'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#2fa4da'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#3a8c3a'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#783ff2'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#7f2df2'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ee5c38'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#59afec'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#7333d9'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#6dda6d'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ff39d4'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#7d29cc'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#fff959'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#3a8c3a'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#df2bff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#32ffc1'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#2b23ff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#c5ff3e'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135166,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#ffae0d'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135167,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#36c786'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135168,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#3a448c'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135169,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#36482b'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135170,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#05003d'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#774eff'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#67ff95'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  },
  {
    bubbleID: 16548135165,
    bubbleMeta: {
      name: 'Rockets',
      description: 'Where do they go if the earth is flat??',
      color: '#d0bf4f'
    },
    bubbleAssets: {
      image: 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
    },
    bubblePreferences: {
      cameraEnabled: false,
      prioritySpeak: false
    }
  }
]

export {
  Bubbles
}
