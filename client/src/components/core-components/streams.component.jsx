/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Streams
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useEffect,
  useRef,
  useState
} from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import GetWindowDimensions from '../../scripts/GetWindowDimensions.jsx'
import StreamScroll from './stream_scroll.component'

const StreamsContainer = styled.div`
  display:         flex;
  flex-direction:  column;
  height:          100vh;
  width:           calc(100% - 10px);
  overflow-x:      hidden;
  overflow-y:      scroll;
  margin:          0 auto;
  justify-content: center;

  /* Prevent Text Selection */
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */

`

const StreamsWrapper = styled.div`
  margin: ${props => props.margin}% 0;
`

const StreamsRow = styled.div`
  display:         flex;
  justify-content: center;
  align-items:     center;
  flex:            1 1 auto;
  margin: 0 ${props => props.margin}%;
`

const Stream = styled.div`
  display:         flex;
  position:        relative;
  justify-content: center;
  align-items:     center;
  aspect-ratio:    16/9;
  overflow:        hidden;
  margin:          3px;

  border-radius:   6px;
  
  &:hover{
    cursor:  pointer;
    
    & > div:nth-child(1)
    {
      opacity: 1;
    }
  }
`

const StreamDefault = styled.div`
  display:          grid;
  align-items:      center;
  overflow:         hidden;
  border-radius:    6px;
  height:           100%;
  width:            1080px;
  
  background-color: #000;
  box-shadow: inset 0 0 0 1.5px ${props => props.highlightColor},
              inset 0 0 0 4px   ${props => props.backgroundColor};
`

const StreamTag = styled.div`
  position:         absolute;
  bottom:           5px;
  left:             5px;
  padding:          5px 8px;
  border-radius:    5px;
  color:            white;
  background-color: rgba(11,12,15,0.8);
  backdrop-filter:  blur(5px) saturate(150%);
  opacity:          0.2;
  transition: opacity ease-in-out 0.1s;
`

export default
function Streams (props) {
  /* React State Hooks */
  const [dimensions, setDimensions] = useState([])
  const [focusedStream, setFocusedStream] = useState(null)

  /* Periodic Call for Window Dimensions */
  GetWindowDimensions(setDimensions)

  const splitArrayIntoChunksOfLength = (arr, len) => {
    const chunks = []; let i = 0; const n = arr.length
    while (i < n) { chunks.push(arr.slice(i, i += len)) }
    return chunks
  }

  const determineOptimalColumns = () => {
    /* TODO: Figure out why this is making chrome disappear upon resize */
    return Math.floor(dimensions.width / 400)
  }

  const calculateRowMarginPercentage = (actual, expected, i) => {
    const emptySpaces = expected - actual
    const calculatedMargin = emptySpaces / expected
    const marginSplit = calculatedMargin / 2
    const toPercentage = marginSplit * 100
    const isInitialRow = i === 0

    if (actual === expected) { return 0 } else { return isInitialRow ? 0 : toPercentage }
  }

  const handleStreamFocus = (stream) => {
    setFocusedStream(focusedStream === null ? stream : null)
  }

  const VideoProcessing = (props) => {
    const ref = useRef()

    useEffect(() => {
      if (ref.current) { ref.current.srcObject = props.user.stream }
    }, [ref])

    return (
      <StreamDefault key={props.key}>
        <video
          style={{
            minHeight: '100%',
            minWidth: '100%'
          }}
          ref={ref}
          autoPlay={true}
          // TODO: Monitor this closely...
          muted={true} // Chrome 71 is requiring it be muted first... then unmute
        />
      </StreamDefault>
    )
  }

  const RenderFocusStreams = (stream) => {
    const filterPeers = props.usersData.filter(stream => stream !== focusedStream)

    return (
      <StreamsWrapper>
        <StreamsRow margin={calculateRowMarginPercentage(1, determineOptimalColumns(), 0)}>
          <Stream
            onClick={() => setFocusedStream(null)}
          >
            <StreamTag> {focusedStream.name} </StreamTag>
            <StreamDefault
              highlightColor={focusedStream.borderColor}
              backgroundColor={'#0f1116'}
            />
          </Stream>
        </StreamsRow>

        <div style={{ height: '2px' }}/>

        {/* TODO: Continue to design this for optimal user experience */}
        <StreamScroll
          streams ={filterPeers}
          streamWidth ={205}
          selectedStream={selection => {

          }}
        />

      </StreamsWrapper>
    )
  }

  const RenderUnFocusedStreams = () => {
    return (
      <StreamsWrapper>
        {
          splitArrayIntoChunksOfLength(props.usersData, determineOptimalColumns()).map(
            (userRow, i) =>
              (
                <StreamsRow
                  margin={calculateRowMarginPercentage(userRow.length, determineOptimalColumns(), i)}
                  key={i}
                >
                  {
                    userRow.map((user, n) =>
                      (
                        <Stream
                          onClick={() => handleStreamFocus(user)}
                          key={n}
                        >
                          {/* console.log(user) */}
                          <StreamTag> {user.username} </StreamTag>

                          {/* TODO: Check if self stream, mute if so */}
                          <VideoProcessing
                            user={user}
                            key={`stream-index-${n}`}
                          />
                        </Stream>
                      ))
                  }
                </StreamsRow>
              ))
        }
        <div style={{ height: 80 }}/>
      </StreamsWrapper>
    )
  }

  return (
    <StreamsContainer>
      {
        focusedStream === null
          ? RenderUnFocusedStreams()
          : RenderFocusStreams()
      }
    </StreamsContainer>
  )
}

Streams.displayName = 'Streams Viewer'
Streams.propTypes = {
  onStreamFocus: PropTypes.func,

  backgroundColor: PropTypes.string,

  tagBackColor: PropTypes.string,
  tagTextColor: PropTypes.string,

  user: PropTypes.all,
  usersData: PropTypes.arrayOf(PropTypes.object),

  key: PropTypes.string
}
Streams.defaultProps = {
  tagBackColor: '',
  tagTextColor: ''
}
