/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Image_Selector
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useEffect,
  useState
} from 'react'
import { useDropzone } from 'react-dropzone'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { notify } from './notifications.component.jsx'

import { ReactComponent as Addition } from '../../assets/svgs/add.svg'

const ImageContainer = styled.div`
  display:          flex;
  overflow:         hidden;
  justify-content:  center;
  align-items:      center;
  position:         relative;
  height:           ${props => props.height};
  width:            ${props => props.width};
  margin:           ${props => props.margin};
  background-color: ${props => props.containerColor};
  border-radius:    100%;
  cursor:           pointer;
  z-index:          2;
`

const ImageSelectorStyle = styled.div`
  height:           calc(100% - 10px);
  width:            calc(100% - 10px);
  
  display:          flex;
  border-radius:    100%;
  background-color: ${props => props.areaColor};

  border-style:     dashed;
  border-color:     ${props => props.borderColor};

  text-align:       center;

  justify-content: center;
  align-items:     center;

  overflow:        hidden;
  
  &:hover {
    filter: brightness(${props => props.brightness});
  }
  
  &:active {
    filter: brightness(${props => props.brightness - 0.2});
  }
`

const ImagePreviewer = styled(ImageSelectorStyle)`

  min-width:    0;
  border-style: none;

  &:hover {
    opacity: 0.7;
  }

  & > img {
    display: block;
    width:   auto;
    height:  100%;
  }
`

export default
function ImageSelector (props) {
  const [files, setFiles] = useState(props.initFile)

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({
    accept: 'image/jpeg, image/png',
    maxFiles: 1,
    maxSize: 50000000,
    onDrop: acceptedFiles => {
      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })))
    }
  })

  /* TODO: Upon Reject - Show it in the box (shake animation would be cool too */
  useEffect(() => {

  }, [isDragReject])

  /* Revoking URI's to prevent memory leak */
  useEffect(() => {
    /* Make sure to revoke the data uris to avoid memory leaks */
    if (files !== undefined) { files.forEach(file => URL.revokeObjectURL(file.preview)) }
  }, [files])

  const renderImage = () => {
    return (
      <ImagePreviewer>
        <div>
          <img
            src ={files[0].preview}
            style ={files[0].img}
            alt ="Post Image"
          />
        </div>
      </ImagePreviewer>
    )
  }

  const renderImageEditor = () => {
    if (files === undefined) {
      return (
        <ImageSelectorStyle
          borderColor ={props.borderColor}
          areaColor ={props.areaColor}
          iconColor ={props.iconColor}
          textColor ={props.textColor}
          {...getRootProps({ className: 'dropzone' })}
          brightness ={props.brightness}
        >
          <input {...getInputProps()}/>
          {
            isDragActive
              ? <>
                                Drag N&apos; Drop Here
              </>
              : <Addition style={{ height: 20, fill: props.iconColor }}/>
          }
        </ImageSelectorStyle>
      )
    } else {
      return (
        <ImagePreviewer
          onClick={() => setFiles(undefined)}
        >
          <img
            src ={files[0].preview}
            style ={files[0].img}
            alt ="User Image"
          />
        </ImagePreviewer>
      )
    }
  }

  return (
    <ImageContainer
      containerColor ={props.containerColor}

      width ={props.width}
      height ={props.height}

      margin ={props.margin}
      marginTop ={props.marginTop}
      marginRight ={props.marginRight}
      marginBottom ={props.marginBottom}
      marginLeft ={props.marginLeft}

      padding ={props.padding}
      paddingTop ={props.paddingTop}
      paddingRight ={props.paddingRight}
      paddingBottom ={props.paddingBottom}
      paddingLeft ={props.paddingLeft}

      borderRadius ={props.borderRadius}
      borderStyle ={props.borderStyle}
    >
      {
        props.enabled
          ? renderImageEditor()
          : renderImage()
      }
    </ImageContainer>
  )
}

ImageSelector.displayName = 'Image Selector & Container'
ImageSelector.propTypes = {
  /* State Pass Up */
  onFile: PropTypes.func,
  enabled: PropTypes.bool,

  initFile: PropTypes.arrayOf(PropTypes.object),

  /* Operation Configuration */
  maxFileCount: PropTypes.number,

  /* Styling Configuration */
  containerColor: PropTypes.string,
  borderColor: PropTypes.string,
  areaColor: PropTypes.string,
  iconColor: PropTypes.string,
  textColor: PropTypes.string,

  width: PropTypes.string,
  height: PropTypes.string,

  margin: PropTypes.string,
  marginTop: PropTypes.number,
  marginRight: PropTypes.number,
  marginBottom: PropTypes.number,
  marginLeft: PropTypes.number,

  padding: PropTypes.string,
  paddingTop: PropTypes.number,
  paddingRight: PropTypes.number,
  paddingBottom: PropTypes.number,
  paddingLeft: PropTypes.number,

  borderRadius: PropTypes.string,
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,

  brightness: PropTypes.number
}
ImageSelector.defaultProps = {
  /* State Pass Up */
  onFile: () => {},
  enabled: true,

  initFile: undefined,

  /* Operation Configuration */
  maxFileCount: 1,

  /* Styling Configuration */
  containerColor: '#ffffff',
  borderColor: '#7e7e81',
  areaColor: '#b8b8b8',
  iconColor: '#7e7e81',
  textColor: '#121212',

  brightness: 0.9,

  width: '110px',
  height: '110px',

  margin: '0 auto'
}
