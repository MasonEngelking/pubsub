/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Checkbox
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { ReactComponent as CheckSVG } from '../../assets/svgs/check.svg'
import PropTypes from 'prop-types'

const CheckboxViewer = styled.div`
  display:         flex;
  justify-content: center;
  align-items:     center;
`

const CheckboxStyle = styled.div`
  display:         flex;
  justify-content: center;
  align-items:     center;
  
  border-style: solid;
  border-width: 2px;
  
  height:           ${props => props.size}px;
  width:            ${props => props.size}px;
  background-color: ${props => props.backgroundColor};
  border-color:     ${props => props.borderColor};
  
  border-radius:   20%;
  
  margin: ${props => props.margin};
  
  &:hover {
    cursor: pointer;
  }
`

export default
function Checkbox ({
  toggled,
  enabled,
  toggledColor,
  toggledBorderColor,
  eBackgroundColor,
  dBackgroundColor,
  eBorderColor,
  dBorderColor,
  size,
  margin,
  iconColor,
  stateMirror
}) {
  const [locToggled, setToggled] = useState(toggled)
  const [locEnabled, setEnabled] = useState(enabled)

  /* Color determination */
  const renderBackgroundColor = toggled ? toggledColor : enabled ? eBackgroundColor : dBackgroundColor
  const renderBorderColor = toggled ? toggledBorderColor : enabled ? eBorderColor : dBorderColor

  /* State Mirror */
  useEffect(() => stateMirror(toggled), [toggled])

  return (
    <CheckboxViewer>

      <CheckboxStyle
        size ={size}
        backgroundColor ={renderBackgroundColor}
        borderColor ={renderBorderColor}
        onClick ={() => setToggled(!toggled)}
        margin ={margin}
      >
        {
          toggled
            ? <CheckSVG
              style={{
                height: '45%',
                fill: iconColor
              }}
            />
            : <></>
        }
      </CheckboxStyle>
    </CheckboxViewer>
  )
}

Checkbox.displayName = 'Checkbox'
Checkbox.propTypes = {
  toggled: PropTypes.bool,
  enabled: PropTypes.bool,
  toggledColor: PropTypes.string,
  toggledBorderColor: PropTypes.string,
  eBackgroundColor: PropTypes.string,
  dBackgroundColor: PropTypes.string,
  eBorderColor: PropTypes.string,
  dBorderColor: PropTypes.string,
  size: PropTypes.number,
  margin: PropTypes.number,
  iconColor: PropTypes.string,
  stateMirror: PropTypes.func
}
Checkbox.defaultProps = {
  stateMirror: () => {},
  onClick: () => {},

  size: 50,

  toggled: false,
  enabled: true,

  toggledColor: '#2fe13b',
  toggledBorderColor: '#86fa8d',

  eBackgroundColor: '#000000',
  eBorderColor: '#ffffff',

  dBackgroundColor: '#000000',
  dBorderColor: '#ffffff',

  iconColor: '#ffffff',

  margin: undefined
}
