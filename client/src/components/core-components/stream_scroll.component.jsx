/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Stream_Scroll
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useState, useRef, useEffect } from 'react'
import styled from 'styled-components'

import { ReactComponent as Arrow } from '../../assets/svgs/up-arrow.svg'

/* SVGs */
import PropTypes from 'prop-types'

const PrimaryViewer = styled.div`
  display: flex;
  justify-content: center;
`

const PrimaryContainer = styled.div`
  
  height:    ${props => props.height};
  position:  relative;
  max-width: 1080px;
  margin:    5px 3px;
  
  &:hover {
    & > div:nth-child(1) {
      & > div {
        opacity: 1;
        transition: opacity 0.1s;
      }
    }
  }
  
  & > div:nth-child(1) {
    & > div {
      opacity: 0.2;
      transition: opacity 0.1s;
    }
  }
`

const NavigationContainer = styled.div`

  position:              absolute;
  display:               grid;
  grid-template-columns: 50px 1fr 50px;
  place-content:         center;
  height:                100%;
  width:                 100%;
  
  justify-items: center;
  
  /* Button Styling */
  & > div:nth-child(1), & > div:nth-child(3)
  {
    height:           40px; 
    width:            40px;
    background-color: #1e1e1e;
    z-index:          10;
    display:          grid;
    align-items:      center;
    grid-template-columns: 1fr;
    
    border-radius: 10px;
    border-style:  solid;
    border-color:  gray;
    
    &:hover{ 
      cursor: pointer;
    }
  }
`

const StreamContainer = styled.div`
  
  height: 100%;
  overflow: hidden;
  display: grid;
  grid-template-columns: 1fr;

  /* Streams Wrapper */
  & > div {
    width: 100%;
    height: 100%;
    display: grid;
    grid-gap: ${props => props.gap}px;
    overflow: hidden;
    grid-template-columns: repeat(${props => props.arraySize}, 1fr);

    /* Spacing issue is detected here... Fix it up! */
    border-radius: ${props => props.borderRadius};

    /* Individual Stream Container */

    & > div {
      border-radius: ${props => props.borderRadius};
      background-color: #0c1111;
      width: ${props => props.streamWidth}px;
      overflow: hidden;
      display: grid;
      margin: 0 auto;
      place-items: center;
      transform: translateX(${props => props.position}px);
      transition: all ease-in-out 0.15s;
      place-content: center;

      & > img {
        height: 100%;
      }
    }
  }
  
`

const LockedStream = styled.div`
  display:          block;
  position:         relative;
  justify-content:  center;
  align-items:      center;
  aspect-ratio:     16/9;
  overflow:         hidden;
  margin-right:     5px;
  width:            ${props => props.width};
  float:            left;
  background-color: #0c1111;
  border-radius:    5px;
  &:hover{cursor:   pointer;}
`

const StreamTag = styled.div`
  position:         absolute;
  bottom:           5px;
  left:             5px;
  padding:          5px 8px;
  border-radius:    5px;
  color:            white;
  background-color: rgba(11,12,15,0.8);
  backdrop-filter:  blur(5px) saturate(150%);
`

export default
function StreamScroll (props) {
  const [ref, setRef] = useState()

  /* Necessary Variables */
  const streams = props.streams
  const streamsArrayLength = streams.length
  let [frontStreamIndex, setFrontStreamIndex] = useState(0)
  const [position, setPosition] = useState(0)

  const handleLeftButtonPress = () => {
    /* If the front streams index is greater than its starting position, go ahead */
    if (frontStreamIndex > 0) {
      setFrontStreamIndex(frontStreamIndex -= 1)
      const position = (frontStreamIndex * -(props.streamWidth + props.gap))
      setPosition(position)
    } else if (frontStreamIndex === 0) {
      /* Displacement effect */
      setPosition(25)

      /* Resetting */
      setTimeout(() => { setPosition(0) }, 150)
    }
  }

  const handleRightButtonPress = () => {

  }

  return (
    <>
      <PrimaryViewer>
        <PrimaryContainer ref={setRef}>

          <NavigationContainer
            height ={props.height}
          >
            {/* Button 1 */}
            <div
              onClick={handleLeftButtonPress}
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignContent: 'center',
                borderWidth: 1.5
              }}
            >
              <Arrow
                style={{
                  fill: 'white',
                  height: 12,
                  marginRight: 2,
                  transform: 'rotate(-90deg)'
                }}
              />
            </div>

            {/* Gap */}
            <div/>

            {/* Button 2 */}
            <div
              onClick={handleRightButtonPress}
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignContent: 'center',
                borderWidth: 1.5
              }}
            >
              <Arrow
                style={{
                  fill: 'white',
                  height: 12,
                  marginLeft: 2,
                  transform: 'rotate(90deg)'
                }}
              />
            </div>
          </NavigationContainer>

          <StreamContainer
            position ={position}
            arraySize ={streamsArrayLength}
            streamWidth ={props.streamWidth}

            height ={props.height}
            minHeight ={props.minHeight}
            maxHeight ={props.maxHeight}

            gap ={props.gap}

            borderRadius={props.borderRadius}
          >

            {/* Stream Wrapper */}
            <div>

              {props.streams.map((stream, i) => (
                <LockedStream
                  key={i}
                  width={props.streamWidth}
                >
                  <StreamTag> {stream.name} </StreamTag>
                </LockedStream>
              ))}

            </div>

          </StreamContainer>

        </PrimaryContainer>
      </PrimaryViewer>
    </>
  )
}

StreamScroll.displayName = 'Stream Scroll Viewer'

StreamScroll.propTypes = {

  streams: PropTypes.arrayOf(PropTypes.element).isRequired,

  gap: PropTypes.number,

  streamWidth: PropTypes.number,

  height: PropTypes.string,
  minHeight: PropTypes.string,
  maxHeight: PropTypes.string,

  width: PropTypes.string,
  minWidth: PropTypes.string,
  maxWidth: PropTypes.string,

  borderRadius: PropTypes.string,
  borderStyle: PropTypes.string
}

StreamScroll.defaultProps = {

  gap: 6,

  streamWidth: 150,

  height: 'auto',
  maxWidth: '1080px',

  borderRadius: '4px'
}
