/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Bubble_View
 *
 * @function
 *
 * @param {Object} props
 *
 * @description Attempts to establish a connection to the specified BubbleID, handles the corresponding peers
 *              once retrieved,
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

/* React Imports */
import React, {
  useEffect,
  useState,
  useContext
} from 'react'
import styled, { ThemeContext } from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import allAppActions from '../../redux/actions/appActions.js'

/* Networking Imports */
import axios from 'axios'

/* Audio Imports */
import JoinSound from '../../assets/audio/bubble_join.wav'
import LeaveSound from '../../assets/audio/bubble_leave.wav'

/* Components */
import Streams from '../../components/core-components/streams.component.jsx'
import SocketClient from '../../scripts/SocketClient.js'
import { notify } from '../../components/core-components/notifications.component.jsx'
import prettyConsoleLog from '../../scripts/prettyConsoleLog.js'

const BubbleViewStyled = styled.div`
  height: 100vh;
  width:  100vw;
  background-color: #08080a;
`

const MAX_NUM_PEERS = 20
const COMPOSED_ADDRESS = process.env.REACT_APP_API_PROTO + '://' + process.env.REACT_APP_API_VOICE_IP_OR_DNS + ':' + process.env.REACT_APP_API_VOICE_PORT

export default
function BubbleView () {
  /* React Redux Var */
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const mediaManager = useSelector(state => state.mediaManager)
  const { bubbleID } = useParams()

  const [tempUUID, setTempUUID] = useState(Math.floor(Math.random() * 458973243))

  const theme = useContext(ThemeContext)

  /* Defining the Join & Leave Sound Tracks */
  const joinAudio = new Audio(JoinSound)
  const leaveAudio = new Audio(LeaveSound)
  joinAudio.volume = 0.20
  leaveAudio.volume = 0.20

  /* React State Hooks */
  const [users, setUsers] = useState([])

  async function getMediaStreams () {
    // Helpful Variables
    let userMediaContraints
    let userStream

    // Shorthand for selected devices
    const videoDeviceID = mediaManager.selectedDevices.videoInput.deviceId
    const inputDeviceID = mediaManager.selectedDevices.audioInput.deviceId

    // Ensure selected video device ready
    if (videoDeviceID !== 'NO_DEVICES' && videoDeviceID !== 'PERMS_NEEDED') {
      userMediaContraints = {
        video: {
          width: { min: 320, max: 1920 },
          height: { min: 240, max: 1080 },
          deviceId: mediaManager.selectedDevices.videoInput.deviceId,
          frameRate: { ideal: 30, max: 60 }
        }
      }
    }

    // Ensure selected input device ready
    if (inputDeviceID !== 'NO_DEVICES' && inputDeviceID !== 'PERMS_NEEDED') {
      userMediaContraints = {
        ...userMediaContraints,
        audio: {
          deviceId: mediaManager.selectedDevices.audioInput.deviceId
        }
      }
    }

    // If all is good, set the restraints
    if (
      typeof userMediaContraints.video !== 'undefined' ||
            typeof userMediaContraints.audio !== 'undefined'
    ) { userStream = await navigator.mediaDevices.getUserMedia(userMediaContraints) }

    return userStream
  }

  async function sendPeerData (socket) {
    // Retrieving media-stream then appending tracks to the peer object
    const userStream = await getMediaStreams()

    // Creating a peer object
    const peer = new RTCPeerConnection({ iceServers: [{ urls: 'stun:stun.stunprotocol.org' }] })

    // Defining the local and remote description
    peer.onnegotiationneeded = async () => {
      const offer = await peer.createOffer()
      await peer.setLocalDescription(offer)

      let userObject = {
        ltd: peer.localDescription,
        socketID: socket.socket.id,
        uuid: tempUUID,
        bubbleID,
        username: 'self-username',
        imgString: 'self-img-string'
      }

      const { data } = await axios.post(COMPOSED_ADDRESS + '/api/v1/voice/provide-stream', userObject)

      const desc = new RTCSessionDescription(data.ltd)
      peer.setRemoteDescription(desc)
        .catch(e => console.log('Error setting remote description:', e))

      userObject = {
        peer,
        uuid: tempUUID,
        bubbleID,
        username: 'self-username',
        imgString: 'self-img-string',
        stream: userStream,
        isSelf: true
      }

      // TODO: Implement Redundancy Checks
      await setUsers(prevState => [...prevState, userObject])
    }

    userStream.getTracks().forEach(track => peer.addTrack(track, userStream))
  }

  async function retrievePeersData (index, arePeerRemaining, peersToConnect) {
    let cont = arePeerRemaining

    // Prevent from going over maximum of twenty!
    if (index > MAX_NUM_PEERS) { return }

    if (!arePeerRemaining) {
      // After retrieving the set of peers to connect, set the state!
      setUsers(peersToConnect)
    } else {
      // Creating a new Peer object with the specified Ice Servers
      const peer = new RTCPeerConnection({ iceServers: [{ urls: 'stun:stun.stunprotocol.org' }] })

      // TODO: e.streams[0] refers to the combined media stream (video/audio)
      let userStream
      peer.ontrack = (e) => { userStream = e.streams[0] }

      // Determine the Peer negotiation
      peer.onnegotiationneeded = async () => {
        // Establish an Offer and Provide to New Peer Object
        const offer = await peer.createOffer()
        await peer.setLocalDescription(offer)

        const payload = {
          ltd: peer.localDescription,
          index,
          uuid: tempUUID,
          bubbleID
        }

        await axios.post(COMPOSED_ADDRESS + '/api/v1/voice/retrieve-peer-streams', payload)
          .then(async res => {
            const { uuid, username, imgString, isPeerRemaining, ltd } = res.data

            cont = isPeerRemaining

            const desc = new RTCSessionDescription(ltd)
            await peer.setRemoteDescription(desc).catch(err => console.log('Error setting remote desc:', err))

            const userObject = {
              peer,
              uuid,
              username,
              imgString,
              stream: userStream
            }

            // Update peersToConnect
            peersToConnect = [...peersToConnect, userObject]

            // Recursively continuing the search
            await retrievePeersData(index + 1, cont, peersToConnect)
          })
      }

      // Lastly, set the Peer's transceiver to be receiving only for video
      peer.addTransceiver('video', { direction: 'recvonly' })
      peer.addTransceiver('audio', { direction: 'recvonly' })
    }
  }

  function retrievePeerData (peerUUID) {
    if (tempUUID !== peerUUID) {
      // Creating a new Peer object with the specified Ice Servers
      const peer = new RTCPeerConnection({ iceServers: [{ urls: 'stun:stun.stunprotocol.org' }] })

      let userStream
      peer.ontrack = (e) => { userStream = e.streams[0] }

      // Determine the Peer negotiation
      peer.onnegotiationneeded = async () => {
        // Establish an Offer and Provide to New Peer Object
        const offer = await peer.createOffer()
        await peer.setLocalDescription(offer)

        const payload = {
          ltd: peer.localDescription,
          bubbleID,
          peerUUID
        }

        const { data } = await axios.post(COMPOSED_ADDRESS + '/api/v1/voice/retrieve-peer-stream', payload)
        const { username, imgString, ltd } = data

        const desc = new RTCSessionDescription(ltd)
        await peer.setRemoteDescription(desc).catch(err => console.log('Error setting remote desc:', err))

        const userObject = {
          peer,
          uuid: peerUUID,
          username,
          imgString,
          stream: userStream
        }

        // TODO: Implement Redundancy Checks
        await setUsers(prevState => [...prevState, userObject])
      }

      // Lastly, set the Peer's transceiver to be receiving only for video
      peer.addTransceiver('video', { direction: 'recvonly' })
      peer.addTransceiver('audio', { direction: 'recvonly' })
    }
  }

  function onUserJoin (socket) {
    socket.on('user-join', (uuid) => {
      retrievePeerData(uuid)
    })
  }

  function onUserLeave (socket) {
    socket.on('user-leave', uuid => {
      // console.log("THIS USER LEFT:", uuid)

      // Remove them from set of users
      setUsers(prevUsers => prevUsers.filter(user => user.uuid !== uuid))
    })
  }

  function socketLifetimeHandlers (socket) {
    socket.on('connect_error', () => {
      socket.absoluteDisconnect = true
      navigate(-1)
      notify('Strange...', 'We are having trouble connecting to the specified bubble... Try again later!', 'error')
    })

    socket.on('disconnect', () => {
      socket.absoluteDisconnect = false
    })
  }

  /* ~~~~~~~~~~~~~~~~~~~ */

  /* HEART OF THE BUBBLE VIEW */
  useEffect(() => {
    // Establish the socket connection
    // console.log(COMPOSED_ADDRESS)
    const socket = new SocketClient(COMPOSED_ADDRESS, {
      reconnection: false,
      transports: ['websocket'],
      query: {
        accessToken: '12345',
        bubbleID
      }
    })

    socket.on('connect', () => {
      /* If not connected, prevent from moving forward */
      if (!socket.isConnected) {
        notify('Uh oh!', 'We couldn\'t connect to the voice server... Please try again later!', 'error')
        navigate('/app')
        return
      }

      // Initial Express Requests
      sendPeerData(socket)
      retrievePeersData(0, true, [])

      // Socket Event Messages
      onUserJoin(socket)
      onUserLeave(socket)

      socketLifetimeHandlers(socket)
    })

    // Ensuring the socket is closed
    return () => {
      if (!socket.isConnected) {
        return
      }
      // Close Socket Connection to Voice Server
      socket.disconnect(true)
      // Close all peer connections per given stream
      users.forEach(user => user.peer.close())
    }
  }, [])

  /* UI/Audio useEffect Logic */
  useEffect(() => {
    joinAudio?.play()
    return () => leaveAudio?.play()
  }, [])

  /* Updating Streams */
  useEffect(() => {
    /* Retrieving User Media */
    /* TODO: Implement Logic for Updating Streams */

    return () => {
      console.log('')
    }
  }, [mediaManager.selectedDevices])

  /* Set the Navbar type */
  useEffect(() => {
    try {
      dispatch(allAppActions.setNavbarType('call'))
    } catch (e) {
      console.log(e)
    }
  }, [])

  return (
    <BubbleViewStyled>
      <Streams
        backgroundColor={theme.call.backgroundColor}
        usersData ={users}
      />
    </BubbleViewStyled>
  )
}
