/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name User.Model
 *
 * @returns {MongoDB_Model} MongoDB Model
 *
 * @author Christian C. Larcomb
 */

import mongoose from 'mongoose'

const userSchema = mongoose.Schema({

  _id: Number,

  meta: {
    user_jwt: String,
    dateCreatedEpoch: Number
  },
  user_information: {
    username: String,
    email: String,
    age: String,
    password: String
  },
  status: {
    email_verified: Boolean,
    creator_verified: Boolean,
    restricted_until_epoch: Number,
    banned: Boolean,
    privileged: Boolean
  },
  recovery: {
    codes: [String],
    phone_number: Number,
    security_questions: [
      {
        question: String,
        answer: String
      },
      {
        question: String,
        answer: String
      }
    ]
  },
  platform: {
    description: String,
    layout: String,
    manufacturer: String,
    name: String,
    os: String,
    prerelease: String,
    product: String,
    ua: String,
    version: String,
    architecture: String,
    family: String
  },
  networkInfo: {
    ip_address: String
  }

})

export default mongoose.model('User', userSchema, 'Users')
