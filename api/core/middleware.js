/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name API.Middleware
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */
import {getDateAndTimeAsString, logLevelPresentCheck} from "./global.middleware.js"
import axios from "axios"
import userModel from "../database/models/user.model.js"
import os from "os";

/**
 * TODO: Add JSDoc
 */
export async function healthCheckAllNodes(req,res) {
  let endpoints = {
    voice:    `http://${process.env.API_VOICE_DCKR_DNS}:${process.env.API_VOICE_PORT}/api/${process.env.API_VERSION}/voice/healthcheck/`,
    database: `http://${process.env.API_DB_DCKR_DNS}:${process.env.API_DB_PORT}/api/${process.env.API_VERSION}/db/healthcheck/`,
    crypto:   `http://${process.env.API_CRYPTO_DCKR_DNS}:${process.env.API_CRYPTO_PORT}/api/${process.env.API_VERSION}/crypto/healthcheck/`,
    wallet:   `http://${process.env.API_WALLET_DCKR_DNS}:${process.env.API_WALLET_PORT}/api/${process.env.API_VERSION}/wallet/healthcheck/`,
    cdn:      `http://${process.env.API_CDN_DCKR_DNS}:${process.env.API_CDN_PORT}/api/${process.env.API_VERSION}/cdn/healthcheck/`,
    sessions: `http://${process.env.API_SESSIONS_DCKR_DNS}:${process.env.API_SESSIONS_PORT}/api/${process.env.API_VERSION}/sessions/healthcheck/`
  }

  let reportingHealthy = [],
    reportingUnhealthy = []

  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        level: 'INFO',
        content: { endpoints }
      }
    })

  for (let endpoint in endpoints) {
    try{
      await axios.get(endpoints[endpoint], {
        headers: {
          connection:    'keep-alive',
          authorization: `Bearer ${res.locals.authToken}`,
          secureid:      process.env.API_CORE_SECUREID
        }
      })
        .then((res) => {
          console.log(endpoints[endpoint])
          console.log(res.data)
          reportingHealthy.push(endpoint)
        })
        .catch((res) => {
          console.log(endpoints[endpoint])
          console.log(res)
          reportingUnhealthy.push(endpoint)
        })
    } catch (err) {
      console.log('ERR THROWN')
    }
  }

  res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Successfully reached PubSub Core!',
        pid:     process.pid
      },
      payload: {
        reportingHealthy,
        reportingUnhealthy
      }
    }
  })
}

/**
 * TODO: Add JSDoc
 */
export function handleQuery(req, res, next){
  const {context} = req.headers
  
  const {API_DB_IP_OR_DNS, API_DB_PORT} = process.env

  const url = `http://${API_DB_IP_OR_DNS}:${API_DB_PORT}/api/v1/db/query`

  axios.post(url, req.body, {headers: { context: req.headers?.context, 'ms-secure-key': process.env.API_MS_SECURE_KEY}})
    .then((response) => {
      console.log({'res data': response.data})
      return res.status(200).send(response.data)
    }).catch((err) => {
      console.log(err)
      return res.status(500).send(err)
    })
}

/**
 * TODO: Add JSDoc
 */
export async function handleCreateViaDatabase(req,res,next){

  /* DATABASE PROCESSES */ 
  const {context} = req.headers

  const {API_DB_IP_OR_DNS, API_DB_PORT} = process.env

  const urlDatabase = `http://${API_DB_IP_OR_DNS}:${API_DB_PORT}/api/v1/db/create`

  let response

  try {
    response = await axios.post(urlDatabase, req.body, {headers: { context: req.headers?.context, 'ms-secure-key': process.env.API_MS_SECURE_KEY}})
  } catch(err) {
    console.log({
      'PubSub Response': {
        header: {
          status:  500,
          message: 'Internal server error. Unable to create document.',
          pid:     process.pid,
        },
        payload: err
      }
    })
    return res.status(500).send({
      'PubSub Response': {
        header: {
          status:  500,
          message: 'Internal server error. Unable to create document.',
          pid:     process.pid,
        },
        payload: err
      }
    })
  }
  return next() 
}

/**
 * TODO: Add JSDoc
 */
export function handleCreateViaCDN(req,res,next){

    /* Data Contents */
    let   bubbleID
    const bubbleImage  = req.files.bubbleImage
    const bubbleObject = JSON.parse(req.body.bubbleObject)
  
    let cryptoTaskComplete   = false
    let databaseTaskComplete = false
    let cdnTaskComplete      = false

  /* CDN PROCESSES */
    const formData = new FormData()
    formData.set('bubbleImage',  bubbleImage)
    formData.set('bubbleID',  bubbleID)

    axios.post(cdnEndpoint + cdnUploadImage, {
        bubbleImageBuffer: bubbleImage.data,
        bubbleID:          bubbleID
    })
        .then(response => {
            console.log({
                'PubSub Response': {
                    header: {
                        status:       200,
                        message:      'Bubble Image uploaded to CDN successfully!',
                        pid:          process.pid,
                        port:         nodes.CDN.PORTS[0],
                        cdnEndpoint:  cdnEndpoint + cdnUploadImage
                    },
                    payload: {...response}
                }
            })
            cdnTaskComplete = true
        })
        .catch(err => {
            console.log({
                'PubSub Response': {
                    header: {
                        status:       500,
                        message:      'Internal server error. Something went wrong with the CDN request.',
                        pid:          process.pid,
                        port:         nodes.CDN.PORTS[0],
                        cdnEndpoint:  cdnEndpoint + cdnUploadImage
                    },
                    payload: err
                }
            })
            return res.status(500).send({
                'PubSub Response': {
                    header: {
                        status:       500,
                        message:      'Internal server error. Something went wrong with the CDN request.',
                        pid:          process.pid,
                        port:         nodes.CDN.PORTS[0],
                        cdnEndpoint:  cdnEndpoint + cdnUploadImage
                    },
                    payload: err
                }
            })
        })
  

  /* Upon Completion of All Tasks */
  if(cryptoTaskComplete && dbaSubtaskComplete && cdnSubtaskComplete){

    if(logLevelPresentCheck('INFO'))
        console.log({
            'PubSub Response': {
                header: {
                    status:      200,
                    message:     'Bubble creation tasks and sub-tasks completed successfully.',
                    pid:         process.pid
                },
                payload: {
                    bubbleID:     bubbleID,
                    bubbleImgURI: cdnServeURI + cdnServeBubbles + bubbleID + '.png',
                    bubbleLink:   'http://localhost:3000/app/bubbles/' + bubbleID
                }
            }
        })

    return res.status(200).send({
        'PubSub Response': {
            header: {
                status:      200,
                message:     'Bubble creation tasks and sub-tasks completed successfully.',
                pid:         process.pid
            },
            payload: {
                bubbleID:     bubbleID,
                bubbleImgURI: cdnServeURI + cdnServeBubbles + bubbleID + '.png',
                bubbleLink:   'http://localhost:3000/app/bubbles/' + bubbleID
            }
        }
    })

  } else {
      return res.status(500).send({
          'PubSub Response': {
              header: {
                  status:      500,
                  message:     'Internal server error. CORE failed to complete all sub-tasks.',
                  pid:         process.pid,
                  port:        "REPLACE"
              }
          }
      })
  }
}

/**
 * TODO: Add JSDoc
 */
export function preventMissingAuthToken(req,res,next) {
  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
    return next()

  if(req.headers.authorization !== undefined){
    res.locals.authToken = req.headers.authorization.split(' ')[1]
    return next()
  }

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            status:  'Error',
            code:    403,
            message: `Missing request authorization credentials.`
          }
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  res.status(403).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Missing request authorization credentials.`,
        worker:  process.pid
      }
    }
  })
}

/**
 * TODO: Add JSDoc
 */
export async function preventInvalidAuthToken(req,res,next) {

  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
  return next()

  const {context} = req.headers

  const requestBody = {array: [res.locals.authToken]}
  
  const {API_CRYPTO_IP_OR_DNS, API_CRYPTO_PORT} = process.env
  
  const url = `http://${API_CRYPTO_IP_OR_DNS}:${API_CRYPTO_PORT}/api/v1/crypto/decrypt`
  let response
  try{
    response = await axios.post(url, requestBody, {headers: { context: 'jwts', 'ms-secure-key': process.env.API_MS_SECURE_KEY}})
  }
  catch(err){
    if(logLevelPresentCheck('ERR'))
      console.log({
        "PubSub Logging": {
          level: 'ERR',
          response: {
            header: {
              status:  'Error',
              code:    403,
              message: `Invalid auth token provided.`,
              worker:  process.pid
            }
          },
          diagnostics: {
            node:     process.env.API_NODE_NAME,
            datetime: getDateAndTimeAsString(),
            network: {
              remoteAddress: req.socket.remoteAddress       || undefined,
              xForwardedFor: req.headers['x-forwarded-for'] || undefined
            },
            os: {
              process:    process.pid,
              cpus:       os.cpus(),
              arch:       os.arch(),
              endianness: os.endianness(),
              version:    os.version(),
              memory: {
                free:  os.freemem(),
                total: os.totalmem()
              },
              priority:   os.getPriority(process.pid),
              hostname:   os.hostname(),
            }
          }
        }
      })
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    403,
          message: `Invalid auth token provided.`,
          worker:  process.pid
        }
      }
    })
  }
  
  const contents = response.data['PubSub Response']?.payload[0]

  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        level: 'INFO',
        tokenContents: contents
      }
    })

  res.locals.authTokenContents = contents
  return next()
}

/**
 * TODO: Complete JSDoc
 */
export async function preventWithActiveSuspension (req,res,next){
  return next()
}

/**
 * TODO: Add JSDoc
 */
export function preventIncorrectAuthTokenAJWT(req, res, next) {
  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
    return next()

  const {type} = res.locals.authTokenContents

  // Continue when request has/is:
  // - A validated AJWT
  // - A validated hCaptcha request
  if(type === 'ajwt' || res.locals.hCaptchaSuccess)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            code:    403,
            message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
            err:     'Expecting token type: SJWT'
          },
          err: "Expecting a valid ajwt."
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  return res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
        worker:  process.pid
      },
      err: "Expecting a valid ajwt."
    }
  })
}

/**
 * TODO: Add JSDoc
 */
export function preventIncorrectAuthTokenSJWT(req, res, next) {
  const {type} = res.locals.authTokenContents

  // Requesting token is not a valid AJWT or HJWT - Ignore
  if(type === 'sjwt')
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        content: {
          response: {
            code:    403,
            message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
            err:     'Expecting token type: SJWT'
          }
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
        worker:  process.pid
      },
      err: "Expecting SJWT"
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function preventMalformedHCaptcha(req,res,next)
{
  let invalidFields = []
  const {
    hCaptcha= {
      // ip: undefined,
      response: undefined,
      siteKey: undefined
    }
  } = req.body

  // If hCaptcha was not provided, skip this
  if(req.body.hCaptcha === undefined)
    return next()

  // hCaptcha.ip       === undefined ? invalidFields.push('Missing hCaptcha.ip')       : {}
  hCaptcha.response === undefined ? invalidFields.push('Missing hCaptcha.response') : {}
  hCaptcha.siteKey  === undefined ? invalidFields.push('Missing hCaptcha.siteKey')  : {}

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            status:  'Error',
            code:    400,
            message: 'hCaptcha object is malformed.',
            pid:     process.pid
          },
          err: invalidFields
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'hCaptcha object is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function verifyHCaptchaToken(req,res,next)
{
  // TODO: Should this be moved to crypto?

  // If hCaptcha was not provided - skip...
  if(req.body.hCaptcha === undefined)
    return next()

  const verifyURL = "https://hcaptcha.com/siteverify"
  const {response, siteKey} = req.body.hCaptcha

  // If no token was provided, ignore this step
  if(response === undefined)
    return next()

  const params = new URLSearchParams()
  params.append('response', response)
  params.append('secret', process.env.API_HCAPTCHA_SECRET)
  params.append('siteKey', siteKey)

  axios.post(verifyURL,params)
    .then(response => {
      const { data } = response,
        errorCodes = data['error-codes'],
        success    = data.success

      if(success)
      {
        res.locals.hCaptchaSuccess = true
        return next()
      }

      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Logging": {
            level: 'ERR',
            response: {
              header: {
                status: 'Error',
                code:   400,
                message: 'hCaptcha validation failed.',
                process: process.pid
              },
              err: errorCodes,
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            }
          }
        })

      return res.status(400).send({
        "PubSub Response": {
          header: {
            status: 'Error',
            code:   400,
            message: 'hCaptcha validation failed.',
            process: process.pid
          },
          err: errorCodes
        }
      })

    })
    .catch(err => {
      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Response": {
            level: 'ERR',
            response: {
              header: {
                status:  'Error',
                code:    400,
                message: 'hCaptcha responded with a request error',
                pid:     process.pid
              },
              err: err
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            }
          }
        })
      return res.status(400).send({
        "PubSub Response": {
          header: {
            status:  'Error',
            code:    400,
            message: 'hCaptcha responded with a request error',
            pid:     process.pid
          },
          err: err
        }
      })
    })
}

/**
 * TODO: Add JSDoc
 */
export function handleDeleteViaDatabase(req, res, next){

  const {context} = req.headers
  
  const {API_DB_IP_OR_DNS, API_DB_PORT} = process.env

  const url = `http://${API_DB_IP_OR_DNS}:${API_DB_PORT}/api/v1/db/delete`

  axios.post(url, req.body, {headers: { context: req.headers?.context, 'ms-secure-key': process.env.API_MS_SECURE_KEY}})
    .then((response) => {
      console.log({'res data': response.data})
      return next()
    }).catch((err) => {
      console.log(err)
      return res.status(500).send(err)
    })
}

/**
 * TODO: Add JSDoc
 */
export function handleDeleteViaCDN(req, res, next){

  
}

/**
 * TODO: Add JSDoc
 */
export function handleUpdateViaDatabase(req, res, next){

  const {context} = req.headers
  
  const {API_DB_IP_OR_DNS, API_DB_PORT} = process.env

  const url = `http://${API_DB_IP_OR_DNS}:${API_DB_PORT}/api/v1/db/update`

  axios.post(url, req.body, {headers: { context: req.headers?.context, 'ms-secure-key': process.env.API_MS_SECURE_KEY}})
    .then((response) => {
      console.log({'res data': response.data})
      return next()
    }).catch((err) => {
      console.log(err)
      return res.status(500).send(err)
    })
}

/**
 * TODO: Add JSDoc
 */
export function handleUpdateViaCDN(req, res, next){

}

/**
 * TODO: Add JSDOC here!
 */
export async function filenameParser(req, res, next)
{
  let file = req.files?.image
  if (!file)
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Invalid or no image provided.',
          pid:     process.pid
        }
      }
    })

  let filenameRe = /([^\\]*)\./
  let extRe      = /(?:\.([^.]+))?$/
  let filename   = filenameRe.exec(file?.name)[1]
  let extension  = extRe.exec(file?.name)[1]

  if(filename !== null || extension !== null){
    if(logLevelPresentCheck('info'))
      console.log({
        'PubSub Logging': {
          header: {
            status:  'Healthy',
            message: 'Successfully parsed the image filename and extension.',
            pid:     process.pid
          }
        }
      })

    res.locals.filename  = filename
    res.locals.extension = extension

    return next()
  }

  if(logLevelPresentCheck('err'))
    console.log({
      'PubSub Logging': {
        header: {
          status: 'Severe',
          message: 'Failed to parse the image filename or extension.',
          pid: process.pid
        }
      }
    })

  return res.status(400).send({
    'PubSub Response': {
      header: {
        status:  400,
        message: 'Failed to parse the image filename or extension.',
        pid:     process.pid
      }
    }
  })

}

/**
 * TODO: Add JSDOC here!
 */
export function appendFilenameExtensionInfoToRes(req, res, next)
{
  res.locals.imageFileEnhanced = {
    ...req.files?.image,
    filename: res.locals?.filename,
    extension: res.locals?.extension
  }

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function configParser(req, res, next)
{
  let configBuffer = req.files?.config?.data
  let configObject = JSON.parse(configBuffer.toString())

  if(configObject === undefined)
    return req.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Failed to parse the config document.',
          pid:     process.pid
        }
      }
    })

  if(logLevelPresentCheck('info'))
    console.log({
      'PubSub Logging': {
        header: {
          status:  'Success',
          message: 'Config JSON file successfully parsed and appended to req.files.configObject.',
          pid:     process.pid
        }
      }
    })

  res.locals.configObject = configObject

  return next()
}