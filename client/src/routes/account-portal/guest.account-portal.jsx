/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Guest_Account-Portal
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useContext,
  useEffect,
  useState
} from 'react'
import styled, { ThemeContext } from 'styled-components'
import { SECTION_COLORS } from '../../constants/themes.constants.js'

/* Redux Imports */
import { useSelector, useDispatch } from 'react-redux'
import allActions from '../../redux/actions/index.js'

/* Core Components */
import Form from '../../components/core-components/form.component.jsx'
import Field from '../../components/core-components/field.component.jsx'
import Emblem from '../../components/core-components/emblem.component.jsx'
import Button from '../../components/core-components/button.component.jsx'
import { notify } from '../../components/core-components/notifications.component.jsx'
import Dropdown from '../../components/core-components/dropdown.component.jsx'

/* SVG Imports */
import { ReactComponent as GuestSVG } from '../../assets/svgs/guest.svg'
import { ReactComponent as HeadphonesSVG } from '../../assets/svgs/headphones.svg'
import { ReactComponent as MicSVG } from '../../assets/svgs/mic.svg'
import { ReactComponent as VideoSVG } from '../../assets/svgs/video.svg'
import Disclaimer from '../../components/core-components/disclaimer.component.jsx'

const GuestSVGStyled = styled(GuestSVG)`
  fill: white;
  height: 35px;
`

/* This is an Implementation Component */
export default
function GuestAccountPortal (props) {
  const theme = useContext(ThemeContext)

  const mediaManager = useSelector(state => state.mediaManager)
  const dispatch = useDispatch()

  /* RETRIEVING: Set of all available inputs */
  const reduxAudioInputs = mediaManager.mediaDevices.audioInputs
  const reduxAudioOutputs = mediaManager.mediaDevices.audioOutputs
  const reduxVideoInputs = mediaManager.mediaDevices.videoInputs

  /* RETRIEVING: Set of initial selections */
  const reduxSelectedAudioInput = mediaManager.selectedDevices.audioInput
  const reduxSelectedAudioOutput = mediaManager.selectedDevices.audioOutput
  const reduxSelectedVideoInput = mediaManager.selectedDevices.videoInput

  /* Handling Media Device Permission(s) Request(s) */
  useEffect(() => {
    if (mediaManager.devicePermissions.video === 'prompt') {
      navigator.mediaDevices.getUserMedia({ video: true },
        /* Upon Success */
        () => {
          console.log('video success')
          dispatch(allActions.mediaActions.devicePermissionsUpdateVideo(true))
        },
        /* Upon Fail */
        () => {
          console.log('video fail')
          dispatch(allActions.mediaActions.devicePermissionsUpdateVideo(false))
        })
    }

    if (mediaManager.devicePermissions.audio === 'prompt') {
      navigator.mediaDevices.getUserMedia({ audio: true },
        /* Upon Success */
        () => {
          console.log('audio success')
          dispatch(allActions.mediaActions.devicePermissionsUpdateAudio(true))
        },
        /* Upon Fail */
        () => {
          console.log('audio fail')
          dispatch(allActions.mediaActions.devicePermissionsUpdateAudio(false))
        })
    }
  }, [])

  return (
    <div style={{ width: 900 }}>

      <Form
        props ={props}
        borderColor ={SECTION_COLORS.guest[1]}
        backgroundColor={theme.form.backgroundColor}
        minWidth ={250}
        padding ={10}
        columns ={2}
        margin={'10px'}
      >

        <Emblem
          props={props}
          colorOne={SECTION_COLORS.guest[0]}
          colorTwo={SECTION_COLORS.guest[1]}
          title={'Guest'}
          gridColumn={'1/3'}
          icon={<GuestSVGStyled/>}
        />

        {/* Primary Info */}
        <div>

          <Field
            title ={'Bubble ID'}
            minWidth ={255}
            maxCharCount ={50}
            enabled ={true}
            initText ={'Bubble ID'}
            required ={true}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            margin ={'0 0 10px 0'}
          />

          <Field
            title ={'Passphrase'}
            footnote ={'No passphrase? Leave this field blank.'}
            minWidth ={255}
            maxCharCount ={50}
            enabled ={true}
            initText ={'Passphrase'}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            margin ={'10px 0'}
          />

          <Field
            title ={'Display Name'}
            required ={true}
            minWidth ={255}
            maxCharCount ={50}
            enabled ={true}
            initText ={'Display Name'}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            margin ={'10px 0'}
          />

        </div>

        {/* Preferences & Toggles */}
        <div>

          <Dropdown
            title ={'Audio Output'}
            // Todo: Implement checks: footnote={"Test Output"}

            displayToggle ={true}
            toggleInitState ={true}

            margin ={'0 0 20px 0'}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            dBackgroundColor={theme.field.disabled.backgroundColor}
            dBorderColor ={theme.field.disabled.borderColor}
            icon ={<HeadphonesSVG/>}
            items ={
              reduxAudioOutputs.map(item => {
                return {
                  id: item.deviceId,
                  text: item.label
                }
              })
            }
            selection ={{
              id: reduxSelectedAudioOutput.deviceId,
              text: reduxSelectedAudioOutput.label
            }}
            stateMirror ={(state) => {
              /* DEBUGGING OUTPUTS */
              // console.log("IM UP", state)
              // console.log(selectedDevice)
              // console.log(reduxSelectedAudioOutput)
              const selectedDevice = reduxAudioOutputs.find(item => item.deviceId === state.id)

              dispatch(allActions.mediaActions.selectedMediaStateUpdate(
                reduxSelectedAudioInput,
                selectedDevice,
                reduxSelectedVideoInput
              ))
            }}
          />

          <Dropdown
            title={'Audio Input'}
            // Todo: Implement checks: footnote={"Test Input"}

            displayToggle ={true}
            toggleInitState ={false}

            toggle ={true}

            margin ={'0 0 20px 0'}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            dBackgroundColor={theme.field.disabled.backgroundColor}
            dBorderColor ={theme.field.disabled.borderColor}
            icon ={<MicSVG/>}
            items ={
              reduxAudioInputs.map(item => {
                return {
                  id: item.deviceId,
                  text: item.label
                }
              })
            }
            selection ={{
              id: reduxSelectedAudioInput.deviceId,
              text: reduxSelectedAudioInput.label
            }}
            stateMirror ={(state) => {
              const selectedDevice = reduxAudioInputs.find(item => item.deviceId === state.id)

              dispatch(allActions.mediaActions.selectedMediaStateUpdate(
                selectedDevice,
                reduxSelectedAudioOutput,
                reduxSelectedVideoInput
              ))
            }}
          />

          <Dropdown
            title={'Video Input'}
            // Todo: Implement checks: footnote={"Preview Video"}

            displayToggle ={true}
            toggleInitState ={false}

            margin ={'0 0 20px 0'}
            enabled ={true}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            dBackgroundColor={theme.field.disabled.backgroundColor}
            dBorderColor ={theme.field.disabled.borderColor}
            icon ={
              <VideoSVG style={{ height: 27 }}/>
            }
            items ={reduxVideoInputs.map(item => {
              return {
                id: item.deviceId,
                text: item.label
              }
            })}
            selection ={{
              id: reduxSelectedVideoInput.deviceId,
              text: reduxSelectedVideoInput.label
            }}
            stateMirror ={(state) => {
              const selectedDevice = reduxVideoInputs.find(item => item.deviceId === state.id)

              dispatch(allActions.mediaActions.selectedMediaStateUpdate(
                reduxSelectedAudioInput,
                reduxSelectedAudioOutput,
                selectedDevice
              ))
            }}
          />

        </div>

        <div style={{
          gridColumn: '1/3',
          display: 'flex',
          position: 'relative',
          justifyContent: 'center'
        }}>
          <Button
            text ={'Cancel'}
            width ="195px"
            linkTo ='/account-portal/'
            margin ="0 5px 0 0"
            backgroundColor={theme.button.enabled.backgroundColor}
            borderColor ={theme.button.enabled.borderColor}
          />
          <Button
            text ={'Join'}
            width ="195px"
            margin ="0 0 0 5px"
            onClick={e => {
              notify('Woah!', "We're having trouble at the moment. Try back in a bit!", 'error')
            }}
            backgroundColor={theme.button.enabled.backgroundColor}
            borderColor ={theme.button.enabled.borderColor}
          />

        </div>

        {
          !mediaManager.devicePermissions.audio &&
                    (<div style={{ gridColumn: '1/3' }}>
                      <Disclaimer
                        title={'NOTE!'}
                        text={'Permissions to access Audio and/or Video devices are currently blocked.' +
                                ' Please reset your device access permissions in your browser settings then refresh this page.' +
                                ' Once prompted, please allow device permissions to gain access to PubSub.'}
                        fontSize={'15px'}
                        borderRadius={'6'}
                        margin={'0'}
                      />
                    </div>)
        }

      </Form>
    </div>
  )
}
