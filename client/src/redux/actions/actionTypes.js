/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/* Media_State Actions */
export const SET_AVAILABLE_MEDIA_STATE = 'SET_AVAILABLE_MEDIA_STATE'
export const SET_AVAILABLE_MEDIA_STATE_AUDIOIN = 'SET_AVAILABLE_MEDIA_STATE_AUDIOIN'
export const SET_AVAILABLE_MEDIA_STATE_AUDIOOUT = 'SET_AVAILABLE_MEDIA_STATE_AUDIOOUT'
export const SET_AVAILABLE_MEDIA_STATE_VIDEOIN = 'SET_AVAILABLE_MEDIA_STATE_VIDEOIN'
export const SET_SELECTED_MEDIA_STATE = 'SET_SELECTED_MEDIA_STATE'
export const SET_SELECTED_MEDIA_STATE_AUDIOIN = 'SET_SELECTED_MEDIA_STATE_AUDIOIN'
export const SET_SELECTED_MEDIA_STATE_AUDIOOUT = 'SET_SELECTED_MEDIA_STATE_AUDIOOUT'
export const SET_SELECTED_MEDIA_STATE_VIDEOIN = 'SET_SELECTED_MEDIA_STATE_VIDEOIN'

/* Permission Actions */
export const SET_DEVICE_PERMISSION_AUDIO = 'SET_DEVICE_PERMISSION_AUDIO'
export const SET_DEVICE_PERMISSION_VIDEO = 'SET_DEVICE_PERMISSION_VIDEO'

/* App State Actions */
export const SET_SOCKET_ATTEMPT_SECONDS = 'SET_SOCKET_ATTEMPT_SECONDS'
export const SET_APP_IS_READY = 'SET_APP_IS_READY'
export const SET_APP_IS_MOUNTED = 'SET_APP_IS_MOUNTED'
export const SET_APP_IN_FATAL_STATE = 'SET_APP_IN_FATAL_STATE'
export const SET_NAVBAR_TYPE = 'SET_NAVBAR_TYPE'
export const SET_READINESS_PERCENTAGE = 'SET_READINESS_PERCENTAGE'

/* User State Actions */
export const SET_USER_IS_LOGGED_IN = 'SET_USER_IS_LOGGED_IN'
