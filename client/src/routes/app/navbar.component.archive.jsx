/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Navbar_Nav
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useContext,
  useEffect,
  useRef,
  useState
} from 'react'
import styled,
{ ThemeContext } from 'styled-components'
import { Outlet, Link, useNavigate } from 'react-router-dom'
import Foco from 'react-foco'

/* Core-Components */
import NavBar from '../../components/core-components/navbar.component.jsx'
import Button from '../../components/core-components/button.component.jsx'
import Form from '../../components/core-components/form.component.jsx'
import ColorPicker from '../../components/core-components/colorpicker.component.jsx'
import ImageSelector from '../../components/core-components/imageselector.component.jsx'
import Field from '../../components/core-components/field.component.jsx'
import BubbleVote from '../../components/core-components/bubble_vote.component.jsx'
import DeviceSelectionList from '../../components/core-components/device_selection_list.component.jsx'

/* hub-Navbar */
import { ReactComponent as CompanyLogo } from '../../assets/svgs/logo_rep_solo.svg'
import { ReactComponent as LikeDislikeSVG } from '../../assets/svgs/likedislike.svg'
import { ReactComponent as MiscSVG, ReactComponent as DotsSVG } from '../../assets/svgs/dots.svg'
/* Call NavBar */
import { ReactComponent as HeadphonesSVG } from '../../assets/svgs/headphones.svg'
import { ReactComponent as MicSVG } from '../../assets/svgs/mic.svg'
import { ReactComponent as VideoSVG } from '../../assets/svgs/video.svg'
import { ReactComponent as ExitSVG } from '../../assets/svgs/exit.svg'
import { ReactComponent as ChangeViewSVG } from '../../assets/svgs/change_view.svg'
/* Misc Panel */
import { ReactComponent as ReportSVG } from '../../assets/svgs/form.svg'
import { ReactComponent as LegalSVG } from '../../assets/svgs/legal.svg'
import { ReactComponent as AboutSVG } from '../../assets/svgs/about.svg'
import { ReactComponent as SafetySVG } from '../../assets/svgs/safety.svg'
import { ReactComponent as AcknowledgementsSVG } from '../../assets/svgs/acknowledgements.svg'
import { ReactComponent as DonateSVG } from '../../assets/svgs/donate.svg'
import { ReactComponent as TeamSVG } from '../../assets/svgs/team.svg'

/* Redux Imports */
import { useDispatch, useSelector } from 'react-redux'
import allActions from '../../redux/actions/index.js'

const CompanyLogoSVGStyled = styled(CompanyLogo)`
  height: 20px;
  fill:   white;
`
const LikeDislikeSVGStyled = styled(LikeDislikeSVG)`
  height: 14px;
  fill:   white;
`
const MiscSVGStyled = styled(MiscSVG)`
  height: 7px;
  fill:   white;
`

/* Containers */
const MenuContainer = styled.div`
  position: absolute;

  left:     50%;
  bottom:   85px;
  z-index:  999;
  transform: translate(-50%, 0)
`

/* Constants */
const Hublets = [
  /* Video Games | hub-let */
  {
    name: 'Video Games',
    hubs: [
      {
        tag: '/coop/',
        name: 'Co-op'
      },
      {
        tag: '/mult/',
        name: 'Multiplayer'
      },
      {
        tag: '/pc/',
        name: 'PC'
      },
      {
        tag: '/vr/',
        name: 'Virtual Reality'
      }
    ]
  },

  /* Interests | hub-let */
  {
    name: 'Interests',
    hubs: [
      {
        tag: '/tech/',
        name: 'Technology'
      },
      {
        tag: '/tech/',
        name: 'Technology'
      },
      {
        tag: '/cc/',
        name: 'Comics & Cartoons'
      },
      {
        tag: '/tf/',
        name: 'Television & Film'
      },
      {
        tag: '/wep/',
        name: 'Weapons'
      },
      {
        tag: '/auto/',
        name: 'Auto'
      },
      {
        tag: '/an/',
        name: 'Animals & Nature'
      },
      {
        tag: '/bg/',
        name: 'Board Games'
      },
      {
        tag: '/fit/',
        name: 'Fitness'
      },
      {
        tag: '/spts/',
        name: 'Sports'
      },
      {
        tag: '/xspts/',
        name: 'Extreme Sports'
      },
      {
        tag: '/toys/',
        name: 'Toys'
      },
      {
        tag: '/yoga/',
        name: 'Yoga'
      }
    ]
  },

  /* Academics | hub-let */
  {
    name: 'Academics',
    hubs: [
      {
        tag: '/bio/',
        name: 'Biology'
      },
      {
        tag: '/sci/',
        name: 'Science'
      },
      {
        tag: '/math/',
        name: 'Mathematics'
      },
      {
        tag: '/hist/',
        name: 'History'
      },
      {
        tag: '/humn/',
        name: 'Humanities'
      },
      {
        tag: '/chem/',
        name: 'Chemistry'
      },
      {
        tag: '/engnrg/',
        name: 'Engineering'
      },
      {
        tag: '/phil/',
        name: 'Philosophy'
      },
      {
        tag: '/cybe/',
        name: 'Cyber'
      },
      {
        tag: '/med/',
        name: 'Medical'
      }
    ]
  },

  /* Creative | hub-let */
  {
    name: 'Creative',
    hubs: [
      {
        tag: '/pd/',
        name: 'Papercraft & Oragami'
      },
      {
        tag: '/pics/',
        name: 'Photography'
      },
      {
        tag: '/food/',
        name: 'Food & Cooking'
      },
      {
        tag: '/ac/',
        name: 'Artwork & Critique'
      },
      {
        tag: '/diy/',
        name: 'Do-It-Yourself'
      },
      {
        tag: '/q/',
        name: 'Quests'
      },
      {
        tag: '/lit/',
        name: 'Literature'
      },
      {
        tag: '/song/',
        name: 'Music'
      },
      {
        tag: '/fash/',
        name: 'Fashion'
      },
      {
        tag: '/gdes/',
        name: 'Graphic Design'
      }
    ]
  },

  /* Life | hub-let */
  {
    name: 'Life',
    hubs: [
      {
        tag: '/pub/',
        name: 'PubSubPub'
      },
      {
        tag: '/para/',
        name: 'Paranormal'
      },
      {
        tag: '/advi/',
        name: 'Advice'
      },
      {
        tag: '/lgbt/',
        name: 'LGBTQ+'
      },
      {
        tag: '/cn/',
        name: 'Current News'
      },
      {
        tag: '/wr/',
        name: 'Workspace Requests'
      },
      {
        tag: '/jobs/',
        name: 'Jobs'
      }
    ]
  },

  /* 18+ | hub-let */
  {
    name: '18+',
    hubs: [
      {
        tag: '/any/',
        name: 'Anything'
      },
      {
        tag: '/pi/',
        name: 'Politically Incorrect'
      },
      {
        tag: '/int/',
        name: 'International'
      },
      {
        tag: '/sdat/',
        name: 'Speed Dating'
      }
    ]
  }
]

const UsernameContainer = styled.div`
  display:         flex;
  justify-content: space-between;
  align-items:     center;
  padding:         4px 0 2px 0;
  /* Name Container */
  & > div:nth-child(1)
  {
    & > div:nth-child(1){
      font-size:   18px;
      color:       #0095dd;
      font-weight: 700;
    }
    & > div:nth-child(2){
      font-size: 13px;
      opacity:   0.4;
    }
  }
`

const EmailPhoneContainer = styled.div`
  display:         flex;
  justify-content: space-between;
  align-items:     center;
  padding:         2px 0;
  /* Name Container */
  & > div:nth-child(1)
  {
    & > div:nth-child(1){
      font-size: 13px;
    }
    & > div:nth-child(2){
      font-size: 12px;
      opacity: 0.4;
    }
  }
`

export default
function NavBarView (props) {
  const dispatch = useDispatch()
  const theme = useContext(ThemeContext)
  const mediaManager = useSelector(state => state.mediaManager)
  const navbarManager = useSelector(state => state.appManager)

  const navigate = useNavigate()

  const MenuRef = useRef()
  const NavRef = useRef()

  const [showMenu, setShowMenu] = useState(false)
  const [menu, setMenu] = useState('profile')
  const [hub, setHub] = useState('/pub/')

  const [userColor, setUserColor] = useState('#b54542')
  const [createBubbleColor, setCreateBubbleColor] = useState('#bf6b6b')

  const handleNavbarBorderColor = () => { return userColor }

  const handleMenuBorderColor = () => {
    /* TODO: Retrieve Color from JWT */

    if (menu === 'create-menu') { return createBubbleColor } else { return userColor }
  }

  const TitleRender = (title) => {
    return (
      <div
        style={{
          color: '#ffffff',
          margin: '-2px 0 7px 0',
          textAlign: 'center',
          fontSize: 18
        }}
      >
        {title}
      </div>
    )
  }

  const handleMenuRender = () => {
    const ProfileMenu = () => {
      return (
        <div style={{ color: '#ffffff' }}>

          {TitleRender('Profile')}

          <ColorPicker
            height ={'90px'}
            defaultColor={{ hex: userColor }}
            stateMirror ={state => setUserColor(state.hex)}
            key ={'profile-color-picker'}
          />

          <ImageSelector
            height ={'103px'}
            width ={'103px'}
            margin ={'-70px auto 10px auto'}

            containerColor={theme.imageSelector.containerColor}
            areaColor ={theme.imageSelector.areaColor}
            borderColor ={theme.imageSelector.borderColor}
            iconColor ={theme.imageSelector.iconColor}

            /* TODO: FIGURE A BETTER WAY FOR IMAGE SHOW/SELECTION LOGIC */
            initFile ={undefined}

            key ={'profile-image-selector'}
          />

          <div
            style={{
              fontSize: 13,
              opacity: 0.5,
              margin: '-38px 0 10px 0'
            }}
          >
                        145 days
          </div>

          <UsernameContainer>
            <div>
              <div style={{ color: userColor }}>
                                NotEnoughKarma
              </div>
              <div>Username</div>
            </div>
            <Button
              text ={'Edit'}
              width ={'50px'}
              height ={'40px'}
              borderColor ={'#020202'}
              borderWidth ={1}
              backgroundColor={theme.button.enabled.backgroundColor}
              borderRadius ={'6px'}
            />
          </UsernameContainer>

          <EmailPhoneContainer>
            <div>
              <div>********@gmail.com</div>
              <div>Email</div>
            </div>
            <Button
              text ={'Edit'}
              width ={'50px'}
              height ={'40px'}
              borderColor ={'#020202'}
              borderWidth ={1}
              backgroundColor={theme.button.enabled.backgroundColor}
              borderRadius ={'6px'}
            />
          </EmailPhoneContainer>

          <EmailPhoneContainer>
            <div>
              <div>(***) ***-1234</div>
              <div>Phone</div>
            </div>
            <Button
              text={'Edit'}
              width={'50px'}
              height={'40px'}
              borderColor ={'#020202'}
              borderWidth ={1}
              backgroundColor={theme.button.enabled.backgroundColor}
              borderRadius ={'6px'}
            />
          </EmailPhoneContainer>

          <Button
            borderColor ={'#020202'}
            borderWidth ={1}
            backgroundColor={theme.button.enabled.backgroundColor}
            borderRadius ={'6px'}
            text ={'Advanced Settings'}
            margin ={'5px 0 0 0'}
          />

        </div>
      )
    }

    const hubMenu = () => {
      return (
        <>
          {TitleRender('Hubs')}
          <div
            style={{
              height: '550px',
              width: '100%',
              color: '#ffffff',
              borderRadius: 3,
              overflowY: 'scroll',
              overflowX: 'hidden'
            }}
          >
            {
              Hublets.map((hublet, i) => {
                return (
                  <div key={i} style={{ margin: '20px 0' }}>

                    <div
                      style={{
                        marginBottom: '8px'
                      }}
                    >
                      {hublet.name}
                    </div>

                    <div
                      style={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        gap: '6px',
                        alignItems: 'center'
                      }}
                    >
                      {
                        hublet.hubs.map(hub => {
                          return (

                            <Button
                              text ={hub.name}
                              display ={'block'}
                              width ={'100%'}
                              height ={'52px'}
                              borderRadius ={'15px'}
                              borderStyle ={'none'}
                              padding ={'0px 10px'}
                              margin ={'0'}
                              linkTo ={`/app/hub${hub.tag}`}
                              backgroundColor={theme.hubs.backgroundColor}
                              contents={
                                <div>
                                  <div
                                    style={{
                                      fontSize: 18
                                    }}
                                  >
                                    {hub.tag}
                                  </div>
                                  <div
                                    style={{
                                      fontSize: 12,
                                      opacity: 0.3
                                    }}
                                  >
                                    {hub.name}
                                  </div>
                                </div>
                              }
                              onClick={() => {
                                setShowMenu(false)
                                setHub(hub.tag)
                              }}
                              key={hub.tag}
                            />

                          )
                        })
                      }
                    </div>
                  </div>
                )
              })
            }
          </div>
        </>
      )
    }

    const CreateMenu = () => {
      return (
        <div style={{ color: '#ffffff' }}>

          {TitleRender(`Create ${hub} Topic`)}

          <ColorPicker
            height ={'90px'}
            defaultColor={{ hex: createBubbleColor }}
            stateMirror ={state => setCreateBubbleColor(state.hex)}
            key ={'create_bubble-color-picker'}
          />

          <ImageSelector
            height ={'103px'}
            width ={'103px'}
            margin ={'-70px auto 5px auto'}
            containerColor={theme.imageSelector.containerColor}
            areaColor ={theme.imageSelector.areaColor}
            borderColor ={theme.imageSelector.borderColor}
            iconColor ={theme.imageSelector.iconColor}
            key ={'create_bubble-image-selector'}
          />

          <Field
            title ={'Title'}
            initText ={'Title'}
            maxCharCount ={24}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            required ={true}
          />

          <Field
            title ={'Description'}
            initText ={'Description'}
            maxCharCount ={100}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
            required ={true}
          />

          <Field
            title ={'Tags'}
            initText ={'Enter a tag | Press space or enter after'}
            maxCharCount ={8}
            margin ={'2px 0 10px 0'}
            eBackgroundColor={theme.field.enabled.backgroundColor}
            eBorderColor ={theme.field.enabled.borderColor}
          />

          <Button
            borderColor ={'#020202'}
            borderWidth ={1}
            backgroundColor={theme.button.enabled.backgroundColor}
            borderRadius ={'6px'}
            text ={'Advanced Settings'}
            width={'100%'}
            margin ={'5px 0 0 0'}
          />

          <Button
            borderColor ={'#020202'}
            borderWidth ={1}
            backgroundColor={theme.button.enabled.backgroundColor}
            borderRadius ={'6px'}
            text ={'Submit'}
            margin ={'5px 0 0 0'}
          />

        </div>
      )
    }

    const LikeDislikeMenu = () => {
      return (
        <>
          {TitleRender(`${hub} Topic Vote`)}

          <BubbleVote
            backgroundColor={theme.bubbleVote.backgroundColor}
            borderColor ={theme.bubbleVote.borderColor}
          />
        </>
      )
    }

    const MiscMenu = () => {
      const menuOptions = [
        {
          text: 'About PubSub',
          icon: <AboutSVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        },
        {
          text: 'Contact / Report',
          icon: <ReportSVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        },
        {
          text: 'Donate',
          icon: <DonateSVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        },
        {
          text: 'Legal',
          icon: <LegalSVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        },
        {
          text: 'Safety',
          icon: <SafetySVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        },

        /*
                {
                    text: "Join the Team",
                    icon: <TeamSVG
                        style={{
                            height: 26,
                            fill: "#ffffff"
                        }}
                    />
                },
                */

        {
          text: 'Acknowledgements',
          icon: <AcknowledgementsSVG
            style={{
              height: 26,
              fill: '#ffffff'
            }}
          />
        }
      ]

      return (
        <div>
          {TitleRender('Misc')}

          <div
            style={{
              display: 'grid',
              gridTemplateColumns: '1fr 1fr 1fr',
              gap: 5
            }}
          >
            {
              menuOptions.map((item, i) => (
                <Button
                  key={i}
                  margin ={'0'}
                  height ={'80px'}
                  borderStyle ={'none'}
                  backgroundColor={theme.miscMenu.backgroundColor}
                  display ={'block'}
                  width ={'100%'}
                  contents={
                    <div>
                      {item.icon}

                      <div
                        style={{
                          position: 'absolute',
                          left: 5,
                          bottom: 5,
                          fontSize: 11,
                          opacity: 0.6
                        }}
                      >
                        {item.text}
                      </div>
                    </div>
                  }
                />
              ))
            }
          </div>
        </div>
      )
    }

    const menuSelectionEngine = () => {
      switch (menu) {
      case 'profile-menu': return ProfileMenu()
      case 'hub-menu': return hubMenu()
      case 'create-menu': return CreateMenu()
      case 'likedislike-menu': return LikeDislikeMenu()
      case 'info-menu': return MiscMenu()
      }
    }

    if (showMenu) {
      return (
        <MenuContainer ref={MenuRef}>
          <Form
            width ={'320px'}
            maxHeight ={600}
            padding ={10}
            backgroundColor={theme.form.backgroundColor}
            borderColor ={handleMenuBorderColor}
            borderRadius ={10}
          >
            {menuSelectionEngine()}
          </Form>
        </MenuContainer>
      )
    }
  }

  useEffect(() => {
    /* Clear menu selection upon being hidden */
    if (!showMenu) { setMenu('') }
  }, [showMenu])

  const handleMenuClick = (menuPassed) => {
    if (menuPassed === menu) {
      setShowMenu(!showMenu)
    } else if (menu === '') {
      setMenu(menuPassed)
      setShowMenu(!showMenu)
    } else {
      setMenu(menuPassed)
    }
  }

  /* ~~~ */

  /* HANDLE NAVBAR RENDER */
  const NavBarTypeRender = () => {
    if (navbarManager.navbarType === 'hubs') {
      return (
        <>
          <Button
            height ={'46px'}
            width ={'46px'}
            margin ={'0 9px 0 8px'}
            borderRadius ={'100%'}
            padding ={'0'}
            contents ={
              <div>
                {/* Implement Image Logic */}
              </div>
            }
            onClick ={() => handleMenuClick('profile-menu')}
            tooltipContents={'Profile'}
            tooltipID ={'profile-button'}
          />

          <Button
            height ={'46px'}
            text ={hub}
            width ={'60px'}
            margin ={'0'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            onClick ={() => handleMenuClick('hub-menu')}
            tooltipContents={'Hub Selection'}
            tooltipID ={'hub-selection-button'}
          />
          <Button
            height ={'46px'}
            contents ={<CompanyLogoSVGStyled/>}
            width ={'60px'}
            margin ={'0'}
            backgroundColor ={theme.navbar.backgroundColor}
            effect ={'rainbow-glow'}
            onClick ={() => handleMenuClick('create-menu')}
            tooltipContents={'Create Topic'}
            tooltipID ={'create-hub-button'}
          />
          <Button
            height ={'46px'}
            contents ={<LikeDislikeSVGStyled/>}
            width ={'60px'}
            margin ={'0'}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            onClick ={() => handleMenuClick('likedislike-menu')}
            tooltipContents={'Topic Vote'}
            tooltipID ={'topic-vote-button'}
          />
          <Button
            height ={'46px'}
            contents ={<MiscSVGStyled/>}
            width ={'60px'}
            margin ={'0'}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            onClick ={() => handleMenuClick('info-menu')}
            tooltipContents={'Miscellaneous'}
            tooltipID ={'miscellaneous-button'}
          />
        </>
      )
    } else if (navbarManager.navbarType === 'call') {
      const MenuTitle = styled.div`
              color:      #ffffff;
              margin:     -2px 0 4px 0;
              text-align: center;
              font-size:  16px;

              -webkit-touch-callout: none; /* iOS Safari */
              -webkit-user-select:   none; /* Safari */
              -khtml-user-select:    none; /* Konqueror HTML */
              -moz-user-select:      none; /* Old versions of Firefox */
              -ms-user-select:       none; /* Internet Explorer/Edge */
              user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
            `

      return (
        <>
          {/* Image Viewer */}
          <Button
            height ={'46px'}
            width ={'46px'}
            margin ={'0 9px 0 8px'}
            borderRadius ={'100%'}
            padding ={'0'}
            contents ={
              <div>
                {/* Implement Image Logic */}
              </div>
            }
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                borderColor ={handleNavbarBorderColor()}
                minWidth ={240}
                padding ={8}
              >

                <ColorPicker
                  height={'90px'}
                  defaultColor={handleNavbarBorderColor()}
                  enabled={false}
                />

                <ImageSelector
                  height ={'103px'}
                  width ={'103px'}
                  margin ={'-70px auto 0px auto'}

                  containerColor={theme.imageSelector.containerColor}
                  areaColor ={theme.imageSelector.areaColor}
                  borderColor ={theme.imageSelector.borderColor}
                  iconColor ={theme.imageSelector.iconColor}
                />

                <div
                  style={{
                    fontSize: 24,
                    textAlign: 'center',
                    color: handleNavbarBorderColor(),
                    marginBottom: 10,
                    fontWeight: 700
                  }}
                >
                                    NotEnoughKarma
                </div>

                <div
                  style={{
                    fontSize: 16,
                    textAlign: 'left',
                    color: theme.core.fontColor,
                    backgroundColor: '#000000',
                    borderRadius: 5,
                    minHeight: '50px',
                    padding: 5
                  }}
                >
                                    NotEnoughKarma
                </div>
              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Profile'}
            tooltipID ={'profile-button'}
          />

          <div
            style={{
              height: 28,
              width: 2,
              backgroundColor: 'rgba(255,255,255,0.2)',
              borderRadius: 5,
              margin: '0 10px'
            }}
          />

          {/* Headphones Button */}
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={
              <HeadphonesSVG
                style={{
                  fill: '#fff',
                  height: 24
                }}
              />
            }
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                borderColor ={handleNavbarBorderColor()}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >
                <>
                  <MenuTitle> Output Devices </MenuTitle>

                  <DeviceSelectionList
                    availableDevices={mediaManager.mediaDevices.audioOutputs}
                    selectedDevice ={mediaManager.selectedDevices.audioOutput}
                    onClick={selectedDevice => {
                      const foundDevice = mediaManager
                        .mediaDevices
                        .audioOutputs
                        .find(input => {
                          return input.deviceId === selectedDevice.deviceId
                        })

                      dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioOut(foundDevice))
                    }}
                    icon ={
                      <HeadphonesSVG
                        style={{
                          fill: '#fff',
                          height: 22,
                          marginLeft: 4
                        }}
                      />
                    }
                  />

                  {
                    mediaManager.selectedDevices.audioInput.deviceId === 'null'
                      ? <></>
                      : <Button
                        margin={'8px 0 0 0'}
                        onClick={() => {
                        }}
                        borderRadius={'8px'}
                        text={'Deafen'}
                      />
                  }
                </>
              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Output Devices'}
            tooltipID ={'output-device-button'}
          />

          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<MicSVG
              style={{
                fill: '#fff',
                height: 24
              }}
            />}
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                borderColor ={handleNavbarBorderColor()}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >
                <MenuTitle> Input Devices </MenuTitle>

                <DeviceSelectionList
                  availableDevices={mediaManager.mediaDevices.audioInputs}
                  selectedDevice ={mediaManager.selectedDevices.audioInput}
                  onClick={selectedDevice => {
                    const foundDevice = mediaManager
                      .mediaDevices
                      .audioInputs
                      .find(input => {
                        return input.deviceId === selectedDevice.deviceId
                      })

                    dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioIn(foundDevice))
                  }}
                  icon ={
                    <MicSVG
                      style={{
                        fill: '#fff',
                        height: 22,
                        marginLeft: 4
                      }}
                    />
                  }
                />

                {
                  mediaManager.selectedDevices.audioInput.deviceId === 'null'
                    ? <></>
                    : <Button
                      margin={'8px 0 0 0'}
                      onClick={() => {
                      }}
                      borderRadius={'8px'}
                      text={'Mute'}
                    />
                }

              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Input Devices'}
            tooltipID ={'input-device-button'}
          />
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<VideoSVG
              style={{
                fill: '#fff',
                width: 35
              }}
            />}
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                borderColor ={handleNavbarBorderColor()}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >
                <MenuTitle> Video Devices </MenuTitle>

                <DeviceSelectionList
                  availableDevices={mediaManager.mediaDevices.videoInputs}
                  selectedDevice ={mediaManager.selectedDevices.videoInput}
                  onClick={selectedDevice => {
                    const foundDevice = mediaManager
                      .mediaDevices
                      .videoInputs
                      .find(input => { return input.deviceId === selectedDevice.deviceId })
                    dispatch(allActions.mediaActions.selectedMediaStateUpdateVideoIn(foundDevice))
                  }}
                  icon={
                    <VideoSVG
                      style={{
                        fill: '#fff',
                        height: 28,
                        marginLeft: 4
                      }}
                    />
                  }
                />

                {
                  mediaManager.selectedDevices.videoInput.deviceId === 'null'
                    ? <></>
                    : <Button
                      margin={'8px 0 0 0'}
                      onClick={() => {
                        /* Perform Video Toggle Operation */
                      }}
                      borderRadius={'8px'}
                      text={'Disable Video'}
                    />
                }

              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Video Devices'}
            tooltipID ={'video-device-button'}
          />
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<ExitSVG
              style={{
                fill: '#fff',
                height: 21
              }}
            />}
            onClick ={() => { navigate(-1) }}

            tooltipContents={'Exit Bubble'}
            tooltipID ={'exit-bubble-button'}
          />

          {/* Misc button if needed in the future */}
          {/*
                    <Button
                        height          ={"46px"}
                        width           ={"60px"}
                        margin          ={"0 2px"}
                        fontSize        ={18}
                        backgroundColor ={theme.navbar.backgroundColor}
                        borderStyle     ={"none"}
                        contents        ={<MiscSVG
                            style={{
                                fill:  "#fff",
                                width: 32
                            }}
                        />}
                        menuContents={
                            <Form
                                backgroundColor={theme.navbar.backgroundColor}
                                borderColor    ={handleNavbarBorderColor()}
                                padding        ={8}
                                height         ={""}
                                minHeight      ={80}
                            >
                                <div
                                    style={{
                                        color:     "#ffffff",
                                        margin:    "-2px 0 0px 0",
                                        textAlign: "center",
                                        fontSize:  16
                                    }}
                                >
                                    Misc Options
                                    </div>
                                </div>
                            </Form>
                        }
                        menuOptions={{
                            modifiers: [
                                {
                                    name: 'offset',
                                    options: {offset: [0, 16]},
                                },
                            ]
                        }}
                    />
                    */}

          <div
            style={{
              height: 28,
              width: 2,
              backgroundColor: 'rgba(255,255,255,0.2)',
              borderRadius: 5,
              margin: '0 10px'
            }}
          />

          <Button
            height ={'46px'}
            text ={hub}
            width ={'60px'}
            margin ={'0'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<ChangeViewSVG
              style={{
                fill: '#fff',
                width: 36
              }}
            />}
            onClick ={() => {
              /* Introduce the widget panel... or popper? */
            }}

            tooltipContents={'Toggle Menu'}
            tooltipID ={'toggle-menu-button'}
          />
        </>
      )
    }
  }

  return (
    <Foco onClickOutside={() => setShowMenu(false)}>

      {handleMenuRender()}

      <div ref={NavRef}>
        <NavBar
          height ={46}
          minHeight ={46}
          maxHeight ={46}
          backgroundColor={theme.navbar.backgroundColor}
          borderColor ={userColor}
          bottom ={15}
          display ={'flex'}
          justifyContent ={'space-between'}
          minWidth ={330}
          alignItems ={'center'}
        >

          {NavBarTypeRender()}

        </NavBar>
      </div>
    </Foco>
  )
}
