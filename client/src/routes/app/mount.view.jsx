/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name AppMount
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */
import React, { useEffect, useState } from 'react'
import { Outlet } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import allActions from '../../redux/actions/index.js'
import {
  MediaStateManager,
  CookieStateManager,
  NetworkConnectionManager
} from '../../scripts/PreflightManagers.jsx'
import NavBarView from './navbar.component'
import styled from 'styled-components'
import { DARK_THEME } from '../../constants/themes.constants.js'

const AppContainer = styled.div`
  position: relative;
  display: grid;
  grid-template-rows: 1fr 60px;
  height: 100%;
  width: 100%;
  background-color: ${DARK_THEME.navbar.backgroundColor};
`

const OutletWrapper = styled.div`
  height: calc(100% - 20px);
  width: calc(100% - 20px);
  background-color: ${DARK_THEME.site.backgroundColor};
  margin: 10px;
  border-radius: 20px;
  overflow: hidden;
`

export default
function AppMount (props) {
  const [percentage, setPercentage] = useState(0)

  const dispatch = useDispatch()
  const appManager = useSelector(state => state.appManager)
  const isAppReady = true // appManager.isReady

  /* All App Lifecycle Managers */
  const [mediaReady] = MediaStateManager()
  const [networkReady] = NetworkConnectionManager()
  const [cookiesReady] = CookieStateManager()

  /* Handle Percentage Updates */
  useEffect(() => {
    setPercentage(() => {
      const mediaPerc = mediaReady ? 33 : 0
      const cookiePerc = cookiesReady ? 33 : 0
      const networkPerc = networkReady ? 33 : 0
      return mediaPerc + networkPerc + cookiePerc
    })
  }, [mediaReady, networkReady, cookiesReady])

  /* Update Readiness Percentage for Rendering */
  useEffect(() => {
    dispatch(allActions.appActions.setReadinessPercentage(percentage))

    if (percentage === 100) {
      dispatch(allActions.appActions.setAppIsReady(true))
      dispatch(allActions.appActions.setAppIsMounted(true))
    } else {
      dispatch(allActions.appActions.setAppIsReady(false))
    }
  }, [percentage])

  return (
    <AppContainer>
      <OutletWrapper>
        <Outlet/>
      </OutletWrapper>
      <NavBarView/>
    </AppContainer>
  )
}
