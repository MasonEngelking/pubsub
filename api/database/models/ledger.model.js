/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

import mongoose               from 'mongoose'
import mongooseLong           from 'mongoose-long'
import {getDateAsCustomEpoch} from '../global.middleware.js'
import {getSnowflakeID}       from '../middleware.js'

mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const ledgerSchema = new mongoose.Schema({

  _id: {type: Long, default: await getSnowflakeID()}, // TUID (Transaction Unique ID)

  comets:    Number,
  dateEpoch: Long,

  requester: {
    uuid: {type: Long, ref: 'User'},
    suid: Long,
  },

  recipient: {
    uuid: {type: Long, ref: 'User'},
    suid: Long,
  },

  context: {
    system: Boolean,
    buid:   {type:Long, ref:'Bubble'},
    quid:   {type:Long, ref:'Quickie'}
  },

  transactions: [
    {
      type:      String,                      // Payout, etc.
      uuid:      {type:Long, ref:'User'},     // User who performed transaction
      deduction: Number,                      // Deduction to above comets
      dateEpoch: Long,                        // When it happened!

      destination: {                          // Where it's going
        card: {
          cardEndingNumbers: {hashed: String},
          cardholder:        {hashed: String},
          cardType:          {hashed: String}
          // TODO: Add details
        },

        paypal: {
          email:    {hashed: String},
          business: {hashed: String},
          name:     {hashed: String}
          // TODO: Add details
        }
      }
    }
  ],

  payment: {
    type:      String,
    dateEpoch: Long,

    subtotal:  Number,
    tax:       Number,
    fees:      Number,
    total:     Number,

    geo: {
      currency:  String,
      region:    String,
    },

    comets: Number,

    card: {
      cardEndingNumbers: {hashed: String},
      cardholder:        {hashed: String},
      cardType:          {hashed: String}
      // TODO: Add details
    },

    paypal: {
      email:    {hashed: String},
      business: {hashed: String},
      name:     {hashed: String}
      // TODO: Add details
    }
  },

}, {_id: false})

export default mongoose.model('Ledgers', ledgerSchema, 'Ledger')

