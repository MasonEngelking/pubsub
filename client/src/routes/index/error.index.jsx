/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Error
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useContext } from 'react'
import styled, { ThemeContext, keyframes } from 'styled-components'

/* Components */
import Button from '../../components/core-components/button.component'

/* SVG Imports */
import { ReactComponent as CompanyLogo } from '../../assets/svgs/logo_rep_solo.svg'
import { ReactComponent as CompanyLogoRep } from '../../assets/svgs/logo_rep_emblem.svg'
import { ReactComponent as CompanyLogoText } from '../../assets/svgs/logo_text_full.svg'
import { ReactComponent as CactusSVG } from '../../assets/svgs/404.svg'
import { ReactComponent as StaryNightSVG } from '../../assets/svgs/stary.animated.svg'

/* Styles */
const CompanyLogoSVGStyled = styled(CompanyLogo)`
  height: 30px;
  fill:   white;
`

const floatingAnimationText = keyframes`
  0% {
    transform: translate(0, 0px)
    rotate(0deg);

    translateX(3px);
  }
  50% {
    transform: translate(0, 10px)
    rotate(2deg);
    translateX(-3px);
  }
  100% {
    transform: translate(0, 0px)
    rotate(0deg);
    translateX(3px);
  }
`

const floatingAnimationSVG = keyframes`
  0% {
    transform: translate(0, 0px)
    rotate(0deg);

    translateX(-3px);
  }
  50% {
    transform: translate(0, -10px)
    rotate(2deg);
    translateX(3px);
  }
  100% {
    transform: translate(0, 0px)
    rotate(0deg);
    translateX(-3px);
  }
`

const CactusSVGStyled = styled(CactusSVG)`
  height: 180px;
  fill:   ${props => props.fill};
  
  animation-name: ${floatingAnimationSVG};
  animation-duration: 4.5s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
`

const ErrorStyled = styled.div`
  font-size:   160px;
  color:      ${props => props.color};
  font-weight: 700;

  animation-name: ${floatingAnimationText};
  animation-duration: 4s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
`

const StaryNightSVGStyled = styled(StaryNightSVG)`
  position: absolute;
  opacity:  0.5;

  flex-shrink: 0;
  min-width:  100%;
  min-height: 100%
`

const Container = styled.div`
  width:           100vw;
  height:          100vh;
  display:         flex;
  flex-direction:  column;
  justify-content: center;
  align-items:     center;
`

const ApplicationLogoContainer = styled.div`
  position:        absolute;
  display:         flex;
  justify-content: center;
  align-items:     center;
  left:            20px;
  top:             20px;
  height:          30px;
`

const CompanyLogoRepStyled = styled(CompanyLogoRep)`
  fill:         white;
  height:       95%;
  margin-right: 12px;
`

const CompanyLogoTextStyled = styled(CompanyLogoText)`
  fill:    white;
  height:  75%;
  opacity: 0.3;
`

export default
function ErrorIndex (props) {
  const theme = useContext(ThemeContext)

  return (
    <Container>

      {/* Subject to change */}
      <StaryNightSVGStyled/>

      <ApplicationLogoContainer>
        <CompanyLogoRepStyled/>
        <CompanyLogoTextStyled/>
      </ApplicationLogoContainer>

      <div>

        <div
          style={{
            display: 'flex'
          }}
        >
          <CactusSVGStyled
            fill={theme.core.fontColor}
          />

          <ErrorStyled
            color={theme.core.fontColor}
          >
                        404
          </ErrorStyled>
        </div>

        <div
          style={{
            fontSize: 26,
            color: theme.core.fontColor,
            maxWidth: 550,
            textAlign: 'center'
          }}
        >
                    We could not find what you were looking for!</div>
        <div
          style={{
            fontSize: 22,
            color: theme.core.fontColor,
            maxWidth: 550,
            textAlign: 'center'
          }}
        >
                    Return to safety by clicking the button below!
        </div>
      </div>

      <Button
        height ={'70px'}
        contents ={<CompanyLogoSVGStyled/>}
        width ={'100px'}
        margin ={'30px'}
        backgroundColor ={theme.navbar.backgroundColor}
        effect ={'rainbow-glow'}
        onClick ={() => {}}
        borderRadius ={'16px'}
        linkTo={'/account-portal'}
        tooltipID={'return-to-account-portal'}
        tooltipContents={
          <div>
                        Return to account portal!
          </div>
        }
      />
    </Container>
  )
}
