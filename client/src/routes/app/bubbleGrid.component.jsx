/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Bubble_Grid
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

/* React & Style Imports */
import React,
{ useState, useEffect } from 'react'
import styled from 'styled-components'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'

import { Bubbles } from '../../misc/demo_data/demo_bubbles.js'

/* Redux Imports */
import { useDispatch } from 'react-redux'
import allActions from '../../redux/actions/index.js'

import { CircularProgress } from '@mui/material'

/* State Handler */
import GetWindowDimensions from '../../scripts/GetWindowDimensions.jsx'

/* Styled Containers */
const Bubble = styled.div`
  display: flex;
  height: 120px;
  width:  120px;
  background-color: #171b1e;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border-style: solid;
  border-width: 2px;
  border-radius: 100%;
  box-shadow: inset 0 0 0 1.5px ${props => props.borderColor};
  
  img {
    min-height: 100%;
    min-width: 100%;
  }

  &:hover {
    cursor: pointer;
  }
`

export default
function BubblesDisplay (props) {
  /* Redux */
  const [bubbles, setBubbles] = useState([])
  const params = useParams()

  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  })
  let rowTracker = -1

  const numOfCols = Math.floor(Math.floor(dimensions.width) / 120) // Determining the # of possible cols.
  const nocAdjusted = numOfCols - (numOfCols % 2) // Otherwise, Odd #s Break the Aesthetic

  GetWindowDimensions(setDimensions)

  /* Retrieve data from API */
  useEffect(() => {
    // console.log(Bubbles)
  }, [])

  const handleBubbleRender = () => {
    if (!Bubbles) {
      return (
        <>
          <CircularProgress sx={ { svg: { circle: { stroke: 'white' } } } }/>
          <h4 style={{ color: 'white' }}>LOADING</h4>
        </>
      )
    } else {
      return (
        Bubbles.map((obj, i) => {
          if (i % nocAdjusted === 0) { rowTracker += 2 }

          return (
            <div
              key={i}
              style={{
                gridColumn: (i % nocAdjusted) + 1,
                gridRow: rowTracker + (i % 2)
              }}
            >
              {/* TODO: Implement passing state down via link to individual bubble */}
              <Link
                to={{
                  pathname: `/app/bubble/${obj.bubbleID}`,
                  state: { bubbleData: obj }
                }}
              >
                <Bubble style={{
                  borderColor: obj.bubbleMeta.color
                }}>
                  <img src={obj.bubbleAssets.image} alt={'hi'}/>
                </Bubble>
              </Link>
            </div>
          )
        })
      )
    }
  }

  return handleBubbleRender()
}
