/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Mount
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect } from 'react'
import { Outlet } from 'react-router-dom'
import styled, { ThemeProvider } from 'styled-components'

import { DARK_THEME, LIGHT_THEME } from '../constants/themes.constants.js'
import Notifications from '../components/core-components/notifications.component.jsx'
import PropTypes from 'prop-types'

const MountContainer = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: ${props => props.siteColor};
`

export default
function Mount ({
  darkMode
}) {
  /* Perform Initial Website Checks (If Necessary) */
  // useEffect(() => {}, [])

  const DETERMINED_THEME = darkMode ? DARK_THEME : LIGHT_THEME
  const siteColor = DETERMINED_THEME.site.backgroundColor

  return (
    <ThemeProvider theme={DETERMINED_THEME}>
      <Notifications
        backgroundColor ={DETERMINED_THEME.notification.backgroundColor}
        closeBackgroundColor={DETERMINED_THEME.button.enabled.back}
        closeBorderColor ={DETERMINED_THEME.button.enabled.bordercolor}
        fill ={DETERMINED_THEME.button.enabled.fill}
      />
      <MountContainer siteColor={siteColor}>
        <Outlet/>
      </MountContainer>
    </ThemeProvider>
  )
}

Mount.displayName = 'Mount'
Mount.propTypes = {
  darkMode: PropTypes.bool
}
Mount.defaultProps = {
  darkMode: true
}
