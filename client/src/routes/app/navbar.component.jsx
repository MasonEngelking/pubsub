/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Navbar_Nav
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useContext,
  useEffect,
  useRef,
  useState
} from 'react'
import styled,
{ ThemeContext } from 'styled-components'
import { Outlet, Link, useNavigate } from 'react-router-dom'
import { Menu, Tooltip, MenuItem, Paper } from '@mui/material'

/* Core-Components */
import NavBar from '../../components/core-components/navbar.component.jsx'
import Button from '../../components/core-components/button.component.jsx'
import Form from '../../components/core-components/form.component.jsx'
import ColorPicker from '../../components/core-components/colorpicker.component.jsx'
import ImageSelector from '../../components/core-components/imageselector.component.jsx'
import Field from '../../components/core-components/field.component.jsx'
import BubbleVote from '../../components/core-components/bubble_vote.component.jsx'
import DeviceSelectionList from '../../components/core-components/device_selection_list.component.jsx'

/* hub-Navbar */
import { ReactComponent as CompanyLogo } from '../../assets/svgs/logo_rep_solo.svg'
import { ReactComponent as LikeDislikeSVG } from '../../assets/svgs/likedislike.svg'
import { ReactComponent as MiscSVG, ReactComponent as DotsSVG } from '../../assets/svgs/dots.svg'
/* Call NavBar */
import { ReactComponent as HeadphonesSVG } from '../../assets/svgs/headphones.svg'
import { ReactComponent as MicSVG } from '../../assets/svgs/mic.svg'
import { ReactComponent as VideoSVG } from '../../assets/svgs/video.svg'
import { ReactComponent as ExitSVG } from '../../assets/svgs/exit.svg'
import { ReactComponent as ChangeViewSVG } from '../../assets/svgs/change_view.svg'
/* Misc Panel */
import { ReactComponent as ReportSVG } from '../../assets/svgs/form.svg'
import { ReactComponent as LegalSVG } from '../../assets/svgs/legal.svg'
import { ReactComponent as AboutSVG } from '../../assets/svgs/about.svg'
import { ReactComponent as SafetySVG } from '../../assets/svgs/safety.svg'
import { ReactComponent as AcknowledgementsSVG } from '../../assets/svgs/acknowledgements.svg'
import { ReactComponent as DonateSVG } from '../../assets/svgs/donate.svg'
import { ReactComponent as TeamSVG } from '../../assets/svgs/team.svg'

/* Redux Imports */
import { useDispatch, useSelector } from 'react-redux'
import allActions from '../../redux/actions/index.js'

import {
  usePopupState,
  bindTrigger,
  bindMenu
} from 'material-ui-popup-state/hooks'
import { DARK_THEME } from '../../constants/themes.constants.js'

const CompanyLogoSVGStyled = styled(CompanyLogo)`
  height: 20px;
  fill:   white;
`
const LikeDislikeSVGStyled = styled(LikeDislikeSVG)`
  height: 14px;
  fill:   white;
`
const MiscSVGStyled = styled(MiscSVG)`
  width: 28px;
  fill:   white;
`

/* Constants */
const Hublets = [
  /* Video Games | hub-let */
  {
    name: 'Video Games',
    hubs: [
      {
        tag: '/coop/',
        name: 'Co-op'
      },
      {
        tag: '/mult/',
        name: 'Multiplayer'
      },
      {
        tag: '/pc/',
        name: 'PC'
      },
      {
        tag: '/vr/',
        name: 'Virtual Reality'
      }
    ]
  },

  /* Interests | hub-let */
  {
    name: 'Interests',
    hubs: [
      {
        tag: '/tech/',
        name: 'Technology'
      },
      {
        tag: '/tech/',
        name: 'Technology'
      },
      {
        tag: '/cc/',
        name: 'Comics & Cartoons'
      },
      {
        tag: '/tf/',
        name: 'Television & Film'
      },
      {
        tag: '/wep/',
        name: 'Weapons'
      },
      {
        tag: '/auto/',
        name: 'Auto'
      },
      {
        tag: '/an/',
        name: 'Animals & Nature'
      },
      {
        tag: '/bg/',
        name: 'Board Games'
      },
      {
        tag: '/fit/',
        name: 'Fitness'
      },
      {
        tag: '/spts/',
        name: 'Sports'
      },
      {
        tag: '/xspts/',
        name: 'Extreme Sports'
      },
      {
        tag: '/toys/',
        name: 'Toys'
      },
      {
        tag: '/yoga/',
        name: 'Yoga'
      }
    ]
  },

  /* Academics | hub-let */
  {
    name: 'Academics',
    hubs: [
      {
        tag: '/bio/',
        name: 'Biology'
      },
      {
        tag: '/sci/',
        name: 'Science'
      },
      {
        tag: '/math/',
        name: 'Mathematics'
      },
      {
        tag: '/hist/',
        name: 'History'
      },
      {
        tag: '/humn/',
        name: 'Humanities'
      },
      {
        tag: '/chem/',
        name: 'Chemistry'
      },
      {
        tag: '/engnrg/',
        name: 'Engineering'
      },
      {
        tag: '/phil/',
        name: 'Philosophy'
      },
      {
        tag: '/cybe/',
        name: 'Cyber'
      },
      {
        tag: '/med/',
        name: 'Medical'
      }
    ]
  },

  /* Creative | hub-let */
  {
    name: 'Creative',
    hubs: [
      {
        tag: '/pd/',
        name: 'Papercraft & Oragami'
      },
      {
        tag: '/pics/',
        name: 'Photography'
      },
      {
        tag: '/food/',
        name: 'Food & Cooking'
      },
      {
        tag: '/ac/',
        name: 'Artwork & Critique'
      },
      {
        tag: '/diy/',
        name: 'Do-It-Yourself'
      },
      {
        tag: '/q/',
        name: 'Quests'
      },
      {
        tag: '/lit/',
        name: 'Literature'
      },
      {
        tag: '/song/',
        name: 'Music'
      },
      {
        tag: '/fash/',
        name: 'Fashion'
      },
      {
        tag: '/gdes/',
        name: 'Graphic Design'
      }
    ]
  },

  /* Life | hub-let */
  {
    name: 'Life',
    hubs: [
      {
        tag: '/pub/',
        name: 'PubSubPub'
      },
      {
        tag: '/para/',
        name: 'Paranormal'
      },
      {
        tag: '/advi/',
        name: 'Advice'
      },
      {
        tag: '/lgbt/',
        name: 'LGBTQ+'
      },
      {
        tag: '/cn/',
        name: 'Current News'
      },
      {
        tag: '/wr/',
        name: 'Workspace Requests'
      },
      {
        tag: '/jobs/',
        name: 'Jobs'
      }
    ]
  },

  /* 18+ | hub-let */
  {
    name: '18+',
    hubs: [
      {
        tag: '/any/',
        name: 'Anything'
      },
      {
        tag: '/pi/',
        name: 'Politically Incorrect'
      },
      {
        tag: '/int/',
        name: 'International'
      },
      {
        tag: '/sdat/',
        name: 'Speed Dating'
      }
    ]
  }
]

const NavBarContainer = styled.div`
  display: flex;
  height:  50px;
  width:   calc(100% - 20px);
  justify-content: space-between;
  padding: 0 10px 10px 10px;
  section {
    display: flex;
    
    div {
      margin: 0 8px;
    }
  }
`

export default
function NavBarView (props) {
  const dispatch = useDispatch()
  const theme = useContext(ThemeContext)
  const navigate = useNavigate()

  const mediaManager = useSelector(state => state.mediaManager)
  const navbarManager = useSelector(state => state.appManager)

  const RenderProfileMenu = () => {
    return (
      <Form
        backgroundColor={DARK_THEME.navbar.backgroundColor}
        borderColor={DARK_THEME.navbar.borderColor}
        padding={0}
        height={'500px'}
        width={'275px'}
        overflow={'hidden'}
      >
        <div style={{
          height: 110,
          width: '100%',
          backgroundColor: 'white'
        }}/>
        <div style={{
          backgroundColor: 'black',
          width: 80,
          height: 80
        }}/>
      </Form>
    )
  }

  const RenderHubMenu = () => {
    return (
      <Form
        backgroundColor={DARK_THEME.navbar.backgroundColor}
        borderColor={DARK_THEME.navbar.borderColor}
      >
        Hi
      </Form>
    )
  }

  const RenderCreateMenu = () => {
    return (
      <Form
        backgroundColor={DARK_THEME.navbar.backgroundColor}
        borderColor={DARK_THEME.navbar.borderColor}
      >
        Hi
      </Form>
    )
  }

  const RenderTopicsMenu = () => {
    return (
      <Form
        backgroundColor={DARK_THEME.navbar.backgroundColor}
        borderColor={DARK_THEME.navbar.borderColor}
      >
        Hi
      </Form>
    )
  }

  const RenderMiscMenu = () => {
    return (
      <Form
        backgroundColor={DARK_THEME.navbar.backgroundColor}
        borderColor={DARK_THEME.navbar.borderColor}
      >
        Hi
      </Form>
    )
  }

  const HubNavBar = () => {
    const profilePopup = usePopupState({ variant: 'popover', popupId: 'profile' })
    const hubsPopup = usePopupState({ variant: 'popover', popupId: 'profile' })
    const createPopup = usePopupState({ variant: 'popover', popupId: 'profile' })
    const topicsPopup = usePopupState({ variant: 'popover', popupId: 'profile' })
    const menuPopup = usePopupState({ variant: 'popover', popupId: 'profile' })

    return (
      <>
        <Menu
          {...bindMenu(profilePopup)}
          sx={{ top: -53, left: -5 }}
        >
          <RenderProfileMenu/>
        </Menu>
        <Menu
          {...bindMenu(hubsPopup)}
          sx={{ top: -53, left: -90 }}
        >
          <RenderHubMenu/>
        </Menu>
        <Menu
          {...bindMenu(createPopup)}
          sx={{ top: -53, left: -90 }}
        >
          <RenderCreateMenu/>
        </Menu>
        <Menu
          {...bindMenu(topicsPopup)}
          sx={{ top: -53, left: -90 }}
        >
          <RenderTopicsMenu/>
        </Menu>
        <Menu
          {...bindMenu(menuPopup)}
          sx={{ top: -53, left: 5 }}
        >
          <RenderMiscMenu/>
        </Menu>

        <Button
          {...bindTrigger(profilePopup)}
          width={'50px'}
          height={'50px'}
          borderRadius={'100%'}
          margin ={'0'}
          padding={'0'}
          contents={''}
          tooltipContents={'Profile'}
        />
        <section>
          <Button
            {...bindTrigger(hubsPopup)}
            height ={'50px'}
            width ={'60px'}
            margin ={'0'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            tooltipContents={'Hubs'}
            borderRadius={'10px'}
            contents={'/pol/'}
          />
          <Button
            {...bindTrigger(createPopup)}
            height ={'50px'}
            contents ={<CompanyLogoSVGStyled/>}
            width ={'60px'}
            margin ={'0'}
            backgroundColor ={theme.navbar.backgroundColor}
            effect ={'rainbow-glow'}
            tooltipContents={'Pub!'}
            borderRadius={'10px'}
          />
          <Button
            {...bindTrigger(topicsPopup)}
            height ={'50px'}
            contents ={<LikeDislikeSVGStyled/>}
            width ={'60px'}
            margin ={'0'}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            tooltipContents={'Topics'}
            borderRadius={'10px'}
          />
        </section>
        <Button
          {...bindTrigger(menuPopup)}
          height ={'50px'}
          width ={'50px'}
          contents ={<MiscSVGStyled/>}
          margin ={'0'}
          backgroundColor ={theme.navbar.backgroundColor}
          borderStyle ={'none'}
          tooltipContents={'More'}
          borderRadius={'100%'}
        />
      </>
    )
  }

  const CallNavBar = () => {
    return (
      <>
        {/* Image Viewer */}
        <Button
          height ={'46px'}
          width ={'46px'}
          margin ={'0 9px 0 8px'}
          borderRadius ={'100%'}
          padding ={'0'}
          contents ={
            <div>
              {/* Implement Image Logic */}
            </div>
          }
          menuContents={
            <Form
              backgroundColor={theme.navbar.backgroundColor}
              minWidth ={240}
              padding ={8}
            >

              <ColorPicker
                height={'90px'}
                enabled={false}
              />

              <ImageSelector
                height ={'103px'}
                width ={'103px'}
                margin ={'-70px auto 0px auto'}

                containerColor={theme.imageSelector.containerColor}
                areaColor ={theme.imageSelector.areaColor}
                borderColor ={theme.imageSelector.borderColor}
                iconColor ={theme.imageSelector.iconColor}
              />

              <div
                style={{
                  fontSize: 24,
                  textAlign: 'center',
                  marginBottom: 10,
                  fontWeight: 700
                }}
              >
                NotEnoughKarma
              </div>

              <div
                style={{
                  fontSize: 16,
                  textAlign: 'left',
                  color: theme.core.fontColor,
                  backgroundColor: '#000000',
                  borderRadius: 5,
                  minHeight: '50px',
                  padding: 5
                }}
              >
                NotEnoughKarma
              </div>
            </Form>
          }
          menuOptions={{
            modifiers: [
              {
                name: 'offset',
                options: { offset: [0, 16] }
              }
            ]
          }}

          tooltipContents={'Profile'}
          tooltipID ={'profile-button'}
        />

        <section>
          <div
            style={{
              height: 28,
              width: 2,
              backgroundColor: 'rgba(255,255,255,0.2)',
              borderRadius: 5,
              margin: '0 10px'
            }}
          />

          {/* Headphones Button */}
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={
              <HeadphonesSVG
                style={{
                  fill: '#fff',
                  height: 24
                }}
              />
            }
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >
                <>
                  <DeviceSelectionList
                    availableDevices={mediaManager.mediaDevices.audioOutputs}
                    selectedDevice ={mediaManager.selectedDevices.audioOutput}
                    onClick={selectedDevice => {
                      const foundDevice = mediaManager
                        .mediaDevices
                        .audioOutputs
                        .find(input => {
                          return input.deviceId === selectedDevice.deviceId
                        })

                      dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioOut(foundDevice))
                    }}
                    icon ={
                      <HeadphonesSVG
                        style={{
                          fill: '#fff',
                          height: 22,
                          marginLeft: 4
                        }}
                      />
                    }
                  />

                  {
                    mediaManager.selectedDevices.audioInput.deviceId === 'null'
                      ? <></>
                      : <Button
                        margin={'8px 0 0 0'}
                        onClick={() => {
                        }}
                        borderRadius={'8px'}
                        text={'Deafen'}
                      />
                  }
                </>
              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Output Devices'}
            tooltipID ={'output-device-button'}
          />

          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<MicSVG
              style={{
                fill: '#fff',
                height: 24
              }}
            />}
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >

                <DeviceSelectionList
                  availableDevices={mediaManager.mediaDevices.audioInputs}
                  selectedDevice ={mediaManager.selectedDevices.audioInput}
                  onClick={selectedDevice => {
                    const foundDevice = mediaManager
                      .mediaDevices
                      .audioInputs
                      .find(input => {
                        return input.deviceId === selectedDevice.deviceId
                      })

                    dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioIn(foundDevice))
                  }}
                  icon ={
                    <MicSVG
                      style={{
                        fill: '#fff',
                        height: 22,
                        marginLeft: 4
                      }}
                    />
                  }
                />

                {
                  mediaManager.selectedDevices.audioInput.deviceId === 'null'
                    ? <></>
                    : <Button
                      margin={'8px 0 0 0'}
                      onClick={() => {
                      }}
                      borderRadius={'8px'}
                      text={'Mute'}
                    />
                }

              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Input Devices'}
            tooltipID ={'input-device-button'}
          />
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<VideoSVG
              style={{
                fill: '#fff',
                width: 35
              }}
            />}
            menuContents={
              <Form
                backgroundColor={theme.navbar.backgroundColor}
                minWidth ={240}
                padding ={8}
                height ={''}
                minHeight ={80}
              >

                <DeviceSelectionList
                  availableDevices={mediaManager.mediaDevices.videoInputs}
                  selectedDevice ={mediaManager.selectedDevices.videoInput}
                  onClick={selectedDevice => {
                    const foundDevice = mediaManager
                      .mediaDevices
                      .videoInputs
                      .find(input => { return input.deviceId === selectedDevice.deviceId })
                    dispatch(allActions.mediaActions.selectedMediaStateUpdateVideoIn(foundDevice))
                  }}
                  icon={
                    <VideoSVG
                      style={{
                        fill: '#fff',
                        height: 28,
                        marginLeft: 4
                      }}
                    />
                  }
                />

                {
                  mediaManager.selectedDevices.videoInput.deviceId === 'null'
                    ? <></>
                    : <Button
                      margin={'8px 0 0 0'}
                      onClick={() => {
                        /* Perform Video Toggle Operation */
                      }}
                      borderRadius={'8px'}
                      text={'Disable Video'}
                    />
                }

              </Form>
            }
            menuOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [0, 16] }
                }
              ]
            }}

            tooltipContents={'Video Devices'}
            tooltipID ={'video-device-button'}
          />
          <Button
            height ={'46px'}
            width ={'60px'}
            margin ={'0 2px'}
            fontSize ={18}
            backgroundColor ={theme.navbar.backgroundColor}
            borderStyle ={'none'}
            contents ={<ExitSVG
              style={{
                fill: '#fff',
                height: 21
              }}
            />}
            onClick ={() => { navigate(-1) }}

            tooltipContents={'Exit Bubble'}
            tooltipID ={'exit-bubble-button'}
          />

          <div
            style={{
              height: 28,
              width: 2,
              backgroundColor: 'rgba(255,255,255,0.2)',
              borderRadius: 5,
              margin: '0 10px'
            }}
          />
        </section>

        <Button
          height ={'46px'}
          width ={'60px'}
          margin ={'0'}
          fontSize ={18}
          backgroundColor ={theme.navbar.backgroundColor}
          borderStyle ={'none'}
          contents ={<ChangeViewSVG
            style={{
              fill: '#fff',
              width: 36
            }}
          />}
          onClick ={() => {
            /* Introduce the widget panel... or popper? */
          }}

          tooltipContents={'Toggle Menu'}
          tooltipID ={'toggle-menu-button'}
        />
      </>
    )
  }

  return (
    <NavBarContainer>
      {navbarManager.navbarType ? <HubNavBar/> : <CallNavBar/>}
    </NavBarContainer>
  )
}
