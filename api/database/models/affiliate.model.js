/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

import mongoose               from 'mongoose'
import mongooseLong           from 'mongoose-long'
import {getDateAsCustomEpoch} from '../global.middleware.js'
import {getSnowflakeID}       from '../middleware.js'

mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const transactionSchema = new mongoose.Schema({

  _id: {type: Long, default: await getSnowflakeID()}, // AUID (Affiliate Unique ID)

  active:     Boolean,
  dateEpoch:  Long,
  name:       String,
  suid:       Long,

  address: [
    {
      active:    Boolean,
      dateEpoch: Long,
      streetOne: {hashed: String},
      streetTwo: {hashed: String},
      city:      {hashed: String},
      state:     {hashed: String},
      zipCode:   {hashed: String},
      country:   {hashed: String}
    }
  ]

})

