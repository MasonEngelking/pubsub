/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Dropdown_Menu
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Foco from 'react-foco'

const DropdownMenuStyled = styled.div`
  
  position:        absolute;

  align-items:     center;
  justify-content: space-between;
  
  opacity:         100% !important;
  
  width:           calc(100%);
  z-index:         55;
  margin-left:     -13px;
  
  top:             -2px;

  background-color: ${props => props.backgroundColor} !important;
  border-color:     ${props => props.borderColor}     !important;
  
  border-width:     ${props => props.borderWidth}px  !important;
  border-style:     ${props => props.borderStyle}    !important;
  border-radius:    ${props => props.borderRadius}px !important;

  &:hover {
    cursor: ${props => props.enabled ? 'pointer' : 'default'};
  }
`

const DropdownItem = styled.div`
  display:       flex;
  align-items:   center;
  color:         white;
  border-radius: 5px;
  padding:       15px 10px;
  margin:        3px;
  font-size:     14px;
  
  &:hover {
    cursor: pointer;
    background-color: rgba(255, 255, 255, 3%);
  }
`

export default
function DropdownMenu ({
  toggled,
  stateMirror,
  onOffClick,
  initItem,
  items,
  backgroundColor,
  borderColor,
  borderStyle,
  borderWidth,
  borderRadius,
  fontSize,
  fontColor,
  width,
  maxWidth,
  minWidth,
  height,
  maxHeight,
  minHeight,
  margin,
  padding
}) {
  const handleMenuRender = () => {
    if (toggled) {
      return (
        <Foco
          onClickOutside={() => { onOffClick() }}
        >
          <DropdownMenuStyled
            backgroundColor={backgroundColor}
            borderColor ={borderColor}
            borderStyle ={borderStyle}
            borderWidth ={borderWidth}
            borderRadius ={borderRadius}
            padding ={padding}
            fontSize ={fontSize}
            fontColor ={fontColor}
          >
            {
              items.map((item, i) => {
                return (
                  <DropdownItem
                    key={i}
                    onClick={() => {
                      stateMirror(item)
                    }}
                  >
                    {item.text}
                  </DropdownItem>
                )
              })
            }
          </DropdownMenuStyled>
        </Foco>
      )
    } else {
      return <></>
    }
  }

  return handleMenuRender()
}

DropdownMenu.displayName = 'Dropdown Menu'
/* Defining Default Values for Props */
DropdownMenu.propTypes = {
  /* Callbacks */
  toggled: PropTypes.bool.isRequired,
  stateMirror: PropTypes.func.isRequired,
  onOffClick: PropTypes.func.isRequired,

  /* Items to render */
  initItem: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)).isRequired,

  /* Styling Defaults */
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,

  /* ~ */
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  /* ~ */
  fontSize: PropTypes.number,
  fontColor: PropTypes.string,
  /* ~ Adjust Container Width ~ */
  width: PropTypes.string,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  /* ~ Adjust Dropdown Height ~ */
  height: PropTypes.string,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  /* ~~ */
  margin: PropTypes.string,
  padding: PropTypes.number
}

DropdownMenu.defaultProps = {
  /* Callbacks */
  toggled: false,
  stateMirror: () => {},
  onOffClick: () => {},

  /* Items to render */
  initItem: 'Empty',
  items: [{
    key: 'Null',
    label: 'Empty'
  }],

  /* Styling Defaults */
  backgroundColor: '#000000',
  borderColor: '#ffffff',

  /* ~ */
  borderStyle: 'solid',
  borderWidth: 1.5,
  borderRadius: 8,
  /* ~ */
  fontSize: 14,
  fontColor: '#fff',
  /* ~ Adjust Container Width ~ */
  width: undefined,
  maxWidth: undefined,
  minWidth: 200,
  /* ~ Adjust Dropdown Height ~ */
  height: undefined,
  maxHeight: undefined,
  minHeight: 100,
  /* ~~ */
  margin: '3px 0 1px 0',
  padding: 15
}
