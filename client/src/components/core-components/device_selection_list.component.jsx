/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Device_Selection_List
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useState } from 'react'
import styled from 'styled-components'
import Slider from './slider.component.jsx'
import PropTypes from 'prop-types'

const AudioDeviceSelection = styled.div`
  display:               grid;
  border-radius:         8px;
  min-height:            55px;
  grid-template-columns: 35px 1fr 25px;
  color:                 white;
  margin:                2px 0;
  
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  
  &:hover{
    background-color: #191919
  }
`

const NoDeviceAvailable = styled.div`
  color:           white;
  opacity:         0.5;
  display:         flex;
  justify-content: center;
  align-items:     center;
  height:          55px;
`

const ToggleButton = styled.div`

  display: flex;

  & > div {
    display:          flex;
    background-color: ${props => props.isSelected ? 'green' : '#212121'};
    width:            100%;
    border-radius:    5px;
    margin:           4px;
    cursor:           pointer;
  }
`

export default
function DeviceSelectionList (props) {
  const { availableDevices, selectedDevice, icon } = props
  const [volumeLevel, setVolumeLevel] = useState(50)

  return (
    <>
      {
        availableDevices.length === 1 && availableDevices[0].deviceId === 'null'
          ? <NoDeviceAvailable>
              No devices available
          </NoDeviceAvailable>
          : availableDevices
            .map(device => {
              const isSelected = selectedDevice.label === device.label

              return (
                <AudioDeviceSelection
                  key={device.deviceId}
                >

                  <div
                    style={{
                      width: 35,
                      height: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    {icon}
                  </div>

                  <div
                    style={{
                      margin: '2px 0',
                      padding: '0 6px',
                      display: 'flex',
                      justifyContent: 'space-evenly',
                      flexDirection: 'column'
                    }}
                  >

                    <div style={{ fontSize: 15 }}>
                      {device.label}
                    </div>

                    <Slider
                      stateMirror ={state => { setVolumeLevel(state) }}
                      defaultValue={0}
                      enabled ={isSelected}
                    />

                    <div
                      style={{
                        opacity: 0.3,
                        fontSize: 12
                      }}
                    >
                      Audio Level: {isSelected ? volumeLevel : '-'}%
                    </div>

                  </div>

                  <ToggleButton
                    onClick={() => {
                      props.onClick(device)
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        backgroundColor: isSelected ? 'green' : '#212121',
                        width: '100%',
                        borderRadius: 5,
                        margin: 4
                      }}
                    />
                  </ToggleButton>

                </AudioDeviceSelection>
              )
            })
      }
    </>
  )
}
DeviceSelectionList.displayName = 'Display Selection List'

DeviceSelectionList.propTypes = {
  onClick: PropTypes.func,
  availableDevices: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedDevice: PropTypes.object.isRequired,
  icon: PropTypes.object.isRequired
}
DeviceSelectionList.defaultProps = {
  onClick: () => {}
}
