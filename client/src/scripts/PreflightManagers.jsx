/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/* REDUX */
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import allActions from '../redux/actions/index.js'

import { io } from 'socket.io-client'
import { NETWORK_CONFIG } from '../config.js'
import prettyConsoleLog from './prettyConsoleLog.js'
import axios from 'axios'

export function MediaStateManager () {
  const dispatch = useDispatch()
  const selectedMediaDevices = useSelector(state => state.mediaManager.selectedDevices)
  const permittedMediaDevices = useSelector(state => state.mediaManager.devicePermissions)

  const PermissionsNeededArray = [{ deviceId: 'PERMS_NEEDED', label: 'Permissions needed...' }]
  const EmptyDeviceArray = [{ deviceId: 'NO_DEVICES', label: 'No devices available...' }]

  const handleDevicesOutput = (devices, deviceType) => {
    const devicesFilteredOut = devices.filter(device => device.kind === deviceType && device.deviceId !== 'default')
    if (devicesFilteredOut.length === 0) { return EmptyDeviceArray } else { return devicesFilteredOut }
  }

  /* Initial Media Check */
  useEffect(() => {
    /* Retrieving Devices:
        * - Filtering out "Default Devices"
        * - Initial Permissions Check
        * - Permission Confirmation
        * - Logic Processing
        * - Setting Redux State
        * */
    navigator.mediaDevices.enumerateDevices()
      .then(async devices => {
        /* Retrieving default devices for each form of input */
        let audioInputDevices = handleDevicesOutput(devices, 'audioinput')
        let audioOutputDevices = handleDevicesOutput(devices, 'audiooutput')
        let videoInputDevices = handleDevicesOutput(devices, 'videoinput')

        /* Retrieving Navs Permission States for Audio devices */
        await navigator.permissions
          .query({ name: 'microphone' })
          .then(result => {
            const audioPermState = result.state
            const audioPermsGranted = audioPermState === 'granted'

            dispatch(allActions.mediaActions.devicePermissionsUpdateAudio(audioPermState))

            if (!audioPermsGranted) {
              audioInputDevices = PermissionsNeededArray
              audioOutputDevices = PermissionsNeededArray
            }
          })
        /* Retrieving Navs Permission States for Video devices */
        await navigator.permissions
          .query({ name: 'camera' })
          .then(result => {
            const videoPermState = result.state
            const videoPermsGranted = videoPermState === 'granted'

            dispatch(allActions.mediaActions.devicePermissionsUpdateVideo(videoPermState))

            if (!videoPermsGranted) videoInputDevices = PermissionsNeededArray
          })

        /* Setting all available devices */
        dispatch(allActions.mediaActions.availableMediaStateUpdate(
          audioInputDevices,
          audioOutputDevices,
          videoInputDevices
        ))

        /* Setting initial devices */
        dispatch(allActions.mediaActions.selectedMediaStateUpdate(
          audioInputDevices[0],
          audioOutputDevices[0],
          videoInputDevices[0]
        ))
      })
  }, [])

  /* Handling Device Changes */
  useEffect(() => {
    navigator.mediaDevices.ondevicechange = () => {
      navigator.mediaDevices.enumerateDevices()
        .then(devices => {
          /* Retrieving default devices for each form of input */
          const audioInputDevices = handleDevicesOutput(devices, 'audioinput')
          const audioOutputDevices = handleDevicesOutput(devices, 'audiooutput')
          const videoInputDevices = handleDevicesOutput(devices, 'videoinput')

          /* Handling Device Update */
          navigator.permissions
            .query({ name: 'microphone' })
            .then(result => {
              const audioPermState = result.state
              const audioPermsGranted = audioPermState === 'granted'

              if (audioPermsGranted) {
                dispatch(allActions.mediaActions.devicePermissionsUpdateAudio(audioPermsGranted))

                /* HANDLE AUDIO-IN MEDIA SET */
                const isSelectedAudioInDeviceAvailable = audioInputDevices.filter(item => item.deviceId === selectedMediaDevices.audioInput.deviceId).length !== 0
                const isSelectedAudioInDeviceDefault = selectedMediaDevices.audioInput.deviceId === 'null'
                if (!isSelectedAudioInDeviceAvailable || isSelectedAudioInDeviceDefault) { dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioIn(audioInputDevices[0])) }

                /* HANDLE AUDIO-OUT MEDIA SET */
                const isSelectedAudioOutDeviceAvailable = audioOutputDevices.filter(item => item.deviceId === selectedMediaDevices.audioOutput.deviceId).length !== 0
                const isSelectedAudioOutDeviceDefault = selectedMediaDevices.audioOutput.deviceId === 'null'
                if (!isSelectedAudioOutDeviceAvailable || isSelectedAudioOutDeviceDefault) { dispatch(allActions.mediaActions.selectedMediaStateUpdateAudioOut(audioOutputDevices[0])) }
              }
            })
          navigator.permissions
            .query({ name: 'camera' })
            .then(result => {
              const videoPermState = result.state
              const videoPermsGranted = videoPermState === 'granted'

              if (videoPermsGranted) {
                dispatch(allActions.mediaActions.devicePermissionsUpdateVideo(videoPermsGranted))

                /* HANDLE VIDEO-IN MEDIA SET */
                const isSelectedVideoInDeviceAvailable = videoInputDevices.filter(item => item.deviceId === selectedMediaDevices.videoInput.deviceId).length !== 0
                const isSelectedVideoInDeviceDefault = selectedMediaDevices.videoInput.deviceId === 'null'
                if (!isSelectedVideoInDeviceAvailable || isSelectedVideoInDeviceDefault) { dispatch(allActions.mediaActions.selectedMediaStateUpdateVideoIn(videoInputDevices[0])) }
              }
            })

          /* TODO: Update this to parsed into multiple function calls then place in the above permissions checks to prevent strange bugs */
          dispatch(allActions.mediaActions.availableMediaStateUpdate(
            audioInputDevices,
            audioOutputDevices,
            videoInputDevices
          ))
        })
    }
  }, [selectedMediaDevices])

  /* Either Audio or Video must be permitted... */
  return [
    permittedMediaDevices.audio === 'granted' ||
    permittedMediaDevices.video === 'granted'
  ]
}

export function CookieStateManager () {
  // Cookie Parser
  function getCookie (cname) {
    const name = cname + '='
    const ca = document.cookie.split(';')
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i]
      while (c.charAt(0) === ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // Retrieving Required Cookies
  const AJWT_COOKIE = getCookie('ajwt')
  const SJWT_COOKIE = getCookie('sjwt')

  // console.log({ AJWT_COOKIE, SJWT_COOKIE })

  return [
    AJWT_COOKIE.length !== 0 &&
    SJWT_COOKIE.length !== 0
  ]
}

export function NetworkConnectionManager () {
  const dispatch = useDispatch()
  const [socketCheck, setSocketCheck] = useState(false)
  const [APICheck, setAPICheck] = useState(false)
  const [voiceCheck, setVoiceCheck] = useState(false)

  const handleCoreSocketConnection = () => {
    const MAX_ATTEMPTS = 3
    let attempts = 0
    let seconds = 2
    try {
      /* Handle Reconnection Upon Close */
      const tryReconnect = () => {
        /* Setting a decaying delay for reconnect */
        const reconnectTimeout = setTimeout(() => {
          prettyConsoleLog('info', 'Starting socket connection attempt...')
          /* Attempt to reconnect & Set state accordingly */
          socket.io.open(err => {
            if (!err) {
              dispatch(allActions.appActions.setSocketAttemptSeconds(2))
              prettyConsoleLog('success', 'Reconnected to PubSub API!')
            }
            clearTimeout(reconnectTimeout)
          })
        }, seconds * 1000)
      }

      /* Attempt to establish websocket connection to Gateway */
      const socket = io(`https://${NETWORK_CONFIG.CORE.IP}:${NETWORK_CONFIG.CORE.PORTS[0]}`, {
        reconnection: false,
        transports: ['websocket']
      })

      /* Update Render State Upon Healthy Socket Connection */
      socket.on('connect', () => {
        prettyConsoleLog('success', 'Established connection to PubSub Core!')
        setSocketCheck(true)
      })

      /* Re-Attempt Connection Upon Client-Side Connection Error */
      socket.on('connect_error', () => {
        setSocketCheck(false)
        seconds *= 2
        dispatch(allActions.appActions.setSocketAttemptSeconds(seconds))
        prettyConsoleLog('error', 'Failed to establish socket to PubSub API.')
        if (attempts < MAX_ATTEMPTS) {
          prettyConsoleLog('info', `Retrying in ${seconds} seconds...`)
          prettyConsoleLog('info', `Attempts remaining: ${MAX_ATTEMPTS - attempts}`)
          tryReconnect()
        }
        if (attempts === MAX_ATTEMPTS) { prettyConsoleLog('warn', 'Returning back to home.') }
        attempts++
      })

      /* Received Disconnect from Server, attempt to reconnect depending on reason */
      /* TODO: This needs to determine intent of disconnect (error or on purpose) */
      socket.on('disconnect', () => {
        setSocketCheck(false)
        seconds = 2
        dispatch(allActions.appActions.setSocketAttemptSeconds(seconds))
        if (attempts < MAX_ATTEMPTS) {
          prettyConsoleLog('error', 'Socket connection dropped with PubSub API!')
          tryReconnect()
          attempts = 0
        }
        if (attempts === MAX_ATTEMPTS) {
          prettyConsoleLog('warn', 'Returning back to home.')
        }
      })
    } catch (e) {
      console.log('Had trouble connecting to the ISM!')
      console.log(e)
    }
  }

  const handleVoiceAvailabilityCheck = () => {
    const MAX_ATTEMPTS = 3
    let attempts = 0
    let seconds = 2
    try {
      // eslint-disable-next-line no-unmodified-loop-condition
      while (attempts < MAX_ATTEMPTS && APICheck) {
        const reconnectTimeout = setTimeout(() => {
          prettyConsoleLog('info', 'Attempting to healthcheck PubSub API')

          axios.post(process.env.REACT_APP_API_ENDPOINT + '/api/v1/voice/healthcheck')
            .then(res => {
              if (res.status === 200) {
                setVoiceCheck(true)
              }
            })
            .catch(err => prettyConsoleLog('error', 'Axios responded with: ' + err))
          attempts++
          seconds *= 2
          clearTimeout(reconnectTimeout)
        }, seconds * 1000)
      }
    } catch (e) {
      prettyConsoleLog('error', 'Axios had an error when attempting to healthcheck the API.')
    }
  }

  const handleAPIAvailabilityCheck = () => {
    const MAX_ATTEMPTS = 3
    let attempts = 0
    let seconds = 2
    try {
      // eslint-disable-next-line no-unmodified-loop-condition
      while (attempts < MAX_ATTEMPTS && APICheck) {
        const reconnectTimeout = setTimeout(() => {
          prettyConsoleLog('info', 'Attempting to healthcheck PubSub API')

          axios.post(process.env.REACT_APP_API_ENDPOINT + '/api/v1/core/healthcheck/all')
            .then(res => {
              if (res.status === 200) {
                setAPICheck(true)
              }
            })
            .catch(err => prettyConsoleLog('error', 'Axios responded with: ' + err))
          attempts++
          seconds *= 2
          clearTimeout(reconnectTimeout)
        }, seconds * 1000)
      }
    } catch (e) {
      prettyConsoleLog('error', 'Axios had an error when attempting to healthcheck the API.')
    }
  }

  useEffect(() => {
    handleCoreSocketConnection()
    handleVoiceAvailabilityCheck()
    handleAPIAvailabilityCheck()
  }, [])

  return [socketCheck && APICheck && voiceCheck]
}
