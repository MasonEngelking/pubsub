/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Legal
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { Tooltip } from '@mui/material'
import { InsertLink } from '@mui/icons-material'
import { Link, useLocation } from 'react-router-dom'
import { MISC_COLORS } from '../../constants/themes.constants.js'

const LegalContainer = styled.div`
  -webkit-user-select: none;
  user-select:         none;
  color:               white;
  a { color: ${MISC_COLORS.link.default} }
  margin-top: 70px;
`

const ContentContainer = styled.div`
  overflow-x:      hidden;
  position:        relative;
  display:         grid;
  justify-content: center;
  
  ::-webkit-scrollbar { width: 10px; }

  /* Track */
  ::-webkit-scrollbar-track { background: #EDDFDA; }

  /* Handle */
  ::-webkit-scrollbar-thumb { background: #252525; }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover { background: #171717; }
`

const ContentWrapper = styled.div`
  width:     100vw;
  max-width: 1200px;
`

const TermsAndConditions = styled.div`
  font-size:   38px;
  padding:     20px;
  text-align:  justify;
  line-height: 32px;
  
  & > div {
    & > div:nth-child(1) {
      display:               grid;
      gap:                   10px;
      grid-template-columns: auto auto 1fr;
      align-items:           center;
      justify-content:       center;
      margin-bottom:         10px;
      font-size:             28px;
      
      & > div:nth-child(2) {
        display: flex;
        justify-content: center;
        
        &:hover {
          cursor: pointer;
        }
      }
    }
    
    & > div:nth-child(2) {
      font-size: 18px;
      opacity: 0.7;
    }
  }
`

const CopyLocationButton = styled.div`
  height: 35px;
  width:  35px;
  border-radius: 100%;
  border-style:  solid;
  background-color: rgba(111, 49, 200, 0.3);
  border-color:     rgba(92, 2, 228, 0.3);
  display:       flex;
  place-items:   center;
  align-content: center;
  
  &:hover {
    background-color: ${MISC_COLORS.app.primary};
    border-color:     ${MISC_COLORS.app.second};
  }
  
  &:active { filter: brightness(0.9); }
`

export default
function Legal (props) {
  const location = useLocation()

  const [tooltipText, setTooltipText] = useState('Copy Link')

  const overviewRef = useRef(null)
  const yourAccountRef = useRef(null)
  const acceptableRef = useRef(null)
  const refundsRef = useRef(null)
  const smsRef = useRef(null)
  const disputesRef = useRef(null)
  const indemnificationRef = useRef(null)
  const intellectualRef = useRef(null)
  const disclaimerRef = useRef(null)
  const waiverRef = useRef(null)
  const actsOfGodRef = useRef(null)
  const providingRef = useRef(null)
  const terminationRef = useRef(null)
  const automaticPaymentsRef = useRef(null)
  const privacyRef = useRef(null)
  const cookieRef = useRef(null)

  const pathnames = [
    'overview',
    'your-account',
    'acceptable-use',
    'refunds',
    'sms-texting-messaging',
    'handling-disputes',
    'indemnification',
    'intellectual-property-rights',
    'disclaimers-warranties-limitations-liability',
    'waiver-severability-assignment-of-rights',
    'acts-of-god',
    'providing-of-services',
    'termination-of-services',
    'automatic-payments',
    'privacy-policy',
    'cookie-policy'
  ]

  const selectionMap = {
    [pathnames[0]]: overviewRef,
    [pathnames[1]]: yourAccountRef,
    [pathnames[2]]: acceptableRef,
    [pathnames[3]]: refundsRef,
    [pathnames[4]]: smsRef,
    [pathnames[5]]: disputesRef,
    [pathnames[6]]: indemnificationRef,
    [pathnames[7]]: intellectualRef,
    [pathnames[8]]: disclaimerRef,
    [pathnames[9]]: waiverRef,
    [pathnames[10]]: actsOfGodRef,
    [pathnames[11]]: providingRef,
    [pathnames[12]]: terminationRef,
    [pathnames[13]]: automaticPaymentsRef,
    [pathnames[14]]: privacyRef,
    [pathnames[15]]: cookieRef
  }

  useEffect(() => {
    const selection = location.hash.split('#')[1]
    selectionMap[selection]?.current?.scrollIntoView({ behavior: 'smooth' })
  }, [])

  const RenderTermsOfService = () => {
    const Sections = [
      {
        title: 'Overview',
        pathname: pathnames[0],
        ref: selectionMap[pathnames[0]],
        jsx: (
          <div>
            <div>
              These terms of service (“Terms”) cover your use and access to the services, products, software and websites
              (“Services”) that are provided by PubSub LLC and any of our affiliates (“PubSub”, “company”, “we”, “us” or
              “our”). By using our Services and website, you agree to be bound by these Terms as well as our Privacy Policy.
              If you do not agree with these Terms, or the terms contained in our Privacy Policy, then you should not use
              our Services or this website.
            </div>
            <br/>
            <div>
              Additionally, these Terms supersede and replace any other prior or existing agreements, or terms and
              conditions that may be applicable. These Terms create no third party beneficiary rights.
            </div>
          </div>
        )
      },

      {
        title: 'Your Account',
        pathname: pathnames[1],
        ref: selectionMap[pathnames[1]],
        jsx: (
          <div>
            <div>
              When setting up and maintaining your account, you must provide and continue to provide accurate and complete
              information, including a valid email address. You have complete responsibility for your account and
              everything that happens on your account, including for any harm or damage (to us or anyone else) caused
              by someone using your account without your permission. This means you need to be careful with your
              password. You may not transfer your account to someone else or use someone else’s account. If you contact
              us to request access to an account, we will not grant you such access unless you can provide us with the
              information that we need to prove you are the owner of that account. In the event of the death of a user,
              the account of that user will be closed.
            </div>
            <br/>
            <div>
              You may not share your account login credentials with anyone else. You are responsible for what happens
              with your account and Company will not intervene in disputes between users who have shared account login
              credentials. You must notify us immediately upon learning that someone else may be using your account
              without your permission (or if you suspect any other breach of security) by contacting us. We may request
              some information from you to confirm that you are indeed the owner of your account.
            </div>
          </div>
        )
      },

      {
        title: 'Acceptable Use',
        pathname: pathnames[2],
        ref: selectionMap[pathnames[2]],
        jsx: (
          <div>
            <div>
              You may not access or use the Website for any purpose other than that for which we make the Website
              available. The Website may not be used in connection with any commercial endeavors except those that are
              specifically endorsed or approved by us.
            </div>
            <div>
              As a user of the Website, you agree not to:
            </div>
            <ol>
              <li>
                Systematically retrieve data or other content from the Website to create or compile, directly or
                indirectly, a collection, compilation, database, or directory without written permission from us.
              </li>
              <li>
                Trick, defraud, or mislead us and other users, especially in any attempt to learn sensitive account
                information such as user passwords.
              </li>
              <li>
                Circumvent, disable, or otherwise interfere with security-related features of the Website, including
                features that prevent or restrict the use or copying of any Content or enforce limitations on the use
                of the Website and/or the Content contained therein.
              </li>
              <li>
                Disparage, tarnish, or otherwise harm, in our opinion, us and/or the Website.
              </li>
              <li>
                Use any information obtained from the Website in order to harass, abuse, or harm another person.
              </li>
              <li>
                Make improper use of our support services or submit false reports of abuse or misconduct.
              </li>
              <li>
                Use the Website in a manner inconsistent with any applicable laws or regulations.
              </li>
              <li>
                Engage in unauthorized framing of or linking to the Website.
              </li>
              <li>
                Upload or transmit (or attempt to upload or to transmit) viruses, Trojan horses, or other material,
                including excessive use of capital letters and spamming (continuous posting of repetitive text), that
                interferes with any party’s uninterrupted use and enjoyment of the Website or modifies, impairs,
                disrupts, alters, or interferes with the use, features, functions, operation, or maintenance of the
                Website.
              </li>
              <li>
                Engage in any automated use of the system, such as using scripts to send comments or messages, or using
                any data mining, robots, or similar data gathering and extraction tools.
              </li>
              <li>
                Delete the copyright or other proprietary rights notice from any Content.
              </li>
              <li>
                Attempt to impersonate another user or person or use the username of another user.
              </li>
              <li>
                Upload or transmit (or attempt to upload or to transmit) any material that acts as a passive or active
                information collection or transmission mechanism, including without limitation, clear graphics
                interchange formats (“gifs”), 1×1 pixels, web bugs, cookies, or other similar devices (sometimes
                referred to as “spyware” or “passive collection mechanisms” or “pcms”).
              </li>
              <li>
                Interfere with, disrupt, or create an undue burden on the Website or the networks or services connected
                to the Website.
              </li>
              <li>
                Harass, annoy, intimidate, or threaten any of our employees or agents engaged in providing any portion
                of the Website to you.
              </li>
              <li>
                Attempt to bypass any measures of the Website designed to prevent or restrict access to the Website, or
                any portion of the Website.
              </li>
              <li>
                Copy or adapt the Website’s software.
              </li>
              <li>
                Except as permitted by applicable law, decipher, decompile, disassemble, or reverse engineer any of the
                software comprising or in any way making up a part of the Website.
              </li>
              <li>
                Except as may be the result of standard search engine or Internet browser usage, use, launch, develop,
                or distribute any automated system, including without limitation, any spider, robot, cheat utility,
                scraper, or offline reader that accesses the Website, or using or launching any unauthorized script or
                other software.
              </li>
              <li>
                Use a buying agent or purchasing agent to make purchases on the Website.
              </li>
              <li>
                Make any unauthorized use of the Website, including collecting usernames and/or email addresses of users
                by electronic or other means for the purpose of sending unsolicited email, or creating user accounts by
                automated means or under false pretenses.
              </li>
              <li>
                Use the Website as part of any effort to compete with us or otherwise use the Website and/or the Content
                for any revenue-generating endeavor or commercial enterprise.
              </li>
            </ol>
            <div>
              Furthermore, if we suspect your account has been used for an unauthorized, illegal or criminal purpose, you are
              granting us express authorization to share information about you and your account with law enforcement.
            </div>
          </div>
        )
      },

      {
        title: 'Refunds',
        pathname: pathnames[3],
        ref: selectionMap[pathnames[3]],
        jsx: (
          <div>
            <div>
              Unless otherwise stated herein, all purchases are final and non-refundable after 30 days. No refunds will be
              given for any charges or credits more than 30 days old, unless otherwise agreed to in writing between you and
              us and/or is specific to the type of service we are providing or is required by law. We reserve the right to
              issue and/or prorate refunds or credits at our sole discretion unless otherwise required by law. If we issue
              a refund or credit, we are under no obligation to issue the same or similar refund in the future. This refund
              policy does not affect any statutory rights that may apply. All state filing fees are non-refundable.
            </div>
          </div>
        )
      },

      {
        title: 'SMS, Texting, Messaging',
        pathname: pathnames[4],
        ref: selectionMap[pathnames[4]],
        jsx: (
          <div>
            <h3>Overview</h3>
            <div>
              By providing your mobile phone number and checking that you wish to receive communications, you are consenting
              to receive Short Message Service (SMS)/text messages from us. The SMS/Text messages you may receive
              service-related and promotional messages, including: updates, alerts, and information (e.g., order updates,
              account alerts, etc.)  and promotions, specials, and other marketing offers (e.g., cart reminders) through
              your wireless provider to the mobile number you provided, even if your mobile number is registered on any
              state or federal Do Not Call list. SMS/Text messages may be sent using an automatic telephone dialing system
              or other technology.
            </div>

            <br/>
            <h3>Message Frequency</h3>
            <div>
              Message frequency varies but you will not receive more than one (1) message(s) per day. Standard message and
              data rates may apply from your wireless provider and you are responsible for all charges and fees associated
              with text messaging imposed by your wireless provider.
            </div>

            <br/>
            <h3>Voluntary Usage</h3>
            <div>
              Your participation in this program is completely voluntary and you can Opt-Out at any time by submitting an
              inquiry on our website or emailing our support team.
            </div>

            <br/>
            <h3>Liability Clause</h3>
            <div>
              To the extent permitted by applicable law, you agree that we will not be liable for failed, delayed, or
              misdirected delivery of any information sent via SMS/text message, or any errors in such information, and/or
              any action you may or may not take in reliance on the information received via SMS/text message.
            </div>
          </div>
        )
      },

      {
        title: 'Handling Disputes',
        pathname: pathnames[5],
        ref: selectionMap[pathnames[5]],
        jsx: (
          <div>
            <h3>Time Limitation to Start Dispute</h3>
            <div>
              You agree that any action or proceeding by you relating to any dispute must commence within one year after the
              alleged cause of action accrues.
            </div>

            <br/>
            <h3>Dispute Resolution by Binding Arbitration and Class Action Waiver.</h3>
            <div>
              Our team places a tremendous amount of effort into providing our clients with the best of customer
              services. Unfortunately, at times we may not perform to our own standards. If you have had a negative
              experience and would like to let us know; we will try to resolve the issue to your liking quickly. Call us
              at <a href="tel:3022981324">+1 (302) 298-1324</a>. You can also email us
              at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
              As mentioned in our about us page, we value our clients immeasurably and will do everything in our power to
              ensure that you have had a positive experience with our team.
            </div>

            <br/>
            <h3>Unable to Resolve</h3>
            <div>
              All that said, in the unlikely event that we are unable to resolve your complaint to your satisfaction, we
              each agree to resolve those disputes through binding arbitration under the auspices of JAMS Alternative
              Dispute Resolution (“JAMS”). JAMS will administer any such arbitration under its Comprehensive Arbitration
              Rules. If the amount of the claims and counterclaims are less than Two Hundred and Fifty Thousand Dollars
              ($250,000.00), then the JAMS Streamlined Arbitration Rules and Procedures will be used. You agree that any
              arbitration pursuant to these Terms will be on an individual basis, and that you will not bring a claim as
              a plaintiff or class member in a class, consolidated, or representative action. You further agree that class
              arbitrations, class actions and consolidation with other arbitrations will not be allowed. All disputes and
              claims between us will be heard by a single arbitrator.
            </div>
          </div>
        )
      },

      {
        title: 'Indemnification',
        pathname: pathnames[6],
        ref: selectionMap[pathnames[6]],
        jsx: (
          <div>
            <div>
              You agree to protect, defend, indemnify and hold us harmless from and against any and all claims, causes of
              action, liabilities, judgments, penalties, losses, costs, damages and expenses (including attorneys’ fees and
              all related costs and expenses for litigation and/or arbitration) suffered or incurred by us, including,
              without limitation, any claim arising from:
            </div>
            <ul>
              <li>
                Any actual or alleged breach of your obligations under these Terms or the terms contained in our Privacy
                Policy;
              </li>
              <li>
                Your wrongful or improper use of the Services;
              </li>
              <li>
                Your failure to provide us with accurate information about you or your business;
              </li>
              <li>
                Your violation of any third-party right, including without limitation any right of privacy, publicity rights
                or Intellectual Property Rights;
              </li>
              <li>
                Your violation of any law, rule or regulation of the United States or any other country;
              </li>
              <li>
                Any other party’s access and/or use of the Services with your unique name, password or other security code;
              </li>
              <li>
                Any copyright infringement claims that may arise from us scanning Legal Documents or other mail on your behalf;
              </li>
              <li>
                The failure of any third party, including but not limited to the United States Postal Service or any
                commercial delivery or courier service, to provide delivery or courier services accurately and on time;
              </li>
              <li>
                Any loss, damage or destruction of your Legal Documents by any cause whatsoever;
              </li>
              <li>
                Our being named as a defendant in an action based on our status as your registered agent;
              </li>
              <li>
                Any claims or action brought against us relating to your failure to maintain updated information on any of
                our websites.
              </li>
            </ul>
            <div>
              Due to various state requirements and statutes, you must provide us with accurate information concerning the
              services we perform on your behalf. You agree that you are solely responsible for the accuracy, quality,
              integrity, legality, reliability, and appropriateness of your information. You also agree that the accuracy of
              filings which we make on your behalf depends on the information you provide and you agree to promptly notify
              us when any of your information changes. Since your company may be subject to legal process in any county/state
              in which your business is registered or operates, it is crucial for you to provide us with correct and
              up-to-date information. That being said, we do not sell your information, and we keep all information that is
              not required on public documents private.
            </div>
          </div>
        )
      },

      {
        title: 'Intellectual Property Rights - Ownership',
        pathname: pathnames[7],
        ref: selectionMap[pathnames[7]],
        jsx: (
          <div>
            <h3>Overview</h3>
            <div>
              <div>
                Unless otherwise indicated, the Website is our proprietary property and all source code, databases,
                functionality, software, webapp designs, audio, video, text, photographs, and graphics on the Website
                (collectively, the “Content”) and the trademarks, service marks, and logos contained therein (the “Marks”)
                are owned or controlled by us or licensed to us, and are protected by copyright and trademark laws and
                various other intellectual property rights and unfair competition laws, international copyright laws,
                and international conventions. The Content and the Marks are provided on the Website “AS IS” for your
                information and personal use only. Except as expressly provided in these Terms of Use, no part of the
                Website and no Content or Marks may be copied, reproduced, aggregated, republished, uploaded, posted,
                publicly displayed, encoded, translated, transmitted, distributed, sold, licensed, or otherwise exploited
                for any commercial purpose whatsoever, without our express prior written permission.
              </div>
              <br/>
              <div>
                Provided that you are eligible to use the Website, you are granted a limited license to access and use
                the Website and to download or print a copy of any portion of the Content to which you have properly
                gained access solely for your personal, non-commercial use. We reserve all rights not expressly granted
                to you in and to the Website, the Content and the Marks.
              </div>
            </div>

            <br/>
            <h3>Receiving Feedback</h3>
            <div>
              At certain points of time, we collect client feedback to help us improve our services. Again, for the sake of
              clarity, by you submitting an idea, and us implementing it, does not in any way entitle your to compensation.
            </div>
          </div>
        )
      },

      {
        title: 'Disclaimer, Warranties, Limitations, Liability',
        pathname: pathnames[8],
        ref: selectionMap[pathnames[8]],
        jsx: (
          <div>
            <div>This section cannot be simplified for legal purposes.</div>

            <br/>
            <h3>DISCLAIMER OF WARRANTIES</h3>
            <div>
              THE WEBSITE IS PROVIDED ON AN AS-IS AND AS-AVAILABLE BASIS. YOU AGREE THAT YOUR USE OF THE WEBSITE AND OUR
              SERVICES WILL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES,
              EXPRESS OR IMPLIED, IN CONNECTION WITH THE WEBSITE AND YOUR USE THEREOF, INCLUDING, WITHOUT LIMITATION,
              THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE MAKE
              NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THE WEBSITE’S CONTENT OR THE CONTENT
              OF ANY WEBAPPS LINKED TO THE WEBSITE AND WE WILL ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY (1) ERRORS,
              MISTAKES, OR INACCURACIES OF CONTENT AND MATERIALS, (2) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE
              WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE WEBSITE, (3) ANY UNAUTHORIZED ACCESS TO OR USE
              OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN,
              (4) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE WEBSITE, (5) ANY BUGS, VIRUSES, TROJAN
              HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH THE WEBSITE BY ANY THIRD PARTY, AND/OR (6) ANY
              ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT
              OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE WEBSITE. WE DO NOT
              WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY
              A THIRD PARTY THROUGH THE WEBSITE, ANY HYPERLINKED WEBAPP, OR ANY WEBAPP OR MOBILE APPLICATION FEATURED IN
              ANY BANNER OR OTHER ADVERTISING, AND WE WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING
              ANY TRANSACTION BETWEEN YOU AND ANY THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF
              A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE
              CAUTION WHERE APPROPRIATE.
            </div>

            <br/>
            <h3>NON-GUARANTEE</h3>
            <div>
              <div>
                We reserve the right to change, modify, or remove the contents of the Website at any time or for any reason
                at our sole discretion without notice. However, we have no obligation to update any information on our
                Website. We also reserve the right to modify or discontinue all or part of the Website without notice at
                any time. We will not be liable to you or any third party for any modification, price change, suspension,
                or discontinuance of the Website.
              </div>
              <br/>
              <div>
                We cannot guarantee the Website will be available at all times. We may experience hardware, software, or
                other problems or need to perform maintenance related to the Website, resulting in interruptions, delays,
                or errors. We reserve the right to change, revise, update, suspend, discontinue, or otherwise modify the
                Website at any time or for any reason without notice to you. You agree that we have no liability whatsoever
                for any loss, damage, or inconvenience caused by your inability to access or use the Website during any
                downtime or discontinuance of the Website. Nothing in these Terms of Use will be construed to obligate
                us to maintain and support the Website or to supply any corrections, updates, or releases in connection
                therewith. Under no circumstances will we be responsible for any damage, loss, or injury resulting from
                hacking, tampering, or other unauthorized access or use of the services or your account, or the information
                contained therein.
              </div>
            </div>

            <br/>
            <h3>LIMITATION OF LIABILITY</h3>
            <div>
              IN NO EVENT WILL WE OR OUR DIRECTORS, EMPLOYEES, OR AGENTS BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT,
              INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL, OR PUNITIVE DAMAGES, INCLUDING LOST PROFIT, LOST
              REVENUE, LOSS OF DATA, OR OTHER DAMAGES ARISING FROM YOUR USE OF THE WEBSITE, EVEN IF WE HAVE BEEN ADVISED
              OF THE POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, OUR
              LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE
              LIMITED TO THE AMOUNT PAID, IF ANY, BY YOU TO US DURING THE SIX (6) MONTH PERIOD PRIOR TO ANY CAUSE OF
              ACTION ARISING. CERTAIN LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION
              OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS OR LIMITATIONS MAY
              NOT APPLY TO YOU, AND YOU MAY HAVE ADDITIONAL RIGHTS.
            </div>

          </div>
        )
      },

      {
        title: 'Miscellaneous',
        pathname: pathnames[9],
        ref: selectionMap[pathnames[9]],
        jsx: (
          <div>
            These Terms of Use and any policies or operating rules posted by us on the Website or in respect to the
            Website constitute the entire agreement and understanding between you and us. Our failure to exercise or
            enforce any right or provision of these Terms of Use shall not operate as a waiver of such right or provision.
            These Terms of Use operate to the fullest extent permissible by law. We may assign any or all of our rights
            and obligations to others at any time. We shall not be responsible or liable for any loss, damage, delay,
            or failure to act caused by any cause beyond our reasonable control. If any provision or part of a
            provision of these Terms of Use is determined to be unlawful, void, or unenforceable, that provision or
            part of the provision is deemed severable from these Terms of Use and does not affect the validity and
            enforceability of any remaining provisions. There is no joint venture, partnership, employment or agency
            relationship created between you and us as a result of these Terms of Use or use of the Website. You agree
            that these Terms of Use will not be construed against us by virtue of having drafted them. You hereby
            waive any and all defenses you may have based on the electronic form of these Terms of Use and the lack of
            signing by the parties hereto to execute these Terms of Use.
          </div>
        )
      },

      {
        title: 'Acts of God',
        pathname: pathnames[10],
        ref: selectionMap[pathnames[10]],
        jsx: (
          <div>
            <h3>Disclaimer</h3>
            <div>
              This section acts as a catch-all case for situations that are out of both our and the client's control.
              More explicitly, this section frees both of us from liability and obligation in the case of extraordinary
              circumstances that are out of our control like famine, disease, the apocalypse and other acts of God or Gods.
            </div>

            <br/>
            <h3>Full Explanation</h3>
            <div>
              We shall not be considered to be in breach or default of these Terms, and shall not be liable for any cessation,
              interruption, or delay in the performance of our Services or other obligations by reason of earthquake, flood,
              fire, storm, lightning, drought, landslide, hurricane, cyclone, typhoon, tornado, natural disaster, act of God
              or of the public enemy, epidemic, famine or plague, action of a court or public authority, change in law,
              explosion, war, terrorism, armed conflict, labor strike, lockout, boycott or other similar event that is
              beyond our reasonable control, whether foreseen or unforeseen (each a “Force Majeure Event”). If a Force
              Majeure Event continues for more than sixty days in the aggregate, we may immediately terminate our Services
              and shall have no liability for, or as a result of, any such termination.
            </div>
          </div>
        )
      },

      {
        title: 'Providing Services',
        pathname: pathnames[11],
        ref: selectionMap[pathnames[11]],
        jsx: (
          <div>
            <h3>Prior to Service</h3>
            <div>The standard exchange prior to the supplying of services from our consulting firm boils down to the
              drafting and signing of a "Consulting Agreement"; further detailing the contractual relationship and
              obligations between our clients and ourselves.</div>
            <div>This often includes topics such as:</div>
            <ul>
              <li>Description of Services</li>
              <li>Performance of Services</li>
              <li>Payment Structure</li>
              <li>Expense Reimbursement</li>
              <li>New Project Approval</li>
              <li>Term/Termination</li>
              <li>Relationship of Parties</li>
              <li>Employees</li>
              <li>Injuries and Insurance</li>
              <li>Intellectual Property</li>
              <li>Confidentiality</li>
              <li>Unauthorized Disclosure of Information</li>
              <li>Confidentiality after Termination</li>
              <li>Returns of Records</li>
              <li>and more...</li>
            </ul>

            <div>
              Below Agreements work in conjunction with and are in-part summaries of the "Consulting Agreement". Please
              read over all of our Policies and Agreements to ensure we are the best fit for your company's needs.
            </div>

            <br/>
            <h3>Services Provided</h3>
            <div>Our consultants provide services including but not limited to:</div>
            <ul>
              <li>Computer Programming</li>
              <li>Software and Systems Engineering Services</li>
              <li>UI/UX Designing</li>
              <li>Database Engineering</li>
              <li>Data Analytics</li>
              <li>Development Operations</li>
              <li>System Optimizations</li>
              <li>Process Review and Improvements</li>
              <li>Advice, Insights, and Lessons</li>
              <li>Strategic Long-Term Planning</li>
              <li>Integration of our Proprietary Technologies for Maximum Security and Performance</li>
            </ul>
            <div>The aforementioned services are subject to the Termination of Services Agreement below.</div>
          </div>
        )
      },

      {
        title: 'Termination of Services',
        pathname: pathnames[12],
        ref: selectionMap[pathnames[12]],
        jsx: (
          <div>
            <h3>When any Service with us is canceled or terminated, you acknowledge and agree that:</h3>
            <ul>
              <li>Anyone who has access to your account has the proper authority to cancel Services; and</li>
              <li>The termination is binding on the company(s); and</li>
              <li>Upon termination, you waive any and all rights or claims of statutory damages or tort claims.</li>
            </ul>

            <div><b>Consultation Services Termination</b></div>
            <ol>
              <li>
                <b>We can terminate service:</b>&#xa0;
                <div>We reserve the right to terminate your service(s) at any time. Reason for termination may include, but are not limited to:</div>
                <ul>
                  <li>Your failure to provide us with accurate, complete, and up-to-date information for your account.</li>
                  <li>Our inability to locate you after reasonable efforts are made.</li>
                  <li>Your failure to pay for your Service.</li>
                  <li>Abuse of system resources.</li>
                  <li>Any suspected illegal activity.</li>
                  <li>Any other lawful reason.</li>
                </ul>
              </li>
              <li>
                <b>You can terminate service:</b>&#xa0;
                <div>You are permitted to cancel services that continue after project completion anytime.
                  Project development services; we discourage and disallow the cancellation of services involved with developing
                  a product upon reaching 50% completion status.</div>
              </li>
              <li>
                <b>Upon termination you acknowledge and agree that:</b>&#xa0;
                <ul>
                  <li>You release any and all liability, duty, or obligation of any kind agreed upon in the Consulting Agreement.</li>
                  <li>The Confidentiality Agreement will be retained and up-held.</li>
                  <li>As of the date of termination, access to our proprietary technologies and services will be revoked.</li>
                </ul>
              </li>
            </ol>
          </div>
        )
      },

      {
        title: 'Automatic Payments - Billing',
        pathname: pathnames[13],
        ref: selectionMap[pathnames[13]],
        jsx: (
          <div>
            <p>
              Some services that we provide are billed via an automatic payment software. Our clients have the ability
              to opt-into the auto-payment feature in-place of our traditional invoicing procedure. All client accounts
              using the auto-payment feature must provide us with valid and current credit card information and you agree
              that we are authorized to charge such credit card for all purchased Services as well as fees incurred in
              providing you with Services.
            </p>

            <h3>Specifics Regarding Auto-Pay Feature</h3>
            <ul>
              <li>All automatic payments will be charged to the credit or debit card on file for the provided business entity or individual.</li>
              <li>All cancellations must be handled through either your online account or by calling us and letting us know.</li>
              <li>Annual auto-pay charges that fail to process will be rendered an unpaid invoice in your online account and subject to the fees and procedures outlined in the Terms and Conditions.</li>
              <li>Monthly subscription or auto-pay charges that fail to process will result in the cancellation of all applicable Services.</li>
            </ul>

            <h3>Declined Payments/Collections - Simplified</h3>
            <div>
              <p>
                At PubSub, we understand that life provides financial roadblocks at times.
                We insist on not being bothersome when it comes to payments; but like any other business, we collect on
                money owed. To treat our clients with dignity and respect, we provide all information regarding declined
                payments and collections while outlining what our clients are agreeing to.
              </p>
              <h4>Automatic Payment Attempts</h4>
              <p>
                If you are enrolled in an automatic payment service, we will charge the available payment methods that
                you provided for your online account. If all of the methods provided fail, we may suspend your account
                and require payment for our services to be resumed or in order to cancel your services.
              </p>
            </div>

            <h3>Declined Payments/Collections - Full</h3>
            <div>
              <p>
                If we don’t receive payment (“Non-Payment”), you agree to pay all amounts due upon demand to resume or
                cancel your requested Service(s). You also authorize PubSub to charge any and all
                outstanding fees and penalties that become due as a result from such Non-Payment. Additionally, following
                any such Non-Payment, you will not be eligible for monthly-billing or partial payments until your account
                is brought current. Non-Payment may also result in delayed services which include but are not limited to:
                revoked proprietary technology access, lock software functionality, and prevent new services from being
                registered.
              </p>
              <p>
                You agree that you are liable for all third-party collection agency recovery fees and charges. You are
                solely responsible for any and all fees charged to your credit card by the issuer, bank, or financial
                institution including, but not limited to, membership, overdraft, insufficient funds, and over the credit
                limit fees. By failing to notify the Company of billing problems or discrepancies you agree that you waive
                your right to dispute such billing discrepancies. We may modify the price, content, or nature of our
                services at any time. We may provide notice of any such changes by email, notice to you upon log-in, or
                by publishing them on our website.
              </p>
            </div>
          </div>
        )
      },

      {
        title: 'Privacy Policy',
        pathname: pathnames[14],
        ref: selectionMap[pathnames[14]],
        jsx: (
          <div>
            <h3>Update Log</h3>
            <ul>
              <li><b>Updated:</b> August 2022</li>
              <li><b>Updated:</b> July 2022</li>
            </ul>

            <br/>
            <h3>Overview</h3>
            <div>
              <div>
                Thank you for choosing to be part of our community at PubSub LLC, ("Company," "we,"
                "us," or "our"). We are committed to protecting your personal information and your right to privacy. If
                you have any questions or concerns about this privacy notice or our practices with regard to your
                personal information, please contact us using the Contact Us form on the site or at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
              </div>
              <br/>
              <div>
                The purpose of this privacy notice is to explain to you in the clearest way possible what information we collect, how we use it, and what rights you have in relation to it.
              </div>
              <br/>
              <div>
                If you have a disability that makes it difficult for you to read or use this Privacy Policy in its current
                format, please reach out to us at the contact information below so that we may provide you with assistance.
                We can be contacted by our toll free number at &#xa0;
                <a href="tel:3022981324">+1 (302) 298-1324</a> or by email at &#xa0;
                <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
              </div>
              <br/>
              <b>Please read this privacy notice carefully, as it will help you understand what we do with the information that we collect.</b>
            </div>

            <br/>
            <h3>What Information Do We Collect?</h3>
            <div>
              <h4>Personal information you disclose to us</h4>
              <p>
                <b>In Short:</b> We collect personal information that you provide to us.
              </p>
              <p>
                We collect personal information that you voluntarily provide to us when you express an interest in
                obtaining information about us or our Services, when you participate in activities on the Website or
                otherwise when you contact us.
              </p>
              <p>
                The personal information that we collect depends on the context of your interactions with us and the
                Website, the choices you make and the features you use. The personal information we collect may include
                the following:
              </p>

              <h4>Personal Information Provided by You.</h4>
              <div>
                To provide our services and receive payment for those services, we are required to collect information
                such as:
              </div>
              <ul>
                <li>Name</li>
                <li>Address</li>
                <li>Telephone Number</li>
                <li>Email Address</li>
                <li>Banking Information</li>
                <li>Credit Card Number</li>
                <li>Debit Card Number</li>
                <li>Bank Account Numbers</li>
                <li>User Name</li>
                <li>Other online identifiers</li>
              </ul>

              <h4>Information Automatically Collected</h4>
              <p>
                <b>In Short:</b>  Some information — such as your Internet Protocol (IP) address and/or browser and device
                characteristics — is collected automatically when you visit our Website.
              </p>
              <p>
                We automatically collect certain information when you visit, use or navigate the Website. This information
                does not reveal your specific identity (like your name or contact information) but may include device
                and usage information, such as your IP address, browser and device characteristics, operating system,
                language preferences, referring URLs, device name, country, location, information about how and when you
                use our Website and other technical information. This information is primarily needed to maintain the
                security and operation of our Website, and for our internal analytics and reporting purposes.
              </p>
              <b>We use the information we collect or receive:</b>
              <ul>
                <li>
                  <b>Request feedback.</b> We may use your information to request feedback and to contact you about your
                  use of our Website.
                </li>
                <li>
                  <b>To send administrative information to you.</b> We may use your personal information to send you new
                  features information and/or information about changes to our terms, conditions, and policies.
                </li>
                <li>
                  <b>To protect our Services.</b> We may use your information as part of our efforts to keep our Website
                  safe and secure (for example, for fraud monitoring and prevention).
                </li>
                <li>
                  To enforce our terms, conditions and policies for business purposes, to comply with legal and regulatory
                  requirements or in connection with our contract.
                </li>
                <li>
                  <b>To respond to legal requests and prevent harm.</b> If we receive a subpoena or other legal request,
                  we may need to inspect the data we hold to determine how to respond.
                </li>
                <li>
                  <b>To send you marketing and promotional communications.</b> We and/or our third-party marketing
                  partners may use the personal information you send to us for our marketing purposes, if this is in
                  accordance with your marketing preferences. For example, when expressing an interest in obtaining
                  information about us or our Website, subscribing to marketing or otherwise contacting us, we will
                  collect personal information from you. You can opt-out of our marketing emails at any time (see the
                  "WHAT ARE YOUR PRIVACY RIGHTS?" below).
                </li>
                <li>
                  <b>Deliver targeted advertising to you.</b> We may use your information to develop and display
                  personalized content and advertising (and work with third parties who do so) tailored to your
                  interests and/or location and to measure its effectiveness.
                </li>
                <li>
                  <b>For other business purposes.</b> We may use your information for other business purposes, such as
                  data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns
                  and to evaluate and improve our Website, products, marketing and your experience. We may use and store
                  this information in aggregated and anonymized form so that it is not associated with individual end
                  users and does not include personal information. We will not use identifiable personal information
                  without your consent.
                </li>
              </ul>
            </div>

            <br/>
            <h3>Will your information be shared with anyone?</h3>
            <div>
              <div>
                <b>In Short:</b> We only share information with your consent, to comply with laws, to provide you with
                service, to protect your rights, or to fulfill business obligations.
              </div>
              <b>We may process or share your data that we hold based on the following legal basis:</b>
              <ul>
                <li>
                  <b>Consent:</b> We may process your data if you have given us specific consent to use your personal information for a specific purpose.
                </li>
                <li>
                  <b>Legitimate Interests:</b> We may process your data when it is reasonably necessary to achieve our legitimate business interests.
                </li>
                <li>
                  <b>Performance of a Contract:</b> Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our contract.
                </li>
                <li>
                  <b>Legal Obligations:</b> We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal processes, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).
                </li>
                <li>
                  <b>Vital Interests:</b> We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.
                </li>
              </ul>
              <p>More specifically, we may need to process your data or share your personal information in the following situations:</p>
              <ul>
                <li>
                  <b>Business Transfers.</b> We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.
                </li>
              </ul>
            </div>

            <br/>
            <h3>How long do we keep your information?</h3>
            <div>
              <p>
                <b>In Short:</b> We keep your information for as long as necessary to fulfill the purposes outlined in
                this privacy notice unless otherwise required by law.
              </p>
              <p>
                We will only keep your personal information for as long as it is necessary for the purposes set out in
                this privacy notice, unless a longer retention period is required or permitted by law (such as tax,
                accounting or other legal requirements). </p>
              <p>
                When we have no ongoing legitimate business need to process your personal information, we will either
                delete or anonymize such information, or, if this is not possible (for example, because your personal
                information has been stored in backup archives), then we will securely store your personal information and
                isolate it from any further processing until deletion is possible.
              </p>
            </div>

            <br/>
            <h3>How do we keep your information safe?</h3>
            <div>
              <p>
                <b>In Short:</b> We aim to protect your personal information through a system of organizational and
                technical security measures.
              </p>
              <p>
                We have implemented appropriate technical and organizational security measures designed to protect the
                security of any personal information we process. However, despite our safeguards and efforts to secure
                your information, no electronic transmission over the Internet or information storage technology can be
                guaranteed to be 100% secure, so we cannot promise or guarantee that hackers, cybercriminals, or other
                unauthorized third parties will not be able to defeat our security, and improperly collect, access,
                steal, or modify your information. Although we will do our best to protect your personal information,
                transmission of personal information to and from our Website is at your own risk. You should only access
                the Website within a secure environment.
              </p>
            </div>

            <br/>
            <h3>Do we collect information from minors?</h3>
            <div>
              <p>
                <b>In Short:</b> We do not knowingly collect data from or market to children under 18 years of age.
              </p>
              <p>
                We do not knowingly solicit data from or market to children under 18 years of age. By using the Website,
                you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent
                to such minor dependent’s use of the Website. If we learn that personal information from users less than
                18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly
                delete such data from our records. If you become aware of any data we may have collected from children
                under age 18, please contact us through the Contact Us form on the site or via email at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
              </p>
            </div>

            <br/>
            <h3>What are your privacy rights?</h3>
            <div>
              <p>
                <b>In Short:</b> In some regions, such as the European Economic Area (EEA) and United Kingdom (UK), you
                have rights that allow you greater access to and control over your personal information. You may review,
                change, or terminate your account at any time.
              </p>
              <p>
                In some regions (like the EEA and UK), you have certain rights under applicable data protection laws.
                These may include the right
                <ol>
                  <li>to request access and obtain a copy of your personal information,</li>
                  <li>to request rectification or erasure;</li>
                  <li>to restrict the processing of your personal information; and</li>
                  <li>
                    if applicable, to data portability. In certain circumstances, you may also have the right to object
                    to the processing of your personal information. To make such a request, please use the contact details
                    provided below. We will consider and act upon any request in accordance with applicable data protection
                    laws.
                  </li>
                </ol>
              </p>
              <p>
                If we are relying on your consent to process your personal information, you have the right to withdraw
                your consent at any time. Please note however that this will not affect the lawfulness of the processing
                before its withdrawal, nor will it affect the processing of your personal information conducted in
                reliance on lawful processing grounds other than consent.
              </p>
              <p>
                If you are a resident in the EEA or UK and you believe we are unlawfully processing your personal
                information, you also have the right to complain to your local data protection supervisory authority.
                You can find their contact details here: https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
              </p>
              <p>
                If you are a resident in Switzerland, the contact details for the data protection authorities are
                available here: https://www.edoeb.admin.ch/edoeb/en/home.html.
              </p>

              <h4>GENERAL DATA PROTECTION REGULATION (GDPR)</h4>
              <div>
                <p>
                  If you are from the European Economic Area (EEA), PubSub LLC legal basis for collecting
                  and using the personal information described in this Privacy Policy depends on the Personal Data we collect
                  and the specific context in which we collect it.
                </p>
                <b>PubSub LLC may process your Personal Data because:</b>
                <ul>
                  <li>We need to perform a contract with you</li>
                  <li>You have given us permission to do so</li>
                  <li>The processing is in our legitimate interests and it’s not overridden by your rights</li>
                  <li>For payment processing purposes</li>
                  <li>To comply with the law</li>
                </ul>
              </div>

              <h4>Retention of Data</h4>
              <div>
                <p>
                  PubSub LLC will retain your Personal Data only for as long as is necessary for the
                  purposes set out in this Privacy Policy. We will retain and use your Personal Data to the extent
                  necessary to comply with our legal obligations (for example, if we are required to retain your data to
                  comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.
                </p>
                <p>
                  PubSub LLC will also retain Usage Data for internal analysis purposes. Usage Data is
                  generally retained for a shorter period of time, except when this data is used to strengthen the
                  security or to improve the functionality of our Service, or we are legally obligated to retain this
                  data for longer time periods.
                </p>
              </div>

              <h4>Transfer of Data</h4>
              <div>
                <p>
                  Your information, including Personal Data, may be transferred to — and maintained on — computers located
                  outside of your state, province, country or other governmental jurisdiction where the data protection
                  laws may differ than those from your jurisdiction. Your consent to this Privacy Policy followed by your
                  submission of such information represents your agreement to that transfer.
                </p>
                <p>
                  PubSub LLC will take all steps reasonably necessary to ensure that your data is
                  treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will
                  take place to an organization or a country unless there are adequate controls in place including the
                  security of your data and other personal information.
                </p>
              </div>

              <h4>Disclosure of Data. Legal Requirements</h4>
              <div>
                <b>PubSub LLC may disclose your Personal Data in the good faith belief that such action is necessary to:</b>
                <ul>
                  <li>To comply with a legal obligation</li>
                  <li>To protect and defend the rights or property of PubSub LLC</li>
                  <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                  <li>To protect the personal safety of users of the Service or the public</li>
                  <li>To protect against legal liability</li>
                </ul>
              </div>

              <h4>Security of Data</h4>
              <p>
                The security of your data is important to us, but remember that no method of transmission over the
                Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable
                means to protect your Personal Data, we cannot guarantee its absolute security.
              </p>

              <h4>Your rights under the GDPR</h4>
              <div>
                <p>
                  If you are a resident of the European Economic Area (EEA), you have certain data protection rights.
                  PubSub LLC aims to take reasonable steps to allow you to correct, amend, delete, or
                  limit the use of your Personal Data.
                </p>
                <p>
                  If you wish to be informed what Personal Data we hold about you and if you want it to be removed from
                  our systems, please contact us.
                </p>
                <b>In certain circumstances, you have the following data protection rights:</b>
                <ul>
                  <li>
                    The right to access, update or to delete the information we have on you. Whenever made possible,
                    you can access, update or request deletion of your Personal Data directly within your account
                    settings section. If you are unable to perform these actions yourself, please contact us to assist you.
                  </li>
                  <li>
                    The right of rectification. You have the right to have your information rectified if that information
                    is inaccurate or incomplete.
                  </li>
                  <li>
                    The right to object. You have the right to object to our processing of your Personal Data.
                  </li>
                  <li>
                    The right of restriction. You have the right to request that we restrict the processing of your
                    personal information.
                  </li>
                  <li>
                    The right to data portability. You have the right to be provided with a copy of the information we
                    have on you in a structured, machine-readable and commonly used format.
                  </li>
                  <li>
                    The right to withdraw consent. You also have the right to withdraw your consent at any time where
                    PubSub LLC relied on your consent to process your personal information.
                  </li>
                </ul>
                <p>Please note that we may ask you to verify your identity before responding to such requests.</p>
                <p>
                  You have the right to complain to a Data Protection Authority about our collection and use of your
                  Personal Data. For more information, please contact your local data protection authority in the
                  European Economic Area (EEA).
                </p>
                <p>
                  If you are a resident in the EEA or UK and you believe we are unlawfully processing your personal
                  information, you also have the right to complain to your local data protection supervisory authority.
                  You can find their contact details here: https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
                </p>
                <p>
                  If you are a resident in Switzerland, the contact details for the data protection authorities are
                  available here: https://www.edoeb.admin.ch/edoeb/en/home.html.
                </p>
              </div>
            </div>

            <br/>
            <h3>Do California Resident have specific privacy rights?</h3>
            <div>
              <p>
                <b>In Short:</b> Yes, if you are a resident of California, you are granted specific rights regarding
                access to your personal information.
              </p>
              <p>
                California Civil Code Section 1798.83, also known as the "Shine The Light" law, permits our users who
                are California residents to request and obtain from us, once a year and free of charge, information about
                categories of personal information (if any) we disclosed to third parties for direct marketing purposes
                and the names and addresses of all third parties with which we shared personal information in the
                immediately preceding calendar year. If you are a California resident and would like to make such a request,
                please submit your request in writing to us using the contact information provided below.
              </p>
              <p>
                If you are under 18 years of age, reside in California, and have a registered account with the Website,
                you have the right to request removal of unwanted data that you publicly post on the Website. To request
                removal of such data, please contact us using the contact information provided below, and include the
                email address associated with your account and a statement that you reside in California. We will make
                sure the data is not publicly displayed on the Website, but please be aware that the data may not be
                completely or comprehensively removed from all our systems (e.g. backups, etc.).
              </p>

              <h4>CCPA Privacy Notice</h4>
              <div>
                <b>The California Code of Regulations defines a "resident" as:</b>
                <ol>
                  <li>
                    every individual who is in the State of California for other than a temporary or transitory purpose and
                  </li>
                  <li>
                    every individual who is domiciled in the State of California who is outside the State of California
                    for a temporary or transitory purpose
                  </li>
                </ol>
                <p>All other individuals are defined as "non-residents."</p>
                <p>
                  If this definition of "resident" applies to you, we must adhere to certain rights and obligations
                  regarding your personal information.
                </p>
              </div>

              <h4>How do we use and share your personal information?</h4>
              <div>
                <p>
                  More information about our data collection and sharing practices can be found in this privacy notice.
                </p>
                <p>
                  You may contact us at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
                </p>
                <p>
                  If you are using an authorized agent to exercise your right to opt-out we may deny a request if the authorized agent does not submit proof that they have been validly authorized to act on your behalf.
                </p>
              </div>

              <h4>Will your information be shared with anyone else?</h4>
              <div>
                <p>
                  We may disclose your personal information with our service providers pursuant to a written contract
                  between us and each service provider. Each service provider is a for-profit entity that processes the
                  information on our behalf.
                </p>
                <p>
                  We may use your personal information for our own business purposes, such as for undertaking internal
                  research for technological development and demonstration. This is not considered to be "selling" of
                  your personal data.
                </p>
              </div>

              <h4>Your rights with respect to your personal data</h4>
              <div>
                <b>Right to request deletion of the data - Request to delete</b>
                <p>
                  You can ask for the deletion of your personal information. If you ask us to delete your personal
                  information, we will respect your request and delete your personal information, subject to certain
                  exceptions provided by law, such as (but not limited to) if another consumer exercises his or her
                  right to free speech, if we need to comply with a legal obligation or if we need to process your data
                  to protect against illegal activities.
                </p>

                <b>Right to be informed - Request to know</b>
                <p>
                  Depending on the circumstances, you have a right to know:
                </p>
                <ul>
                  <li>whether we collect and use your personal information;</li>
                  <li>the categories of personal information that we collect;</li>
                  <li>the purposes for which the collected personal information is used;</li>
                  <li>whether we sell your personal information to third parties;</li>
                  <li>the categories of personal information that we sold or disclosed for a business purpose;</li>
                  <li>the categories of third parties to whom the personal information was sold or disclosed for a business purpose; and</li>
                  <li>the business or commercial purpose for collecting or selling personal information.</li>
                </ul>
                <p>
                  In accordance with applicable law, we are not obligated to provide or delete consumer information that
                  is de-identified in response to a consumer request or to re-identify individual data to verify a consumer
                  request.
                </p>

                <b>Right to Non-Discrimination for the Exercise of a Consumer’s Privacy Rights</b>
                <p>
                  We will not discriminate against you if you exercise your privacy rights.
                </p>

                <b>Verification process</b>
                <p>
                  Upon receiving your request, we will need to verify your identity to determine you are the same person
                  about whom we have the information in our system. These verification efforts require us to ask you to
                  provide information so that we can match it with information you have previously provided us. For instance,
                  depending on the type of request you submit, we may ask you to provide certain information so that we
                  can match the information you provide with the information we already have on file, or we may contact
                  you through a communication method (e.g. phone or email) that you have previously provided to us. We
                  may also use other verification methods as the circumstances dictate.
                </p>
                <p>
                  We will only use personal information provided in your request to verify your identity or authority
                  to make the request. To the extent possible, we will avoid requesting additional information from you
                  for the purposes of verification. If, however, we cannot verify your identity from the information
                  already maintained by us, we may request that you provide additional information for the purposes of
                  verifying your identity, and for security or fraud-prevention purposes. We will delete such additionally
                  provided information as soon as we finish verifying you.
                </p>

                <b>Other privacy rights</b>
                <ul>
                  <li>
                    you may object to the processing of your personal data
                  </li>
                  <li>
                    you may request correction of your personal data if it is incorrect or no longer relevant, or ask to
                    restrict the processing of the data
                  </li>
                  <li>
                    you can designate an authorized agent to make a request under the CCPA on your behalf. We may deny a
                    request from an authorized agent that does not submit proof that they have been validly authorized
                    to act on your behalf in accordance with the CCPA.
                  </li>
                  <li>
                    you may request to opt-out from future selling of your personal information to third parties. Upon
                    receiving a request to opt-out, we will act upon the request as soon as feasibly possible, but no
                    later than 15 days from the date of the request submission.
                  </li>
                </ul>
                <p>
                  To exercise these rights, you can contact us at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
                  If you have a complaint about how we handle your data, we would like to hear from you.
                </p>
              </div>

              <h4>Do we make updates to this notice?</h4>
              <div>
                <p>
                  <b>In Short:</b> Yes, we will update this notice as necessary to stay compliant with relevant laws.
                </p>
                <p>
                  We may update this privacy notice from time to time. The updated version will be indicated by an updated
                  date and the updated version will be effective as soon as it is accessible. If we make material changes
                  to this privacy notice, we may notify you either by prominently posting a notice of such changes or by
                  directly sending you a notification. We encourage you to review this privacy notice frequently to be
                  informed of how we are protecting your information.
                </p>
              </div>

              <h4>How can you contact us about this notice?</h4>
              <div>
                If you have questions or comments about this notice, you may email us at <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
              </div>
            </div>
          </div>
        )
      },

      {
        title: 'Cookie Policy',
        pathname: pathnames[15],
        ref: selectionMap[pathnames[15]],
        jsx: (
          <div>
            <h3>Overview</h3>
            <div>
              <p>
                Here on this website (the "Website"), we use cookies to make your overall experience on our Website better.
                Specifically, we use cookies to:
              </p>
              <ul>
                <li>Store information for the time you are on the Website (called "Session Cookies")</li>
                <li>Store information to recognize your browser or device each time you visit (called "Persistent Cookies")</li>
                <li>Store your login and password information, if you choose to</li>
                <li>Store your user settings, like audio and display settings</li>
                <li>Analyze your behavior on our Website, so we can continue to improve</li>
                <li>Allow others to advertise on our Website or track information about you to improve advertising to you</li>
              </ul>
              <p>
                By using our Website or any of our services, you consent to our use of cookies. This Cookies Policy will
                explain what cookies are, how we use them, and what your rights are in relation to our use of cookies.
                We will also discuss our third-party cookies and what they mean for you.
              </p>
            </div>

            <h3>What are cookies?</h3>
            <div>
              <p>
                Cookies are small files that are placed on your device to store information. Specifically, cookies are
                small strings of text used to store information that may concern you, your behavior on the web, your
                preferences, or your device. Cookies are mainly used to adapt the operation of the Website to your
                expectations, offering a more personalized browsing experience and memorizing the choices you made
                previously.
              </p>
              <p>
                There are various types of cookies. Cookies do not record or store any personal data from your device.
              </p>
            </div>

            <h3>How we use cookies</h3>
            <div>
              <p>
                Technical cookies, which can also sometimes be called HTML cookies, are used for navigation and to
                facilitate your access to and use of the site. They are necessary for the transmission of communications
                on the network or to supply services requested by you. The use of technical cookies allows the safe and
                efficient use of the Website.
              </p>
              <p>
                You can manage or request the general deactivation or cancellation of cookies through your browser. If you
                do this though, please be advised this action might slow down or prevent access to some parts of the Website.
              </p>
              <p>
                We also use cookies that are retransmitted by an analytics or statistics provider to collect aggregated
                information on the number of users and how they visit the Website. These are also considered technical
                cookies when they operate as described.
              </p>
              <p>Analytics may collect information through log data, such as:</p>
              <ul>
                <li>Internet Protocol (IP) Address;</li>
                <li>Type of Browser and Device;</li>
                <li>Operating System;</li>
                <li>Name of the Internet Service Provider (ISP);</li>
                <li>Country Information</li>
                <li>Date and Time of visit;</li>
                <li>Web page of origin (referral) and exit;</li>
                <li>Possibly the number of clicks.</li>
              </ul>
              <p>
                We do not use this information to identify you, but rather to understand usage trends on our Website.
              </p>
              <p>
                We use session cookies to keep track of how you browse on your visits to the Website. Temporary session
                cookies are deleted automatically at the end of the browsing session - these are mostly used to keep
                track of what you do from page to page, such as with online shopping, keeping track of what is in your
                cart.
              </p>
              <p>
                Persistent cookies, on the other hand, remain active longer than just one particular session. These help
                us recognize you. We also use them to store your login and password info, if you choose, and to store
                your user settings.
              </p>
              <p>
                Third-party cookies: We also utilize third-party cookies, which are cookies sent by a third-party to your
                computer. Persistent cookies are often third-party cookies. The majority of third-party cookies consist
                of tracking cookies used to identify online behavior, understand interests and then customize advertising
                for you.
              </p>
              <p>
                We use remarketing cookies, which place files on your browser or device to allow us to display
                advertisements to you on other websites.
              </p>
              <p>
                When these types of cookies are used, we will ask for your explicit consent.
              </p>
            </div>

            <h3>Consent</h3>
            <p>
              When you arrive to our Website, we will request your consent for cookies through a clearly visible cookie
              banner/ notice at the user’s first visit.
            </p>

            <h3>What can you do about cookies?</h3>
            <div>
              <p>
                If you want, you can prevent the use of cookies, but then you may not be able to use our Website as we
                intend. To proceed without changing the options related to cookies, simply continue to use our Website.
              </p>
              <p>
                You can also manage cookies through the settings of your browser on your device. However, deleting
                cookies from your browser may remove the preferences you have set for the Website, as well as preferences
                you've set for other websites.
              </p>
              <p>
                For further information and support, you can also visit the specific help page of the web browser you
                are using:
              </p>
              <ul>
                <li>Internet Explorer: http://windows.microsoft.com/en-us/windows-vista/block-or-allow-cookies</li>
                <li>Firefox: https://support.mozilla.org/en-us/kb/enable-and-disable-cookies-website-preferences</li>
                <li>Safari: http://www.apple.com/legal/privacy/</li>
                <li>Chrome: https://support.google.com/accounts/answer/61416?hl=en</li>
                <li>Opera: http://www.opera.com/help/tutorials/security/cookies/</li>
              </ul>
            </div>

            <h3>How to contact us</h3>
            <p>
              For any questions on our cookies policy, you can reach us at the following email: <a href="mailto:legal@pubsubpub.com">legal@pubsubpub.com</a>.
            </p>
          </div>
        )
      }
    ]

    return (
      <TermsAndConditions>
        <h1 style={{ marginTop: '40px' }}>
          Terms and Conditions
        </h1>

        <div>
          <div style={{ marginTop: 50, display: 'block' }}>
            <h2>Greetings</h2>
          </div>
          <div style={{ marginTop: 20 }}>
            Hello! At PubSub we strive for privacy,
            security, high-performance, and equally as important, transparency. Below you will find each policy,
            agreement, advisory, and notice collectively as the Terms and Conditions.
          </div>
        </div>

        {
          Sections.map(section => (
            <div ref={section.ref} key={section.pathname} style={{ scrollMarginTop: 60 }}>
              <div>
                <h2>{section.title}</h2>
                <Tooltip title={tooltipText} onClose={() => setTooltipText('Copy Link')}>
                  <CopyLocationButton onClick={() => {
                    navigator.clipboard.writeText(`${window.location.protocol + '//' + window.location.host}/legal#${section.pathname}`)
                      .then(() => { setTooltipText('Link Copied!') })
                  }}>
                    <InsertLink/>
                  </CopyLocationButton>
                </Tooltip>
                <div style={{ backgroundColor: 'grey', height: 1 }}/>
              </div>
              {section.jsx}
            </div>
          ))
        }

      </TermsAndConditions>
    )
  }

  return (
    <LegalContainer>
      <ContentContainer>
        <ContentWrapper>
          <RenderTermsOfService/>
        </ContentWrapper>
      </ContentContainer>
    </LegalContainer>
  )
}
