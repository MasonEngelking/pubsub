/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Color_Picker
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

/* React-Related Imports */
import React, {
  useState,
  useRef,
  useEffect
} from 'react'
import { BlockPicker } from 'react-color'
import PropTypes from 'prop-types'

/* Styled & Design Imports */
import styled from 'styled-components'
import { ReactComponent as ColorPalette } from '../../assets/svgs/color-palette.svg'
import Foco from 'react-foco'

const ColorSelector = styled.div`
  position:         relative;
  height:           ${props => props.height};
  width:            ${props => props.width};
  border-radius:    8px;
  background-color: ${props => props.backgroundColor.hex};
`

const EditColor = styled.div`
  display:          flex;
  position:         absolute;
  right:            5px;
  bottom:           5px;
  background-color: #292929;
  padding:          5px 6px 5px 8px;
  color:            white;
  border-radius:    6px;
  font-size:        14px;
  z-index:          20;
  place-items:      center;
  gap:              5px;
  
  -webkit-touch-callout: none;
  -webkit-user-select:   none;
  -khtml-user-select:    none;
  -moz-user-select:      none;
  -ms-user-select:       none;
  user-select:           none;
  
  &:hover { cursor: pointer; }
`

const ColorPickerContainer = styled.div`
  position: absolute;
  bottom:   -230px;
  right:    -47px;
  z-index:  999;
  filter:   drop-shadow(0px 8px 10px rgba(0,0,0,0.5));
`
const ColorPickerStyled = styled(BlockPicker)`
  height: 100%;
  width:  100%;
`

export default
function ColorPicker (props) {
  const colorPickerRef = useRef(null)

  const [color, setColor] = useState(props.defaultColor)
  const [colorPickerToggle, setColorPickerToggle] = useState(false)

  /* Defining initial state */
  useEffect(() => props.stateMirror({ hex: props.defaultColor }), [])

  /* Passing state up */
  useEffect(() => props.stateMirror(color), [color])

  const handleToggleRender = () => {
    if (colorPickerToggle) {
      return (
        <ColorPickerContainer ref ={colorPickerRef}>
          <ColorPickerStyled
            color ={color}
            onChangeComplete ={color => setColor(color)}
          />
        </ColorPickerContainer>
      )
    }
  }

  return (
    <ColorSelector
      height ={props.height}
      width ={props.width}

      margin ={props.margin}
      marginTop ={props.marginTop}
      marginRight ={props.marginRight}
      marginBottom ={props.marginBottom}
      marginLeft ={props.marginLeft}

      padding ={props.padding}
      paddingTop ={props.paddingTop}
      paddingRight ={props.paddingRight}
      paddingBottom ={props.paddingBottom}
      paddingLeft ={props.paddingLeft}

      borderRadius ={props.borderRadius}
      borderStyle ={props.borderStyle}

      backgroundColor={color}
    >
      <Foco
        onClickOutside={() => { setColorPickerToggle(false) }}
      >
        {
          props.enabled
            ? <EditColor
              onClick={e => setColorPickerToggle(!colorPickerToggle)}>
              {colorPickerToggle ? 'Close' : 'Edit'}
              <ColorPalette style={{ fill: 'white', height: 16 }}/>
            </EditColor>
            : <></>
        }

        {handleToggleRender()}
      </Foco>

    </ColorSelector>
  )
}

ColorPicker.displayName = 'Color Picker'
ColorPicker.propTypes = {
  /* State Pass Up */
  stateMirror: PropTypes.func,
  enabled: PropTypes.bool,

  /* Color Picker */
  defaultColor: PropTypes.object,

  /* Styling Configuration */
  outlineColor: PropTypes.string,
  dottedLineColor: PropTypes.string,
  iconColor: PropTypes.string,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,

  height: PropTypes.string,
  width: PropTypes.string,

  margin: PropTypes.string,
  marginTop: PropTypes.number,
  marginRight: PropTypes.number,
  marginBottom: PropTypes.number,
  marginLeft: PropTypes.number,

  padding: PropTypes.string,
  paddingTop: PropTypes.number,
  paddingRight: PropTypes.number,
  paddingBottom: PropTypes.number,
  paddingLeft: PropTypes.number,

  borderRadius: PropTypes.string,
  borderStyle: PropTypes.string
}
ColorPicker.defaultProps = {
  /* State Pass Up */
  stateMirror: () => {},
  enabled: true,

  /* Color Picker */
  defaultColor: {
    hex: '#555555'
  },

  /* Styling Configuration */
  outlineColor: '',
  dottedLineColor: '',
  iconColor: '',
  textColor: '',
  backgroundColor: '',
  borderColor: '',

  height: '90px',
  width: '100%',

  margin: '0 auto'
}
