/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Bubble.Model
 *
 * @returns {MongoDB_Model} MongoDB Model
 *
 * @author Christian C. Larcomb
 */

import mongoose from 'mongoose'
// Adding Long (Int64) Support for Schema
import mongooseLong           from 'mongoose-long'
import {getSnowflakeID}       from '../middleware.js'
import {getDateAsCustomEpoch} from '../global.middleware.js'

// Type Long support
mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const bubbleSchema = new mongoose.Schema({

    _id: {type: Long, default: await getSnowflakeID()},

    meta: {
        dateEpoch:       {type: Long, default: getDateAsCustomEpoch()},
        expirationEpoch: {type: Long, default: (getDateAsCustomEpoch() + 86400000)},
        region:          String,
        language:        {type: String, default: 'eng'},
        poster: {
            uuid: {type:Long, ref:'User'},
            suid: Long
        }
    },

    content: {
        title:       String,
        description: String,
        tags:        [String],
        category:    String,
        media: {
            imageURL:  String,
            bannerURL: String,
            colorHEX:  {type: String, default: "#0a7bff"}
        }
    },

    preferences: {
        maxNumPeers: [
            {
                uuid:      {type: Long, ref:'User'},
                suid:      Long,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                content:   {type: Number, default: 20}
            }
        ],
        vibesRange: [
            {
                uuid:      {type: Long, ref:'User'},
                suid:      Long,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                active:    {type: Boolean, default: true},
                min:       {type: Number, default: 0},
                max:       {type: Number, default: 99999}
            }
        ]
    },

    status: {
        protected: [
            {
                uuid:         {type: Long, ref:'User'},
                suid:         Long,
                content:      {type: Boolean, default: false},
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                updatingUser: {type:Long, default: false}
            }
        ],
        featured: [
            {
                active:    Boolean,
                content:   Boolean,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                promoter: {
                    uuid:   {type:Long, ref:'Staff'},
                    reason: String
                }
            }
        ],
        restricted: [
            {
                active:          Boolean,
                reason:          String,
                underReview:     Boolean,
                expirationEpoch: Long,
                dateEpoch:       {type: Long, default: getDateAsCustomEpoch()},
                enactingModerator: {
                    uuid: {type:Long, ref:'Staff'}
                }
            }
        ]
    },

    access: {
        paywall: [
            {
                uuid:      {type:Long, ref:'User'},
                suid:      Long,
                comets:    {type:Number, default:0},
                dateEpoch: {type:Long, default: getDateAsCustomEpoch()}
            }
        ],
        users: {
            priority: [
                {
                    uuid:      Long,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()}
                }
            ],
            privileged: [
                {
                    uuid:      Long,
                    type:      String,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()}
                }
            ],
            restricted: [
                {
                    uuid:      Long,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()}
                }
            ]
        }
    },

    analytics: {
        views: {
            count: Number,
            impressions: [
                {
                    status:    String,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                    user:      {type:Long, ref: 'User'}
                }
            ]
        },
        vibes: {
            level: Number,
            impressions: [
                {
                    positive:  Boolean,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                    uuid:      {type:Long, ref: 'User'}
                }
            ]
        }
    },

    secured: {
        inviteCodes: { hashes: [String] },

        streamKey:   { hashed: String },

        ledgers: [{
            type:      String,
            comets:    Number,
            requester: {type:Long, ref:'User'},
            luid:      {type:Long, ref:'Ledger'}
        }]
    },

},{_id: false})

bubbleSchema.query.secureSelection = function () {
    return this.select(`
        -_id
        
        -analytics
    `)
}

bubbleSchema.query.unsecureSelection = function () {
    return this.select(`
        -_id
        -secured
        
        -analytics
    `)
}

bubbleSchema.query.analytics = function () {
    return this.select(`analytics`)
}

export default mongoose.model('Bubble', bubbleSchema, 'Bubbles')