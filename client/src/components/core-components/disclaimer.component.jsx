/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import React from 'react'
import { MISC_COLORS } from '../../constants/themes.constants.js'
import Form from './form.component.jsx'
import PropTypes from 'prop-types'

export default
function Disclaimer (props) {
  return (
    <Form
      minHeight ={70}
      maxHeight ={undefined}
      height ={undefined}
      maxWidth ={undefined}

      borderColor ={props.borderColor}
      backgroundColor={props.backgroundColor}
      margin ={props.margin}
      padding ={'0'}

      justifyContent ={'center'}
      alignItems ={'center'}

      borderRadius={props.borderRadius}
    >

      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'auto 1fr',
          alignItems: 'center',
          justifyContent: 'center',
          justifyItems: 'center',
          width: 'auto',
          color: props.color,
          padding: 10,
          gap: '8px'
        }}
      >
        <div style={{
          fontWeight: 700,
          margin: '0 6px'
        }}>
          {props.title}
        </div>
        <div style={{
          fontSize: props.fontSize,
          marginRight: 'auto'
        }}>
          {props.text}
        </div>
      </div>

    </Form>
  )
}

Disclaimer.displayName = 'Disclaimer'
Disclaimer.propTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
  fontSize: PropTypes.string,
  color: PropTypes.string,

  margin: PropTypes.string,

  borderRadius: PropTypes.string,
  borderColor: PropTypes.string,
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.string,
  borderDisplay: PropTypes.string,

  backgroundColor: PropTypes.string
}
Disclaimer.defaultProps = {
  title: 'NOTE',
  text: 'Disclaimer text...',
  color: '#ffffff',
  fontSize: '14px',
  borderRadius: '10',
  margin: '10px 0',
  borderColor: MISC_COLORS.disclaimer.borderColor,
  backgroundColor: MISC_COLORS.disclaimer.backgroundColor
}
