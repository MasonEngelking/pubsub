/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import React from 'react'
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'

const LoadingAnimationKeyframes = keyframes`
    40% {transform: scale(1.3) translate(-2px, -2px)}
    80% {transform: scale(1)}
    100% {transform: scale(1)}
`

const LoadingDots = styled.div`

  width: 60px;
  height: 60px;
  position: absolute;
  top: 50%;
  left: 50%;
  margin: -30px 0 0 -30px;
  transform: rotate(45deg);
  
  & > div {
    width: 6px;
    height: 6px;
    background: white;
    border-radius: 100%;
    float: left;
    margin-bottom: 12px;
    animation: ${LoadingAnimationKeyframes} 2s ease infinite;
    
    &:not(:nth-child(4n+4)){
      margin-right: 12px;
    }
    
            //row 1
    &:nth-child(1){
      animation-delay: 0s
    }

            //row 2
    &:nth-child(2),
    &:nth-child(5){
      animation-delay: .1s;
    }
    
            //row 3
    &:nth-child(3),
    &:nth-child(6),
    &:nth-child(9) {
      animation-delay: .2s
    }
            //row 4
    &:nth-child(4),
    &:nth-child(7),
    &:nth-child(10),
    &:nth-child(13) {
      animation-delay: .3s
    }
            //row 5
    &:nth-child(8),
    &:nth-child(11),
    &:nth-child(14) {
      animation-delay: .4s
    }
            //row 6
    &:nth-child(12),
    &:nth-child(15) {
      animation-delay: .5s
    }
            //row 7
    &:nth-child(16) {
      animation-delay: .6s
    }
    
  }
  
`

export default
function LoadingAnimation ({ scale }) {
  const handleDotRender = () => {
    const dots = []

    for (let i = 0; i < 16; i++) { dots.push(<div/>) }

    return dots
  }
  return (
    <div
      style={{
        position: 'relative',
        height: '100px',
        width: '100px',
        transform: `scale(${scale})`
      }}
    >
      <LoadingDots>
        {handleDotRender().map(dot => dot)}
      </LoadingDots>
    </div>
  )
}

LoadingAnimation.displayName = 'Loading Animation'
LoadingAnimation.propTypes = {
  scale: PropTypes.number
}
LoadingAnimation.defaultProps = {
  scale: 1
}
