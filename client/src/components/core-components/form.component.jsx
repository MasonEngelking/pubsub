/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Form
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const FormStyle = styled.div`
  position:         relative                          !important;
  
  display:          ${props => props.display};
  grid-template-columns: repeat(${props => props.columns}, 1fr);
  grid-gap:         ${props => props.columns > 1 ? '15px' : 0};
  
  width:            ${props => props.width}           !important;
  min-width:        ${props => props.minWidth}px      !important;
  max-width:        ${props => props.maxWidth}px      !important;
  
  height:           ${props => props.height}          !important;
  min-height:       ${props => props.minHeight}px     !important;
  max-height:       ${props => props.maxHeight}px     !important;
  
  background-color: ${props => props.backgroundColor} !important;
  
  border-radius:    ${props => props.borderRadius}px  !important;
  border-color:     ${props => props.borderColor}     !important;
  border-style:     ${props => props.borderStyle}     !important;
  border-width:     ${props => props.borderWidth}     !important;
  
  margin:           ${props => props.margin}          !important;
  padding:          ${props => props.padding}px       !important;
  
  justify-content:  ${props => props.justifyContent}  !important;
  align-items:      ${props => props.alignItems}      !important;
  
  cursor:           ${props => props.cursor}          !important;
  
  overflow:         ${props => props.overflow}        !important;
`

function Form ({
  backgroundColor,
  borderStyle,
  borderColor,
  borderWidth,
  borderRadius,
  width,
  maxWidth,
  minWidth,
  height,
  maxHeight,
  minHeight,
  margin,
  padding,
  columns,
  rows,
  display,
  justifyContent,
  alignItems,
  onClick,
  cursor,
  children,
  overflow
}) {
  return (
    <FormStyle
      backgroundColor={backgroundColor}

      borderStyle ={borderStyle}
      borderColor ={borderColor}
      borderWidth ={borderWidth}
      borderRadius ={borderRadius}

      width ={width}
      maxWidth ={maxWidth}
      minWidth ={minWidth}

      height ={height}
      maxHeight ={maxHeight}
      minHeight ={minHeight}

      margin ={margin}
      padding ={padding}

      columns ={columns}
      rows ={rows}
      display ={display}

      justifyContent ={justifyContent}
      alignItems ={alignItems}

      onClick ={() => onClick()}
      cursor ={cursor}

      overflow={overflow}
    >
      {children}
    </FormStyle>
  )
}

Form.displayName = 'Form'
Form.propTypes = {
  onClick: PropTypes.func,
  cursor: PropTypes.oneOf(['default', 'pointer']),
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,
  columns: PropTypes.number,
  rows: PropTypes.number,
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  width: PropTypes.string,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  height: PropTypes.string,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  margin: PropTypes.string,
  padding: PropTypes.number,
  display: PropTypes.oneOf(['grid', 'block', 'hidden']),
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
  children: PropTypes.element,
  overflow: PropTypes.string
}
Form.defaultProps = {
  onClick: () => {},
  cursor: 'default',
  backgroundColor: '#000000',
  borderColor: '#ffffff',
  columns: 1,
  rows: 1,
  borderStyle: 'solid',
  borderWidth: 2,
  borderRadius: 15,
  minWidth: 200,
  minHeight: 100,
  padding: 15,
  display: 'grid',
  justifyContent: 'center'
}

export default Form
