/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Bubble_View
 *
 * @memberOf nav
 * @function
 *
 * @param {Object} props
 *
 * @description Attempts to establish a connection to the specified BubbleID, handles the corresponding streams
 *              once retrieved,
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useEffect,
  useRef,
  useState,
  useContext
} from 'react'
import styled, { ThemeContext } from 'styled-components'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'
import Peer from 'simple-peer'
import io from 'socket.io-client'

import JoinSound from '../../assets/audio/bubble_join.wav'
import LeaveSound from '../../assets/audio/bubble_leave.wav'

import allNavbarActions from '../../redux/actions/navbarActions.js'

import Streams from '../../components/core-components/streams.component.jsx'

const BubbleViewStyled = styled.div`
  height: 100vh;
  width:  100vw;
  background-color: #08080a;
`

export default
function BubbleView () {
  /* React Redux Var */
  const dispatch = useDispatch()

  const theme = useContext(ThemeContext)

  /* Defining the Join & Leave Sound Tracks */
  const joinAudio = new Audio(JoinSound)
  const leaveAudio = new Audio(LeaveSound)
  joinAudio.volume = 0.20
  leaveAudio.volume = 0.20

  /* React State Hooks */
  const [peers, setPeers] = useState([])

  /* Retrieving Provided Room ID */
  const roomID = useLocation().pathname.split('/')[3]

  /* References to DOM Elements */
  const socketRef = useRef()
  const userVideo = useRef()
  const streamWrapperRef = useRef()
  const peersRef = useRef([])

  const createPeer = (userToSignal, callerID, stream) => {
    const peer = new Peer({
      initiator: true,
      trickle: false,
      stream
    })

    peer.on('signal', signal => {
      socketRef.current.emit('sending_signal', { userToSignal, callerID, signal })
    })

    return peer
  }

  const addPeer = (incomingSignal, callerID, stream) => {
    const peer = new Peer({
      initiator: false,
      trickle: false,
      stream
    })

    peer.on('signal', signal => {
      socketRef.current.emit('returning_signal', { signal, callerID })
    })

    peer.signal(incomingSignal)

    return peer
  }

  /* UI useEffect Logic */
  useEffect(() => {
    /* Audio Alert */
    joinAudio.play()
    return () => { leaveAudio.play() }
  }, [])

  /* TODO: Refactor Network Topology */
  /* Socket useEffect Logic */

  useEffect(() => {
    let userVideoReference

    /* TODO: ADAPT... */
    socketRef.current = ''

    try {
      // If media devices are available...
      navigator.mediaDevices.getUserMedia({ video: { width: 640, height: 480 }, audio: true })
        .then(stream => {
          userVideo.current.srcObject = stream
          userVideoReference = userVideo.current.srcObject

          socketRef.current.emit('join_bubble', roomID)

          socketRef.current.on('user_left', userID => {
            /* Remove from State */
            // eslint-disable-next-line array-callback-return
            setPeers(peers => peers.map(peer => {
              console.log(peer)
              console.log(`User leaving: ${userID}`)
            }))
            /* Remove user Ref */
            peersRef.current.map(peersRef => peersRef.id !== userID)
          })

          socketRef.current.on('receive_bubble_peers', async users => {
            const peers = []
            await users.forEach(userID => {
              const peer = createPeer(userID, socketRef.current.id, stream)
              peersRef.current.push({
                peerID: userID,
                peer
              })
              peers.push({ peerID: userID, peer })
            })
            await setPeers(peers)
          })

          socketRef.current.on('user_joined', async payload => {
            const peer = addPeer(payload.signal, payload.callerID, stream)

            peersRef.current.push({ peerID: payload.callerID, peer })

            await setPeers(users => [...users, { peerID: payload.callerID, peer }])
          })

          socketRef.current.on('receiving_returned_signal', payload => {
            const item = peersRef.current.find(p => p.peerID === payload.id)
            item.peer.signal(payload.signal)
          })
        })
        .catch(err => { console.log(err) })
    } catch (e) {
      // If media devices are not available...
    }

    /* Handle ahead of time? */
    return () => {
      /* Remove Self from Socket Peer List */
      // socketRef.current.emit('leave_bubble')
      /* Disconnect from Information Socket */
      // socketRef.current.disconnect()

      /* If Media Avail. - Attempt to turn it off. */
      try {
        userVideoReference.getTracks().forEach(track => { track.stop() })
      } catch (e) {

      }

      /* Destroying Socket Connections */
      peersRef.current.forEach(peer => { peer.peer.destroy() })
    }
  }, [])

  /* Set the navbar type */
  useEffect(() => {
    dispatch(allNavbarActions.setNavbarType('call'))
  }, [])

  return (
        <BubbleViewStyled>
            <Streams
                backgroundColor={theme.call.backgroundColor}
            />
        </BubbleViewStyled>
  )
}
