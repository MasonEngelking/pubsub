/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name IndexNavBar
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */
import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { ReactComponent as PubSubLogo } from '../../assets/svgs/logo_rep_emblem.svg'
import { ReactComponent as PubSubLogoText } from '../../assets/svgs/logo_text_full.svg'
import Button from '../../components/core-components/button.component'

const Container = styled.nav`
  z-index: 9999;
  height: 70px;
  width: 100%;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  fill: white;
  backdrop-filter: blur(20px);
  background: rgb(15, 17, 22, 0.9) radial-gradient(ellipse at top, rgba(255, 255, 255, 0.05) 2%, rgba(0, 0, 0, 0) 70%);
`

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  min-width: 320px;
  max-width: 1200px;
  padding: 0 20px;
  
  & > ul {
    display: flex;
    align-items: center;
    list-style: none;
    padding: 0;
    margin: 0;
    
    & > li {
      margin: auto 0;
      float:   left;
      padding: 0 6px;
      font-size: 14px;
      
      & > a {
        &:hover {
          cursor: pointer;
          filter: brightness(0.8);
        }
        &:active {
          filter: brightness(0.6);
        }
        all: unset;
      }
    }
  }
`

export default
function IndexNavBar (props) {
  return (
    <Container>
      <Wrapper>
        <ul>
          <li style={{ height: 26, width: 35 }}><PubSubLogo style={{ height: 26, paddingRight: 5 }}/></li>
          <li><h2 style={{ fontSize: 20 }}>PUBSUB</h2></li>
        </ul>
        <ul>
          <li><Link to={'/'}>HOME</Link></li>
          <li><Link to={'about'}>ABOUT</Link></li>
          <li><Link to={'contact'}>CONTACT</Link></li>
          <li><Link to={'legal'}>LEGAL</Link></li>
          <li>
            <Button
              linkTo={'app'}
              margin={'0 0 0 10px'}
              borderWidth={3}
              borderRadius={'100px'}
              padding={'12px'}
              height={'45px'}
              backgroundColor={'rgba(0,0,0,0)'}
              borderColor={'rgba(102,0,255,0.9)'}
              contents={'OPEN APP'}
            />
          </li>
        </ul>
      </Wrapper>
    </Container>
  )
}
