/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Notifications
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, {
  useEffect,
  useState
} from 'react'
import styled from 'styled-components'
import ee from 'event-emitter'
import { NOTIFICATION_COLORS }
  from '../../constants/themes.constants.js'
import Button from './button.component.jsx'

/* SVG Imports */
import { ReactComponent as WarningSVG } from '../../assets/svgs/alert.notify.svg'
import { ReactComponent as ErrorSVG } from '../../assets/svgs/error.notify.svg'
import { ReactComponent as SuccessSVG } from '../../assets/svgs/success.notify.svg'
import { ReactComponent as InfoSVG } from '../../assets/svgs/info.notify.svg'
import { ReactComponent as UpdateSVG } from '../../assets/svgs/update.notify.svg'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

// eslint-disable-next-line new-cap
const emitter = new ee()

const NotificationContainer = styled.div`
  /* Box Behavior */
  display:  flex;
  position: fixed;
  
  /* Prop Configurable Coloring */
  background-color: ${props => props.backgroundColor};
  color:            ${props => props.color};
  
  /* Positioning and Indexing */
  left:      50%;
  transform: translate(-50%, 0);
  z-index:   999;

  /* Size Limitations */
  min-width:  375px;
  min-height: 75px;
  
  /* Border Definitions */
  border-radius: 15px;
  border-color:  ${props => props.borderColor};
  border-width:  1.5px;
  border-style:  solid;

  /* Notification Drop-Shadow */
  -webkit-filter: drop-shadow(0px 4px 1px rgba(0, 0, 0, 0.04));
  filter:         drop-shadow(0px 4px 1px rgba(0, 0, 0, 0.04));

  /* Displays and Hides Notification Panel */
  transition: bottom 0.25s ease, opacity 0.25s ease;
  bottom:     ${props => props.shown ? '85' : '-160'}px;
  opacity:    ${props => props.shown ? '1' : '0'};
`

const IconContainer = styled.div`
  width:           60px;
  display:         flex;
  justify-content: center;
  align-items:     center;
  
  & > svg {
    height: 36px;
    fill:   #fff;
  }
`

const TextContainer = styled.div`
  margin:    auto 10px auto 0;
  max-width: 500px;
  
  & > div:nth-child(1){
    margin-top: 10px;
    font-size:  20px;
  }
  & > div:nth-child(2){
    margin-top:    5px;
    margin-bottom: 10px;
    font-size:     15px;
    opacity:       0.5;
  }
`

/* Leveraged by other components */
export const notify = (title, message, type) => emitter.emit('notification', title, message, type)

/* Notification handling and placement on parent component */
export default
function Notifications ({
  backgroundColor,
  color,
  closeBackgroundColor,
  closeBorderColor
}) {
  const DELAY_IN_SECONDS = 5 * 1000

  const appManager = useSelector(state => state.appManager)

  const [show, setShow] = useState(false)
  const [title, setTitle] = useState()
  const [type, setType] = useState()
  const [message, setMessage] = useState()

  /* Handle Incoming Notifications */
  useEffect(() => emitter.on('notification', (title, message, type) => handleNotification(title, message, type)), [])

  const handleAlertColor = () => {
    switch (type) {
    case 'warning': return NOTIFICATION_COLORS.warning
    case 'update': return NOTIFICATION_COLORS.update
    case 'info': return NOTIFICATION_COLORS.info
    case 'success': return NOTIFICATION_COLORS.success
    case 'error': return NOTIFICATION_COLORS.error
    default: return NOTIFICATION_COLORS.default
    }
  }

  const handleIconRender = () => {
    switch (type) {
    case 'warning':return <WarningSVG/>
    case 'update': return <UpdateSVG/>
    case 'success':return <SuccessSVG/>
    case 'info': return <InfoSVG/>
    case 'error': return <ErrorSVG/>
    default:
    }
  }

  /* Sets the state of the notification and displays it: Implements a timeout */
  const handleNotification = (title, message, type) => {
    /* Defining Essential Fields */
    setTitle(title)
    setMessage(message)
    setType(type)

    /* Show the user the message */
    setShow(true)

    /* After timeout, set it to not show and clear notification contents */
    const timeoutID = setTimeout(() => setShow(false), DELAY_IN_SECONDS)

    return () => clearTimeout(timeoutID)
  }

  return (
    <NotificationContainer
      shown ={show}
      backgroundColor ={backgroundColor}
      borderColor ={handleAlertColor}
      color ={color}
    >

      <IconContainer>
        {handleIconRender()}
      </IconContainer>

      <TextContainer>
        <div>
          {title}
        </div>
        <div>
          {message}
        </div>
      </TextContainer>

      <div style={{ margin: 'auto' }}/>

      <Button
        text ="✕"
        height ={''}
        autoHeight ={true}
        width ="35px"
        borderRadius="8px"
        margin ="5px"
        fontSize ={19}
        onClick ={() => setShow(false)}
      />

    </NotificationContainer>
  )
}

Notifications.displayName = 'Notifications'
Notifications.propTypes = {
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  closeBackgroundColor: PropTypes.string,
  closeBorderColor: PropTypes.string
}
Notifications.defaultProps = {
  backgroundColor: '#111111',
  color: '#ffffff',
  closeBackgroundColor: '',
  closeBorderColor: ''
}
