/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Sign-Up_Account-Portal
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useContext, useEffect, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { SUBJECT_COLORS, MISC_COLORS } from '../../constants/themes.constants.js'
import { notify } from '../../components/core-components/notifications.component.jsx'
import { useNavigate } from 'react-router-dom'

/* Core Components */
import NavBar from '../../components/core-components/navbar.component.jsx'
import Button from '../../components/core-components/button.component.jsx'
import SubjectIndicator from '../../components/core-components/subject_indicator.component.jsx'
import Checkbox from '../../components/core-components/checkbox.component.jsx'
import Form from '../../components/core-components/form.component.jsx'
import Emblem from '../../components/core-components/emblem.component.jsx'
import ImageSelector from '../../components/core-components/imageselector.component.jsx'
import ColorPicker from '../../components/core-components/colorpicker.component.jsx'
import Field from '../../components/core-components/field.component.jsx'

/* SVGs */
import { ReactComponent as SafetySVG } from '../../assets/svgs/safety.svg'
import { ReactComponent as LegalSVG } from '../../assets/svgs/legal.svg'
import { ReactComponent as CredentialsSVG } from '../../assets/svgs/id.svg'
import { ReactComponent as VerificationSVG } from '../../assets/svgs/check.svg'
import { ReactComponent as ArrowSVG } from '../../assets/svgs/up-arrow.svg'
import Disclaimer from '../../components/core-components/disclaimer.component.jsx'

const SectionViewer = styled.div`
  display:       flex;
  height:        calc(100%);
  max-width:     1300px;
  width:         100vw;
  position:      relative;
  padding:       0 10px;
  overflow:      hidden;
  
  justify-content: center;
  align-items:     center;
`

const SectionContainer = styled.div`
  display: flex;
`

const TextSection = styled.div`
  height:          auto;
  width:           350px;
  color:           white;
  display:         flex;
  justify-content: center;
  margin-right:    20px;
  align-items:     center;

  /* Prevent Drag */
  -webkit-touch-callout: none;
  -webkit-user-select:   none;
  -khtml-user-select:    none;
  -moz-user-select:      none;
  -ms-user-select:       none;
  user-select:           none;
  
  /* Text Container */
  & > div {width: 300px;}
  
  /* Individual Text Wrappers */
  & > div > div {
    
    /* Title */
    &:nth-child(1) {
      font-size:     30px;
      margin-bottom: 15px;
    }
    /* Description */
    &:nth-child(2) {
      font-size: 18px;
      & > div { margin: 15px 0 }
    }
    
  }
`

const FormSection = styled.div``

export default
function SignUpAccountPortal (props) {
  /* Component States */
  const theme = useContext(ThemeContext)
  const navigate = useNavigate()
  const MAX_PAGE_NUM = 4
  const subjectColors = [
    SUBJECT_COLORS.welcome[1],
    SUBJECT_COLORS.safety[1],
    SUBJECT_COLORS.legal[1],
    SUBJECT_COLORS.credentials[1],
    SUBJECT_COLORS.verification[1]
  ]
  const [page, setPage] = useState(0)
  const [borderColor, setBorderColor] = useState(subjectColors[0])

  /* ~~~ */

  /* Safety States */
  const [checkOne, setCheckOne] = useState(false)
  const [checkTwo, setCheckTwo] = useState(false)
  const [checkThree, setCheckThree] = useState(false)
  const [checkFour, setCheckFour] = useState(false)
  const [checkFive, setCheckFive] = useState(false)
  const [checkSix, setCheckSix] = useState(false)
  const allSafetyChecks = [
    checkOne,
    checkTwo,
    checkThree,
    checkFour,
    checkFive,
    checkSix
  ]

  /* Legal States */
  const [legalOneCheck, setLegalOneCheck] = useState(false)
  const [legalTwoCheck, setLegalTwoCheck] = useState(false)
  const [legalThreeCheck, setLegalThreeCheck] = useState(false)
  const [legalFourCheck, setLegalFourCheck] = useState(false)
  const allLegalChecks = [
    [legalOneCheck, setLegalOneCheck],
    [legalTwoCheck, setLegalTwoCheck],
    [legalThreeCheck, setLegalThreeCheck],
    [legalFourCheck, setLegalFourCheck]
  ]

  /* Credential States */
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [color, setColor] = useState('#555555')

  /* Verification States */
  const [captchaToggle, setCaptchaToggle] = useState(true)

  /* Update border color on page render */
  useEffect(() => setBorderColor(subjectColors[page]), [page])

  const WelcomeSection = () => {
    /* Component Styling */
    const SectionStyle = styled.div`
          
          min-width:     500px;
            
          /* Container to Center */
          & > div {
            /* Title */
            & > div:nth-child(1)
            {
              color:         white;
              font-size:     32px;
              margin-bottom: 15px;
            }
            /* Forms */
            & > div:nth-child(2)
            {
              display:   flex;
              flex-wrap: wrap;
            }
          }
        `

    /* Component Rendering */
    return (
      <SectionStyle>
        <div>
          <div> Modules to complete! </div>

          {/* DIV: Form Wrapper */}
          <div>
            <Form
              margin ={'35px 5px 0 5px'}
              backgroundColor={theme.form.backgroundColor}
              borderColor ={subjectColors[1]}

              minWidth ={''}
            >
              <Emblem
                margin={'-45px 0 0 0'}
                title={'Safety'}
                description={
                  'We want to establish clear and open safety agreements ' +
                                    'before our users can create an account. The key is holding those with ' +
                                    'malicious intent accountable!'
                }
                gap={5}
                colorOne={subjectColors[1]}
                colorTwo={subjectColors[1]}
                icon={
                  <SafetySVG
                    style={{
                      fill: '#ffffff',
                      height: 40
                    }}
                  />}
              />
            </Form>

            <Form
              margin ={'35px 5px 0 5px'}
              backgroundColor={theme.form.backgroundColor}
              borderColor ={subjectColors[2]}

              minWidth ={''}
            >
              <Emblem
                margin={'-45px 0 0 0'}
                title={'Legal'}
                description={
                  "Believe us, we know this can get boring but we've made it as interactive as possible." +
                                    ' Please read through our legal legal so you know how our application operates and our' +
                                    'legal obligations.'
                }
                gap={5}
                colorOne={subjectColors[2]}
                colorTwo={subjectColors[2]}
                icon={
                  <LegalSVG
                    style={{
                      fill: '#ffffff',
                      height: 40
                    }}
                  />}
              />
            </Form>

            <Form
              margin ={'35px 5px 0 5px'}
              backgroundColor={theme.form.backgroundColor}
              borderColor ={subjectColors[3]}

              minWidth ={''}
            >
              <Emblem
                margin={'-45px 0 0 0'}
                title={'Credentials'}
                description={
                  'Pretty straight forward; give your new account difficult but easy to remember (for you)' +
                                    " credentials. We'll guide you through the process."
                }
                gap={5}
                colorOne={subjectColors[3]}
                colorTwo={subjectColors[3]}
                icon={
                  <CredentialsSVG
                    style={{
                      fill: '#ffffff',
                      height: 35
                    }}
                  />}
              />
            </Form>

            <Form
              margin ={'35px 5px 0 5px'}
              backgroundColor={theme.form.backgroundColor}
              borderColor ={subjectColors[4]}

              minWidth ={''}
            >
              <Emblem
                margin={'-45px 0 0 0'}
                title={'Verification'}
                description={
                  'Last but not least, we need to make sure that you are you! This helps us prevent ' +
                                    'bad-actors from gaining access to our platform or keeping them off once they have' +
                                    'violated our policies.'
                }
                gap={5}
                colorOne={subjectColors[4]}
                colorTwo={subjectColors[4]}
                icon={
                  <VerificationSVG
                    style={{
                      fill: '#ffffff',
                      height: 28
                    }}
                  />}
              />
            </Form>
          </div>
        </div>
      </SectionStyle>
    )
  }

  const SafetySection = () => {
    const safetyDisclaimer =
            'We take all offenses very seriously. If our users violate our safety, user, and legal policies, we' +
            ' are at our own discretion to involve either legal authorities or other related safety officials. ' +
            'We understand that there are bad people in this world and we will do our part to mitigate their bad' +
            " behaviors. Granted... That's not always possible. Please ensure your own safety!"

    const SectionStyle = styled.div`
          
          width:       732px;
          display:     flex;
          flex-wrap:   wrap;
          color:       white;
          align-items: flex-start;
          
          /* First 2 Text Field */
          & > div > div:nth-child(1), 
          & > div > div:nth-child(2) 
          {
            -webkit-touch-callout: none;
            -webkit-user-select:   none;
            -khtml-user-select:    none;
            -moz-user-select:      none;
            -ms-user-select:       none;
            user-select:           none;
            margin-bottom: 12px;
          }
          
          /* Text Boxes */
          & > div > div:nth-child(1),
          & > div > div:nth-child(2)
          {
            /* Prompts */
            & > div:nth-child(1){
              font-size: 14px;
              opacity:   50%;
            }
            /* Big Text */
            & > div:nth-child(2){
              font-size:   18px;
              width:       200px;
              margin-left: auto;
              
              & > ul {
                margin:    4px 0;
                font-size: 12px;
              }
            }
            
          }
        `

    return (
      <div
        style={{
          width: 724
        }}
      >

        <Disclaimer
          text ={safetyDisclaimer}
        />

        <SectionStyle>
          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckOne(!checkOne)}
          >
            <div>
              <div>
                                Before...
              </div>
              <div style={{ color: '#4fb0f8' }}>
                                Being able to access PubSubPub.
              </div>
            </div>
            <div>
              <div>
                                You agree to...
              </div>
              <div style={{ color: '#4ff860' }}v>
                                Being 18+ years of age.
              </div>
            </div>

            <Checkbox
              size ={22}

              toggled ={checkOne}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>

          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckTwo(!checkTwo)}
          >
            <div>
              <div>
                                If someone...
              </div>
              <div style={{ color: '#f84f4f' }}>
                                Threatens you and/or others.
              </div>
            </div>
            <div>
              <div>
                                You will...
              </div>
              <div style={{ color: '#4ff860' }}>
                                Report them!
              </div>
            </div>

            <Checkbox
              size ={22}

              toggled ={checkTwo}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>

          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckThree(!checkThree)}
          >
            <div>
              <div>
                                If someone...
              </div>
              <div style={{ color: '#f84f4f' }}>
                                Doxxes you and/or others
              </div>
            </div>
            <div>
              <div>
                                You will...
              </div>
              <div style={{ color: '#4ff860' }}>
                                Report them!
              </div>
            </div>

            <Checkbox
              size ={22}

              toggled ={checkThree}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>

          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckFour(!checkFour)}
          >
            <div>
              <div>
                                If someone...
              </div>
              <div style={{ color: '#f84f4f' }}>
                                Displays NSFW content/behavior without consent.
              </div>
            </div>
            <div>
              <div>
                                You will...
              </div>
              <div style={{ color: '#4ff860' }}>
                                Ask them to stop. If they don&apos;t: Report them!
              </div>
            </div>

            <Checkbox
              size ={22}

              toggled ={checkFour}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>

          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckFive(!checkFive)}
          >

            {/* Pre */}
            <div>
              <div>
                                If someone...
              </div>
              <div style={{ color: '#f84f4f' }}>
                                Asks for personal information:
                <ul>
                  <li>Name</li>
                  <li>Address</li>
                  <li>Social-Media Links</li>
                  <li>Phone Numbers</li>
                  <li>Email</li>
                  <li>Where you went/go to school</li>
                  <li>Where you work</li>
                  <li>Other information that could personally identify you...</li>
                </ul>
              </div>
            </div>

            {/* Post */}
            <div>
              <div>
                                You will...
              </div>
              <div style={{ color: '#4ff860' }}>
                                Ask them to stop! If they don&apos;t: Report them!
              </div>
            </div>

            <Checkbox
              size ={22}

              toggled ={checkFive}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>

          <Form
            width ={225}
            maxWidth ={undefined}
            minWidth ={undefined}
            borderColor ={subjectColors[1]}
            backgroundColor={theme.form.backgroundColor}
            margin ={'5px'}
            display ={'block'}
            cursor ={'pointer'}
            onClick ={() => setCheckSix(!checkSix)}
          >
            <div>
              <div>
                                If someone...
              </div>
              <div style={{ color: '#f84f4f' }}>
                                Displays other odd behavior and you are not sure whether or not it&apos;s reportable.
              </div>
            </div>
            <div>
              <div>
                                You will...
              </div>
              <div style={{ color: '#4ff860' }}>
                                Report it anyways and we will review it!
              </div>
            </div>

            <Checkbox
              size ={22}
              toggled ={checkSix}
              toggledColor ={theme.checkbox.misc.toggled}

              eBackgroundColor={theme.checkbox.enabled.backgroundColor}
              dBackgroundColor={theme.checkbox.disabled.backgroundColor}
              eBorderColor ={theme.checkbox.enabled.borderColor}
              dBorderColor ={theme.checkbox.disabled.borderColor}

              margin ={'10px 0 0 0'}
            />
          </Form>
        </SectionStyle>

      </div>
    )
  }

  const LegalSection = () => {
    const TOSDoc = {
      title: 'Terms of Service',
      body: '[Insert Terms of Service Agreement]'
    }
    const UserPrivacyDOC = {
      title: 'User Privacy',
      body: '[Insert User Privacy Policy]'
    }
    const UseOfCookiesDOC = {
      title: 'Use of Cookies',
      body: '[Insert Cookies Disclaimer]'
    }
    const CommunityGuidelinesDOC = {
      title: 'Community Guidelines',
      body: '[Insert Community Guidelines]'
    }
    const AllDOCS = [
      TOSDoc,
      UserPrivacyDOC,
      UseOfCookiesDOC,
      CommunityGuidelinesDOC
    ]

    const SectionStyle = styled.div`
          display: flex;
          height:  100%;
          width:   100%;
          color:   white;
          
          /* Selector Container */
          & > div:nth-child(1)
          {
            width:           170px;
            margin-right:    15px;
            display:         flex;
            flex-direction:  column;
            justify-content: center;
            
            /* Selector Options */
            & > div 
            {
              height:      45px;
              display:     flex;
              padding:     8px 5px;
              margin:      5px 0;
              font-size:   15px;
              align-items: center;
              border-radius: 6px;
              
              &:hover {
                background-color: rgba(255,255,255, 0.07);
                cursor: pointer;
              }
            }
          }

          /* All of the legal document contents */
          & > div:nth-child(2)
          {
            overflow-y:    scroll;
            min-width:     280px;
            max-width:     600px;
            height:        100%;
            padding-right: 20px;
          }
        `

    const handleRenderList = () => {
      return (
        <div>
          {
            AllDOCS.map((doc, i) => (
              <div
                key={i}
                onClick={() => allLegalChecks[i][1](!allLegalChecks[i][0])}
              >
                <Checkbox
                  size ={18}
                  margin ={'0 10px 0 0'}

                  eBorderColor ={theme.checkbox.enabled.backgroundColor}
                  eBackgroundColor={theme.checkbox.enabled.backgroundColor}

                  toggled={allLegalChecks[i][0]}
                />
                <div>
                  {doc.title}
                </div>
              </div>
            ))
          }
          <Button
            text={'Accept All'}
            onClick={() => {
              allLegalChecks.map((item, i) => {
                return allLegalChecks[i][1](true)
              })
            }}
          />
        </div>
      )
    }

    const handleRenderDocuments = () => {
      const DocumentStyle = styled.div`
              & > div:nth-of-type(odd)
              {
                font-size:     28px;
                margin-top:    14px;
                margin-bottom: 8px;
              }
              & > div:nth-of-type(even)
              {
                text-align:    justify;
                text-justify:  inter-word;
                margin-bottom: 40px;
              }
            `
      return (
        <DocumentStyle>
          {
            AllDOCS.map(doc => (
              <>
                <div>
                  {doc.title}
                </div>
                <div>
                  {doc.body}
                </div>
              </>
            ))
          }
        </DocumentStyle>
      )
    }

    return (
      <Form
        minWidth ={300}
        height ={'650px'}
        borderColor ={subjectColors[2]}
        backgroundColor={theme.form.backgroundColor}
        margin ={'5px'}
        padding ={10}
        display ={'flex'}
        justifyContent ={'center'}
        alignItems ={'center'}
      >
        <SectionStyle>
          {handleRenderList()}
          {handleRenderDocuments()}
        </SectionStyle>
      </Form>
    )
  }

  const CredentialsSection = () => {
    return (
      <Form
        backgroundColor={theme.form.backgroundColor}
        borderColor ={subjectColors[3]}
        padding ={10}
        minWidth ={300}
      >
        <ColorPicker
          height ={'100px'}
          width ={'auto'}
          defaultColor={{ hex: color }}
          stateMirror ={state => setColor(state.hex)}
          key ={'profile-setup-color-picker'}
        />

        <ImageSelector
          margin ={'-65px auto 10px auto'}
          width ={'115px'}
          height ={'115px'}
          borderColor ={theme.imageSelector.borderColor}
          containerColor={theme.imageSelector.containerColor}
          areaColor ={theme.imageSelector.areaColor}
          iconColor ={theme.imageSelector.iconColor}
          key ={'profile-setup-image-selector'}
        />

        <div
          style={{
            marginTop: -12,
            marginBottom: 14,
            fontSize: 26,
            fontWeight: 700,
            textAlign: 'center',
            height: 30,
            color
          }}
        >
          {username}
        </div>

        <Field
          required ={true}
          title ={'Username'}
          initText ={'Username'}
          stateMirror ={state => setUsername(state)}
          eBackgroundColor={theme.field.enabled.backgroundColor}
          eBorderColor ={theme.field.enabled.borderColor}
          maxCharCount ={24}
        />

        <Field
          required ={true}
          title ={'Email'}
          initText ={'Email'}
          stateMirror ={state => setEmail(state)}
          eBackgroundColor={theme.field.enabled.backgroundColor}
          eBorderColor ={theme.field.enabled.borderColor}
        />

        <Field
          required ={true}
          title ={'Password'}
          initText ={'Password'}
          stateMirror ={state => setPassword(state)}
          eBackgroundColor={theme.field.enabled.backgroundColor}
          eBorderColor ={theme.field.enabled.borderColor}
        />
        <Field
          required ={true}
          title ={'Confirm Password'}
          initText ={'Confirm Password'}
          stateMirror ={state => setConfirmPassword(state)}
          eBackgroundColor={theme.field.enabled.backgroundColor}
          eBorderColor ={theme.field.enabled.borderColor}
        />
      </Form>
    )
  }

  const VerificationSection = () => {
    const SectionStyle = styled.div`
          
          color:      white;
          text-align: center;
        
          & > div:nth-child(1)
          {
            font-size:     20px;
            margin-bottom: 10px;
          }
        `

    const handleCaptchaOnLoad = () => {}

    const handleCaptchaOnVerify = () => {}

    const handleCaptchaOnExpired = () => {}

    const handleEmailVerification = () => {}

    if (captchaToggle) {
      /* Handle Captcha */
      return (
        <Form
          backgroundColor={theme.form.backgroundColor}
          borderColor ={subjectColors[4]}
          padding ={10}
        >
          <SectionStyle>
            <div>Captcha</div>

            <div
              style={{
                fontSize: 14,
                opacity: 0.5,
                marginBottom: 15
              }}
            >
              Let&apos;s make sure that you are not a robot!
            </div>

            <div>
              {/* TODO: Add hCaptcha Implementation */}
            </div>
          </SectionStyle>
        </Form>
      )
    } else {
      return (
      /* Upon Captcha Completion -> Send Auth Token to Email & Await */
        <Form
          backgroundColor={theme.form.backgroundColor}
          borderColor ={subjectColors[4]}
          padding ={10}
          maxWidth ={250}
        >
          <SectionStyle>
            <div>Email Verification</div>

            <div
              style={{
                fontSize: 14,
                opacity: 0.5
              }}
            >
              We have sent a verification email to your email,
              go agead and retrieve the verificiation token and
              place it below.
            </div>

            <Field
              required ={true}
              title ={'Token'}
              initText ={'Token'}
              footnote ={'Copy and paste the code into the box above'}
              displayCharCount={false}
              eBackgroundColor={theme.field.enabled.backgroundColor}
              eBorderColor ={theme.field.enabled.borderColor}
              margin ={'15px 0 5px 0'}
            />
          </SectionStyle>
        </Form>
      )
    }
  }

  const subjectContents = [
    {
      title: 'Howdy!',
      description: [
        'Before we can get you into the conversation Bubbles on PubSubPub, we will need you to' +
                'complete a couple of modules first! This is to ensure that we are on the same page when it comes' +
                'to: Safety, Legal Agreements, Account Details, and Account Verification/Security.',

        'These modules greatly assist in keeping our threads/bubbles clean of malicious nav and behaviors ' +
                'so please bare with us.',

        "Without further ado, let's get started!",

        'Click NEXT when you are ready!'
      ],
      contents: WelcomeSection()
    },
    {
      title: '1. SAFETY',
      description: [
        "Before you join in on the fun, let's talk about safety really quick! Carefully read and" +
                'check off all of the boxes before continuing onto the next step!'
      ],
      contents: SafetySection()
    },
    {
      title: '2. LEGAL',
      description: [
        "Let's get down to the nitty gritty. We cannot allow individuals access some features on" +
                'our platform until they agree to the corresponding documents displayed in the document navigator.',

        'Please read over everything carefully and make sure you AGREE to them before selecting the AGREE button.',

        'Note, to view the documents, just scroll down inside the document viewer and the boxes will check off upon' +
                'document reading completion allowing you to click Agree'
      ],
      contents: LegalSection()
    },
    {
      title: '3. CREDENTIALS',
      description: [
        'Provide unique credentials for your user account and afterwards we will perform a few' +
                'security checks'
      ],
      contents: CredentialsSection()
    },
    {
      title: '4. VERIFICATION',
      description: [
        "We know... Quite the boring process. We'll make it quick. Please complete the captcha, fetch" +
                'the verification code sent to either your phone or email, then complete the sign-up process.'
      ],
      contents: VerificationSection()
    },
    {
      title: 'DONE!',
      description: [
        'Before you go!',

        "We've generated some account recovery tokens that can be used in the future in the event that you" +
                'have lost your account credentials.',

        'Please keep these in a safe place away from others as they will be able to reset your account' +
                'credentials without hesitation!',

        'Continue to PubSubPub by clicking NEXT!'
      ],
      contents: VerificationSection()
    }
  ]

  const handlePageRender = () => {
    return (
      <SectionViewer>
        <SectionContainer>

          <TextSection>
            <div>
              <div>
                {subjectContents[page].title}
              </div>
              <div>
                {subjectContents[page].description.map((text, i) => { return <div key={i}>{text}</div> })}
              </div>
            </div>
          </TextSection>

          <FormSection>
            {subjectContents[page].contents}
          </FormSection>
        </SectionContainer>
      </SectionViewer>
    )
  }

  const handleNextButtonClick = (num) => {
    /* Handle Safety Page Blocker */
    if (allSafetyChecks.includes(false) && page === 1) {
      notify('Uh oh!', 'Please check all of the above boxes before continuing!', 'warning')
      return
    }

    /* Handle Legal Page Blocker */
    if (allLegalChecks.map(check => check[0]).includes(false) && page === 2) {
      notify('Uh oh!', 'Please agree to all of the legal documents before continuing!', 'warning')
      return
    }

    /* Handle Next && Back */
    if (page < MAX_PAGE_NUM && page >= 0) {
      return setPage(prev => prev + num)
    }
  }

  return (
    <>
      <NavBar
        height ={46}
        minHeight ={46}
        maxHeight ={46}
        backgroundColor={theme.navbar.backgroundColor}
        borderColor ={borderColor}
        bottom ={15}
      >
        <Button
          width ={'75px'}
          height ={'46px'}
          margin ={'0'}
          backgroundColor={theme.button.enabled.backgroundColor}
          borderColor ={theme.button.enabled.borderColor}

          contents={
            page === 0
              ? <div style={{
                display: 'flex',
                justifyContent: 'center',
                justifyItems: 'center',
                alignItems: 'center'
              }}>
                <ArrowSVG
                  style={{
                    height: 7,
                    fill: '#ffffff',
                    transform: 'rotate(-90deg)',
                    marginRight: 3
                  }}
                />
                <div
                  style={{
                    marginTop: 1
                  }}
                >
                                    Cancel
                </div>
              </div>
              : <div style={{
                display: 'flex',
                justifyContent: 'center',
                justifyItems: 'center',
                alignItems: 'center'
              }}>
                <ArrowSVG
                  style={{
                    height: 7,
                    fill: '#ffffff',
                    transform: 'rotate(-90deg)',
                    marginRight: 3
                  }}
                />
                <div
                  style={{
                    marginTop: 1
                  }}
                >
                                    Back
                </div>
              </div>

          }
          onClick ={() => {
            page > 0 ? setPage(page - 1) : navigate('/account-portal')
          }}
        />

        <SubjectIndicator pageIndex={page}/>

        <Button
          width ={'75px'}
          height={'46px'}
          contents={
            <div style={{
              display: 'flex',
              justifyContent: 'center',
              justifyItems: 'center',
              alignItems: 'center'
            }}>
              <div style={{ marginTop: 1 }}>
                                Next
              </div>

              <ArrowSVG
                style={{
                  height: 7,
                  fill: '#ffffff',
                  transform: 'rotate(90deg)',
                  marginLeft: 3
                }}
              />
            </div>
          }
          margin ={'0'}
          backgroundColor={theme.button.enabled.backgroundColor}
          borderColor ={theme.button.enabled.borderColor}
          onClick ={() => handleNextButtonClick(1)}
        />
      </NavBar>

      <SectionViewer>
        {handlePageRender()}
      </SectionViewer>
    </>
  )
}
