/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Threads_View
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

/* React & Style Imports */
import React, { useEffect } from 'react'
import styled from 'styled-components'
import { Outlet } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import allAppActions from '../../redux/actions/appActions.js'

/* Styled Containers */
const Container = styled.div`
  padding: 0;
  overflow-y: scroll;
  overflow-x: hidden;
  height: 100%;

  ::-webkit-scrollbar {
    width: 12px;
    display: flex;
  }
  
  /* Track */

  ::-webkit-scrollbar-track {
    width: 5px;
    border-radius: 10px;
  }

  /* Handle */

  ::-webkit-scrollbar-thumb {
    border: 4px solid rgba(0, 0, 0, 0);
    background-clip: padding-box;
    border-radius: 9999px;
    background-color: rgba(255,255,255, 0.2);
  }

  /* Handle on hover */

  ::-webkit-scrollbar-thumb:hover {

    border: 4px solid rgba(0, 0, 0, 0);
    background-clip: padding-box;
    border-radius: 9999px;
    background-color: rgba(255,255,255, 1);
  }
`

const BubbleGrid = styled.div`
  display:       grid;
  padding:       20px;
  margin:        0 auto;
  align-items:   center;
  justify-items: center;
`

function HubsView (props) {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(allAppActions.setNavbarType('hubs'))
  }, [])

  return (
    <Container>
      <BubbleGrid>
        <Outlet/>
      </BubbleGrid>
    </Container>
  )
}

export default HubsView
