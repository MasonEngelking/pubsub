/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name User.Model
 *
 * @returns {MongoDB_Model} MongoDB Model
 *
 * @author Christian C. Larcomb
 */

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

import mongoose     from 'mongoose'
import mongooseLong from 'mongoose-long'
import {getDateAsCustomEpoch} from '../global.middleware.js'
import {getSnowflakeID}       from '../middleware.js'
mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const staffSchema = new mongoose.Schema({
  _id: {type: Long, default: await getSnowflakeID()}, // STUID

  identifier: {

    firstName: [
      {
        content:   String,
        dateEpoch: Long
      }
    ],

    middleName: [
      {
        content:   String,
        dateEpoch: Long
      }
    ],

    lastName: [
      {
        content:   String,
        dateEpoch: Long
      }
    ],

    gender: {
      display: Boolean,
      details: [
        {
          content:   String,
          dateEpoch: Long
        }
      ]
    },

    bio: [
      {
        active:    {type: Boolean, default: true},
        content:   String,
        dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
        suid:      Long
      }
    ]

  },

  psAccounts: [ { account: {type: Long, ref: 'User'} } ],

  privilege: [
    {
      type:           String, // Dev, Admin, Mod, Intern
      reason:         String,
      dateEpoch:      Long,
      expiresOnEpoch: Long,
      enactingStaff:  { type: Long, ref: 'Staff' }
    }
  ],

  media: {
    avatar: [
      {
        active:     {type: Boolean, default: true},
        contentURL: String,
        dateEpoch:  {type: Long, default: getDateAsCustomEpoch()},
        suid:       Long
      }
    ],
  },

  actions: {
    user: [
      {
        type:           String, // Suspension, Disabled/Deleted Content
        expiresOnEpoch: Long,
        reason:         String,
        content: { type: Long, ref: 'User'}
      }
    ],
    bubble: [
      {
        type:           String, // Suspension, Disabled/Deleted Content
        expiresOnEpoch: Long,
        reason:         String,
        content: { type: Long, ref: 'Bubble'}
      }
    ],
    quickie: [
      {
        type:           String, // Suspension, Disabled/Deleted Content
        expiresOnEpoch: Long,
        reason:         String,
        content: { type: Long, ref: 'Quickie'}
      }
    ]
  },

  secured: {

    euid: Long, // Employee ID

    account: [
      {
        sajwt:     {hashed: String},  // Secured-Account JSON Web-Token
        dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
      }
    ],

    credentials: {
      email: {
        content:   {hashed: String},
        dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
        ssuid:     Long,
        verified: {
          content:   Boolean,
          dateEpoch: Number
        }
      },
      passwords: [
        {
          content:     {hashed:       String},
          updatingSjwt:{hashTrailing: String},
          dateEpoch:   {type: Long, default: getDateAsCustomEpoch()},
          ssuid:       Long
        }
      ],
      dateOfBirths: [
        {
          content:      {hashed: String},
          updatingSjwt: {hashTrailing: String},
          dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
          ssuid:        Long
        }
      ]
    },

    recovery: {
      codes: [
        {
          dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
          codeSet: {
            hashes: [String]
          },
          suid:      Long,
        }
      ],
      phones: [
        {
          verified:          Boolean,
          dateEpoch:         Long,
          dateVerifiedEpoch: Long,
          number: {
            hashed: String,
            trail:  String,
          },
          suid:              Long,
        }
      ]
    },

    // Prevents Login from Foreign IP's or Mac Addresses
    // TODO: Go more in-depth in the future
    registrations: {
      TwoFactorAuth: {},
      macAddresses:  { hashes: [String] }
    },

    // Staff Accounts Can Only Have One Active Session, remainder are ignored
    // Staff Logins Are Restricted to One Mac (Device) Address
    sessions: [
      {
        ssjwt:             {hashed: String},  // Secured-Session JSON Web-Token
        ssuid:             Long,              // Secure-Session Unique ID
        dateAccessedEpoch: {type: Long, default: getDateAsCustomEpoch()},
        platform: {
          description:  String,
          layout:       String,
          manufacturer: String,
          name:         String,
          os:           String,
          prerelease:   String,
          product:      String,
          ua:           String,
          version:      String,
          architecture: String,
          family:       String
        },
        networkInfo: {
          location: String,
          ip:       {hashed: String}
        }
      }
    ]
  }
})

export default mongoose.model('Staff', staffSchema, 'Team')