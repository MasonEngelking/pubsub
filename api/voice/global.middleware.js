/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
import jwt from 'jsonwebtoken'
import os  from 'os'
import axios from "axios"

/**
 * Just a promise-based wrapper for jwtVerify function from jsonwebtoken
 */
function jwtVerify(token, secret, options) {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      secret,
      options,
      (err, contents) => err ? reject(err) : resolve(contents)
    )
  })
}

export function logLevelPresentCheck(logLevel){
  const logLevels = process.env.LOG_LEVEL.split(',')
  return logLevels.includes(logLevel)
}

/**
 * @name getCustomEpochTime
 * @memberOf api.v1.dba.get
 * @function
 * @param {Object} res - Returns response status code and JSON objects.
 * @description Returns whether or not the access_jwt exists. Expiration verification should be performed by EPC.
 * @returns {Object} {
 *     'PubSub Response': {
 *         header: {
 *            status: Number,
 *            message: String,
 *            pid: Number
 *         }
 *     }
 * }
 */
export function getDateAndTimeAsString() {
  const estDateAsString = new Date().toLocaleString('en-US', {timeZone: 'America/New_York'})
  if(logLevelPresentCheck('HLPR'))
    console.log(`INFO | EST Date String Generated Value: ${estDateAsString}`)
  return estDateAsString
}

/**
 * TODO: Complete JSDoc
 * @returns {number}
 */
export function getDateAsCustomEpoch() {
  const customEpoch = Date.now() - process.env.API_CUSTOM_EPOCH
  if(logLevelPresentCheck('HLPR'))
    console.log(`INFO | Custom Epoch Generated Value: ${customEpoch}`)
  return customEpoch
}

/**
 * TODO: Add JSDoc
 */
export function handleAppListen(err) {
  if (err) {
    if(logLevelPresentCheck('ERR'))
      console.log({
        'PubSub Logging': {
          level: 'ERR',
          content: {
            message: `PubSub ${process.env.API_NODE_NAME} failed to start.`,
            err: err
          },
          diagnostics: {
            node:     process.env.API_NODE_NAME,
            datetime: getDateAndTimeAsString(),
            os: {
              process:    process.pid,
              cpus:       os.cpus(),
              arch:       os.arch(),
              endianness: os.endianness(),
              version:    os.version(),
              memory: {
                free:  os.freemem(),
                total: os.totalmem()
              },
              priority:   os.getPriority(process.pid),
              hostname:   os.hostname(),
            }
          }
        }
      })
  } else {
    if(logLevelPresentCheck('SYS'))
      console.log({
        'PubSub Logging': {
          level: 'SYS',
          content: {
            message: `PubSub ${process.env.API_NODE_NAME} started successfully.`
          }
        }
      })
  }
}

/**
 * TODO: Add JSDoc
 */
export function preventMissingAuthToken(req,res,next) {
  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
    return next()

  if(req.headers.authorization !== undefined){
    res.locals.authToken = req.headers.authorization.split(' ')[1]
    return next()
  }

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            status:  'Error',
            code:    403,
            message: `Missing request authorization credentials.`
          }
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  res.status(403).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Missing request authorization credentials.`,
        worker:  process.pid
      }
    }
  })
}

/**
 * TODO: Add JSDoc
 */
export function preventInvalidAuthToken(req,res,next) {
  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
    return next()

  jwtVerify(res.locals.authToken, process.env.API_JWT_SECRET, {})
    .then(contents => {
      // console.log(contents)
      if(logLevelPresentCheck('INFO'))
        console.log({
          "PubSub Logging": {
            level: 'INFO',
            tokenContents: contents
          }
        })
      res.locals.authTokenContents = contents
      next()
    })
    .catch(() => {
      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Logging": {
            level: 'ERR',
            response: {
              header: {
                status:  'Error',
                code:    403,
                message: `Invalid auth token provided.`,
                worker:  process.pid
              }
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            }
          }
        })
      res.status(400).send({
        "PubSub Response": {
          header: {
            status:  'Error',
            code:    403,
            message: `Invalid auth token provided.`,
            worker:  process.pid
          }
        }
      })
    })
}

/**
 * TODO: Add JSDoc
 */
export function preventIncorrectAuthTokenAJWT(req, res, next) {
  // Ignoring if hCaptcha request
  if(res.locals.hCaptchaSuccess)
    return next()

  const {type} = res.locals.authTokenContents

  // Continue when request has/is:
  // - A validated AJWT
  // - A validated hCaptcha request
  if(type === 'ajwt' || res.locals.hCaptchaSuccess)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            code:    403,
            message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
            err:     'Expecting token type: SJWT'
          },
          err: "Expecting a valid ajwt."
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
        worker:  process.pid
      },
      err: "Expecting a valid ajwt."
    }
  })
}

/**
 * TODO: Add JSDoc
 */
export function preventIncorrectAuthTokenSJWT(req, res, next) {
  const {type} = res.locals.authTokenContents

  // Requesting token is not a valid AJWT or HJWT - Ignore
  if(type === 'sjwt')
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        content: {
          response: {
            code:    403,
            message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
            err:     'Expecting token type: SJWT'
          }
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })
  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    403,
        message: `Incorrect auth token provided... Why (•ิ_•ิ)?`,
        worker:  process.pid
      },
      err: "Expecting SJWT"
    }
  })
}

/**
 * TODO: Add JSDoc
 */
 export function isTrustedSource(req, res, next) {
  const msSecureKey = req.headers['ms-secure-key']

  if(msSecureKey !== process.env.API_MS_SECURE_KEY)
    return res.status(403).send({
      'PubSub Response': {
        header: {
          status:   'Error',
          code:     403,
          response: 'Request was not from a trusted internal node source!',
          node:     process.env.API_NODE_NAME,
          worker:   process.pid
        }
      }
    })

  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        level: 'INFO',
        content: {
          isTrustedSource: res.locals.msSecured
        }
      }
    })
    
  return next()
}

/**
 * TODO: Add JSDoc
 */
export function handleHeathCheck(req, res) {
  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        level: 'INFO',
        response: {
          status: 'Healthy',
          code: 200,
          message: 'Request is responsive!'
        }
      }
    })
  res.status(200).send({
    'PubSub Response': {
      header: {
        status:   'Healthy',
        code:     200,
        response: 'Request is responsive!',
        node:     process.env.API_NODE_NAME,
        worker:   process.pid
      }
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export function preventMalformedHCaptcha(req,res,next)
{
  let invalidFields = []
  const {
    hCaptcha={
      //ip:undefined,
      response:undefined,
      siteKey:undefined
    }
  } = req.body

  // If hCaptcha was not provided, skip this
  if(req.body.hCaptcha === undefined)
    return next()

  //hCaptcha.ip       === undefined ? invalidFields.push('Missing hCaptcha.ip')       : {}
  hCaptcha.response === undefined ? invalidFields.push('Missing hCaptcha.response') : {}
  hCaptcha.siteKey  === undefined ? invalidFields.push('Missing hCaptcha.siteKey')  : {}

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            status:  'Error',
            code:    400,
            message: 'hCaptcha object is malformed.',
            pid:     process.pid
          },
          err: invalidFields
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'hCaptcha object is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function verifyHCaptchaToken(req,res,next)
{
  // If hCaptcha was not provided - skip...
  if(req.body.hCaptcha === undefined)
    return next()

  const verifyURL = "https://hcaptcha.com/siteverify"
  const {response, siteKey} = req.body.hCaptcha

  // If no token was provided, ignore this step
  if(response === undefined)
    return next()

  const params = new URLSearchParams()
  params.append('response', response)
  params.append('secret', process.env.API_HCAPTCHA_SECRET)
  params.append('siteKey', siteKey)

  // TODO: Send Axios Request
  axios.post(verifyURL,
    params
  )
    .then(response => {
      const { data } = response,
        errorCodes = data['error-codes'],
        success    = data.success

      if(success)
      {
        res.locals.hCaptchaSuccess = true
        return next()
      }

      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Logging": {
            level: 'ERR',
            response: {
              header: {
                status: 'Error',
                code:   400,
                message: 'hCaptcha validation failed.',
                process: process.pid
              },
              err: errorCodes,
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            }
          }
        })

      return res.status(400).send({
        "PubSub Response": {
          header: {
            status: 'Error',
            code:   400,
            message: 'hCaptcha validation failed.',
            process: process.pid
          },
          err: errorCodes
        }
      })

    })
    .catch(err => {
      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Response": {
            level: 'ERR',
            response: {
              header: {
                status:  'Error',
                code:    400,
                message: 'hCaptcha responded with a request error',
                pid:     process.pid
              },
              err: err
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            }
          }
        })
      return res.status(400).send({
        "PubSub Response": {
          header: {
            status:  'Error',
            code:    400,
            message: 'hCaptcha responded with a request error',
            pid:     process.pid
          },
          err: err
        }
      })
    })
}