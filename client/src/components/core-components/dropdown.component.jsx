/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Dropdown
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { MISC_COLORS } from '../../constants/themes.constants.js'
import Toggle from './toggle.component.jsx'
import { ReactComponent as ArrowSVG } from '../../assets/svgs/up-arrow.svg'
import DropdownMenu from './dropdownmenu.component.jsx'
import PropTypes from 'prop-types'

const ArrowSVGStyle = styled(ArrowSVG)`
  fill: ${props => props.color};
  height: 6px;
  transform: rotate(180deg);
`

const DropdownViewer = styled.div`

  display:  flex;
  
  justify-content: center;
  align-items:     center;
  
  position: relative !important;
  opacity:  100%     !important;
  
  width:      ${props => props.width}px    !important;
  min-width:  ${props => props.minWidth}px !important;
  max-width:  ${props => props.maxWidth}px !important;

  margin:  ${props => props.margin}  !important;

  /* Cannot highlight text */
  -webkit-touch-callout: none;
  -webkit-user-select:   none;
  -khtml-user-select:    none;
  -moz-user-select:      none;
  -ms-user-select:       none;
  user-select:           none;
`

const DropdownContainer = styled.div`
    width: calc(100% - 24px);
`

const DropdownWrapper = styled.div`
  display:         flex;
  align-items:     center;
  justify-content: space-between;
  position:        relative !important;
  
  opacity: 100% !important;
  
  height: ${props => props.height}px;
  width:  calc(100% - 24px);
  
  background-color: ${props => props.backgroundColor} !important;
  border-color:     ${props => props.borderColor}     !important;
  
  border-width:  ${props => props.borderWidth}px  !important;
  border-style:  ${props => props.borderStyle}    !important;
  border-radius: ${props => props.borderRadius}px !important;
  
  margin: 3px 0 1px 0;
  padding: 0 12px !important;

  &:hover {cursor: ${props => props.enabled ? 'pointer' : 'default'};}
`

const DropdownText = styled.div`
  color:     white;
  font-size: 14px;
  opacity:  ${props => props.enabled ? 1 : 0.1};
`

const IconContainer = styled.div`
  height:          auto;
  width:           50px;
  display:         flex;
  align-items:     center;
  justify-content: center;
  fill:            ${props => props.enabled ? '#ffffff' : props.color};
  margin-right:    5px;
  
  margin-top:    ${props => props.marginTop};
  margin-bottom: ${props => props.marginBottom};
  & > svg {
    height: 20px;
  }
`

const TitleWrapper = styled.div`
  display:    flex;
  text-align: left;
  font-size:  13px;
  color:      white;
  
  width: 100%;

  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  
  & > div { margin-right: 5px; }
`

const FootnoteWrapper = styled.div`
  display: flex;
  font-size: 12px;

  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  
  & > div:nth-child(1){
    color: gray;
  }
  & > div:nth-child(2){
    margin: 0 auto;
  }
  & > div:nth-child(3){
    color: white;
  }
`

const LinkStyled = styled(Link)`
  color:           ${MISC_COLORS.link.default};
  text-decoration: none;

  &:hover  {color: ${MISC_COLORS.link.hover}}
  &:active {color: ${MISC_COLORS.link.default}}
`

export default
function Dropdown ({
  enabled,
  eBackgroundColor,
  dBackgroundColor,
  eBorderColor,
  dBorderColor,
  displayToggle,
  toggleInitState,
  title,
  footnote,
  footnoteLink,
  footnoteCallback,
  icon,
  selection,
  required,
  width,
  maxWidth,
  minWidth,
  height,
  minHeight,
  maxHeight,
  margin,
  stateMirror,
  borderRadius,
  borderStyle,
  borderWidth,
  padding,
  fontSize,
  fontColor,
  items
}) {
  /* State-Based Variables */
  const [isEnabled, setEnabled] = useState(enabled)
  const [displayMenu, setDisplayMenu] = useState(false)

  /* Functions for Field Wrappers */
  const backgroundColor = isEnabled ? eBackgroundColor : dBackgroundColor
  const borderColor = isEnabled ? eBorderColor : dBorderColor

  const handleToggleRender = () => {
    if (displayToggle) {
      return (
        <>
          <div style={{ width: 10 }}/>
          <Toggle
            backgroundColor={backgroundColor}
            borderColor ={borderColor}
            stateMirror ={e => setEnabled(e)}
            toggled ={toggleInitState}
            margin={`${adjustMarginTop()} 0 ${adjustMarginBottom()} 0`}
          />
        </>
      )
    }
  }

  const adjustMarginTop = () => {
    if (typeof title !== 'undefined') { return '19px' } else { return '0' }
  }

  const adjustMarginBottom = () => {
    if (typeof footnote !== 'undefined') { return '16px' } else { return '0' }
  }

  const handleIconRender = () => {
    if (typeof icon !== 'undefined') {
      return (
        <IconContainer
          enabled ={isEnabled}
          color ={borderColor}

          marginTop ={adjustMarginTop()}
          marginBottom ={adjustMarginBottom()}
        >
          {icon}
        </IconContainer>
      )
    }
  }

  const handleTitleRender = () => {
    return (
      <TitleWrapper>
        <div>{title}</div>
        <div style={{ color: MISC_COLORS.requirementRed }}>{required ? '*' : ''}</div>
      </TitleWrapper>
    )
  }

  const handleTextRender = () => {
    const selectionText = selection.text

    /* TODO: Implement useRef() in the future for width calculations... */
    if (selectionText.length > 33) { return selectionText.slice(0, 33) + '...' } else { return selectionText }
  }

  const handleFootnoteRender = () => {
    return (
      <FootnoteWrapper>
        {
          typeof footnoteLink !== 'undefined'
            ? <LinkStyled to={footnoteLink}>
              <div>{footnote}</div>
            </LinkStyled>
            : <div>{footnote}</div>
        }
        <div/>
      </FootnoteWrapper>
    )
  }

  const handleDropdownClick = () => {
    if (enabled) { setDisplayMenu(!displayMenu) }
  }

  return (
    <DropdownViewer
      width ={width}
      maxWidth ={maxWidth}
      minWidth ={minWidth}
      margin ={margin}
    >

      { handleIconRender() }

      {/* Handle Title, Dropdown (Icon, Field, Toggle), and Footnote */}
      <DropdownContainer>

        {handleTitleRender()}

        {/* Dropdown Contents */}
        <DropdownWrapper
          backgroundColor ={backgroundColor}
          borderColor ={borderColor}
          borderStyle ={borderStyle}
          borderWidth ={borderWidth}
          borderRadius ={borderRadius}
          padding ={padding}
          fontSize ={fontSize}
          fontColor ={fontColor}
          enabled ={isEnabled}
          onClick ={handleDropdownClick}
          height ={height}
          maxHeight ={maxHeight}
          minHeight ={minHeight}
        >

          <DropdownMenu
            backgroundColor={backgroundColor}
            borderColor ={borderColor}
            borderStyle ={borderStyle}
            borderWidth ={borderWidth}
            borderRadius ={borderRadius}
            padding ={padding}
            fontSize ={fontSize}
            fontColor ={fontColor}
            items ={items}
            toggled ={displayMenu}
            stateMirror ={state => {
              setDisplayMenu(false)
              stateMirror(state)
            }}
            onOffClick={() => setDisplayMenu(false)}
          />

          <DropdownText enabled={isEnabled}>
            { handleTextRender() }
          </DropdownText>

          <ArrowSVGStyle color={borderColor}/>

        </DropdownWrapper>

        {handleFootnoteRender()}

      </DropdownContainer>

      {handleToggleRender()}

    </DropdownViewer>
  )
}

/* Defining Default Values for Props */
Dropdown.displayName = 'Dropdown'
Dropdown.propTypes = {
  /* Callbacks */
  stateMirror: PropTypes.func,
  toggleMirror: () => {},

  /* Items to render */
  initItem: PropTypes.object,
  items: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),

  /* Toggle Specific State */
  displayToggle: PropTypes.bool,
  toggleInitState: PropTypes.bool,

  /* Additional options */
  icon: PropTypes.element,
  title: PropTypes.string,
  footnote: PropTypes.string,
  footnoteLink: PropTypes.string,
  footnoteCallback: PropTypes.func,

  /* Determines rendering and behavior */
  required: PropTypes.bool,
  enabled: PropTypes.bool,

  /* Styling Defaults */
  eBackgroundColor: PropTypes.string,
  eBorderColor: PropTypes.string,
  dBackgroundColor: PropTypes.string,
  dBorderColor: PropTypes.string,
  /* ~ */
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  /* ~ */
  fontSize: PropTypes.number,
  fontColor: PropTypes.string,
  /* ~ Adjust Container Width ~ */
  width: PropTypes.number,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  /* ~ Adjust Dropdown Height ~ */
  height: PropTypes.number,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,

  selection: PropTypes.object,

  margin: PropTypes.string,
  padding: PropTypes.number
}
Dropdown.defaultProps = {
  /* Callbacks */
  stateMirror: () => {},
  toggleMirror: () => {},

  /* Items to render */
  initItem: {
    id: 'Null',
    text: 'Empty'
  },
  items: [{
    id: 'Null',
    text: 'Empty'
  }],

  displayToggle: false,
  toggleInitState: false,

  footnoteCallback: () => {},

  required: false,
  enabled: true,

  eBackgroundColor: '#000000',
  eBorderColor: '#ffffff',
  dBackgroundColor: '#000000',
  dBorderColor: '#ffffff',

  borderStyle: 'solid',
  borderWidth: 1.5,
  borderRadius: 8,

  fontSize: 14,
  fontColor: '#fff',

  minWidth: 200,

  height: 45,

  margin: '3px 0 1px 0',
  padding: 15
}
