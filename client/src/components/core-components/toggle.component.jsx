/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Toggle
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

// justify-nav: ${props => props.toggled ? "flex-end" : "flex-start"};

const ToggleContainer = styled.div`
  
`

const ToggleWrapper = styled.div`
  
  display:  flex;
  position: relative !important;
  
  opacity: 100% !important;
  
  height:     ${props => props.height}px    !important;
  min-height: ${props => props.minHeight}px !important;
  max-height: ${props => props.maxHeight}px !important;
  
  width:      ${props => props.width}px    !important;
  min-width:  ${props => props.minWidth}px !important;
  max-width:  ${props => props.maxWidth}px !important;
  
  background-color: ${props => props.backgroundColor} !important;
  border-color:     ${props => props.borderColor}    !important;
  
  border-width:  ${props => props.borderWidth}px  !important;
  border-style:  ${props => props.borderStyle}    !important;
  border-radius: ${props => props.borderRadius}px !important;
  
  margin: ${props => props.margin} !important;
  
  &:hover {
    cursor: pointer;
  }
`

const ToggleIndicator = styled.div`
  
  /* Displays and Hides Notification Panel */
  transition: all 0.1s ease;
  
  height: auto;
  
  background-color: ${props => props.toggled ? props.toggledColor : props.untoggledColor};
  width: 40%;
  
  border-radius: 3px;
  margin: 3px 3px 3px ${props => props.toggled ? '50%' : '3px'};
`

export default
function Toggle ({
  stateMirror,
  toggled,
  backgroundColor,
  borderColor,
  borderStyle,
  borderWidth,
  borderRadius,
  width,
  maxWidth,
  minWidth,
  height,
  maxHeight,
  minHeight,
  margin,
  padding,
  toggledColor,
  untoggledColor
}) {
  const [isToggled, setToggled] = useState(toggled)

  /* Event Updates */
  useEffect(() => {
    /* Pass the state up */
    stateMirror(isToggled)
  }, [isToggled])

  return (
    <ToggleContainer>
      <ToggleWrapper
        onClick={() => { setToggled(!toggled) }}
        backgroundColor={backgroundColor}
        borderColor ={borderColor}

        borderStyle ={borderStyle}
        borderWidth ={borderWidth}
        borderRadius={borderRadius}

        width ={width}
        maxWidth ={maxWidth}
        minWidth ={minWidth}

        height ={height}
        maxHeight ={maxHeight}
        minHeight ={minHeight}

        margin ={margin}
        padding ={padding}

        toggledColor={toggledColor}
        untoggledColor={untoggledColor}

        toggled={isToggled}
      >
        <ToggleIndicator
          toggledColor={toggledColor}
          untoggledColor={untoggledColor}

          toggled={isToggled}
        />
      </ToggleWrapper>
    </ToggleContainer>
  )
}

Toggle.displayName = 'Toggle'
Toggle.propTypes = {
  stateMirror: PropTypes.func,
  toggled: PropTypes.bool,
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  width: PropTypes.number,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  height: PropTypes.number,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  margin: PropTypes.number,
  padding: PropTypes.number,
  toggledColor: PropTypes.string,
  untoggledColor: PropTypes.string
}
Toggle.defaultProps = {
  stateMirror: () => {},

  enabled: true,
  toggled: false,

  backgroundColor: '#000000',
  borderColor: '#ffffff',

  toggledColor: '#49a34f',
  untoggledColor: '#1d1d1d',

  borderStyle: 'solid',
  borderWidth: 1.5,
  borderRadius: 8,

  width: 35,
  maxWidth: undefined,
  minWidth: undefined,

  height: 45,
  maxHeight: undefined,
  minHeight: undefined,

  margin: '1px 0 1px 0',
  padding: 15
}
