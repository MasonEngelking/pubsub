import {getDateAsCustomEpoch} from "./global.middleware"

let availableBubbles = []

// const example = [
//   {
//     buid: 123456789,
//     messages: [
//      {
//        uuid: 34092392304,
//        muid: 23049823049,
//        contents: "Woah, I'm a message!"
//      }
//     ]
//   }
// ]

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function handleBubbleCreate() {
  // TODO: Add the received bubble to the above array to be
}

export function handleBubbleDelete() {
  // TODO: Delete the specified bubble from the above array
}

export function handleMessageCreate() {
  // TODO: Add the received message to the above array
}

export function handleMessageDelete() {
  // TODO: Delete the specified message from the above array
}

export function handleMessageQuery(){

}

/**
 * @description Every second updates the availableBubbles array with Bubbles
 * @returns {Promise<void>}
 * @constructor
 */
export async function PurgeExpiredBubblesHandler() {
  availableBubbles = availableBubbles.filter(bubble => {
    const { exp } = bubble
    if (exp > getDateAsCustomEpoch())
      return bubble
  })
  await sleep(1000)
  await PurgeExpiredBubblesHandler()
}