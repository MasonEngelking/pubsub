/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Container
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import { Outlet } from 'react-router-dom'
import styled from 'styled-components'
import { ReactComponent as CompanyLogoRep } from '../../assets/svgs/logo_rep_emblem.svg'
import { ReactComponent as CompanyLogoText } from '../../assets/svgs/logo_text_full.svg'
import { ReactComponent as StaryNightSVG } from '../../assets/svgs/stary.animated.svg'

const StaryNightSVGStyled = styled(StaryNightSVG)`
  position: absolute;
  opacity: 0.5;
  flex-shrink: 0;
  min-width: 100%;
  min-height: 100%
`

const CompanyLogoRepStyled = styled(CompanyLogoRep)`
  fill:   white;
  height: 95%;
  margin-right: 12px;
`

const CompanyLogoTextStyled = styled(CompanyLogoText)`
  fill:   white;
  height: 75%;
  opacity: 0.3;
`

const AccountPortalContainer = styled.div`
  height:          100vh;
  width:           100vw;
  position:        relative;
  display:         flex;
  justify-content: center;
  align-items:     center;
`

const ApplicationLogoContainer = styled.div`
  position:        absolute;
  display:         flex;
  justify-content: center;
  align-items:     center;
  left:            20px;
  top:             20px;
  height:          30px;
`

export default
function AccountPortal (props) {
  return (
    <AccountPortalContainer>

      {/* Subject to change */}
      <StaryNightSVGStyled/>

      {/* Once all the assets are acquired, insert them here... */}
      <ApplicationLogoContainer>
        <CompanyLogoRepStyled/>
        <CompanyLogoTextStyled/>
      </ApplicationLogoContainer>

      {/* Render the centered Fields */}
      <Outlet/>

    </AccountPortalContainer>
  )
}
