/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/*
* Note: Colors are in descending order from darkest to brightest
*/

export const SECTION_COLORS = {
  signUp: ['#35bd71', '#148d4a'],
  signIn: ['#4b78db', '#1442aa'],
  recovery: ['#35bd71', '#148d4a'],
  guest: ['#9861cd', '#7735bb'],
  homepage: ['#e19350', '#ce7121']
}

export const SUBJECT_COLORS = {
  welcome: ['#35bd71', '#148d4a'],
  safety: ['#4b78db', '#1442aa'],
  legal: ['#e19350', '#ce7121'],
  credentials: ['#9861cd', '#7735bb'],
  verification: ['#cd6161', '#bb3535']
}

export const NOTIFICATION_COLORS = {
  default: '#555555',
  warning: '#e7cc35',
  info: '#e78735',
  error: '#e73535',
  update: '#358be7',
  success: '#1ecc2a'
}

export const MISC_COLORS = {
  requirementRed: '#e81c1c',
  availableGreen: '#49a34f',
  link: {
    default: '#4c94ff',
    hover: '#88b8ff'
  },
  app: {
    primary: 'rgb(111, 49, 200)',
    second: 'rgb(92, 2, 228)'
  },
  disclaimer: {
    borderColor: '#e7cc35',
    backgroundColor: 'rgba(231,204,53,0.12)'
  }
}

/* Global Theming */
export const DARK_THEME = {

  /* ~~~ */
  core: {
    fontColor: '#ffffff'
  },

  /* ~~~ */
  notification: {
    backgroundColor: '#0b0c0f'
  },

  /* ~~~ */
  site: {
    backgroundColor: '#0f1116'
  },

  /* ~~~ */
  form: {
    backgroundColor: '#0d0f13'
  },

  /* ~~~ */
  navbar: {
    backgroundColor: '#09090c',
    borderColor: '#1f242e'
  },

  tooltip: {
    backgroundColor: '#09090c',
    borderColor: '#1f242e'
  },

  /* ~~~ */
  button: {
    enabled: {
      backgroundColor: '#080808',
      highlight: '#151515',
      selected: '#151515',
      borderColor: '#1d1d1d',
      fill: '#ffffff'
    },
    disabled: {
      backgroundColor: '#0b0c0f',
      highlight: '#0b0c0f',
      selected: '#0b0c0f',
      borderColor: '#0b0c0f',
      fill: '#1a1c23'
    },
    submit: {
      backgroundColor: '#49a34f',
      highlight: '#57bd5d',
      selected: '#57bd5d',
      border: '#49a34f'
    }
  },

  /* ~~~ */
  field: {
    enabled: {
      backgroundColor: '#0b0c0f',
      borderColor: '#1a1c23',
      text: '#ffffff'
    },
    disabled: {
      backgroundColor: '#121319',
      borderColor: '#1a1c23',
      text: '#1d1d1d'
    }
  },

  /* ~~~ */
  toggle: {
    backgroundColor: '#0b0c0f',
    borderColor: '#1a1c23',
    enabled: '#49a34f',
    disabled: '#1d1d1d'
  },

  /* ~~~ */
  checkbox: {
    enabled: {
      backgroundColor: '#16181e',
      borderColor: '#20222c'
    },
    disabled: {
      backgroundColor: 'rgba(22,24,30,0.2)',
      borderColor: 'rgba(32,34,44,0.2)'
    },
    misc: {
      iconColor: '#ffffff',
      toggledColor: '#21bf33',
      toggledBorderColor: '#67e075'
    }
  },

  /* ~~~ */
  listItem: {
    highlight: '#15181f',
    borderColor: '#191d25'
  },

  /* ~~~ */
  hubs: {
    backgroundColor: '#15171f',
    borderColor: ''
  },

  miscMenu: {
    backgroundColor: '#0d0f13',
    borderColor: ''
  },

  /* ~~~ */
  imageSelector: {
    containerColor: '#0d0f13',
    borderColor: '#54627d',
    areaColor: '#1d222a',
    iconColor: '#ffffff',
    textColor: '#ffffff'
  },

  /* ~~~ */
  bubbleVote: {
    backgroundColor: '#0b0c0f',
    borderColor: '#1a1c23',
    text: '#ffffff'
  },

  /* ~~~ */
  call: {
    backgroundColor: '#08080a'
  }
}

export const LIGHT_THEME = {

}
