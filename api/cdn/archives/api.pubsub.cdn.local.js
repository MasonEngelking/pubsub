/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name CDN.API
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */

/* EXPRESS DEP */
import express    from 'express'
import path       from 'path'
import cors       from 'cors'

/* MIDDLE-WARE DEP */
import {
    imageProcessing
} from '../middleware.js'

/* OS DEP */
import os        from 'os'
import cluster   from 'cluster'
import fs        from 'fs'
import * as Path from 'path'

/* IMAGE PROC. DEP */
// import sharp from 'sharp'

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

/* APP BEHAVIOR CONFIGURATION */
const app = express()
app.use(cors())
app.use(express.json())
app.use((req, res, next) =>
{
    res.header('Access-Control-Allow-Origin',  '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    next()
})
app.use('/static-nav', express.static(path.join('/','ps-static_content')))

/* ~~~ MASTER PROCESS ~~~ */
if (cluster.isMaster)
{
    console.log(`Master process <${process.pid}> is running.`)

    /* For Workers */
    for(let i = 0; i < os.cpus().length; i++)
        cluster.fork()

    cluster.on('exit', (worker, code, signal) => console.log(`Worker ${worker.process.pid} died.`))
}
/* WORKER PROCESSES -> HANDLE API CALLS */
else {

    app.listen(process.env.API_PORT, err =>
    {
        if (err)
        {
            console.log({
                'PublicSquare Response': {
                    message: 'PS-CDN_SC failed to initialize!',
                    worker:  process.pid,
                    port:    process.env.API_PORT
                }
            })
        } else {
            console.log({
                'PublicSquare Response': {
                    message: 'PS-CDN_SC is Live!',
                    worker:  process.pid,
                    port:    process.env.API_PORT
                }
            })
        }
    })

    /* -------------------- *
    * TODO: Add JSDOC here!
    * --------------------- */
    app.post('/api/v1/cdn/upload/image/:directory',
        imageProcessing,
        (req, res) =>
        {
            const acceptableDirectories = [
                "bubble-images",
                "profile-images",
                "attachments"
            ]
            const directoryValid    = acceptableDirectories.includes(req.params.directory)
            const bubbleImageBuffer = req.body.bubbleImageBuffer
            const imageArray        = new Uint8Array(bubbleImageBuffer.data)
            const bubbleID          = req.body.bubbleID
            const fileName          = bubbleID + '.png'

            if(directoryValid)
            {
                /* TODO: Add Implementation */
            }

            if(bubbleImageBuffer === undefined && bubbleID === undefined)
            {
                console.log({
                    'PubSub Response': {
                        header: {
                            status: 404,
                            message: 'File and/or ID was not provided. Terminating the request.',
                            pid: process.pid
                        }
                    }
                })
                res.status(404).send({
                    'PubSub Response': {
                        header: {
                            status: 404,
                            message: 'File and/or ID was not provided. Terminating the request.',
                            pid: process.pid
                        }
                    }
                }).end()

            } else {

                fs.writeFile(`/ps-static_content/bubble-imgs/${fileName}`, imageArray, err =>
                {
                    if(err)
                    {
                        console.log({
                            'PubSub Response': {
                                header: {
                                    status: 500,
                                    message: 'Failed to write to filesystem.',
                                    pid: process.pid
                                },
                                payload: {...err}
                            }
                        })
                        res.status(500).send({
                            'PubSub Response': {
                                header: {
                                    status: 500,
                                    message: 'Failed to write to filesystem.',
                                    pid: process.pid
                                },
                                payload: {...err}
                            }
                        })
                    } else {
                        console.log({
                            'PubSub Response': {
                                header: {
                                    status: 200,
                                    message: 'Successfully written image to CDN.',
                                    pid: process.pid
                                }
                            }
                        })
                        res.status(200).send({
                            'PubSub Response': {
                                header: {
                                    status: 200,
                                    message: 'Successfully written image to CDN.',
                                    pid: process.pid
                                }
                            }
                        })
                    }
                })
            }
        })
}