/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name User.Model
 *
 * @returns {MongoDB_Model} MongoDB Model
 *
 * @author Christian C. Larcomb
 */

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

import mongoose               from 'mongoose'
import mongooseLong           from 'mongoose-long'
import {getDateAsCustomEpoch} from '../global.middleware.js'
import {getSnowflakeID}       from '../middleware.js'
mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const userSchema = new mongoose.Schema({

    _id: {type: Long, default: await getSnowflakeID()}, // UUID

    identifier: {
        usernames: [
            {
                active:       {type: Boolean, default: true},
                content:      String,
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ],
        tags: [
            {
                active:       {type: Boolean, default: true},
                content:      String,
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ],
        bios: [
            {
                active:       {type: Boolean, default: true},
                content:      String,
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ]
    },

    media: {
        avatar: [
            {
                active:       {type: Boolean, default: true},
                contentURL:   String,
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ],
        banner: [
            {
                active:       {type: Boolean, default: true},
                contentURL:   String,
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ],
        ambience: [
            {
                active:       {type: Boolean, default: true},
                colorHex:     {type: String, default: '#2A2DEE'},
                dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                suid:         Long
            }
        ]
    },

    status: {
        vibes: {
            level: {type: Number, default: 50},
            interactions: [
                {
                    uuid:           {type:Long, ref:'User'},
                    balance:        Number,
                    contextContent: String
                }
            ]
        },
        suspensions: [
            {
                overturned:      Boolean,
                reason:          String,
                underReview:     Boolean,
                expirationEpoch: Long,
                dateEpoch:       Long,
                modInduced:      Boolean,
                enactingModerator: {
                    uuid:     Long,
                    username: String,
                    tag:      String
                }
            }
        ],
        deletions: [
            {
                meta: {
                    reason:          String,
                    dateEpoch:       Long,
                    completionEpoch: Long     // Typically, 60 days after initiation
                },
                underReviewLog: [
                    {
                        initialization: {
                            reason:          String,
                            expirationEpoch: Long,    // Max expiration for review is 60 days for final decision
                            staff: { type: Long, ref: 'Staff' }
                        },
                        conclusion: {
                            status: Boolean,
                            reason: String,
                            staff: { type: Long, ref: 'Staff' }
                        },
                    }
                ],
                prevented: {
                    status:     Boolean,
                    reason:     String,
                    dateEpoch:  Long,
                    modInduced: Boolean,
                    enactingModerator: {
                        uuid:     Long,
                        username: String,
                        tag:      String
                    }
                },

            }
        ],
        staff: { account: {type: Long, ref: 'Staff'} },
        verifications: [
            {
                active:          Boolean,
                reason:          String,
                dateEpoch:       Long,
                expirationEpoch: Long,
                enactingModerator: {
                    uuid:     Long,
                    username: String,
                    tag:      String
                }
            }
        ],
        tiers: [
            {
                tuid:            Long,
                content:         {type: String, default: 'F'},  // F, 1S/AD, 1S, 2S, 3S, 4S, 5S
                dateEpoch:       {type: Long, default: getDateAsCustomEpoch()},
                expirationEpoch: {type: Long, default: 0}
            }
        ],
        affiliations: [
          {
            status: [
                {
                    type:            String,  // Healthy, Deprecated, Blacklisted, Banned
                    reason:          String,
                    underReview:     Boolean,
                    expirationEpoch: Long,
                    dateEpoch:       Long,
                    enactingModerator: {
                        uuid:     Long,
                        username: String,
                        tag:      String
                    }
                }
            ],
            details: [{type:Long, ref:'Affiliate'}]
          }
        ],
    },

    peers: {
        preferences: {/* TODO: Needs to be enhanced */},
        friends: [
            {
                uuid:   {type:Long, ref:'User'},
                status: String,
                initiation: [
                    {
                        uuid:      Long,
                        dateEpoch: Long,
                    }
                ],
                confirmation: [
                    {
                        uuid:      Long,
                        dateEpoch: Long
                    }
                ],
                rejection: [
                    {
                        uuid:      Long,
                        dateEpoch: Long
                    }
                ]
            }
        ],
        blocked: [
            {
                uuid:      {type:Long, ref:'User'},
                status:    Boolean,
                dateEpoch: Long
            }
        ]
    },

    secured: {

        credentials: {
            email: {
                content:   {hashed: String},
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                suid:      Long,
                verified: {
                    content:   Boolean,
                    dateEpoch: Number
                }
            },
            passwords: [
                {
                    content:      {hashed: String},
                    updatingSjwt: {hashTrailing: String},
                    dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                    suid:         Long
                }
            ],
            dateOfBirths: [
                {
                    content:      {hashed: String},
                    updatingSjwt: {hashTrailing: String},
                    dateEpoch:    {type: Long, default: getDateAsCustomEpoch()},
                    suid:         Long
                }
            ]
        },

        recovery: {
            codes: [
                {
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                    codeSet:   {hashes: [String]},
                    suid:      Long,
                }
            ],
            phones: [
                {
                    verified:          Boolean,
                    dateEpoch:         Long,
                    dateVerifiedEpoch: Long,
                    suid: {Long},
                    number: {
                        hashed: String,
                        trail:  String,
                    }
                }
            ],
            twoFactorAuth: [
                {
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                    // TODO: Determine what information to put here...
                }
            ]
        },

        sessions: [{
            suid: Long,
            sjwt: {
                hashed:    String,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()}
            },
            active:    {type: Boolean, default: true},
            dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
            platform: {
                description:  String,
                layout:       String,
                manufacturer: String,
                name:         String,
                os:           String,
                prerelease:   String,
                product:      String,
                ua:           String,
                version:      String,
                architecture: String,
                family:       String
            },
            networkInfo: {
                ip: {hashed: String}
            }
        }],

        wallet: {
            comets:  {type:Number, default: 0},
            ledgers: [{ luid: {type:Long, ref: 'Ledger'} }]
        }
    }
}, {_id: false})

userSchema.query.secureSelection = function () {
    return this.select(`
        -_id
        
        -meta.ajwt
        
        -media.avatar.sjwt
        -media.banner.sjwt
        -media.ambience.sjwt
        
        -secured.credentials
        -secured.session.sjwt
        -secured.recovery.codes.codeSet
        -secured.recovery.phones.num.hash
        -secured.recovery.securityQuestions
        -secured.sessions.sjwt
    `)
}
userSchema.query.unsecureSelection = function () {
    return this.select(`
        -_id
        
        -meta.ajwt
        
        -media.avatar.sjwt
        -media.banner.sjwt
        -media.ambience.sjwt
        
        -secured
    `)
}

export default mongoose.model('User', userSchema, 'Users')