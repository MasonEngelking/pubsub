/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import React, {
  useContext,
  useEffect,
  useState
} from 'react'
import Loading from '../../components/core-components/loading.component.jsx'
import styled,
{ ThemeContext } from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { ReactComponent as StaryNightSVG } from '../../assets/svgs/stary.animated.svg'

import { ReactComponent as InfoButton } from '../../assets/svgs/info.notify.svg'
import Button from '../../components/core-components/button.component'

const Container = styled.div`
  display:         flex;
  justify-content: center;
  place-items:     center;
  height:          100%;
  width:           100%;
`

const StaryNightSVGStyled = styled(StaryNightSVG)`
  position: absolute;
  opacity: 0.5;

  flex-shrink: 0;
  min-width: 100%;
  min-height: 100%;
  z-index: 1;
`

const ProgressBar = styled.div`
  height: 8px;
  background-color: #212121;
  margin: 0 0 20px 0;
  width: 100%;

  & > div {
    height: 100%;
    width: ${props => props.loadPercent}%;
    background-color: #1cd547;
    transition: width ease-in-out 0.2s;
  }
`

export default
function AppStatusView (props) {
  /* Theming & Redux */
  const theme = useContext(ThemeContext)
  const appManager = useSelector(state => state.appManager)

  // Leveraged for countdown rendering
  const [countdown, setCountdown] = useState(appManager.socketStatus.socketAttemptSeconds)

  const handleCountdown = (seconds) => {
    /* Prevent SetTimeout if App is not mounted yet */
    setTimeout(() => {
      if (seconds > 0) {
        setCountdown(seconds - 1)
        handleCountdown(seconds - 1)
      }
    }, 1000)
  }

  useEffect(() => { handleCountdown(appManager.socketStatus.socketAttemptSeconds) }, [appManager.socketStatus.socketAttemptSeconds])

  const handleCardRender = () => {
    if (appManager.isMounted) {
      return (
        <div
          style={{
            height: '425px',
            width: '325px',
            borderRadius: '10px',
            backgroundColor: theme.navbar.backgroundColor,
            color: 'white',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            gap: 6,
            zIndex: 2,
            position: 'relative'
          }}
        >
          <Loading
            scale={1.5}
          />
          <div
            style={{
              height: 35
            }}
          />
          <div
            style={{
              fontSize: 35,
              marginBottom: 10,
              fontWeight: 700
            }}
          >
                        🤕
          </div>
          <div
            style={{
              fontSize: 18
            }}
          >
                        We&apos;re having trouble connecting.
          </div>
          <div
            style={{
              fontSize: 18,
              marginBottom: 35
            }}
          >
                        Retrying in {countdown} seconds...
          </div>

          <div
            style={{
              position: 'absolute',
              bottom: 0,
              width: '100%'
            }}
          >
            <a
              href="https://status.pubsub.pub/"
              style={{ textDecoration: 'none' }}
              target={'_blank'} rel="noreferrer"
            >
              <Button
                margin={'10px'}
                backgroundColor={theme.navbar.backgroundColor}
                borderStyle={'none'}
                contents={
                  <div
                    style={{
                      display: 'flex',
                      gap: '6px',
                      justifyContent: 'left',
                      alignItems: 'center'
                    }}
                  >
                    <InfoButton
                      style={{
                        fill: 'white',
                        height: 16
                      }}
                    />
                    <div>
                                            Check App Status
                    </div>
                  </div>
                }
              />
            </a>
          </div>
        </div>
      )
    } else {
      return (
        <div
          style={{
            height: '400px',
            width: '325px',
            borderRadius: '10px',
            backgroundColor: theme.navbar.backgroundColor,
            color: 'white',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            gap: 6,
            zIndex: 2,
            position: 'relative'
          }}
        >
          <div style={{ height: 10 }}/>
          <Loading
            scale={1.5}
          />
          <div
            style={{
              height: 35
            }}
          />
          <ProgressBar
            loadPercent={appManager.readinessPercentage}
          >
            <div/>
          </ProgressBar>
          <h2 style={{ fontSize: 24, marginBottom: 20 }}>
            Starting...
          </h2>
          <h3 style={{ fontSize: 18, margin: 0 }}>
            Fun fact:
          </h3>
          <div
            style={{
              fontSize: 16,
              padding: '0 10px',
              textAlign: 'center',
              opacity: 0.3
            }}
          >
            We have easter eggs! (⌐■_■)
          </div>
        </div>
      )
    }
  }

  return (
    <Container>

      <StaryNightSVGStyled/>

      { handleCardRender() }
    </Container>
  )
}
