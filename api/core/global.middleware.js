/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
import os  from 'os'
import axios from "axios"

/**
 * Just a promise-based wrapper for jwtVerify function from jsonwebtoken
 */
function jwtVerify(token, secret, options) {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      secret,
      options,
      (err, contents) => err ? reject(err) : resolve(contents)
    )
  })
}

export function logLevelPresentCheck(logLevel){
  const logLevels = process.env.LOG_LEVEL.split(',')
  return logLevels.includes(logLevel)
}

/**
 * @name getCustomEpochTime
 * @memberOf api.v1.dba.get
 * @function
 * @param {Object} res - Returns response status code and JSON objects.
 * @description Returns whether or not the access_jwt exists. Expiration verification should be performed by EPC.
 * @returns {Object} {
 *     'PubSub Response': {
 *         header: {
 *            status: Number,
 *            message: String,
 *            pid: Number
 *         }
 *     }
 * }
 */
export function getDateAndTimeAsString() {
  const estDateAsString = new Date().toLocaleString('en-US', {timeZone: 'America/New_York'})
  if(logLevelPresentCheck('HLPR'))
    console.log(`INFO | EST Date String Generated Value: ${estDateAsString}`)
  return estDateAsString
}

/**
 * TODO: Complete JSDoc
 * @returns {number}
 */
export function getDateAsCustomEpoch() {
  const customEpoch = Date.now() - process.env.API_CUSTOM_EPOCH
  if(logLevelPresentCheck('HLPR'))
    console.log(`INFO | Custom Epoch Generated Value: ${customEpoch}`)
  return customEpoch
}

/**
 * TODO: Add JSDoc
 */
export function handleAppListen(err) {
  if (err) {
    if(logLevelPresentCheck('ERR'))
      console.log({
        'PubSub Logging': {
          level: 'ERR',
          content: {
            message: `PubSub ${process.env.API_NODE_NAME} failed to start.`,
            err: err
          },
          diagnostics: {
            node:     process.env.API_NODE_NAME,
            datetime: getDateAndTimeAsString(),
            os: {
              process:    process.pid,
              cpus:       os.cpus(),
              arch:       os.arch(),
              endianness: os.endianness(),
              version:    os.version(),
              memory: {
                free:  os.freemem(),
                total: os.totalmem()
              },
              priority:   os.getPriority(process.pid),
              hostname:   os.hostname(),
            }
          }
        }
      })
  } else {
    if(logLevelPresentCheck('SYS'))
      console.log({
        'PubSub Logging': {
          level: 'SYS',
          content: {
            message: `PubSub ${process.env.API_NODE_NAME} started successfully.`
          }
        }
      })
  }
}