/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import axios from 'axios'

export function checkEmailWithRegex (text) {
  const match = text.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
  return match !== null
}

export function checkPhoneWithRegex (number) {
  const match = number.match(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/)
  return match !== null
}

export function getDateAndTimeAsString () {
  return new Date().toLocaleString('en-US', { timeZone: 'America/New_York' })
}

export const SendAxiosRequest = (
  requestType,
  hCaptchaToken,
  hCaptchaStateSetter,
  axiosResponseSetter,
  clientInfo,
  requestContents
) => {
  /* Required Headers */
  const headers = {
    'Request-Type': requestType,
    'hCaptcha-Response-Key': hCaptchaToken,
    'hCaptcha-Site-Key': process.env.REACT_APP_HCAPTCHA_SITE_KEY
  }

  /* Device Information */
  const {
    userAgent,
    languages,
    vendor,
    deviceMemory,
    hardwareConcurrency,
    userAgentData,
    webdriver,
    plugins,
    cookieEnabled
  } = navigator

  /* Building Request Body */
  const body = {
    client: clientInfo,
    content: requestContents,
    meta: {
      dateCreated: getDateAndTimeAsString(),
      platform: {
        userAgent,
        languages,
        vendor,
        deviceMemory,
        hardwareConcurrency,
        userAgentData,
        webdriver,
        plugins,
        cookieEnabled
      }
    }
  }

  /* Sending the axios request */
  axios({
    method: 'post',
    url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/core/request`,
    data: body,
    headers
  })
    .then((res) => {
      hCaptchaStateSetter(true)
    })
    .catch((err) => {
      console.log(err)
      /* To Inform the User of Error Message */
      axiosResponseSetter({ code: err.response?.status })

      /* Update rendering */
      hCaptchaStateSetter(false)
    })
}
