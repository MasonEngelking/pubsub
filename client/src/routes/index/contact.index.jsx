/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Contact
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useRef, useState } from 'react'
import { Button, CircularProgress, TextField } from '@mui/material'
import styled from 'styled-components'
import {
  GoogleMap,
  useLoadScript,
  MarkerF
} from '@react-google-maps/api'
// import Modal from '../components/Modal.contact.component.jsx'
import {
  checkEmailWithRegex,
  checkPhoneWithRegex
} from '../../scripts/global.js'

const ContactContainer = styled.div`
  position:   fixed;
  height:     100%;
  width:      100vw;
  overflow:   hidden;
  color:      white;
  margin-top: 70px;
`

const ContentWrapper = styled.div`
  height: calc(100% - 70px);
  width:  100%;
  
  /* Styling Mobile */
  @media screen and (max-width: 1000px){
    overflow-y: scroll;
  }
`

const ContentGrid = styled.div`
  display:               grid;
  max-width:             1200px;
  grid-auto-rows:        1fr  auto;
  grid-template-columns: auto 1.3fr;
  gap:                   35px;
  margin:                auto;
  padding:               30px;
  
  @media screen and (min-width: 1000px){ height: calc(100% - 60px); }

  @media screen and (max-width: 1000px){
    height:                auto;
    grid-auto-rows:        auto 350px auto;
    grid-template-columns: 1fr;
    padding:               40px 20px 20px 20px;
  }
  
  & > div:nth-child(1) {
    @media screen and (min-width: 1000px){
      grid-area: 1 / 1 / span 2;
    }
  }
`

const MapHolder = styled.div`
  position:         relative;
  background-color: #cec6c2;
  border-radius:    40px;
  overflow:         hidden;
  
  filter: drop-shadow(0 10px 10px rgba(0,0,0,0.15));

  // Rounding Tab Highlight
  & > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(4) {
    border-radius: 40px;
    border-color: #151515 !important;
  }
  
  // Hiding Google Credentials
  & > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(15) {
    display: none;
  }
  // Hiding Google Credentials
  & > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(17) {
    display: none;
  }
`

const ContactInfoHolder = styled.div`
  border-radius: 40px;
  background:    linear-gradient(90deg, #353535, #202020);

  /* The Grid */
  & > div {
    margin:        35px;
    display:       grid;
    align-items:   center;
    justify-items: center;
    gap:           30px;
    color:         white;
    line-height:   18px;
    grid-template-columns: 1fr 1fr;

    @media screen and (max-width: 600px){
      height: 200px;
      justify-items: left;
      grid-template-columns: 1fr;
      gap: 0;
      margin: 30px;
    }

    /* Styling All Fields */
    & > div {
      & > h2 {
        font-size: 18px;
        font-family: Yeseva, sans-serif;
        font-weight: 100;
      }

      & > div {
        font-size: 14px;
        opacity: 0.7;
      }
    }

    /* Styling Communicate */
    & > div:nth-child(1) {
      & > div {
        display: grid;
        grid-template-columns: auto auto;
        gap: 4px;

        & > div:nth-child(2n) {
          margin-left: 10px;
        }
      }
    }
    
    /* Styling Address */
    & > div:nth-child(2) {
      max-width: 150px;
      min-width: 120px;
    }

    /* Styling Connect */
    & > div:nth-child(3) {
      & > div {
        display: grid;
        grid-template-columns: auto auto auto;
        gap: 8px;

        & > div {
          height: 30px;
          width: 30px;
          background-color: white;
        }
      }
    }
  }
`

const FormHolder = styled.div`
  display:         flex;
  justify-content: center;
  align-content:   center;
  
  @media screen and (min-width: 1000px){
    max-width:       550px;
    min-width:       400px;
  }

  & > h1 {
    font-family: CoralBlush, sans-serif;
    font-weight: 100;
    font-size:   100px;
    height: 90px;
    width: 300px;
    transform: rotate(-6deg);
    margin-top: -40px;
    margin-bottom: 40px;
  }

  /* Inner Grid */
  & > div {
    display: grid;
    gap:     13px;
    height:  auto;
    margin:  auto;
    width:   100%;
    
    @media screen and (min-width: 1000px){
      padding: 0 20px;
      grid-template-columns: auto auto;
    }

    /* Text */
    & > div:nth-child(1) {
      @media screen and (min-width: 1000px){
        grid-area: 1 / 1 / span 1 / span 2;
      }
      

      & > h1 {
        font-weight: normal;
        font-size: 80px;
        font-family: CoralBlush, sans-serif;
        transform: rotate(-4deg);
        margin-top:   -30px;
        margin-bottom: 10px;
      }
    }

    /* Msg */
    & > div:nth-child(6) {
      @media screen and (min-width: 1000px){
        grid-area: 4 / 1 / span 1 / span 2;
      }
      
    }

    /* Submit Button */
    & > button:nth-child(7) {
      @media screen and (min-width: 1000px){
        grid-area: 5 / 1 / span 1 / span 2;
      }
      
    }
  }
`

const DEFAULT_CLIENT_INFO_STATE = {
  name: '',
  company: '',
  email: '',
  phone: '',
  message: ''
}

export default
function Contact (props) {
  const containerRef = useRef()

  const [clientInfo, setClientInfo] = useState(DEFAULT_CLIENT_INFO_STATE)
  const [toggleModal, setToggleModal] = useState(false)
  const [isEmailValid, setIsEmailValid] = useState(undefined)
  const [isPhoneValid, setIsPhoneValid] = useState(undefined)

  const { isLoaded } = useLoadScript({ googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY })

  useEffect(() => {
    containerRef.current.style = window.innerHeight
  }, [containerRef, window.innerHeight])

  const RenderMap = () => {
    const longLat = {
      lat: 39.15674,
      lng: -75.52417
    }

    if (!isLoaded) {
      return (
        <div style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          placeContent: 'center',
          alignItems: 'center'
        }}>
          <CircularProgress/>
        </div>
      )
    }
    return (
      <GoogleMap
        zoom ={17}
        center={longLat}
        mapContainerClassName={'map-container'}
        options={{
          disableDefaultUI: true,
          disableDoubleClickZoom: true,
          minZoom: 17,
          maxZoom: 19,
          backgroundColor: '#cec6c2'
        }}
      >
        <MarkerF
          position={longLat}
          zIndex ={9999}
          title ={'PubSub LLC - Mailing Address'}
        />
      </GoogleMap>
    )
  }

  // const RenderModal = () => {
  //   return (
  //     <Modal
  //       toggle ={toggleModal}
  //       onClose ={async hCaptchaStatus => {
  //         /* Reset Everything Upon Success */
  //         if (hCaptchaStatus) { await setClientInfo(DEFAULT_CLIENT_INFO_STATE) }
  //
  //         await setToggleModal(false)
  //       }}
  //       data ={{ clientInfo }}
  //     />
  //   )
  // }

  const areRequiredFieldsPresent = () => {
    return (
      clientInfo.name !== '' &&
      clientInfo.email !== '' &&
      clientInfo.message !== '' &&
      isEmailValid && isPhoneValid
    )
  }

  return (
    <>
      {/* RenderModal() */}

      <ContactContainer ref={containerRef}>

        <ContentWrapper>

          <ContentGrid>

            <FormHolder>
              <div>
                <div>
                  <h1>Contact Us</h1>
                  <h2>Greetings</h2>
                  <div>Please reach out to us if you have any questions, comments, or concerns. We will reach back out as soon as possible!</div>
                </div>

                <>
                  <TextField
                    required
                    fullWidth
                    id ="fullWidth"
                    label ="Name"
                    variant ="outlined"
                    value ={clientInfo.name}
                    onChange={input => setClientInfo({ ...clientInfo, name: input.target.value })}
                    inputProps={{ maxLength: 36 }}
                  />

                  <TextField
                    fullWidth
                    id ="fullWidth"
                    label ="Company (Optional)"
                    variant ="outlined"
                    value ={clientInfo.company}
                    onChange={input => setClientInfo({ ...clientInfo, company: input.target.value })}
                    inputProps={{ maxLength: 36 }}
                  />

                  <TextField
                    required
                    fullWidth
                    id ="outlined-basic"
                    label ="Email"
                    error ={clientInfo.email !== '' && !isEmailValid}
                    variant ="outlined"
                    value ={clientInfo.email}
                    onChange ={input => {
                      /* Setting the state */
                      setClientInfo({ ...clientInfo, email: input.target.value })

                      /* Ensuring Valid Email */
                      setIsEmailValid(checkEmailWithRegex(input.target.value))

                      /* Resetting valid on empty */
                      if (input.target.value === '') { setIsEmailValid(undefined) }
                    }}
                    inputProps={{ maxLength: 120 }}
                  />

                  <TextField
                    fullWidth
                    id ="fullWidth"
                    label ="Phone Number (Optional)"
                    variant ="outlined"
                    value ={clientInfo.phone}
                    error ={clientInfo.phone !== '' && !isPhoneValid}
                    onChange={input => {
                      /* Updating Info */
                      setClientInfo({ ...clientInfo, phone: input.target.value })

                      /* Checking if the state is valid... */
                      setIsPhoneValid(checkPhoneWithRegex(input.target.value))

                      /* Valid if empty (Optional) */
                      if (input.target.value === '') { setIsPhoneValid(true) }
                    }}
                    inputProps={{ maxLength: 36 }}
                  />

                  <TextField
                    required
                    fullWidth
                    multiline={true}
                    id ="fullWidth"
                    label ="Message"
                    variant ="outlined"
                    rows ={4}
                    rowsMax ={4}
                    value ={clientInfo.message}
                    onChange={input => setClientInfo({ ...clientInfo, message: input.target.value })}
                    inputProps={{ maxLength: 300 }}
                  />

                  <Button
                    variant="contained"
                    sx ={{
                      height: '65px',
                      borderRadius: '8px',
                      backgroundColor: areRequiredFieldsPresent() ? '#353535' : '#858585',
                      opacity: areRequiredFieldsPresent() ? 1 : 0.5,
                      boxShadow: 'none',
                      '&:hover': {
                        backgroundColor: areRequiredFieldsPresent() ? '#252525' : '#858585',
                        boxShadow: areRequiredFieldsPresent() ? '' : 'none'
                      }
                    }}
                    onClick={() => {
                      if (areRequiredFieldsPresent()) { setToggleModal(true) }
                    }}
                  >
                    {areRequiredFieldsPresent() ? 'Submit' : 'Fields Required'}
                  </Button>
                </>
              </div>
            </FormHolder>

            <MapHolder>
              {RenderMap()}
            </MapHolder>

            <ContactInfoHolder>
              <div>

                <div>
                  <h2>Communicate</h2>
                  <div>
                    <div>Email</div>
                    <div>contact@pubsubpub.com</div>
                    <div>Phone</div>
                    <div>+1 (302) 298-1324</div>
                  </div>
                </div>

                <div>
                  <h2>Address</h2>
                  <div>
                    <div>8 The Green, STE B, Dover, Delaware, 19901, USA</div>
                  </div>
                </div>

              </div>
            </ContactInfoHolder>

          </ContentGrid>

        </ContentWrapper>

      </ContactContainer>
    </>
  )
}
