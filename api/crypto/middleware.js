/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
import {
  getDateAndTimeAsString,
  logLevelPresentCheck
} from './global.middleware.js'
import flakeID   from 'flake-idgen'
import intFormat from 'biguint-format'
import os        from 'os'
import jwt       from 'jsonwebtoken'
import * as crypto from 'crypto'
import AES from 'crypto-js/aes.js'
import Utf8 from 'crypto-js/enc-utf8.js'
import bcrypt from 'bcrypt'

/* Create Flake ID instance */
let snowflakeCounter = 0
const flake = new flakeID({
  epoch:   process.env.API_CUSTOM_EPOCH,
  counter: snowflakeCounter
})

async function handleSnowflakeGen(){
  /* Handle Snowflake Counter */
  snowflakeCounter++
  snowflakeCounter %= 4096

  /* Generate Snowflake with Counter */
  const snowflakeAsBuffer = await flake.next()
  const snowflakeAsInt = await intFormat(snowflakeAsBuffer, 'dec')

  return snowflakeAsInt
}

/**
 * TODO: Add JSDOC here!
 */
function handleCheck(focalPoint, permittedContent, typeOfCheck, res, next) {
  let check = permittedContent.filter(type => type === focalPoint).length > 0

  if(check)
    return next()

  res.status(400).send({
    'PubSub Response': {
      header: {
        status:  400,
        message: `Request config \'${typeOfCheck}\' is invalid.`,
        worker:  process.pid
      }
    }
  })
}

/**
 * Just a promise-based wrapper for jwtVerify function from jsonwebtoken
 */
function jwtVerify(token, options={}) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.API_JWT_SECRET, options, (err, contents) => err ? reject(err) : resolve(contents))
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function generateSnowflakeID(req, res, next) {
  const context = req.headers['context']
  if (context !== 'snowflake')
    return next()

  const genSnowflake = await handleSnowflakeGen()
  return res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Snowflake generated.',
        pid:     process.pid
      },
      payload: {
        snowflake: genSnowflake
      }
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function generateAccessKeys(req, res, next) {
  const context = req.headers['context']
  if (context !== 'access-keys')
    return next()

  const accessKeys = []
  const amountOfKeys = req.body?.amount || 4
  for (let i = 0; i < amountOfKeys; i++)
    accessKeys.push(crypto.randomBytes(3).toString('hex').toUpperCase() + "-" + crypto.randomBytes(3).toString('hex').toUpperCase())

  return res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Access keys generated.',
        pid:     process.pid
      },
      payload: accessKeys
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidContextType(req,res,next) {
  let { context=undefined } = req.headers,
    permittedContent = ['jwts','snowflake','access-keys','stream-keys']
  handleCheck(context, permittedContent, 'content type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidEncryptionContextType(req,res,next) {
  let { context=undefined } = req.headers,
    permittedContent = ['aes','bcrypt','sha256']
  handleCheck(context, permittedContent, 'encryption type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidDecryptionContextType(req,res,next) {
  let { context=undefined } = req.headers,
    permittedContent = ['aes','bcrypt','jwts']
  handleCheck(context, permittedContent, 'decryption type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidJWTData(req,res,next) {
  const { context } = req.headers['context']
  if (context !== 'jwts')
    return next()

  let invalidFields = []
  const {
    data= {
      ids: {
        uuid: undefined,
        suid: undefined
      },
      typesRequested: {
        sjwt: undefined,
        ajwt: undefined
      }
    }
  } = req.body

  req.body.data        === undefined ? invalidFields.push('Missing: config') : {}
  data.typesRequested  === undefined ? invalidFields.push('Missing: config.typesRequested ...') : {}

  /* One or the other must be defined */
  !data.typesRequested.sjwt &&
  !data.typesRequested.ajwt
    ? invalidFields.push('Missing: config.typesRequested.ajwt or config.typesRequested.sjwt') : {}

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        level: 'ERR',
        response: {
          header: {
            status:  'Error',
            code:    400,
            message: 'Config is malformed.',
            pid:     process.pid
          },
          err: invalidFields
        },
        diagnostics: {
          node:     process.env.API_NODE_NAME,
          datetime: getDateAndTimeAsString(),
          network: {
            remoteAddress: req.socket.remoteAddress       || undefined,
            xForwardedFor: req.headers['x-forwarded-for'] || undefined
          },
          os: {
            process:    process.pid,
            cpus:       os.cpus(),
            arch:       os.arch(),
            endianness: os.endianness(),
            version:    os.version(),
            memory: {
              free:  os.freemem(),
              total: os.totalmem()
            },
            priority:   os.getPriority(process.pid),
            hostname:   os.hostname(),
          }
        }
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Config is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function generateJWTs(req,res,next) {
  const context = req.headers['context']
  if (context !== 'jwts')
    return next()

  let errors = [], tokens = []

  const { authTokenContents=undefined } = res.locals

  const {
    ids,
    typesRequested
  } = req.body

  // Get UUID from Token || Get it locally or remotely
  let uuid = ids?.uuid || await handleSnowflakeGen()
  if(authTokenContents?.uuid !== undefined) uuid = authTokenContents?.uuid

  // Get SUID from Token || Get it locally or remotely
  let suid = ids?.suid || await handleSnowflakeGen()
  if(authTokenContents?.suid !== undefined) suid = authTokenContents?.suid

  /* Generated Tokens Array */
  let gens = []

  const status = {
    verified:   false,
    tier:       'free',
    privileged: false,
    affiliate:  false
  }

  /* Payload of the Token */
  const payload = {
    uuid,
    suid,
    status
  }

  /* Session JSON Web Token */
  if(typesRequested?.sjwt) {
    /* Setting Expiration */
    const setExpiration = {expiresIn: '7d'}

    // Generating Token w/ Above Configs
    let token = jwt.sign(
      {
        type: 'sjwt',
        ...payload
      },
      process.env.API_JWT_SECRET,
      setExpiration)

    /* Pushing Token with Details */
    gens.push({
      type: 'sjwt',
      ...payload,
      token
    })
  }

  /* Access JSON Web Token */
  if(typesRequested?.ajwt) {
    /* Setting Expiration */
    const setExpiration = {expiresIn: '12h'}

    // Generating Token w/ Above Configs
    let token = jwt.sign(
      {
        type: 'ajwt',
        ...payload
      },
      process.env.API_JWT_SECRET,
      setExpiration)

    gens.push({
      type: 'ajwt',
      ...payload,
      token
    })
  }

  if(gens.length === 0)
  {
    if(logLevelPresentCheck('ERR'))
      console.log({
        "PubSub Logging": {
          level: 'ERR',
          response: {
            header: {
              status:  'Error',
              code:    400,
              message: 'Token generation failed!',
              process: process.pid
            }
          },
          diagnostics: {
            node:     process.env.API_NODE_NAME,
            datetime: getDateAndTimeAsString(),
            network: {
              remoteAddress: req.socket.remoteAddress       || undefined,
              xForwardedFor: req.headers['x-forwarded-for'] || undefined
            },
            os: {
              process:    process.pid,
              cpus:       os.cpus(),
              arch:       os.arch(),
              endianness: os.endianness(),
              version:    os.version(),
              memory: {
                free:  os.freemem(),
                total: os.totalmem()
              },
              priority:   os.getPriority(process.pid),
              hostname:   os.hostname(),
            }
          }
        }
      })
    return res.status(400).send({
      "PubSub Status": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Token generation failed!',
          process: process.pid
        }
      }
    })
  }

  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        level: 'INFO',
        response: {
          header: {
            status:  'Success',
            code:    200,
            message: 'Successfully generated the requested tokens!',
            process: process.pid
          },
          payload: gens
        }
      }
    })

  // No error, return the tokens
  return res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Successfully generated the requested tokens!',
        process: process.pid
      },
      payload: gens
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function generateStreamKey(req,res,next){
  const context = req.headers['context']
  if (context !== 'stream-keys')
    return next()

  const streamKeys = []
  const amountOfKeys = req.body?.amount || 1
  for (let i = 0; i < amountOfKeys; i++)
    streamKeys.push(crypto.randomBytes(128).toString('hex').toUpperCase())

  return res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Stream keys generated.',
        pid:     process.pid
      },
      payload: streamKeys
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function encryptUsingAES(req,res,next){
  const { context } = req.headers
  if (context !== 'aes')
    return next()

  /* Loop through hashes */
  for (let i in req.body.array){
    const encryptedHash = encryptWithAES(req.body.array[i])
    res.locals.array.push(encryptedHash)
  }

  /* Move onto the next set */
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function encryptUsingSHA256(req,res,next){
  const { context } = req.headers
  if (context !== 'sha256')
    return next()

  /* Loop through hashes */
  for (let i in req.body.array){
    const encryptedHash = encryptWithSha256(req.body.array[i])
    res.locals.array.push(encryptedHash)
  }

  /* Move onto the next set */
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function encryptUsingBcrypt(req,res,next){
  const { context } = req.headers
  if (context !== 'bcrypt')
    return next()

  /* Loop through hashes */
  for (let i in req.body.array){
    const encryptedHash = encryptWithBcrypt(req.body.array[i])
    res.locals.array.push(encryptedHash)
  }

  /* Move onto the next set */
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function decryptUsingJWT (req,res,next) {
  const { context } = req.headers
  if (context !== 'jwts')
    return next()

  let data
  for (let i in req.body?.array) {
    try {
      data = await jwtVerify(req.body.array[i])
      res.locals.array.push(data)
    } catch (err) {}
  }

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function decryptUsingBcrypt(req,res,next){
  const { context } = req.headers
  if (context !== 'bcrypt')
    return next()

  /* Loop through hashes */
  for (let i in req.body.array){
    /* Array of an array - This is hacky but on a time constraint */
    const credentialsVerified = verifyHashWithBcrypt(req.body.array[i][0], req.body.array[i][1])
    res.locals.array.push(credentialsVerified)
  }

  /* Move onto the next set */
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function decryptUsingAES(req,res,next){
  const { context } = req.headers
  if (context !== 'aes')
    return next()

  /* Loop through hashes */
  for (let i in req.body.array){
    const decryptedHash = decryptWithAES(req.body.array[i])
    res.locals.array.push(decryptedHash)
  }

  /* Move onto the next set */
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function returnArray(req,res) {
  /* If the encryption set is empty, something went wrong up the pipeline */
  if (res.locals?.array === undefined || res.locals?.array.length === 0)
    return res.status(400).send({
      header: {
        status:  'Error',
        code:    400,
        message: 'Something went wrong when returning an array.',
        pid:     process.pid
      }
    })

  /* Return the generated tokens */
  return res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: 'Array returned with some data... Perhaps.',
        pid:     process.pid
      },
      payload: res.locals.array
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function preventEmptyArrayRequest(req,res,next) {
  if (req.body?.array === undefined || req.body?.array.length === 0)
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Malformed request. Requires hashes array in request body.',
          pid:     process.pid
        }
      }
    })

  res.locals.array = []
  return next()
}

/* Encryption Helpers */
/**
 * TODO: Add JSDOC here!
 */
export function encryptWithAES (text) {
  return AES.encrypt(text, process.env.API_DATA_SALT).toString()
}

/**
 * TODO: Add JSDOC here!
 */
export function encryptWithSha256(text){
  return crypto.createHash('sha256').update(text + process.env.API_DATA_SALT).digest('base64')
}

/**
 * TODO: Add JSDOC here!
 */
export function encryptWithBcrypt(text){
  const salt = bcrypt.genSaltSync(parseInt(process.env.API_SALT_ROUNDS))
  const hash = bcrypt.hashSync(text, salt)
  return hash
}

/**
 * TODO: Add JSDOC here!
 */
export function decryptWithAES (ciphertext) {
  const bytes = AES.decrypt(ciphertext, process.env.API_DATA_SALT)
  const originalText = bytes.toString(Utf8)
  return originalText
}

/**
 * TODO: Add JSDOC here!
 */
export function verifyHashWithBcrypt(text, ciphertext){
  return bcrypt.compareSync(text, ciphertext)
}

/**
 * TODO: Add JSDoc
 */
 export async function authTokenProcessing(req,res,next) {
  const authToken = req.headers?.authorization?.split(' ')[1]

  /* If no token was provided, ignore */
  if (authToken === undefined)
    return next()

  /* Getting the contents of the auth token */
  await jwtVerify(authToken, process.env.API_JWT_SECRET, {})
    .then(contents => {
      if(logLevelPresentCheck('INFO'))
        console.log({
          "PubSub Logging": {
            level: 'INFO',
            tokenContents: contents
          }
        })
      
      if (contents?.type !== 'sjwt')
        return res.status(400).send({
          "PubSub Response": {
            header: {
              status: "Error",
              code: 400,
              message: "Invalid token type provided... Why?",
              pid: process.pid
            }
          }
        })
        
      res.locals.authTokenContents = contents
      return next()
    })
    .catch(err => {
      if(logLevelPresentCheck('ERR'))
        console.log({
          "PubSub Logging": {
            level: 'ERR',
            response: {
              header: {
                status:  'Error',
                code:    403,
                message: 'Failed to decrypt the provided auth token.',
                worker:  process.pid
              }
            },
            diagnostics: {
              node:     process.env.API_NODE_NAME,
              datetime: getDateAndTimeAsString(),
              network: {
                remoteAddress: req.socket.remoteAddress       || undefined,
                xForwardedFor: req.headers['x-forwarded-for'] || undefined
              },
              os: {
                process:    process.pid,
                cpus:       os.cpus(),
                arch:       os.arch(),
                endianness: os.endianness(),
                version:    os.version(),
                memory: {
                  free:  os.freemem(),
                  total: os.totalmem()
                },
                priority:   os.getPriority(process.pid),
                hostname:   os.hostname(),
              }
            },
            err
          }
        })
      return next()
    })
}