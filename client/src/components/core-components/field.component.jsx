/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Field
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { MISC_COLORS } from '../../constants/themes.constants.js'
import Toggle from './toggle.component.jsx'
import PropTypes from 'prop-types'

const FieldContainer = styled.div`
  display: flex                     !important;
  margin:  ${props => props.margin} !important;
  padding: 0                        !important;
`

const FieldBoop = styled.div`
  display: block;
  position: relative !important;
  
  opacity:  100%     !important;
  width:    100%     !important;
  height:   auto     !important;
`

const TitleWrapper = styled.div`
  display:    flex;
  text-align: left;
  font-size:  13px;
  color:      white;

  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  
  & > div { margin-right: 5px; }
`

const FieldWrapper = styled.div`
  
  position: relative !important;
  
  opacity: 100% !important;
  
  height:     ${props => props.height}px    !important;
  min-height: ${props => props.minHeight}px !important;
  max-height: ${props => props.maxHeight}px !important;
  
  width:      ${props => props.width}px    !important;
  min-width:  ${props => props.minWidth}px !important;
  max-width:  ${props => props.maxWidth}px !important;
  
  background-color: ${props => props.backgroundColor} !important;
  border-color:  ${props => props.borderColor}    !important;
  
  border-width:  ${props => props.borderWidth}px  !important;
  border-style:  ${props => props.borderStyle}    !important;
  border-radius: ${props => props.borderRadius}px !important;
  
  padding: 0 12px !important;
  margin:  3px 0 1px 0  !important;
  
  & > input {
    
    height: 100% !important;
    width:  100% !important;

    color:            ${props => props.fontColor} !important;
    opacity:          ${props => !props.initClick ? 1 : 0.3};
    background-color: rgba(0,0,0,0)               !important;

    border-style:       hidden !important;
    -webkit-appearance: none   !important;
    outline:            none   !important;
    
    margin:  0 !important;
    padding: 0 !important;
    
    font-size: ${props => props.fontSize}px !important;
  }
`

const FootnoteWrapper = styled.div`
  display: flex;
  font-size: 12px;

  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select:   none; /* Safari */
  -khtml-user-select:    none; /* Konqueror HTML */
  -moz-user-select:      none; /* Old versions of Firefox */
  -ms-user-select:       none; /* Internet Explorer/Edge */
  user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  
  & > div:nth-child(1){
    color: gray;
  }
  & > div:nth-child(2){
    margin: 0 auto;
  }
  & > div:nth-child(3){
    color: white;
  }
`

const LinkStyled = styled(Link)`
  color:           ${MISC_COLORS.link.default};
  text-decoration: none;

  &:hover  {color: ${MISC_COLORS.link.hover}}
  &:active {color: ${MISC_COLORS.link.default}}
`

const IconContainer = styled.div`
  height:          auto;
  width:           50px;
  display:         flex;
  align-items:     center;
  justify-content: center;
  fill:            ${props => props.enabled ? '#ffffff' : props.color};
  & > svg {
    height: 20px;
  }
`

export default
function Field (props) {
  /* NOTE:
    * - Instead of changing nav-editable for the description box, just set a counter which
    *   if it goes negative will turn red. Leave it to the component user what to do with the
    *   beyond max-char limit. We can return a 'if limit has been reached' boolean for easier
    *   callback usage. */

  /* State-Based Variables */
  const [enabled, setEnabled] = useState(true)
  const [text, setText] = useState(props.initText)
  const [initClick, setInitClick] = useState(true)

  /* Functions for Field Wrappers */
  const backgroundColor = props.enabled ? props.eBackgroundColor : props.dBackgroundColor
  const borderColor = props.enabled ? props.eBorderColor : props.dBorderColor

  /* Event Updates */
  useEffect(() => { props.stateMirror(text) }, [text])

  const handleIconRender = () => {
    if (typeof props.icon !== 'undefined') {
      return (
        <IconContainer
          enabled={enabled}
          color={borderColor}
        >
          {props.icon}
        </IconContainer>
      )
    }
  }

  /* Logic Implementations */
  const handleTextInput = e => {
    /* Title or Description Render */
    if (props.titleDescriptionToggle) {
      /* If text is less than max */
      if (e.target.value.length - 1 < props.maxCharCount) {
        /* Update State */
        setText(e.target.value)
      } else {
        e.preventDefault()
      }
    } else {
      /* Update State */
      setText(e.target.innerHTML)
    }
  }

  const handleOnClick = e => {
    if (initClick) {
      /* Reset Toggle */
      setInitClick(false)
      /* Title or Description Logic */
      if (props.titleDescriptionToggle) {
        setText('')
      } else {
        e.target.innerHTML = ''
        setText('')
      }
    }
  }

  const handleOffClick = e => {
    if (text === '') {
      setInitClick(true)
      setText(props.initText)
    }
  }

  /* Deterministic Renders */
  const FieldTypeRender = () => {
    if (props.titleDescriptionToggle) { return TitleFieldTypeRender() } else { return DescriptionFieldTypeRender() }
  }

  const handleTitleRender = () => {
    return (
      <TitleWrapper>
        <div>{props.title}</div>
        <div style={{ color: MISC_COLORS.requirementRed }}>{props.required ? '*' : ''}</div>
      </TitleWrapper>
    )
  }

  const TitleFieldTypeRender = () => {
    return (
      <FieldWrapper
        backgroundColor={backgroundColor}
        borderColor ={borderColor}

        /* Styling Defaults */
        borderStyle ={props.borderStyle}

        borderWidth ={props.borderWidth}
        borderRadius={props.borderRadius}

        width ={props.width}
        maxWidth ={props.maxWidth}
        minWidth ={props.minWidth}

        height ={props.height}
        maxHeight ={props.maxHeight}
        minHeight ={props.minHeight}

        margin ={props.margin}
        padding ={props.padding}

        fontSize ={props.fontSize}
        fontColor ={props.fontColor}

        initClick ={initClick}
      >

        <input
          type ={props.inputType}
          name ="input_title"
          value ={text}
          onInput={e => props.enabled ? handleTextInput(e) : {}}
          onClick={e => props.enabled ? handleOnClick(e) : {}}
          onBlur ={e => props.enabled ? handleOffClick(e) : {}}
          /* Figure the logic to make this work... */
          onKeyUp={e => props.onKeyUp(e, text, setText)}
        />

      </FieldWrapper>
    )
  }

  const DescriptionFieldTypeRender = () => {
    return (
      <FieldWrapper
        backgroundColor={backgroundColor}
        borderColor ={borderColor}

        /* Styling Defaults */
        borderStyle ={props.borderStyle}

        borderWidth ={props.borderWidth}
        borderRadius={props.borderRadius}

        width ={props.width}
        maxWidth ={props.maxWidth}
        minWidth ={props.minWidth}

        height ={props.height}
        maxHeight ={props.maxHeight}
        minHeight ={props.minHeight}

        margin ={props.margin}
        padding ={props.padding}

        fontSize ={props.fontSize}
        fontColor ={props.fontColor}

        initClick ={initClick}
      >
        <div
          style={{ overflowY: 'scroll' }}
          contentEditable={props.enabled}
          suppressContentEditableWarning={true}
          onInput={e => handleTextInput(e)}
          onFocus={e => handleOnClick(e)}
          onBlur ={e => handleOffClick(e)}
        >
          {props.initText}
        </div>
      </FieldWrapper>
    )
  }

  const handleCharacterCount = () => {
    if (!initClick) { return props.maxCharCount - text.length } else { return props.maxCharCount }
  }

  const handleFootnoteRender = () => {
    return (
      <FootnoteWrapper>
        {
          typeof props.footnoteLink !== 'undefined'
            ? <LinkStyled to={props.footnoteLink}>
              <div>{props.footnote}</div>
            </LinkStyled>
            : <div>{props.footnote}</div>
        }
        <div/>

        <div>
          {
            props.displayCharCount
              ? handleCharacterCount()
              : <></>
          }
        </div>
      </FootnoteWrapper>
    )
  }

  /* Returned Component */
  return (
    <FieldContainer
      margin={props.margin}
    >
      { handleIconRender() }
      <FieldBoop>
        { handleTitleRender() }
        { FieldTypeRender() }
        { handleFootnoteRender() }
      </FieldBoop>
    </FieldContainer>
  )
}

Field.displayName = 'Field'
Field.propTypes = {
  /* Functionality Defaults */
  onKeyUp: PropTypes.func,
  stateMirror: PropTypes.func,
  maxCharCount: PropTypes.number,
  titleDescriptionToggle: PropTypes.bool,
  initText: PropTypes.string,
  inputType: PropTypes.string,
  title: PropTypes.string,
  displayCharCount: PropTypes.number,
  footnote: PropTypes.string,
  footnoteLink: PropTypes.string,
  required: PropTypes.bool,
  enabled: PropTypes.bool,
  eBackgroundColor: PropTypes.string,
  eBorderColor: PropTypes.string,
  dBackgroundColor: PropTypes.string,
  dBorderColor: PropTypes.string,
  borderStyle: PropTypes.string,
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  fontSize: PropTypes.number,
  fontColor: PropTypes.string,
  width: PropTypes.number,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  height: PropTypes.number,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  margin: PropTypes.string,
  padding: PropTypes.number,
  icon: PropTypes.element
}
/* Defining Default Values for Props */
Field.defaultProps = {
  /* Functionality Defaults */
  onKeyUp: () => {},
  stateMirror: () => {},
  maxCharCount: 50,

  titleDescriptionToggle: true,
  initText: 'Text',
  inputType: 'text',

  title: undefined,
  displayCharCount: true,
  footnote: undefined,
  footnoteLink: undefined,

  required: false,
  enabled: true,

  /* Styling Defaults */
  eBackgroundColor: '#000000',
  eBorderColor: '#ffffff',
  dBackgroundColor: '#000000',
  dBorderColor: '#ffffff',

  borderStyle: 'solid',
  borderWidth: 1.5,
  borderRadius: 8,

  fontSize: 14,
  fontColor: '#fff',

  width: undefined,
  maxWidth: undefined,
  minWidth: 200,

  height: 45,
  maxHeight: undefined,
  minHeight: undefined,

  margin: '3px 0',
  padding: 15

}
