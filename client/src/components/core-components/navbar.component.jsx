/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Navbar
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import styled from 'styled-components'
import Form from './form.component.jsx'
import Button from './button.component.jsx'
import PropTypes from 'prop-types'

const NavBarViewer = styled.div`
  
  position:  ${props => props.position} !important;
  top:       ${props => props.top}px    !important;
  bottom:    ${props => props.bottom}px !important;
  
  transition: width 0.3s;
  
  /* This ensures the navbar is absolutely centered & cannot be changed */
  left:      50%;
  z-index:   999;
  transform: translate(-50%, 0)
`

export default
function NavBar (props) {
  return (

    <NavBarViewer
      top ={props.top}
      bottom ={props.bottom}
      position ={props.position}
      ref ={props.ref}
    >
      <Form
        backgroundColor={props.backgroundColor}
        borderStyle ={props.borderStyle}
        borderColor ={props.borderColor}
        borderWidth ={props.borderWidth}
        borderRadius ={props.borderRadius}
        width ={props.width}
        maxWidth ={props.maxWidth}
        minWidth ={props.minWidth}
        height ={props.height}
        maxHeight ={props.maxHeight}
        minHeight ={props.minHeight}
        margin ={props.margin}
        padding ={props.padding}
        columns ={props.columns}
        rows ={props.rows}
        display ={props.display}
        justifyContent ={props.justifyContent}
        alignItems ={props.alignItems}
      >
        {props.children}
      </Form>
    </NavBarViewer>

  )
}

NavBar.displayName = 'NavBar'
NavBar.propTypes = {
  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,
  columns: PropTypes.number,
  rows: PropTypes.number,
  borderStyle: PropTypes.oneOf([
    'solid',
    'dashed',
    'dotted',
    'double',
    'groove',
    'ridge',
    'inset',
    'outset',
    'hidden',
    'none'
  ]),
  borderWidth: PropTypes.number,
  borderRadius: PropTypes.number,
  width: PropTypes.string,
  maxWidth: PropTypes.number,
  minWidth: PropTypes.number,
  height: PropTypes.number,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  margin: PropTypes.string,
  padding: PropTypes.number,
  display: PropTypes.oneOf([
    'flex',
    'grid',
    'block'
  ]),
  justifyContent: PropTypes.oneOf([
    'center',
    'start',
    'end',
    'flex-start',
    'flex-end',
    'left',
    'right',
    'normal',
    'space-between',
    'space-around',
    'space-evenly'
  ]),
  alignItems: PropTypes.oneOf([
    'center'
  ]),
  position: PropTypes.oneOf([
    'static',
    'relative',
    'fixed',
    'absolute',
    'sticky'
  ]),
  top: PropTypes.number,
  bottom: PropTypes.number,
  ref: PropTypes.element,
  children: PropTypes.element
}
NavBar.defaultProps = {
  backgroundColor: '#000000',
  borderColor: '#ffffff',

  columns: 1,
  rows: 1,

  borderStyle: 'solid',
  borderWidth: 2,
  borderRadius: 10,
  minWidth: 200,

  height: 40,
  maxHeight: 40,
  minHeight: 40,
  padding: 5,

  display: 'flex',
  justifyContent: 'space-between',
  position: 'absolute',

  bottom: 0
}
