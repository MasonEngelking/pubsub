/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name BubbleChat
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

/* Essential Imports */
import React, {
  useState,
  useEffect,
  useRef
} from 'react'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'
import io from 'socket.io-client'
import Peer from 'simple-peer'

/* SVG Imports */
import { ReactComponent as HeadphonesEnabled } from '../../assets/svgs/headphones.svg'
import { ReactComponent as MicEnabled } from '../../assets/svgs/mic.svg'
import { ReactComponent as CameraEnabled } from '../../assets/svgs/video.svg'

import { ReactComponent as EndCallLogo } from '../../assets/svgs/exit.svg'
import { ReactComponent as Misc } from '../../assets/svgs/dots.svg'

/* Audio Imports */
import JoinSound from '../../assets/audio/bubble_join.wav'
import LeaveSound from '../../assets/audio/bubble_leave.wav'

/* Redux */
import allActions from '../../redux/actions/index.js'
import { useDispatch } from 'react-redux'

/* Styled Containers */
const ContainerGrid = styled.div`
  height:             100vh;
  display:            grid;
  grid-template-rows: 110px auto 90px;
`

const InformationBar = styled.div`
  display:         flex;
  justify-content: left;
  place-items:     center;
  color:           #1d1d1d;
  
  & > div:nth-of-type(1) {
    background-color: #1d1d1d;
    height:           85px;
    width:            85px;
    border-radius:    100%;
    overflow:         hidden;
    margin:           0 10px;
  }

  & > div:nth-of-type(2) {
    margin: 0 10px;
    
    & > div:nth-of-type(1) {
      font-size:   22px;
      font-weight: 400;
    }
    
    & > div:nth-of-type(2) {
      font-weight: 200;
      opacity:     50%;
    }
  }
`

const InteractionBar = styled.div`
  display:         flex;
  justify-content: center;
  place-items:     center;
  height:          56px;
  position:        relative;
  top:             10px;
`

const Button = styled.div`
  height:           54px;
  width:            54px;
  background-color: #0D0D0D;
  border-color:     #323232;
  border-width:     1px;
  border-style:     solid;
  border-radius:    100%;
  margin:           0 4px;
  cursor:           pointer;
  display:          flex;
  place-content:    center;
  align-content:    center;
  align-items:      center;
  position:         relative;
  
  /* Styling The Addon Button */
  & > div {
    height:           18px;
    width:            18px;
    background-color: #0d0d0d;
    border-radius:    100%;

    display:          flex;
    justify-content:  center;
    align-items:      center;
    position:         absolute;
    right:            -3px;
    bottom:           -3px;
    
    border-style:     solid;
    border-color:     #323232;
    border-width:     1px;
  }
`

const EndCall = styled(Button)`
  background-color: #F02A2A;
  border-color:     #F02A2A;
`

const Divider = styled.div`
  height:           40px;
  width:            3px;
  background-color: #323232;
  border-radius:    10px;
  margin:           0 8px;
`

const DynamicTextContainer = styled.div`
  display:         flex;
  height:          54px;
  width:           auto;
  border-radius:   200px;
  margin:          0 4px;
  justify-content: center;
  align-items:     center;
  color:           white;
  padding:         0 18px;
  font-size:       18px;

  background-color: #0D0D0D;
  border-color:     #323232;
  border-width:     1px;
  border-style:     solid;
`

const Countdown = styled(DynamicTextContainer)`
  background-color: #2F28FF; !important;
`

const UserRenderContainer = styled.div`
  display:         flex;
  justify-content: center;
  
  & > div {
    max-width:       1040px;
    display:         flex;
    justify-content: left;
    flex-wrap:       wrap;
    align-items:     flex-start;
    align-content:   flex-start;
  }
  
`

const UserNoVideo = styled.div`
  height:          160px;
  width:           250px;
  margin:          5px;
  border-radius:   10px;
  display:         flex;
  justify-content: center;
  align-items:     center;

  font-size:   20px;
  color:       white;
  font-weight: 400;

`

const UserWithVideo = styled.div`
  height:          210px;
  width:           320px;
  margin:          5px;
  border-radius:   10px;
  display:         flex;
  justify-content: center;
  overflow:        hidden;
  position:        relative;

  font-size:       16px;
  color:           white;
  font-weight:     400;
  
  & > video { 
    position: absolute;
    width:    320px; 
  }
  
  /* Username Text Overlay */
  & > div {
    background-color: #1d1d1d;
    border-radius:    100px;
    padding:          4px 12px;
    position:         absolute;
    bottom:           6px;
    opacity:          50%;
  }
  
  &:hover {
    & > div { opacity: 100%; }
  }
`

export default
function BubbleChat () {
  /* React Redux Var */
  const dispatch = useDispatch()

  /* Defining the Join & Leave Sound Tracks */
  const joinAudio = new Audio(JoinSound)
  const leaveAudio = new Audio(LeaveSound)
  joinAudio.volume = 0.20
  leaveAudio.volume = 0.20

  const [peers, setPeers] = useState([])
  const socketRef = useRef()
  const userVideo = useRef()
  const peersRef = useRef([])
  const roomID = useLocation().pathname.split('/')[3]

  const EndCallProcedure = () => {
    /* Ending the call */
  }

  const GenerateBackgroundColor = () => {
    const rand_num = Math.floor(Math.random() * 25)

    const backgroundColors = [
      '#4C2434',
      '#003C75',
      '#2D4C24',
      '#751B00',
      '#29276D',
      '#007527',
      '#34244C',
      '#24494C',
      '#4C3324',
      '#12A06C',
      '#1F1A19',
      '#CEAD2F',
      '#552F18',
      '#1F1724',
      '#31816e',
      '#96663e',
      '#bb5212',
      '#763e96',
      '#93162e',
      '#2d0808',
      '#50653f',
      '#1a1a1a',
      '#5d285d',
      '#9fa95e',
      '#e5ac5a'
    ]

    return backgroundColors[rand_num]
  }

  const VideoProcessing = (props) => {
    const ref = useRef()

    useEffect(() => {
      try {
        props.peer.on('stream', stream => { ref.current.srcObject = stream })
      } catch (e) {
        console.log('Stream is no longer available!')
      }
    }, [])

    return (
      <div key={props.key}>
        <UserWithVideo style={{ backgroundColor: GenerateBackgroundColor() }}>
          <video ref={ref} autoPlay={true} muted={false} playsInline={true} controls={false}/>

          <div >
            {'Test2'}
          </div>
        </UserWithVideo>
      </div>
    )
  }

  const createPeer = (userToSignal, callerID, stream) => {
    const peer = new Peer({
      initiator: true,
      trickle: false,
      stream
    })

    peer.on('signal', signal => {
      socketRef.current.emit('sending_signal', { userToSignal, callerID, signal })
    })

    return peer
  }

  const addPeer = (incomingSignal, callerID, stream) => {
    const peer = new Peer({
      initiator: false,
      trickle: false,
      stream
    })

    peer.on('signal', signal => {
      socketRef.current.emit('returning_signal', { signal, callerID })
    })

    peer.signal(incomingSignal)

    return peer
  }

  /* UI useEffect Logic */
  useEffect(() => {
    /* Audio Alert */
    joinAudio.play()
    return () => {
      leaveAudio.play()
    }
  }, [])

  /* TODO: Refactor Network Topology */
  /* Socket useEffect Logic */
  useEffect(() => {
    let userVideoReference

    /* TODO: ADAPT... */
    socketRef.current = io.connect('127.0.0.1:8000', { transports: ['websocket'] })

    try {
      // If media devices are available...
      navigator.mediaDevices.getUserMedia(
        {
          video: {
            width: 640,
            height: 480
          },
          audio: true
        })
        .then(stream => {
          userVideo.current.srcObject = stream
          userVideoReference = userVideo.current.srcObject

          socketRef.current.emit('join_bubble', roomID)

          /* Retrieve Who Left! */
          socketRef.current.on('user_left', userID => {
            /* Remove from State */
            setPeers(peers => peers.map(peer => {
              console.log(peer)
              console.log(`User leaving: ${userID}`)
            }))
            /* Remove user Ref */
            peersRef.current.map(peersRef => peersRef.id !== userID)
          })

          /* Handle Retrieving Peers */
          socketRef.current.on('receive_bubble_peers', async users => {
            const peers = []
            await users.forEach(userID => {
              const peer = createPeer(userID, socketRef.current.id, stream)
              peersRef.current.push({
                peerID: userID,
                peer
              })
              peers.push({ peerID: userID, peer })
            })
            await setPeers(peers)
          })

          socketRef.current.on('user_joined', async payload => {
            const peer = addPeer(payload.signal, payload.callerID, stream)

            peersRef.current.push({ peerID: payload.callerID, peer })

            await setPeers(users => [...users, { peerID: payload.callerID, peer }])
          })

          socketRef.current.on('receiving_returned_signal', payload => {
            const item = peersRef.current.find(p => p.peerID === payload.id)
            item.peer.signal(payload.signal)
          })
        })
        .catch(err => { console.log(err) })
    } catch (e) {
      // If media devices are not available...
    }

    /* Handle ahead of time? */
    return () => {
      /* Remove Self from Socket Peer List */
      socketRef.current.emit('leave_bubble')
      /* Disconnect from Information Socket */
      socketRef.current.disconnect()

      /* If Media Avail. - Attempt to turn it off. */
      try {
        userVideoReference.getTracks().forEach(track => { track.stop() })
      } catch (e) {

      }

      /* Destroying Socket Connections */
      peersRef.current.forEach(peer => { peer.peer.destroy() })
    }
  }, [])

  return (
    <ContainerGrid>

      <InformationBar>
        <div>

        </div>
        <div>
          <div>
            {useLocation().pathname.split('/')[3]}
          </div>
          <div>
            {'temp'}
          </div>
        </div>
      </InformationBar>

      <UserRenderContainer>
        <div>
          {/* Yourself... */}
          <UserWithVideo style={{ backgroundColor: GenerateBackgroundColor() }}>
            <video
              ref={userVideo}
              autoPlay={true}
              muted={true}
              playsInline={true}
              controls={false}
            />
            <div>
              {'Test1'}
            </div>
          </UserWithVideo>
          {/* Peers... */}
          {peers.map((peer, index) => {
            try {
              return (<VideoProcessing key={index} peer={peer.peer}/>)
            } catch (e) { /* Nothing to display... */ }
          })}
        </div>
      </UserRenderContainer>

    </ContainerGrid>
  )
}
