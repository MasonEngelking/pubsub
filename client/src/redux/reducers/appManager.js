/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import {
  SET_NAVBAR_TYPE,
  SET_SOCKET_ATTEMPT_SECONDS,
  SET_APP_IS_MOUNTED,
  SET_APP_IS_READY,
  SET_APP_IN_FATAL_STATE,
  SET_READINESS_PERCENTAGE
} from '../actions/actionTypes.js'

const initState = {
  /* Core State */
  isReady: false,
  isMounted: false,
  inFatalState: false,

  readinessPercentage: 0,

  /* Deprecate This */
  socketStatus: { socketAttemptSeconds: 4 },

  /* Deprecate This */
  navbarType: 'thread'
}

const appManager = (state = initState, action) => {
  switch (action.type) {
  case SET_APP_IS_READY:
    // eslint-disable-next-line no-case-declarations
    const { isReady } = action.payload
    return {
      ...state,
      isReady
    }

  case SET_READINESS_PERCENTAGE:
    // eslint-disable-next-line no-case-declarations
    const { percent } = action.payload
    return {
      ...state,
      readinessPercentage: percent
    }

  case SET_APP_IS_MOUNTED:
    // eslint-disable-next-line no-case-declarations
    const { isMounted } = action.payload
    return {
      ...state,
      isMounted
    }

  case SET_APP_IN_FATAL_STATE:
    // eslint-disable-next-line no-case-declarations
    const { inFatalState } = action.payload
    return {
      ...state,
      inFatalState
    }

  case SET_SOCKET_ATTEMPT_SECONDS:
    // eslint-disable-next-line no-case-declarations
    const { seconds } = action.payload
    return {
      ...state,
      socketStatus: {
        ...state.socketStatus,
        socketAttemptSeconds: seconds
      }
    }

  case SET_NAVBAR_TYPE:
    // eslint-disable-next-line no-case-declarations
    const { navbarType } = action.payload
    return {
      ...state,
      navbarType
    }

  default: return state
  }
}

export default appManager
