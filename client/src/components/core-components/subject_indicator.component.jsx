/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Subject_Indicator
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import styled from 'styled-components'
import Emblem from './emblem.component.jsx'

import { SUBJECT_COLORS } from '../../constants/themes.constants.js'

/* Page SVGs */
import { ReactComponent as Welcome } from '../../assets/svgs/user_group.svg'
import { ReactComponent as Safety } from '../../assets/svgs/safety.svg'
import { ReactComponent as Legal } from '../../assets/svgs/legal.svg'
import { ReactComponent as Credentials } from '../../assets/svgs/id.svg'
import { ReactComponent as Verification } from '../../assets/svgs/verification.svg'
import PropTypes from 'prop-types'

const RenderSubjects = styled.div`
    display:               grid;
    grid-template-columns: auto auto auto;
    place-items:           center;
    height:                50px;
    width:                 220px;
`

export default
function SubjectIndicator ({ pageIndex }) {
  const IndicatorRender = (page, primary) => {
    switch (page) {
    case 0 : return <>
      <Emblem
        colorOne={SUBJECT_COLORS.welcome[0]}
        colorTwo={SUBJECT_COLORS.welcome[1]}
        icon={
          <Welcome
            style={{
              fill: 'white',
              height: primary ? '28px' : '22px'
            }}
          />}
        title ={'Hello!'}
        titleSize ={primary ? '11px' : '9px'}
        size ={primary ? '35px' : '28px'}
        margin ={'0 0 0 0'}
        gap ={0}
      />

    </>
    case 1 : return <>
      <Emblem
        colorOne={SUBJECT_COLORS.safety[0]}
        colorTwo={SUBJECT_COLORS.safety[1]}
        icon={
          <Safety
            style={{
              fill: 'white',
              height: primary ? '24px' : '16px'
            }}
          />}
        title ={'Safety'}
        titleSize ={primary ? '11px' : '9px'}
        size ={primary ? '35px' : '28px'}
        margin ={'0 0 0 0'}
        gap ={0}
      />
    </>
    case 2 : return <>
      <Emblem
        colorOne={SUBJECT_COLORS.legal[0]}
        colorTwo={SUBJECT_COLORS.legal[1]}
        icon={
          <Legal
            style={{
              fill: 'white',
              height: primary ? '23px' : '17px'
            }}
          />
        }
        title ={'Legal'}
        titleSize ={primary ? '11px' : '9px'}
        size ={primary ? '35px' : '28px'}
        margin ={'0 0 0 0'}
        gap ={0}
      />
    </>
    case 3 : return <>
      <Emblem
        colorOne={SUBJECT_COLORS.credentials[0]}
        colorTwo={SUBJECT_COLORS.credentials[1]}
        icon={
          <Credentials
            style={{
              fill: 'white',
              height: primary ? '19px' : '15px'
            }}/>}
        title ={'ID'}
        titleSize ={primary ? '11px' : '9px'}
        size ={primary ? '35px' : '28px'}
        margin ={'0 0 0 0'}
        gap ={0}
      />
    </>
    case 4 : return <>

      <Emblem
        colorOne={SUBJECT_COLORS.verification[0]}
        colorTwo={SUBJECT_COLORS.verification[1]}
        icon={
          <Verification
            style={{
              fill: 'white',
              height: primary ? '20px' : '15px'
            }}
          />}
        title ={'Verify'}
        titleSize ={primary ? '11px' : '9px'}
        size ={primary ? '35px' : '28px'}
        margin ={'0 0 0 0'}
        gap ={0}
      />

    </>

    default : return <div style={{ width: 30 }}/>
    }
  }

  return (
    <>
      <RenderSubjects>
        {IndicatorRender(pageIndex - 1, false)}
        {IndicatorRender(pageIndex, true)}
        {IndicatorRender(pageIndex + 1, false)}
      </RenderSubjects>
    </>
  )
}

SubjectIndicator.displayName = 'Subject Indicator'
SubjectIndicator.propTypes = {
  pageIndex: PropTypes.number
}
