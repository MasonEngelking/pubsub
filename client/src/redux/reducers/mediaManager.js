/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import {
  SET_AVAILABLE_MEDIA_STATE,
  SET_AVAILABLE_MEDIA_STATE_AUDIOIN,
  SET_AVAILABLE_MEDIA_STATE_AUDIOOUT,
  SET_AVAILABLE_MEDIA_STATE_VIDEOIN,

  SET_SELECTED_MEDIA_STATE,
  SET_SELECTED_MEDIA_STATE_AUDIOIN,
  SET_SELECTED_MEDIA_STATE_AUDIOOUT,
  SET_SELECTED_MEDIA_STATE_VIDEOIN,

  SET_DEVICE_PERMISSION_AUDIO,
  SET_DEVICE_PERMISSION_VIDEO
} from '../actions/actionTypes.js'

const initState = {
  mediaDevices: {
    audioInputs: [{ deviceId: '', label: 'Fetching...' }],
    audioOutputs: [{ deviceId: '', label: 'Fetching...' }],
    videoInputs: [{ deviceId: '', label: 'Fetching...' }]
  },
  selectedDevices: {
    audioInput: { deviceId: '', label: 'Fetching...' },
    audioOutput: { deviceId: '', label: 'Fetching...' },
    videoInput: { deviceId: '', label: 'Fetching...' }
  },
  devicePermissions: {
    audio: 'prompt',
    video: 'prompt'
  }
}

const mediaManager = (state = initState, action) => {
  switch (action.type) {
  /* Available Media Updates */
  case SET_AVAILABLE_MEDIA_STATE:
    // eslint-disable-next-line no-case-declarations
    const { audioInputs, audioOutputs, videoInputs } = action.payload
    return {
      ...state,
      mediaDevices: {
        audioInputs,
        audioOutputs,
        videoInputs
      }
    }

    /* Selected Media Updates */
  case SET_SELECTED_MEDIA_STATE:
    // eslint-disable-next-line no-case-declarations
    const { audioInput, audioOutput, videoInput } = action.payload
    return {
      ...state,
      selectedDevices: {
        audioInput,
        audioOutput,
        videoInput
      }
    }

  case SET_SELECTED_MEDIA_STATE_AUDIOIN:
    // eslint-disable-next-line no-case-declarations
    const { audioIn } = action.payload
    return {
      ...state,
      selectedDevices: {
        ...state.selectedDevices,
        audioInput: audioIn
      }
    }

  case SET_SELECTED_MEDIA_STATE_AUDIOOUT:
    // eslint-disable-next-line no-case-declarations
    const { audioOut } = action.payload
    return {
      ...state,
      selectedDevices: {
        ...state.selectedDevices,
        audioOutput: audioOut
      }
    }

  case SET_SELECTED_MEDIA_STATE_VIDEOIN:
    // eslint-disable-next-line no-case-declarations
    const { videoIn } = action.payload
    return {
      ...state,
      selectedDevices: {
        ...state.selectedDevices,
        videoInput: videoIn
      }
    }

    /* Permission Updates */
  case SET_DEVICE_PERMISSION_AUDIO:
    // eslint-disable-next-line no-case-declarations
    const { audioPermission } = action.payload
    return {
      ...state,
      devicePermissions: {
        ...state.devicePermissions,
        audio: audioPermission
      }
    }

  case SET_DEVICE_PERMISSION_VIDEO:
    // eslint-disable-next-line no-case-declarations
    const { videoPermission } = action.payload
    return {
      ...state,
      devicePermissions: {
        ...state.devicePermissions,
        video: videoPermission
      }
    }

  default: return state
  }
}

export default mediaManager
