/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
import {getDateAsCustomEpoch, logLevelPresentCheck} from './global.middleware.js'
import * as crypto from 'crypto'
import flakeID from 'flake-idgen'
import intFormat from 'biguint-format'
import Bubble from './models/bubble.model.js'
import User from './models/user.model.js'
import Report from './models/report.model.js'
import Quickie from './models/quickie.model.js'
import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import jwt from 'jsonwebtoken'
import axios from 'axios'

const CRYPTO_COMPOSED_URI = 'http://' + process.env.API_CRYPTO_IP_OR_DNS + ':' + process.env.API_CRYPTO_PORT

/**
 * TODO: Add JSDOC here!
 */
function handleUserSuspensionsCheck(suspensions){
  /* If no suspensions - continue */
  if(suspensions.length === 0)
    return false

  /* Determines valid suspensions array */
  const preventions = suspensions.map(suspension => {
    /* If it was overturned - ignore */
    if(suspension.overturned)
      return false

    /* Indefinite suspension */
    if(suspension.expirationEpoch === 0)
      return true

    /* If genuine suspension -> Determine time differences and return accordingly */
    /* Determining time remaining */
    const dateAsEpoch = getDateAsCustomEpoch()
    const restrictedUntilAsString = new Date(suspension.expirationEpoch + process.env.API_CUSTOM_EPOCH)

    /* Preventing access to account during restricted period */
    return suspension.expirationEpoch - dateAsEpoch > 0
  })

  /* Return whether an active suspension is applied */
  return preventions.includes(true)
}

/**
 * TODO: Add JSDOC here!
 */
function handleUserActiveSessionsCheck(sessions){
  /*
  * TODO: - In the future, this will allow for multiple sessions
  *       - For now, it will only come down to one session
  *       - Note: The function receive an array because of this
  *       - This will be updated in the future... For now it will return the latest session.
  */
  return sessions.filter(session => session.active === true)
}

/**
 * TODO: Add JSDOC here!
 */
function handleCheck(focalPoint, permittedContent, typeOfCheck, res, next) {
  let check = permittedContent.filter(type => type === focalPoint).length > 0

  if(check)
    return next()

  res.status(400).send({
    'PubSub Response': {
      header: {
        status:  400,
        message: `Request config \'${typeOfCheck}\' is invalid.`,
        worker:  process.pid
      }
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export function encryptWithSha256(text){
  return crypto.createHash('sha256').update(text + process.env.API_DATA_SALT).digest('base64')
}

/**
 * TODO: Add JSDOC here!
 */
export function verifyHashWithBcrypt(text, ciphertext){
  return bcrypt.compareSync(text, ciphertext)
}

/**
 * TODO: Add JSDOC here!
 */
export async function getSnowflakeID() {
  const flake = new flakeID({
    epoch:      process.env.API_CUSTOM_EPOCH,
    worker:     process.pid % 31,
    datacenter: 0 // TODO: Implement in the future
  })

  const snowflakeAsBuffer = await flake.next()
  return await intFormat(snowflakeAsBuffer, 'dec')
}

/**
 * TODO: Add JSDOC here!
 */
export function determineSchema(req, res, next) {
  console.log("Schema Check")
  const { context } = req.headers

  switch (context){
    case 'bubble':
      res.locals.schema = Bubble
      break;
    case 'quickie':
      res.locals.schema = Quickie
      break;
    case 'user':
      res.locals.schema = User
      break;
    case 'report':
      res.locals.schema = Report
      break;
    default: res.locals.schema = undefined
  }

  if(res.locals.schema === undefined)
  {
    if(logLevelPresentCheck('ERR'))
      console.log({
        'Database Logging': {
          header: {
            status:  'Severe',
            message: "Failed to determine the schema given the provided request.",
            worker:  process.pid
          }
        }
      })
    res.status(500).send({
      'Database Response': {
        header: {
          status:  500,
          message: "Failed to determine the schema given the provided request.",
          worker:  process.pid
        }
      }
    }).end()
  }

  if(logLevelPresentCheck('INFO'))
    console.log({
      'Database Logging': {
        level: 'INFO',
        header: {
          status:  'Healthy',
          message: "Successfully determined the MongoDB Schema given the request.",
          worker:  process.pid
        }
      }
    })

  next()
}

/**
 * TODO: Add JSDOC here!
 */
export function getSchema(req,res,next) {
  const { context } = req.headers

  if (context === 'user')
    res.locals.schema = User
  else if(context === 'bubble')
    res.locals.schema = Bubble

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function generateSchemas(req,res,next) {

  const { context } = req.headers

  if(context === 'report')
    res.locals.schemas = [new Report(res.locals.reportData)]
  else if(context === 'user')
    res.locals.schemas = [new User(res.locals.userData)]
  else if(context === 'bubble')
    res.locals.schemas = [new Bubble(res.locals.bubbleData)]
  else if(context === 'quickie')
    res.locals.schemas = [new Quickie(res.locals.quickieData)]

  next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function updateDocument(req,res) {

  const { context } = req.headers
  const {
    _id=undefined,
    suid=undefined,
    update=undefined,
    location=undefined
  } = req.body

  const userFilter = { _id }

  const dataFilter = {
    _id: _id,
    meta: { opUUID: suid }
  }

  const determinedFilter = context === 'user' ? userFilter : dataFilter

  const response = await res.locals.schema
    // If only by id, then set to retrieve only _id, otherwise use filter as-is
    .updateOne(determinedFilter,
      {$push: {[`${location}`]:
            {
              ...update,
              suid
            }
      }}
    )

  if(!response.acknowledged) {
    if(logLevelPresentCheck('ERR'))
      console.log({
        "PubSub Response": {
          header: {
            status:  'Failed',
            code:    400,
            message: `Document ${_id} failed updated.`,
            process: process.pid
          }
        }
      })
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Failed',
          code:    400,
          message: `Document ${_id} failed updated.`,
          process: process.pid
        }
      }
    })
  }

  if(logLevelPresentCheck('INFO'))
    console.log({
      "PubSub Logging": {
        response: {
          header: {
            status:  'Success',
            code:    200,
            message: `Document ${_id} updated successfully.`,
            process: process.pid
          }
        }
      }
    })
  res.status(200).send({
    "PubSub Response": {
      header: {
        status:  'Success',
        code:    200,
        message: `Document ${_id} updated successfully.`,
        process: process.pid
      }
    }
  })
}

/**
 * TODO: Add JSDOC here!
 */
export async function uploadSchemas(req,res) {
  const { context } = req.headers

  /* Implementing Universal ACID Transactions per Array of Schemas */
  const transactionOptions = {
    readPreference: 'primary' ,
    readConcern:    {level: 'local'},
    writeConcern:   {w: 'majority'}
  }
  const session = await mongoose.startSession();
  session.startTransaction(transactionOptions)

  try {

    for (let index in res.locals.schemasData)
    {
      const data = res.locals.schemasData[index]
      switch (data.type) {
        case 'user':
          await User.create([data.data], {session});
          break;
        case 'bubble':
          await Bubble.create([data.data], {session});
          break;
        case 'quickie':
          await Quickie.create([data.data], {session});
          break;
        case 'report':
          await Report.create([data.data], {session});
          break;
      }
    }

    await session.commitTransaction();
    await session.endSession();

    logLevelPresentCheck('INFO')
      console.log({
        "PubSub Logging": {
          status:  'Success',
          message: 'Schemas Created/Uploaded Successfully as Transaction',
          process: process.pid
        }
      })
    res.status(200).send({
      'PubSub Response': {
        header: {
          status:  'Success',
          code:    200,
          message: 'Schemas Created/Uploaded Successfully as Transaction',
          pid:     process.pid
        },
        payload: res.locals.dataToReturn
      }
    })

  } catch (err) {

    logLevelPresentCheck('ERR')
      console.log({
        "PubSub Logging": {
          status:  'Failed',
          message: 'Intentionally aborted transaction due to document creation failure... Exiting.',
          process: process.pid
        }
      })

    await session.abortTransaction()

    res.status(501).send({
      header: {
        status:  501,
        message: 'Schemas failed to queue within transaction.',
        pid:     process.pid
      }, err: err
    })
  }

}

/**
 * TODO: Add JSDOC here!
 */
export async function preventMalformedUpdateConfig(req,res,next) {
  let invalidFields = []

  const { context } = req.headers
  const {
    _id=undefined,
    location=undefined,
    update=undefined,
    suid=undefined
  } = req.body

  _id      === undefined ? invalidFields.push('Missing _id')      : {}
  location === undefined ? invalidFields.push('Missing location') : {}
  update   === undefined ? invalidFields.push('Missing update')   : {}
  suid     === undefined ? invalidFields.push('Missing suid')     : {}

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Config is malformed.',
          pid:     process.pid
        },
        err: invalidFields
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Config is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export async function preventMalformedQueryConfig(req,res,next) {
  let invalidFields = []
  const {
    findType=undefined,
    queryDepth=undefined,
    filterBy=undefined
  } = req.body

  findType    === undefined ? invalidFields.push('Missing findType')    : {}
  queryDepth  === undefined ? invalidFields.push('Missing queryDepth')  : {}
  filterBy    === undefined ? invalidFields.push('Missing filterBy')    : {}

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Config is malformed.',
          pid:     process.pid
        },
        err: invalidFields
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Config is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  }).end()

}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidAccessType(req,res,next) {
  const { context } = req.headers

  const permittedContent = [
    'login',
    'logout',
    'recover'
  ]

  /* Perform permitted content check */
  handleCheck(context, permittedContent, 'access type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidRecoveryType(req,res,next){
  const { context } = req.headers

  /* Ignore Recovery Check if it's not recovery... */
  if (context !== 'recover')
    return next()

  const { recoveryType=undefined } = req.body?.data

  /* Perform permitted content check */
  const permittedContent = ['code'] // TODO: In the future, add support for other recovery options...
  handleCheck(recoveryType, permittedContent, 'recovery type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidContextType(req,res,next) {
  console.log("Context Check")
    let { context=undefined } = req.headers,
        permittedContent = ['user','bubble','quickie','report']
    handleCheck(context, permittedContent, 'context type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidFindType(req,res,next) {
  console.log("Fine Check")
  let { findType } = req.body,
      permittedContent = ['one','multi','id']
  handleCheck(findType, permittedContent, 'find type', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export function preventInvalidQueryDepth(req,res,next) {
  console.log("Depth Check")
  let { queryDepth } = req.body,
      permittedContent = ['secured', 'unsecured']

  handleCheck(queryDepth, permittedContent, 'query depth', res, next)
}

/**
 * TODO: Add JSDOC here!
 */
export async function retrieveFromMongoDB(req,res) {
  console.log("MongoDB Check")
  const handleDetermineFind = (findType) => findType === 'id' ? {_id: filterBy._id} : filterBy
  
  const handleDetermineLimit = (findType) => {
    switch(findType){
      case 'one':
        return 1;
      case 'id':
        return 1;
      default: return undefined
    }
  }

  const { context=undefined } = req.headers
  const {
    findType,
    filterBy,
    queryDepth
  } = req.body
  const uuid = res.locals.authTokenContents?.uuid

  let docs
  // Ensures that it only returns the data created by the user sjwt
  if(queryDepth === 'secured')
    docs = await res.locals.schema
      // If only by id, then set to retrieve only _id, otherwise use filter as-is
      .find(handleDetermineFind(findType))
      // Protecting content
      .where(context === 'user' ? '_id' : 'meta.opUUID')
      .equals(uuid)
      // Restricting content to be returned
      .secureSelection()
      // Simulating limitations on find types
      .limit(handleDetermineLimit(findType))
  else
    docs = await res.locals.schema
      .find(handleDetermineFind(findType))
      // Restricting content to be returned
      .unsecureSelection()
      // Simulating limitations on find types
      .limit(handleDetermineLimit(findType))

  // If the user couldn't be found...
  if(docs === undefined){
    if(logLevelPresentCheck('ERR'))
      console.log({
        "PubSub Logging": {
          response: {
            header: {
              status:  'Error',
              code:    400,
              message: 'Could not find the specified document.'
            }
          },
          diagnostics: {

          }
        }
      })
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status: 'Error',
          code:   400,
          message: 'Could not find the specified document.'
        }
      }
    })
  }

  return res.status(200).send({
    "PubSub Response": {
      header: {
        status: 'Success',
        code:   200,
        message: 'Requested data has been retrieved given the configuration provided.'
      },
      payload: docs
    }
  })
}

/**/
export async function checkUserExistenceCompliance(req,res,next){
  const { context } = req.headers
  if (context !== 'user')
    return next()

  const { secured } = res.locals.userData
  const infoEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: [ secured?.credentials?.email ]
  }, {
    headers: {
      context: 'sha256',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [ emailHashed ] = infoEncryptedResults?.data['PubSub Response']?.payload
  const userExists = await User.findOne({"secured.credentials.email.content.hashed": emailHashed})

  if(userExists)
    return res.status(400).send({
      "PubSub Response": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Email is already in use. Preventing creation.',
          pid:     process.pid
        },
      }
    })

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function checkCredentialCompliance(req,res,next){

  const { context=undefined } = req.headers

  if(context !== 'user')
    return next()

  /* Username Requirements
    > username is 1-20 characters
    > no _ or . at the beginning
    > no __ or _. or ._ or .. insides
    > allowed characters a-z,A-Z,0-9,._
    > no _ or . at the end
    Possible Alternative: ^(?=[a-zA-Z0-9._]{1,20}$)(?!.*[_.]{2})[^_.].*[^_.]$
   */
  const usernameRegex = /^(?=.{1,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/

  /* Password Requirements
    > Min 6 characters
    > At least 1 uppercase
    > At least 1 lowercase
    > 1 number
    > 1 special character
  */
  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/

  /* Tag Requirements
    > 4 characters in length
    > only 0 through 9 for numbers
   */
  const tagRegex      = /^[0-9]{1,4}$/

  const {username, tag} = res.locals.userData.identifier
  const {password}      = res.locals.userData.secured.credentials

  let complianceChecks = []
  /* Execute the regular expression tests */
  const usernameCompliant = usernameRegex.test(username)
  const passwordCompliant = passwordRegex.test(password)
  const tagCompliant      = tagRegex.test(tag)

  if(!usernameCompliant) complianceChecks.push("Username is not compliant.")
  if(!passwordCompliant) complianceChecks.push("Password is not compliant.")
  if(!tagCompliant)      complianceChecks.push("Tag is not compliant.")

  if(complianceChecks.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Data is not in compliance.',
          pid:     process.pid
        },
        err: complianceChecks
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Data is not in compliance.',
        pid:     process.pid
      },
      err: complianceChecks
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export async function preventMalformedUserData(req,res,next) {

  const { context=undefined } = req.headers

  if(context !== 'user')
    return next()

  let {
    user={
      // _id: undefined - THIS IS NOW GENERATED PER REQ

      identifier: {
        username: undefined,
        tag:      undefined,
        bio:      undefined
      },

      media: {
        avatar:   {contentURL: undefined},
        banner:   {contentURL: undefined},
        ambience: {colorHex:   undefined}
      },

      secured: {
        // account: { ajwt: undefined } - THIS IS NOW GENERATED PER REQ

        credentials: {
          email:       undefined,
          password:    undefined,
          dateOfBirth: undefined
        },

        // recovery: { codes: undefined } - THIS IS NOW GENERATED PER REQ

        session: {
          // sjwt: undefined - THIS IS NOW GENERATED PER REQ
          platform: {
            description:  undefined,
            layout:       undefined,
            manufacturer: undefined,
            name:         undefined,
            os:           undefined,
            prerelease:   undefined,
            product:      undefined,
            ua:           undefined,
            version:      undefined,
            architecture: undefined,
            family:       undefined
          },
          networkInfo: {
            location: undefined,
            ip:       undefined
          }
        }
      }
    }
  } = req.body

  /* Preventing Undefined Data */
  let invalidFields = []
  {
    if(req.body.user  === undefined) invalidFields.push('Missing: user data')

    if(user.identifier.username === undefined) invalidFields.push('Missing: user.identifier.username')
    if(user.identifier.tag      === undefined) invalidFields.push('Missing: user.identifier.tag')
    if(user.identifier.bio      === undefined) invalidFields.push('Missing: user.identifier.bio')

    if(user.media.avatar.contentURL === undefined) invalidFields.push('Missing: user.media.avatar.contentURL')
    if(user.media.banner.contentURL === undefined) invalidFields.push('Missing: user.media.banner.contentURL')
    if(user.media.ambience.colorHex === undefined) invalidFields.push('Missing: user.media.ambience.colorHex')

    if(user.secured.credentials.email             === undefined) invalidFields.push('Missing: user.secured.credentials.email')
    if(user.secured.credentials.password          === undefined) invalidFields.push('Missing: user.secured.credentials.password')
    if(user.secured.credentials.dateOfBirth       === undefined) invalidFields.push('Missing: user.secured.credentials.dateOfBirth')

    if(user.secured.session.platform.description  === undefined) invalidFields.push('Missing: user.secured.sessions.platform.description')
    if(user.secured.session.platform.layout       === undefined) invalidFields.push('Missing: user.secured.sessions.platform.layout')
    if(user.secured.session.platform.manufacturer === undefined) invalidFields.push('Missing: user.secured.sessions.platform.manufacturer')
    if(user.secured.session.platform.name         === undefined) invalidFields.push('Missing: user.secured.sessions.platform.name')
    if(user.secured.session.platform.os           === undefined) invalidFields.push('Missing: user.secured.sessions.platform.os')
    if(user.secured.session.platform.prerelease   === undefined) invalidFields.push('Missing: user.secured.sessions.platform.prerelease')
    if(user.secured.session.platform.product      === undefined) invalidFields.push('Missing: user.secured.sessions.platform.product')
    if(user.secured.session.platform.ua           === undefined) invalidFields.push('Missing: user.secured.sessions.platform.ua')
    if(user.secured.session.platform.version      === undefined) invalidFields.push('Missing: user.secured.sessions.platform.version')
    if(user.secured.session.platform.architecture === undefined) invalidFields.push('Missing: user.secured.sessions.platform.architecture')
    if(user.secured.session.platform.family       === undefined) invalidFields.push('Missing: user.secured.sessions.platform.family')
    if(user.secured.session.networkInfo.ip        === undefined) invalidFields.push('Missing: user.secured.sessions.networkInfo.ip.hashed')
  }

  // Storing the data
  res.locals.userData = user

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'User data is malformed.',
          pid:     process.pid
        },
        err: invalidFields
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'User data is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export async function formatUserData(req,res,next) {
  const { context=undefined } = req.headers

  if(context !== 'user')
    return next()

  const {
    identifier=undefined,
    media=undefined,
    secured=undefined
  } = res.locals.userData

  /* Generate Access (Recovery) Keys */
  let recoveryCodes = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate', undefined, {
      headers: {
        context: 'access-keys',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }
    })
  recoveryCodes = recoveryCodes.data['PubSub Response']?.payload

  /* Generate JWTs, UUID's Tokens */
  let jwtsAndIds = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate', {
      typesRequested: { ajwt: true, sjwt: true }
    }, {
      headers: {
        context: 'jwts',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }
    })
  jwtsAndIds = jwtsAndIds?.data['PubSub Response']?.payload

  const [sjwt] = jwtsAndIds.filter(jwtid => jwtid.type === 'sjwt')
  const [ajwt] = jwtsAndIds.filter(jwtid => jwtid.type === 'ajwt')
  const uuid = sjwt?.uuid
  const suid = sjwt?.suid

  /* ## GENERATE AES HASHES ## */
  /* AES Encrypted Fields */
  const jwtsEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: [ sjwt.token ]
  }, {
    headers: {
      context: 'aes',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [ sjwtHashed ] = jwtsEncryptedResults?.data['PubSub Response']?.payload

  /* ## GENERATE BCRYPT HASHES ## */
  /* BCrypt Encrypted Fields */
  const passwordEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: [ secured?.credentials?.password ]
  }, {
    headers: {
      context: 'bcrypt',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [ passwordHashed ] = passwordEncryptedResults?.data['PubSub Response']?.payload

  /* ## GENERATE SHA256 HASHES ## */
  /* Step 1: All general information encrypted */
  const infoToEncrypt = [
    secured?.session?.networkInfo?.ip,
    secured?.credentials?.email,
    secured?.credentials?.dateOfBirth
  ]
  const infoEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: infoToEncrypt
  }, {
    headers: {
      context: 'sha256',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [
    ipHashed,
    emailHashed,
    dateOfBirthHashed
  ] = infoEncryptedResults?.data['PubSub Response']?.payload
  /* Step 2: Recovery Tokens Hashed */
  const recoveryEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: recoveryCodes
  }, {
    headers: {
      context: 'sha256',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const recoveryCodesHashed = infoEncryptedResults?.data['PubSub Response']?.payload

  const userData = {
    _id: uuid,

    identifier: {
      usernames:[{content: identifier.username, suid}],
      tags:     [{content: identifier.tag,      suid}],
      bios:     [{content: identifier.bio,      suid}]
    },

    media: {
      avatar: [{
        contentURL: media.avatar.contentURL,
        suid: suid
      }],
      banner: [{
        contentURL: media.banner.contentURL,
        suid: suid
      }],
      ambience: [{
        colorHex: media.ambience.colorHex,
        suid: suid
      }]
    },

    // TODO: status: {tier: [{ content: 'free' }]}, see if this populated as defaults are set...
    // TODO: check if peers will populate by default
    secured: {

      credentials: {
        email:         {content:{ hashed: emailHashed },       suid},
        passwords:    [{content:{ hashed: passwordHashed},     suid}],
        dateOfBirths: [{content:{ hashed: dateOfBirthHashed }, suid}]
      },

      recovery: {
        codes: [{codeSet:{ hashes: recoveryCodesHashed }, suid}]
      },

      sessions: [
        {
          ...secured.session,
          suid: suid,
          sjwt: {hashed: sjwtHashed},
          networkInfo: {
            ...secured.session.networkInfo,
            ip: {hashed: ipHashed}
          }
        }
      ]
    }
  }

  /* Updating userData object */
  res.locals.schemasData = [{
      type: 'user',
      data: userData
  }]

  /* Data returned upon request completion */
  res.locals.dataToReturn = {
    sjwt,
    ajwt,
    recoveryCodes
  }

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function preventMalformedBubbleData(req,res,next) {
  const { context=undefined } = req.headers

  if(context !== 'bubble')
    return next()

  let {
    bubble={
      meta: {
        expiresOnEpoch: undefined,
        region:         undefined,
        language:       undefined
      },

      content: {
        title:       undefined,
        description: undefined,
        tags:        [undefined],
        category:    undefined,
        media: {
          imageURL: undefined,
          bannerURL: undefined,
          colorHEX: undefined
        }
      },

      preferences: {
        maxNumPeers: undefined,
        vibesRange: {
          min:       undefined,
          max:       undefined
        }
      },

      status: {
        protected: undefined,
      },

      access: {
        paywall: {
          comets: undefined
        }
      }
    }
  } = req.body

  /* Preventing Undefined Data */
  let invalidFields = []
  {
    if(req.body.bubble   === undefined) invalidFields.push('Missing: bubble data')

    if(bubble.content.title       === undefined) invalidFields.push('Missing: bubble.content.title')
    if(bubble.content.description === undefined) invalidFields.push('Missing: bubble.content.description')
    if(bubble.content.tags        === undefined) invalidFields.push('Missing: bubble.content.tags')
    if(bubble.content.category    === undefined) invalidFields.push('Missing: bubble.content.category')
    if(bubble.content.media.bannerURL === undefined) invalidFields.push('Missing: bubble.content.media.bannerURL')
    if(bubble.content.media.imageURL  === undefined) invalidFields.push('Missing: bubble.content.media.imageURL')
    if(bubble.content.media.colorHEX  === undefined) invalidFields.push('Missing: bubble.content.media.colorHEX')

    if(bubble.meta.region   === undefined) invalidFields.push('Missing: bubble.meta.region')
    if(bubble.meta.language === undefined) invalidFields.push('Missing: bubble.meta.language')

    if(bubble.preferences.maxNumPeers    === undefined) invalidFields.push('Missing: bubble.preferences.maxNumPeers')
    if(bubble.preferences.vibesRange.min === undefined) invalidFields.push('Missing: bubble.preferences.vibesRange.min')
    if(bubble.preferences.vibesRange.max === undefined) invalidFields.push('Missing: bubble.preferences.vibesRange.max')

    if(bubble.status.protected === undefined) invalidFields.push('Missing: bubble.status.protected')
  }

  // Storing the data
  res.locals.bubbleData = bubble

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Bubble data is malformed.',
          pid:     process.pid
        },
        err: invalidFields
      }
    })

  res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Bubble data is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export async function formatBubbleData(req,res,next){
  const { context=undefined } = req.headers
  if(context !== 'bubble')
    return next()

  const {
    content,
    meta,
    preferences,
    status,
    media,
    secured,
    access
  } = res.locals.bubbleData

  /* Generate Stream Key */
  let streamKey = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
    { amount: 1 }, {
      headers: {
        context: 'stream-keys',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }
    })
  streamKey = streamKey.data['PubSub Response']?.payload[0]

  /* Generate Access (Recovery) Keys */
  let inviteCodes = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
    { amount: 1 }, {
    headers: {
      context: 'access-keys',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  inviteCodes = inviteCodes.data['PubSub Response']?.payload

  /* Generate Snowflake ID */
  let snowflakeID = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
    undefined, {
      headers: {
        context: 'snowflake',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }})
  snowflakeID = snowflakeID.data['PubSub Response']?.payload.snowflake

  res.locals.dataToReturn = {
    streamKey,
    inviteCodes,
    snowflakeID
  }

  /* AES Encrypted Fields */
  const inviteCodesEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: inviteCodes
  }, {
    headers: {
      context: 'aes',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const inviteCodesHashed = inviteCodesEncryptedResults?.data['PubSub Response']?.payload

  /* AES Encrypted Fields */
  const streamKeyEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
    array: [ streamKey ]
  }, {
    headers: {
      context: 'aes',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [streamKeyEncrypted] = streamKeyEncryptedResults?.data['PubSub Response']?.payload

  res.locals.schemasData = [{
    type: 'bubble',
    data: {
      _id: snowflakeID,

      meta: {
        ...meta,
        poster: {
          uuid: res.locals?.authTokenContents?.uuid,
          suid: res.locals?.authTokenContents?.suid
        }
      },

      content,

      preferences: {
        maxNumPeers: [{...preferences.maxNumPeers}],
        vibesRange: [{...preferences.vibesRange}]
      },

      status: {
        protected: [{ content: status.protected }]
      },

      access: {
        paywall: [
          {
            comets: access.paywall.comets,
            uuid:   res.locals?.authTokenContents?.uuid,
            suid:   res.locals?.authTokenContents?.suid
          }
        ]
      },

      secured: {
        inviteCodes: {hashes: inviteCodesHashed},
        streamKey:   {hashed: streamKeyEncrypted}
      }
    }
  }]

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function formatQuickieData(req,res,next){
  const { context=undefined } = req.headers
  if(context !== 'quickie')
    return next()

  const {
    content,
    meta,
    preferences,
    status,
    secured
  } = res.locals.quickieData

  /* Generate Snowflake ID */
  let snowflakeID = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
    undefined, {
      headers: {
        context: 'snowflake',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }})
  snowflakeID = snowflakeID.data['PubSub Response']?.payload.snowflake

  res.locals.dataToReturn = {snowflakeID}

  res.locals.schemasData = [{
    type: 'quickie',
    data: {
      _id: snowflakeID,
      content,
      meta: {
        ...meta,
        poster: {
          uuid: res.locals?.authTokenContents?.uuid,
          suid: res.locals?.authTokenContents?.suid
        }
      },
      preferences: {
        maxNumPeers: [{content: preferences.maxNumPeers}],
        vibesRange: [{...preferences.vibesRange}]
      },
      status: {
        protected: [{content: status.protected}]
      }
    }
  }]

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function preventMalformedReportData(req,res,next) {
  const { context=undefined } = req.headers

  if(context !== 'report')
    return next()

  let {
    report={
      content: {
        issue:       undefined,
        direction:   undefined,
        description: undefined,
        ids: {
          quid: undefined,
          buid: undefined,
          uuid: undefined
        }
      }
    }
  } = req.body

  /* Preventing Undefined Data */
  let invalidFields = []
  {
    if(req.body.report === undefined) invalidFields.push('Missing: report data')

    if(report.content.issue     === undefined) invalidFields.push('Missing: report.content.issue')
    if(report.content.direction === undefined) invalidFields.push('Missing: report.content.direction')
    if(report.content.ids       === undefined) invalidFields.push('Missing: report.content.ids')
  }

  // Storing the data
  res.locals.reportData = report

  if(invalidFields.length === 0)
    return next()

  if(logLevelPresentCheck('ERR'))
    console.log({
      "PubSub Logging": {
        header: {
          status:  'Error',
          code:    400,
          message: 'Report data is malformed.',
          pid:     process.pid
        },
        err: invalidFields
      }
    })

  return res.status(400).send({
    "PubSub Response": {
      header: {
        status:  'Error',
        code:    400,
        message: 'Report data is malformed.',
        pid:     process.pid
      },
      err: invalidFields
    }
  }).end()
}

/**
 * TODO: Add JSDOC here!
 */
export async function formatReportData(req,res,next){
  const { context } = req.headers
  if(context !== 'report')
    return next()

  const { content } = res.locals.reportData

  /* Generate Snowflake ID */
  let snowflakeID = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
    undefined, {
      headers: {
        context: 'snowflake',
        ['ms-secure-key']: process.env.API_MS_SECURE_KEY
      }})
  snowflakeID = snowflakeID.data['PubSub Response']?.payload.snowflake

  res.locals.dataToReturn = { snowflakeID }

  res.locals.schemasData = [{
    type: 'report',
    data: {
      _id: snowflakeID,
      meta: {
        reporter: {
          uuid:     res.locals?.authTokenContents?.uuid,
          suid:     res.locals?.authTokenContents?.suid
        }
      },
      content
    }
  }]

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export async function loginProcessing(req,res,next) {
  const { context } = req.headers
  if(context !== 'login')
    return next()

  const {
    data= {
      email: undefined,
      password: undefined
    },
    network = { ip: undefined },
    platform= {
      description: undefined,
      layout: undefined,
      manufacturer: undefined,
      name: undefined,
      os: undefined,
      prerelease: undefined,
      product: undefined,
      ua: undefined,
      version: undefined,
      architecture: undefined,
      family: undefined
    }
  } = req.body

  /* Step 1: Encrypt email for reference */
  const emailEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt',
    { array: [data.email] }, {
    headers: {
      context: 'sha256',
      ['ms-secure-key']: process.env.API_MS_SECURE_KEY
    }
  })
  const [emailEncrypted] = emailEncryptedResults?.data['PubSub Response']?.payload

  const doc = User
    .aggregate([
      /* Adding only necessary fields */
      { $addFields: { email: "$secured.credentials.email" }},
      { $addFields: { suspensions: "$status.suspensions" }},
      { $addFields: { latestSession: { $last: "$secured.sessions"} }},
      { $addFields: { latestPassword: { $last: "$secured.credentials.passwords" }}},
      /* Removing remainder objects */
      { $unset: ['identifier', 'media', 'status', 'peers', 'secured'] },
      /* Matching against the latest email hash */
      { $match: { "email.content.hashed": emailEncrypted } }
    ])
    .exec()
    .then(async mdbData => {
      /* Affirm existence of document */
      if (mdbData === undefined || mdbData.length === 0)
        return res.status(400).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code: 400,
              message: 'MongoDB failed the query operation.',
              process: process.pid
            }
          }
        })

      const {
        _id= undefined,
        email = undefined,
        suspensions = undefined,
        latestPassword = undefined,
        latestAccount = undefined,
        latestSession = undefined
      } = mdbData[0]

      /* STEP 1: CREDENTIALS CHECK */
      const pass = data?.password
      const docPass = latestPassword?.content?.hashed
      const verifiedDecryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/decrypt', {
        array: [[pass, docPass]]
      }, {
        headers: {
          context: 'bcrypt',
          ['ms-secure-key']: process.env.API_MS_SECURE_KEY
        }
      })
      const [verified] = verifiedDecryptedResults?.data['PubSub Response']?.payload

      if (!verified)
        return res.status(401).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code: 401,
              message: 'Invalid credentials provided.',
              process: process.pid
            }
          }
        })

      /* STEP 2: SUSPENSION CHECK */
      const suspended = handleUserSuspensionsCheck(suspensions)
      if (suspended)
        return res.status(401).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code: 401,
              message: 'User account is suspended.',
              process: process.pid
            }
          }
        })

      /* STEP 3: SESSION CHECK */
      const activeSessions = handleUserActiveSessionsCheck([latestSession])
      const networkIPEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt',
        { array: [network?.ip] }, {
        headers: {
          context: 'sha256',
          ['ms-secure-key']: process.env.API_MS_SECURE_KEY
        }
      })
      const [ encryptedNetworkIP ] = networkIPEncryptedResults?.data['PubSub Response']?.payload

      const [matchingSession] = activeSessions.filter(activeSession => {
        if (activeSession?.networkInfo?.ip?.hashed === encryptedNetworkIP)
          return activeSession
      })

      /* Handle Existing Matching Session or Otherwise */
      if (matchingSession) {

        console.log("MATCHING SESSION")

        /* Step 1: Decrypt the Session Token to return */
        const sessionTokenDecryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/decrypt', {
          array: [matchingSession?.sjwt.hashed]
        }, {
          headers: {
            context: 'aes',
            ['ms-secure-key']: process.env.API_MS_SECURE_KEY
          }
        })
        const [sjwtUnhashed] = sessionTokenDecryptedResults?.data['PubSub Response']?.payload

        /* Step 2 - Generate a new Access Token */
        const generatedAccessTokenResults = await axios.post(
          CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate',
          {
              ids: {
                uuid: _id,
                suid: matchingSession?.suid
              },
              typesRequested: {ajwt: true}
          }, {
            headers: {
              ['ms-secure-key']: process.env.API_MS_SECURE_KEY,
              context: 'jwts'
            }
          })
        const [ajwtDetails] = generatedAccessTokenResults?.data['PubSub Response']?.payload

        if(ajwtDetails !== undefined && sjwtUnhashed !== undefined)
          return res.status(200).send({
            "PubSub Response": {
              header: {
                status:  'Success',
                code:    200,
                message: 'User logged in.',
                process: process.pid
              },
              payload: [
                {
                  type: 'sjwt',
                  token: sjwtUnhashed,
                  uuid: _id,
                  suid: matchingSession?.suid
                },
                ajwtDetails
              ]

            }
          })

        return res.status(500).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code:    500,
              message: 'DB node failed to communicate with crypto.',
              process: process.pid
            }
          }
        })

      } else {

        console.log("NON-MATCHING SESSION")

        /* Step 1 - Generate JWTs */
        const genTokensResults = await axios.post(
          CRYPTO_COMPOSED_URI + '/api/v1/crypto/generate', {
            ids: { uuid: _id },
            typesRequested: { sjwt: true, ajwt: true }
          }, {
            headers: {
              context: 'jwts',
              ['ms-secure-key']: process.env.API_MS_SECURE_KEY
            }
          })
        const [sjwtContents, ajwtContents] = genTokensResults?.data['PubSub Response']?.payload

        /* Encrypt SJWT for Long-term Storage */
        const jwtsEncryptedResults = await axios.post(CRYPTO_COMPOSED_URI + '/api/v1/crypto/encrypt', {
          array: [ sjwtContents?.token ]
        }, {
          headers: {
            context: 'aes',
            ['ms-secure-key']: process.env.API_MS_SECURE_KEY
          }
        })
        const [ sjwtHashed ] = jwtsEncryptedResults?.data['PubSub Response']?.payload

        /* STEP 2 - Upload the Session to the Users Document */
        const resSuid = sjwtContents.suid
        const suidAsBigInt = BigInt(resSuid)
        const sessionObject = {
          suid:      suidAsBigInt,
          sjwt: {
            hashed: sjwtHashed
          },
          active:    true,
          dateEpoch: getDateAsCustomEpoch(),
          platform,
          networkInfo: {
            ip: {
              hashed: encryptedNetworkIP
            }
          }
        }

        const dbUploadResult = await User
          .findByIdAndUpdate(_id, { '$push': { 'secured.sessions' : sessionObject }})

        const payload = [
          sjwtContents,
          ajwtContents
        ]
        /* STEP 3 - Return to user the SJWT & AJWT upon success */
        if(dbUploadResult !== undefined)
          return res.status(200).send({
            "PubSub Response": {
              header: {
                status: 'Success',
                code: 200,
                message: 'User logged in.',
                process: process.pid
              },
              payload
            }
          })

        return res.status(500).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code:    500,
              message: 'DB node failed to communicate with crypto.',
              process: process.pid
            }
          }
        })
      }
    })
    .catch(err => {
      return res.status(400).send({
        "PubSub Response": {
          header: {
            status: 'Error',
            code:    400,
            message: 'Unknown error occurred. MongoDB report is below.',
            process: process.pid
          },
          err: err
        }
      })
    })
}

/**
 * TODO: Add JSDOC here!
 */
export async function logoutProcessing(req,res,next) {
  const { context } = req.headers
  if(context !== 'logout')
    return next()

  const ajwt = req.body?.ajwt

  let ajwtContentsResults
  try {
    ajwtContentsResults = await axios.post(
      CRYPTO_COMPOSED_URI + '/api/v1/crypto/decrypt', {
        array: [ ajwt ]
      }, {
        headers: {
          context: 'jwts',
          ['ms-secure-key']: process.env.API_MS_SECURE_KEY
        }
      })
  } catch (e) { return res.status(500).send({
    "PubSub Response": {
      header: {
        status: 'Error',
        code: 500,
        message: 'Failed to connect to Crypto.',
        pid: process.pid
      }}
  })}

  // TODO:
  const [ajwtContents] = ajwtContentsResults?.data['PubSub Response']?.payload
  console.log({uuid: ajwtContents?.uuid})
  const foundUser = await User.findById(ajwtContents?.uuid)
  console.log({foundUser})
  const dbRes = await User.findOneAndUpdate({_id: ajwtContents.uuid}, { 'secured.sessions.$[].active': false })
  console.log(dbRes)

  return

  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function deletionProcessing(req,res,next) {
  return next()
}

/**
 * TODO: Add JSDOC here!
 */
export function recoveryProcessing(req,res,next) {

  const { context } = req.headers
  if(context !== 'recover')
    return next()

  const {
    recoveryType=undefined,
    data={
      email: undefined,
      code:  undefined
    }
  } = req.body

  // Prevent returning unnecessary data
  let fieldToAdd
  switch (recoveryType){
    case 'code':  fieldToAdd = { latestRecoveryCodeSet: { $last: "$secured.recovery.codes" }}
      break;
    // case 'phone': fieldToAdd = { latestPhone: { $last: "$secured.recovery.phones" }}
    //   break;
    // case '2FA': fieldToAdd = { latestTwoFactorAuth: {$last: "$secured.recovery."} }
    //   break;
    default: fieldToAdd = {}
  }

  // Encrypting with SHA for DB comparisons
  const emailHash = encryptWithSha256(data.email)

  const doc = User
    .aggregate([
      /* Dynamic field per specified query */
      { $addFields: fieldToAdd },
      /* Adding necessary fields to complete the request */
      { $addFields: { email:            "$secured.credentials.email"  }},
      { $addFields: { latestAccount:    { $last: "$secured.account"   }}},
      { $addFields: { latestSession:    { $last: "$secured.sessions"  }}},
      { $addFields: { latestSuspension: { $last: "$status.suspension" }}},
      /* Status Related Queries */
      { $addFields: { latestTier:        { $last: "status.tier"             }}},
      { $addFields: { latestVerified:    { $last: "status.verified"         }}},
      { $addFields: { latestAffiliation: { $last: "status.affiliate.status" }}},
      /* Removing remainder objects */
      { $unset: ['identifier', 'media', 'status', 'peers', 'secured'] },
      /* Matching against the latest email hash */
      { $match: { "email.content.hashed": emailHash } }
    ])
    .exec()
    .then(async mdbData => {

      // If no document matched, exit
      if(mdbData === null || mdbData.length === 0)
        return res.status(400).send({
          "PubSub Response": {
            header: {
              status: 'Error',
              code:    400,
              message: 'Failed to locate the queried document...',
              process: process.pid
            }
          }
        })

      // Prepare to check the provided recovery information
      const {
        /* Verification */
        latestRecoveryCodeSet =undefined,
        latestPhone           =undefined,
        /* Acc Info */
        latestAccount    =undefined,
        latestSession    =undefined,
        latestSuspension =undefined,
        /* Session Info */
        latestTier        =undefined,
        latestVerified    =undefined,
        latestAffiliation =undefined
      } = mdbData[0]

      /* Preventing restricted users access */
      const dateAsEpoch             = getDateAsCustomEpoch()
      const restrictionEpoch        = latestSuspension.expiresOnEpoch
      const restrictedUntilAsString = new Date(restrictionEpoch + process.env.API_CUSTOM_EPOCH)
      // When restriction epoch is zero, it implies no relief date
      if(restrictionEpoch === 0)
        return res.status(400).send({
          "PubSub Response": {
            header: {
              status:  'Error',
              code:    400,
              message: 'Specified account has been suspended indefinitely.',
              process: process.pid
            }
          }
        })

      // Preventing access to account during restricted period
      if (restrictionEpoch - dateAsEpoch > 0)
        return res.status(400).send({
          "PubSub Response": {
            header: {
              status:  'Error',
              code:    400,
              message: `Specified account has been suspended until: ${restrictedUntilAsString}.`,
              process: process.pid
            }
          }
        })

      if(config.recoveryType === "code")
      {
        let codeProvidedIsValidated = false
        // TODO: This may be computationally expensive... Explore other more efficient implementations in the future
        latestRecoveryCodeSet
          .codeSet
          .hashes
          .map(codeHash => { if(verifyHashWithBcrypt(data.code, codeHash)) codeProvidedIsValidated = true })

        if(!codeProvidedIsValidated)
          return res.status(400).send({
            "PubSub Response": {
              status:  'Error',
              code:    400,
              message: 'Provided recovery code is invalid.',
              process: process.pid
            }
          })

        /* Get latest affiliate, tier, verified -> Priv always default to false */
        // TODO: When I come back, implement getting the data and storing in new token
        //        - Generate token
        //        - Push to DB
        //        - Send back to user
        //        - Implement Text Processing
        //        - Implement 2FA Processing - No need to implement... You can log in with 2FA (QR).
        //        - Clean up implementation... This looks like shit. Use res.locals and break it into other middleware

        /* Code validated, continue */
        let status = {
          tier: {
            tuid:      undefined,
            content:   'free',
            expiresOn: 0
          },
          verified: {
            vuid:      undefined,
            content:   false,
            expiresOn: 0
          },
          affiliate:  {
            afuid:     undefined,
            content:   false,
            expiresOn: 0
          }
        }

        let calcExpiration = {expiresIn: '7d'}

        // TODO: Replace this with crypto
        const suid = await getSnowflakeID()

        const payload = {
          type: 'sjwt',
          uuid: determinedUUID,
          suid,
          status
        }

        // Generating Token w/ Above Configs
        let sjwt = jwt.sign(
          payload,
          process.env.API_JWT_SECRET,
          calcExpiration)


      // TODO: Return the latest or generate a new...
      res.status(200).send({
        "PubSub Response": {
          status:  'Success',
          code:    200,
          message: 'Recovery code successfully validated.',
          process: process.pid
        },
        payload: {
          ajwt: "",
          sjwt: "",
          uuid: ""
        }
      })
      }


    })
    .catch(err => {
      return res.status(400).send({
        "PubSub Response": {
          header: {
            status: 'Error',
            code:    400,
            message: 'MongoDB presented the error below.',
            process: process.pid
          },
          err: err
        }
      })
    })
}

/**
 * TODO: Add JSDOC here!
 */
export function checkProcessing(req,res,next) {
  const { context } = req.headers
  if(context !== 'check')
    return next()

  const {
    data={
      email:    undefined,
      password: undefined
    }
  } = req.body

  const emailHash = encryptWithSha256(data.email)

  const doc = User
    .aggregate([
      /* Adding only necessary fields */
      { $addFields: { email: "$secured.credentials.email"}},
      /* Removing remainder objects */
      { $unset: ['identifier', 'media', 'status', 'peers', 'secured'] },
      /* Matching against the latest email hash */
      { $match: { "email.content.hashed": emailHash } }
    ])
    .exec()
    .then(mdbData => {

      /* Will throw if empty: no elements in the array to access if empty */
      const { email = { content: { hashed: undefined } } } = mdbData[0]

      return res.status(200).send({
        "PubSub Response": {
          header: {
            status:  'Success',
            code:    200,
            message: 'User exists in the MongoDB.',
            process: process.pid
          }
        }
      })

    })
    .catch(err => {
      return res.status(400).send({
        "PubSub Response": {
          header: {
            status: 'Error',
            code:    400,
            message: 'Failed to locate the queried document...',
            process: process.pid
          }
        }
      })
    })
}

/**
 * TODO: Add JSDOC here!
 */
export function refreshProcessing(req,res,next){

  const {
    config={
      accessType: undefined
    },
    data={
      ajwt: undefined,
      sjwt: undefined
    }
  } = req.body

  next()
}

/**
 * TODO: Complete JSDoc
 */
export function preventInvalidPermission (req,res,next){

}

/**
 * TODO: Complete JSDoc
 */
export function handleUserDeletion (req,res,next){

}

/**
 * TODO: Complete JSDoc
 */
export function handleBubbleDeletion (req,res,next){

}