/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Database.API
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */

/* EXPRESS IMPORTS */
import express   from 'express'
import cors      from 'cors'
import rateLimit from 'express-rate-limit'

/* MIDDLE-WARE IMPORTS */
import {
    checkCredentialCompliance,
    determineSchema,
    formatUserData,
    formatBubbleData,
    formatQuickieData,
    formatReportData,
    preventInvalidFindType,
    preventInvalidQueryDepth,
    preventMalformedBubbleData,
    preventMalformedQueryConfig,
    preventMalformedReportData,
    preventMalformedUserData,
    retrieveFromMongoDB,
    getSchema,
    preventMalformedUpdateConfig,
    updateDocument,
    preventInvalidAccessType,
    loginProcessing,
    recoveryProcessing,
    preventInvalidRecoveryType,
    uploadSchemas,
    logoutProcessing,
    preventInvalidContextType,
    checkUserExistenceCompliance,
    preventInvalidPermission,
    handleUserDeletion,
    handleBubbleDeletion
} from './middleware.js'

/* Global Functions */
import {
    handleHeathCheck,
    handleAppListen,
    isTrustedSource,
} from './global.middleware.js'

/* OS IMPORTS */
import os      from 'os'
import cluster from 'cluster'

/* ENV & JSON CONFIG */
import dotenv from 'dotenv/config'

/* MONGODB IMPORTS */
import mongoose from 'mongoose'
let {
    API_MDB_USR_AND_PASS,
    API_MDB_URI,
    API_MDB_DB_NAME,
    API_MDB_OPTS
} = process.env
let connection_uri = `mongodb+srv://${API_MDB_USR_AND_PASS}@${API_MDB_URI}/${API_MDB_DB_NAME}${API_MDB_OPTS}`

function handleMongoDBConnection(connection_uri) {

    // Err holder
    let err

    // Establish connection to MongoDB Atlas
    mongoose.connect(connection_uri, err)

    return new Promise((resolve, reject) => {
        // Just return the resolve
        if(!err) return resolve()

        let psLog = {
            'PubSub Logging': {
                message: 'PubSub API failed to connect to MongoDB.',
                logging: {
                    error: err,
                    os: {
                        process:    process.pid,
                        cpus:       os.cpus(),
                        arch:       os.arch(),
                        endianness: os.endianness(),
                        version:    os.version(),
                        memory: {
                            free:  os.freemem(),
                            total: os.totalmem()
                        },
                        priority:   os.getPriority(process.pid),
                        hostname:   os.hostname(),
                    }
                }
            }
        }

        reject([err, psLog])
    })
}

// When the process is set to close, kill the connection
process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log({
            "PubSub Logging": {
                status:  'Healthy',
                message: "Mongoose connection closed upon app termination...",
                process: process.pid
            }
        })
        process.exit(0)
    })
})

/* Rate Limitations */
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100,                 // Limit each IP to 100 requests per `window` (here, per 15 minutes)
    standardHeaders: true,    // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false,     // Disable the `X-RateLimit-*` headers
})

/* EXPRESS-APP CONFIGURATION */
const app = express()
app.use(cors())
app.use(express.json())
// TODO: Implement 'app.use(limiter)' in the future for all nodes
app.use((req, res, next) =>
{
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    next()
})

// TODO: CRITICAL -> Implement a caching system such as REDIS to prevent unnecessary expensive DB calls

/* ~~~ MASTER PROCESS ~~~ */
if (cluster.isMaster)
{
    console.log(`Master process <${process.pid}> is running.`)
    /* For Workers */
    for(let i = 0; i < os.cpus().length; i++)
        cluster.fork()

    cluster.on('exit', (worker, code, signal) => {
        console.log(`Worker ${worker.process.pid} died.`)
    })
}

/* ~~~ CHILD PROCESSES ~~~ */
else {

    /** @description Tells the express application to begin listening on the configured network */
    handleMongoDBConnection(connection_uri)
      .then(() => app.listen(process.env.API_PORT, handleAppListen))
      .catch(err => {
          console.log(err)
          process.abort()
      })

    /**
     * @description Used to verify that the node is responding.
     * @status      Operational
     */
    app.get('/api/v1/db/healthcheck',
        /* Security */
        isTrustedSource,
        /* Processing */
        handleHeathCheck)

    /* ~~~ */

    /**
     * @endpoint
     * @name        /api/v1/db/query
     * @memberOf    api.v1.db.query
     * @description Retrieves data from MongoDB based on the users query in a restricted and secure fashion
     * @use-case    When the USER would like to retrieve anything data related
     * @returns     {Object} - MongoDB Bubble Object Data + Response Meta-Data
     * @body
     * config: {
     *  contentType: ['bubble', 'quickie', 'user', 'report'],
     *  findType:    ['one', 'multi', 'id'],
     *  protected:   [true, false],
     *  selection:   ['locked', 'protected', 'public'],
     *  filterBy:    userDefined,
     * }
     * @status TODO: Testing...
     */
    app.post('/api/v1/db/query',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventMalformedQueryConfig,
        preventInvalidContextType,
        preventInvalidFindType,
        preventInvalidQueryDepth,
        /* Processing */
        determineSchema,
        /* Retrieve */
        retrieveFromMongoDB)  // Status: Perform Touch-Ups

    /**
     * @endpoint
     * @name        /ap1/v1/db/create/
     * @memberOf    api.v1.db.create
     * @description Receives a config and model data in the body of the request and handles the creation of the model in the DB accordingly
     * @use-case    Creates all models in the database
     * @return      The documents uuid with a success message
     * @body
     * config: {
     *   contentType: <Model Type>
     * },
     * <Model Type>: Object data
     */
    app.post('/api/v1/db/create',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidContextType,
        preventMalformedBubbleData,
        preventMalformedUserData,
        preventMalformedReportData,
        checkCredentialCompliance,
        checkUserExistenceCompliance,
        /* Processing */
        formatUserData,
        formatBubbleData,
        formatQuickieData,
        formatReportData,
        /* Return */
        uploadSchemas)

    /**
     * @endpoint
     * @name        /ap1/v1/db/update/
     * @memberOf    api.v1.db.update
     * @description Updates parts of DB Models
     * @use-case    Updating data in the DB, updating session tokens
     * @return      Success message with ID of the object updated
     * @body
     * config: {
     *   contentType: <Model Type>
     * },
     * <Model Type>: Object data
     * TODO: Implement:
     *          - Location checks and prevention
     *          - Prevention of updating documents not owned to user
     *          - Admin bypass for:
     *              - Updating privileged array
     *              - Updating suspension array
     *              - Updating tier       array
     *              - Updating verified   array
     *          - Mod bypass for:
     *              - Updating suspension array
     */
    app.post('/api/v1/db/update',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidContextType,
        preventMalformedUpdateConfig,
        /* Processing */
        getSchema,
        updateDocument)

    /**
     * @endpoint
     * @name        /ap1/v1/db/access/
     * @memberOf    api.v1.db.access
     * @description Handles logging in and account recovery
     * @use-case    Account Access
     * @return      Credentials
     * @body
     * config: {
     *   accessType: ['login', 'recover', 'check'],
     *   recoveryType: ['code', 'phone', '2FA']
     * },
     * data: {
     *   email:         String,
     *   password:      String,
     *   code:          String,
     *   phone:         String,
     *   TwoFactorCode: String (coming soon)
     * }
     */
    app.post('/api/v1/db/access',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidAccessType,
        preventInvalidRecoveryType,
        /* Processing */
        loginProcessing,
        logoutProcessing,   // TODO: FR#79 - https://gitlab.com/rsoh/pubsub/-/issues/79
        recoveryProcessing) // TODO: << Differed until a further notice >>

    /**
     * @endpoint
     * @name        /ap1/v1/db/delete/
     * @memberOf    api.v1.db.delete
     * @description Deletes user model from DB
     * @use-case    Deletes models from DB
     * @return      TODO: Make return comment
     * @body
     * TODO: Update body comment
     */
    app.post('/api/v1/db/delete',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidPermission, // TODO: FR#76 - https://gitlab.com/rsoh/pubsub/-/issues/76
        /* Processing */
        handleUserDeletion,       // TODO: FR#77 - https://gitlab.com/rsoh/pubsub/-/issues/77
        handleBubbleDeletion)     // TODO: FR#78 - https://gitlab.com/rsoh/pubsub/-/issues/78
}