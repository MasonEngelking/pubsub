import {getDateAsCustomEpoch} from "./global.middleware";

let availableBubbles = []

// const example = [
//   {
//     buid: 123456789,
//     title: "",
//     description: "",
//     imgURI: "",
//     exp: 123456790
//   }
// ]

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function handleBubbleCreate(req,res,next) {
  // TODO: Add the received bubble to the above array to be
}

export function handleBubbleDelete(req,res,next) {
  // TODO: Add the received bubble to the above array to be
}

export function handleBubbleQuery(req,res,next){

}

/**
 * @description Every second updates the availableBubbles array with Bubbles
 * @returns {Promise<void>}
 * @constructor
 */
export async function PurgeExpiredBubblesHandler() {
  availableBubbles = availableBubbles.filter(bubble => {
    const { exp } = bubble
    if (exp > getDateAsCustomEpoch())
      return bubble
  })
  await sleep(1000)
  await PurgeExpiredBubblesHandler()
}