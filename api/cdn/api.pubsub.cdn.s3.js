/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name CDN.API
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */
import express    from 'express'
import path       from 'path'
import cors       from 'cors'
import fileUpload from 'express-fileupload'
import os         from 'os'
import cluster    from 'cluster'
import dotenv     from 'dotenv/config'
/* Middleware */
import {
    configParser,
    filenameParser,
    getUniqueDate,
    S3Upload,
    appendFilenameExtensionInfoToRes,
    prepDirectoryString,
    preventInvalidContentType,
    preventInvalidFileExtension,
    processAttachmentAssets,
    processBannerAssets,
    processIconAvatarAssets,
    preventInvalidContext
} from './middleware.js'
import {
    handleAppListen,
    isTrustedSource,
    handleHeathCheck
} from './global.middleware.js'

/* Express App Configuration */
const app = express()
app.use(cors())
app.use(fileUpload({
    limits: {fileSize: 5*1024*1024},
    abortOnLimit: true
}))
app.use(express.json())
app.use((res, req, next) => {
    res.header('Access-Control-Allow-Origin',  '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    next()
})

/* ~~~ MASTER PROCESS ~~~ */
if (cluster.isMaster)
{
    console.log(`Master process <${process.pid}> is running.`)

    /* For Workers */
    for(let i = 0; i < os.cpus().length; i++)
        cluster.fork()

    cluster.on('exit', (worker, code, signal) => console.log(`Worker ${worker.process.pid} died.`))
}
/* WORKER PROCESSES -> HANDLE API CALLS */
else {

    /**
     * TODO: Add documentation
     */
    app.get('/api/v1/cdn/healthcheck/',
        /* Security */
        isTrustedSource,
        /* Processing */
        handleHeathCheck)

    /**
     * TODO: Add documentation
     */
    app.post('/api/v1/cdn/create',
        /* Security */
        isTrustedSource,
        /* Parsing */
        filenameParser,
        appendFilenameExtensionInfoToRes,
        configParser,
        prepDirectoryString,
        /* Prevention */
        preventInvalidContext,
        preventInvalidFileExtension,
        preventInvalidContentType,
        /* Processing */
        processAttachmentAssets,
        processBannerAssets,
        processIconAvatarAssets,
        /* Uploading */
        S3Upload)

    /** 
     * TODO: Add documentation
     */
    app.post('/api/v1/cdn/delete',
        /* Security */
        isTrustedSource,
        /* Prevention */
        /* Processing */)

    app.listen(process.env.API_PORT, handleAppListen)
}