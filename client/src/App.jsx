/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name App
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import styled from 'styled-components'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

/* Global Components */
import Mount from './routes/mount.jsx'

/* Web Components */
import Index from './routes/index'
import Home from './routes/index/home.index.jsx'
import About from './routes/index/about.index.jsx'
import Legal from './routes/index/legal.index.jsx'
import Contact from './routes/index/contact.index.jsx'

/* Web-App Components */
import AccountSelector from './routes/account-portal/selector.account-portal.jsx'
import GuestAccountPortal from './routes/account-portal/guest.account-portal.jsx'
import AccountPortal from './routes/account-portal/container.jsx'
import RecoverAccountPortal from './routes/account-portal/recover.account-portal.jsx'
import SignInAccountPortal from './routes/account-portal/sign-in.account-portal.jsx'
import SignUpAccountPortal from './routes/account-portal/sign-up.account-portal.jsx'
import AppContainer from './routes/app/mount.view.jsx'
import BubblesNav from './routes/app/bubbleGrid.component.jsx'
import ErrorIndex from './routes/index/error.index.jsx'
import HubsView from './routes/app/hubs.view.jsx'
import BubbleView from './routes/app/bubble.view.jsx'
import MediaStateManager from './scripts/PreflightManagers.jsx'
import AppStatusView from './routes/app/app-status.view.jsx'
import { createTheme, ThemeProvider } from '@mui/material'
import { DARK_THEME } from './constants/themes.constants.js'

/* Development Components */

/* Styles */
const ApplicationContainer = styled.div`
  height:           100%;
  width:            100vw;
  margin:           0;
  overflow-x:       hidden;
  overflow-y:       hidden;
  position:         relative;
  display:          flex;
  font-family:      brandon-grotesque, sans-serif;
  font-style:       normal;
  
  h1 { font-size: 52px }
  h2 { font-size: 32px }
  h3 { font-size: 22px }
  h4 { font-size: 16px }
`

const CUSTOM_MUI_THEME = createTheme({
  components: {
    MuiMenu: {
      styleOverrides: {
        paper: {
          padding: '0 !important',
          margin: '0 !important',
          background: 'none !important',
          boxShadow: 'none !important',
          ul: {
            padding: '0 !important'
          }
        }
      }
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: '14px',
          padding: '12px',
          borderRadius: '8px',
          borderStyle: 'solid',
          borderColor: DARK_THEME.tooltip.borderColor,
          backgroundColor: DARK_THEME.tooltip.backgroundColor
        }
      }
    }
  }
})

export default
function App (props) {
  /* THEME STATE... */
  const DARK_MODE_ENABLED = true
  const appManager = useSelector(state => state.appManager)
  const isAppReady = true // appManager.isReady

  const handleAppRender = () => {
    if (isAppReady) {
      return (
        <>
          <Route path='' element={<Navigate to={'hub'}/>}/>
          <Route path='hub' element={<HubsView/>}>
            <Route path='' element={<Navigate to={'pub'}/>}/>
            <Route path=':hubLabel' element={<BubblesNav/>}/>
          </Route>
          <Route path='bubble/:bubbleID' element={<BubbleView/>}/>
        </>
      )
    } else {
      return <Route path={'*'} element={<AppStatusView/>}/>
    }
  }

  /* Router for the entire site/application */
  return (
    <ThemeProvider theme={CUSTOM_MUI_THEME}>
      <ApplicationContainer>
        <Router>
          <Routes>
            {/* Website: Upon Mount Logic */}
            <Route path='/' element={<Mount darkMode={DARK_MODE_ENABLED}/>}>

              {/* Default Error Page */}
              <Route path='*' element={<ErrorIndex/>} />

              {/* Website Routes */}
              <Route path='' element={<Index/>}>
                <Route path='' element={<Home/>}/>
                <Route path='about' element={<About/>}/>
                <Route path='legal' element={<Legal/>}/>
                <Route path='contact' element={<Contact/>}/>
              </Route>

              {/* Account-Specific Panels */}
              <Route path='account-portal' element={<AccountPortal/>}>
                <Route path='' element={<AccountSelector/>} />
                <Route path='sign-in' element={<SignInAccountPortal/>} />
                <Route path='sign-up' element={<SignUpAccountPortal/>} />
                <Route path='recovery' element={<RecoverAccountPortal/>} />
                <Route path='guest' element={<GuestAccountPortal/>} />
              </Route>

              {/* Socket Connection Determines Application Rendering */}
              {/* Render App Per Usual */}
              <Route path='app/*' element={<AppContainer/>}>
                {/* Render all other routes upon successful application loading */}
                {handleAppRender()}
              </Route>
            </Route>

          </Routes>
        </Router>
      </ApplicationContainer>
    </ThemeProvider>
  )
}
