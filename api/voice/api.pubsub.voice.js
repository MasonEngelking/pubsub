/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name PubSub_Voice
 *
 * @returns {Express_SocketIO} Express Node and WebSocket
 *
 * @author Christian C. Larcomb
 */

/* API & SOCKET DEP */
import express     from 'express'
import http        from 'http'
//import https       from 'https'
import * as socket from 'socket.io'
import fs          from 'fs'
import wrtc        from 'wrtc'

/* OS DEP */
// TODO:
//  Figure the proper method to multi-threading the voice-server which requires shared data
//   - Furthermore, each Node maintaining state will somehow need to share said state across
//     multiple containers
import os        from 'os'
import cluster   from 'cluster'

/* ENV & JSON CONFIG */
import dotenv           from "dotenv/config"
import bodyParser       from "express"
import {
    handleAppListen,
    handleHeathCheck,
    isTrustedSource,
    preventIncorrectAuthTokenSJWT,
    preventInvalidAuthToken,
    preventMissingAuthToken
} from "./global.middleware.js"

/* ESSENTIAL VARS */
const app  = express()
app.use(express.static('public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
app.use((req, res, next) =>
{
    res.header('Access-Control-Allow-Origin',  '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    // res.header('Vary', 'Origin')
    next()
})

const server = http.createServer(app)
const io     = new socket.Server(server)

let Bubbles = [
    {
        comment:          'Test Bubble',
        id:               "16548135165",
        dateCreatedEpoch: "234234234",
        users: [
            /*
            {
                socketID:  "REMOTE_USER",
                uuid:      "REMOTE_USER",
                username:  "REMOTE_USER",
                imgString: "REMOTE_USER",
                stream:    undefined
            }
             */
        ]
    }
]

/**
 * @description Tells the express application to begin listening on the configured network
 * @status      Operational
 */
server.listen(process.env.API_PORT, handleAppListen)

/** @description Used to verify that the node is responding */
app.get('/api/v1/voice/healthcheck/',
  /* Security */
  isTrustedSource,
  /* Processing */
  handleHeathCheck)

/**
 * @description Provides a video/audio stream to the voice server
 */
app.post('/api/v1/voice/provide-stream', async ({ body }, res) =>
{
    // TODO: Deprecate this
    /** @deprecate As soon as possible. UUID and BubbleID should NOT be outside a JWT for security purposes... */
    const { uuid, username, imgString, bubbleID, socketID, ltd } = body

    let userStream

    let bubbleSpecified          = Bubbles.find(bubble => bubble.id === bubbleID)
    let usersSelfExcluded        = bubbleSpecified.users.filter(user => user.id !== uuid)
    let bubblesSpecifiedExcluded = Bubbles.filter(bubble => bubble.id !== bubbleSpecified.id)

    const peer = new wrtc.RTCPeerConnection({iceServers: [{urls: "stun:stun.stunprotocol.org"}]})

    peer.ontrack = (e) => userStream = e.streams[0]

    const desc = new wrtc.RTCSessionDescription(ltd)
    await peer.setRemoteDescription(desc)
    const answer = await peer.createAnswer()
    await peer.setLocalDescription(answer)
    const payload = { ltd: peer.localDescription }

    // Establish user object to store
    let userObject = {
        socketID:  socketID,
        uuid:      uuid,
        username:  username,
        imgString: imgString,
        stream:    userStream,
        peerObj:   peer
    }

    console.log("Users self excluded:")
    console.log(usersSelfExcluded)

    console.log("user object")
    console.log(userObject)

    bubbleSpecified.users = [
        ...usersSelfExcluded,
        userObject
    ]
    Bubbles = [
        ...bubblesSpecifiedExcluded,
        bubbleSpecified
    ]

    // DEBUGGING OUTPUT: Making sure that the bubbles are being updated correct...
    //console.log("BUBBLES SET")
    //console.log(Bubbles)
    // TODO: Focus on this to ensure user joining is fine
    io.to(bubbleID).emit('user-join', uuid)

    res.json(payload)
})

/**
 * @description Retrieves a set of peer streams from the voice server
 */
app.post("/api/v1/voice/retrieve-peer-streams",
  async ({ body }, res) =>
{
    // TODO: Deprecate this
    /** @deprecate As soon as possible. UUID and BubbleID should NOT be outside a JWT for security purposes... */
    const { bubbleID, uuid, index, ltd } = body

    // Determining Current and Future User Index
    const currentUserIndex = index
    const nextUserIndex = index + 1

    // Retrieving Current Bubble Data, User
    const bubblesSpecified    = Bubbles.find(bubble => bubble.id === bubbleID)
    //console.log("Bubble Specified:", bubbleID)

    if(bubblesSpecified.users.length !== 0)
    {
        const indexedUser         = bubblesSpecified.users[currentUserIndex]
        const isNextUserAvailable = typeof bubblesSpecified.users[nextUserIndex] !== "undefined"

        if (typeof indexedUser !== "undefined" && uuid !== indexedUser.uuid)
        {
            const peer = new wrtc.RTCPeerConnection({iceServers: [{urls: "stun:stun.stunprotocol.org"}]})
            const desc = new wrtc.RTCSessionDescription(ltd)
            await peer.setRemoteDescription(desc)

            indexedUser.stream.getTracks().forEach(track => peer.addTrack(track, indexedUser.stream))
            const answer = await peer.createAnswer()
            await peer.setLocalDescription(answer)

            const userData = {
                ltd: peer.localDescription,
                uuid: indexedUser.uuid,
                username: indexedUser.username,
                imgString: indexedUser.imgString,
                isPeerRemaining: isNextUserAvailable
            }

            res.json(userData)
        }
    }

})

/**
 * TODO: Add JSDoc
 */
app.post("/api/v1/voice/retrieve-peer-stream", async ({body}, res) => {
    //console.log("BUBBLES")
    //console.log(Bubbles)

    // TODO: Deprecate this
    /** @deprecate As soon as possible. UUID and BubbleID should NOT be outside a JWT for security purposes... */
    const { bubbleID, peerUUID, ltd } = body

    // Retrieving Current Bubble Data, User
    const bubblesSpecified = Bubbles.find(bubble => bubble.id === bubbleID)
    const userSpecified = bubblesSpecified.users.find(user => user.uuid === peerUUID)

    if (typeof userSpecified !== "undefined")
    {
        const peer = new wrtc.RTCPeerConnection({iceServers: [{urls: "stun:stun.stunprotocol.org"}]})
        const desc = new wrtc.RTCSessionDescription(ltd)
        await peer.setRemoteDescription(desc)

        userSpecified.stream.getTracks().forEach(track => peer.addTrack(track, userSpecified.stream))
        const answer = await peer.createAnswer()
        await peer.setLocalDescription(answer)

        const userData = {
            ltd: peer.localDescription,
            username: userSpecified.username,
            imgString: userSpecified.imgString
        }

        res.json(userData)
    }

})

/**
 * TODO: Add JSDoc
 */
io.on('connection', socket => {
    // TODO: Implement security measures after functionality has been put in place...
    // Query data
    let token = socket.handshake.query.accessToken
    let bubbleID = socket.handshake.query.bubbleID
    let isUserValid = false
    let isBubbleDataAvailable = typeof Bubbles.find(id => id === bubbleID) !== "undefined"
    let isBubbleRoomAvailable = typeof io.sockets.adapter.rooms[bubbleID] !== "undefined"

    // Announce to console user connection
    console.log(`Socket ${socket.id} established a connection.`)

    /* TEMP OVERRIDES */
    isUserValid           = true
    isBubbleDataAvailable = true
    isBubbleRoomAvailable = true

    if (isUserValid && isBubbleDataAvailable && isBubbleRoomAvailable)
        socket.join(bubbleID)
    else
        socket.disconnect(true)

    // Handle user disconnection
    socket.on('disconnect', () =>
    {
        console.log(`Socket ${socket.id} disconnected... Updating state & Clearing peer connections by alerting clients.`)

        // TODO: This is by no means computationally efficient... Will need to look into alternatives in the future for the sake of performance.
        let bubbleSpecified = Bubbles.filter(bubble => bubble.users.find(user => user.socketID === socket.id))[0]
        //console.log("Bubble Specified:", bubbleSpecified)

        let bubblesSpecifiedExcluded = Bubbles.filter(bubble => bubble.id !== bubbleSpecified.id)
        //console.log("Bubble Specified Ex:", bubblesSpecifiedExcluded)

        let userFound = bubbleSpecified.users.find(user => user.socketID === socket.id)
        //console.log("User Found:", userFound)

        let usersSelfExcluded = bubbleSpecified.users.filter(user => user.uuid !== userFound.uuid)
        //console.log("Users self ex:", usersSelfExcluded)

        bubbleSpecified.users = [...usersSelfExcluded]
        Bubbles = [...bubblesSpecifiedExcluded, bubbleSpecified]

        // Report out to the users of the room the user that left!
        io.to(bubbleID).emit('user-leave', userFound.uuid)
    })
})

/**
 * TODO: Add JSDoc
 */
io.on('connection-error', err => console.log('Connection Error!'))
