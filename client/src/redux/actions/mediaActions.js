/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import {
  SET_AVAILABLE_MEDIA_STATE,
  SET_SELECTED_MEDIA_STATE,

  SET_SELECTED_MEDIA_STATE_AUDIOIN,
  SET_SELECTED_MEDIA_STATE_AUDIOOUT,
  SET_SELECTED_MEDIA_STATE_VIDEOIN,

  SET_DEVICE_PERMISSION_AUDIO,
  SET_DEVICE_PERMISSION_VIDEO
} from './actionTypes.js'

const availableMediaStateUpdate = (audioInputs, audioOutputs, videoInputs) => ({
  type: SET_AVAILABLE_MEDIA_STATE,
  payload: {
    audioInputs,
    audioOutputs,
    videoInputs
  }
})

const selectedMediaStateUpdate = (audioInput, audioOutput, videoInput) => ({
  type: SET_SELECTED_MEDIA_STATE,
  payload: {
    audioInput,
    audioOutput,
    videoInput
  }
})

const selectedMediaStateUpdateAudioIn = (audioIn) => ({
  type: SET_SELECTED_MEDIA_STATE_AUDIOIN,
  payload: {
    audioIn
  }
})

const selectedMediaStateUpdateAudioOut = (audioOut) => ({
  type: SET_SELECTED_MEDIA_STATE_AUDIOOUT,
  payload: {
    audioOut
  }
})

const selectedMediaStateUpdateVideoIn = (videoIn) => ({
  type: SET_SELECTED_MEDIA_STATE_VIDEOIN,
  payload: {
    videoIn
  }
})

const devicePermissionsUpdateAudio = (audioPermission) => ({
  type: SET_DEVICE_PERMISSION_AUDIO,
  payload: {
    audioPermission
  }
})

const devicePermissionsUpdateVideo = (videoPermission) => ({
  type: SET_DEVICE_PERMISSION_VIDEO,
  payload: {
    videoPermission
  }
})

export default {
  availableMediaStateUpdate,
  selectedMediaStateUpdate,
  selectedMediaStateUpdateAudioIn,
  selectedMediaStateUpdateAudioOut,
  selectedMediaStateUpdateVideoIn,

  devicePermissionsUpdateAudio,
  devicePermissionsUpdateVideo
}
