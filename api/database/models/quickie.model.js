/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Quickie.Model
 *
 * @returns {MongoDB_Model} MongoDB Model
 *
 * @author Christian C. Larcomb
 */

import mongoose from 'mongoose'
// Adding Long (Int64) Support for Schema
import mongooseLong from "mongoose-long"
import {getSnowflakeID} from "../middleware.js";
import {getDateAsCustomEpoch} from "../global.middleware.js"
mongooseLong(mongoose)
const Long = mongoose.Schema.Types.Long

const quickieSchema = new mongoose.Schema({
    _id:       {type: Long, default: await getSnowflakeID()},
    dateEpoch: {type: Long, default: getDateAsCustomEpoch()},

    content: {
        tags: [String],
    },

    meta: {
        region:   {type: String, default: "na"},
        language: {type: String, default: "eng"},
        poster: {
            uuid: {type:Long, ref:'User'},
            suid: Long
        },
    },

    preferences: {
        maxNumPeers: [
            {
                dateEpoch: Number,
                content:   Number
            }
        ],
        vibesRange: [
            {
                dateEpoch: Number,
                min:       Number,
                max:       Number
            }
        ]
    },

    status: {
        protected: [
            {
                active:    Boolean,
                content:   Boolean,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                updatingUser: {
                    uuid:     Number,
                    username: String,
                    tag:      String
                }
            }
        ],
        restricted: [
            {
                active:          Boolean,
                reason:          String,
                underReview:     Boolean,
                expirationEpoch: Long,
                dateEpoch:       {type: Long, default: getDateAsCustomEpoch()},
                enactingModerator: {
                    uuid:     Long,
                    username: String,
                    tag:      String
                }
            }
        ]
    },

    access: {
        users: {
            restricted: [
                {
                    uuid:      Long,
                    dateEpoch: {type: Long, default: getDateAsCustomEpoch()}
                }
            ]
        }
    },

    impressions: {
        uniqueJoinCount: {type: Number, default: 0},
        vibes:           {type: Number, default: 0}
    },

    analytics: {
        joinLeaves: [
            {
                status:    String,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                user:      { uuid: Long }
            }
        ],
        vibes: [
            {
                amount:    Number,
                dateEpoch: {type: Long, default: getDateAsCustomEpoch()},
                user:      { uuid: Long }
            }
        ]
    }
},{_id: false})

quickieSchema.query.secureSelection = function () {
    return this.select(`
        -_id
        
        -analytics
    `)
}

quickieSchema.query.unsecureSelection = function () {
    return this.select(`
        -_id
        
        -analytics
        -secured
    `)
}

export default mongoose.model('Quickie', quickieSchema, 'Quickies')