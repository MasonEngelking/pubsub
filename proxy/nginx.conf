limit_req_zone $binary_remote_addr zone=ip:10m rate=5r/s;

upstream client {
    server client:43000;
}

upstream backend {
    server core:43001;
}

# Client Routing
server {
  listen 80;
  listen [::]:80;

  server_name  pubsub.pub  www.pubsub.pub;

  access_log        /var/log/nginx/reverse-access.log;
  error_log         /var/log/nginx/reverse-error.log;

  proxy_set_header  Host $host;
  proxy_set_header  X-Real-IP $remote_addr;
  proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header  X-Forwarded-Host $server_name;

  location / {
      proxy_pass      http://client;
      proxy_redirect  off;

      # If the agent is the EB Healthcheck, return 200
      if ($http_user_agent ~* '^ELB-HealthChecker\/.*$') { return 200; }
  }
}

# API Routing
server {
    listen 8080;
    listen [::]:8080;

    server_name  pubsub.pub  api.pubsub.pub;

    access_log        /var/log/nginx/reverse-access.log;
    error_log         /var/log/nginx/reverse-error.log;

    proxy_set_header  Host $host;
    proxy_set_header  X-Real-IP $remote_addr;
    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Host $server_name;

    location / {
        proxy_pass      http://backend;
        proxy_redirect  off;

        # If the agent is the EB Healthcheck, return 200
        if ($http_user_agent ~* '^ELB-HealthChecker\/.*$') { return 200; }
    }
}

# Voice Server Routing