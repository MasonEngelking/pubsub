/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Home
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import styled from 'styled-components'
import React, {
  useEffect,
  useRef,
  useState
} from 'react'
import { Link } from 'react-router-dom'

/* Company Materials */
import { ReactComponent as EmblemLogo } from '../../assets/svgs/logo_rep_emblem.svg'

import {
  KeyboardDoubleArrowDown
} from '@mui/icons-material'
import { ReactComponent as StaryNightSVG } from '../../assets/svgs/stary.animated.svg'
import Form from '../../components/core-components/form.component'
import { DARK_THEME, MISC_COLORS } from '../../constants/themes.constants.js'
import Emblem from '../../components/core-components/emblem.component'
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos'

const StaryNightSVGStyled = styled(StaryNightSVG)`
  position: absolute;
  opacity: 0.5;
  flex-shrink: 0;
  min-width: 100%;
  min-height: 100%;
`

const IndexBody = styled.div`
  overflow: hidden;
  margin:   0;
  padding:  0;
  height:   100vh;
  width:    auto;

  -webkit-touch-callout: none;
  -webkit-user-select:   none;
  -khtml-user-select:    none;
  -moz-user-select:      none;
  -ms-user-select:       none;
  user-select:           none;
`

const Content = styled.div`
  z-index:    3;
  position:   relative;
  overflow-x: hidden;
  overflow-y: scroll;
  width:      auto;
`

const AbsoluteCenteredContainer = styled.div`
  z-index:  1;
  position: absolute;
  left:     50%;
  top:      50%;
  overflow: hidden;
`

const StageOneContainer = styled.div`
  display: ${props => props.display ? 'block' : 'none'};
  z-index: 5;

  & > svg {
    position: fixed;
    transform: translate(-50%, -50%);
  }

  & > svg:nth-child(2) {
    fill: #b48cf4;
    filter: drop-shadow(0 0 3rem rgb(91, 2, 228));
  }

  & > svg:nth-child(3) {
    fill: #2d046b;
    stroke: aqua;
  }

  & > svg:nth-child(4) {
    fill: #230451;
    stroke: aqua;
  }

  & > svg:nth-child(5) {
    fill: #19023a;
    stroke: aqua;
  }
`

const StageTwoContainer = styled.div`
  position: fixed;
  color:    white;
  z-index:  22;

  @keyframes breath-animation {
    0%   { transform: scale(100%) }
    50%  { transform: scale(98%)  }
    100% { transform: scale(100%) }
  }

  & > div:nth-child(1) {
    & > div {
      display: flex;
      place-items: center;
      justify-content: center;
      position: absolute;
      border-radius: 20px;
      height: 65px;
      width: 65px;

      & > svg {
        height: 42px;
        width: 42px;
      }
    }
    
    & > div:nth-child(2)  { transform: translateX(100px)  translateY(145px)  scale(65%);  }
    & > div:nth-child(5)  { transform: translateX(0)      translateY(-188px) scale(84%);  }
    & > div:nth-child(7)  { transform: translateX(225px)  translateY(65px)   scale(102%); }
    & > div:nth-child(8)  { transform: translateX(-375px) translateY(40px)   scale(98%);  }
    & > div:nth-child(9)  { transform: translateX(-400px) translateY(-150px) scale(87%);  }
    & > div:nth-child(10) { transform: translateX(-250px) translateY(250px)  scale(91%);  }
    & > div:nth-child(11) { transform: translateX(350px)  translateY(-60px)  scale(60%);  }
    & > div:nth-child(12) { transform: translateX(-175px) translateY(-90px)  scale(65%);  }
    & > div:nth-child(13) { transform: translateX(-75px)  translateY(-180px) scale(65%);  }
    & > div:nth-child(14) { transform: translateX(-20px)  translateY(-340px) scale(105%); }
    & > div:nth-child(15) { transform: translateX(75px)   translateY(-220px) scale(65%);  }
    & > div:nth-child(16) { transform: translateX(25px)   translateY(-110px) scale(75%);  }
  }
  
  & > div:nth-child(2) {
    & > svg {
      fill:   white;
      height: 14px;
      
      & > g {
        opacity: 1;
        display: block;
      }
    }
  }
`

const ScrollDownIndicator = styled.div`

  @keyframes updown {
    0%   {transform: translateY(0);}
    50%  {transform: translateY(10px);}
    100% {transform: translateY(0);}
  }
  
  position:      fixed;
  display:       grid;
  justify-items: center;
  height:        auto;
  width:         auto;
  transform:     translate(-50%, 0);
  bottom:        30px;
  font-size:     18px;
  
  & > svg {
    fill: white;
    height: 28px;
    width:  28px;
    animation-name: updown;
    animation-duration: 4s;
    animation-iteration-count: infinite;
  }
`

const CardContainer = styled.div`
  position: absolute;
  bottom: 1px;
  display: flex;
  height: 100vh;
  width: 100%;
  justify-content: center;
  align-items: center;
  z-index: 0;
  flex-direction: column;
  
  & > div {
    height: 300px;
    width: 225px;
    margin: 10px;
    background-color: white;
    color: white;
  }
`

export default
function AboutUs (props) {
  const contentRef = useRef(null)

  const [scrollLoc, setScrollLoc] = useState(0)
  const [cursorPos, setCursorPos] = useState({ x: 0, y: 0 })

  /* Setting browser height through window innerHeight dynamically */
  useEffect(() => {
    // console.log(window.innerHeight)
    contentRef.current.style.height = (window.innerHeight) + 'px'
  }, [contentRef, window.innerHeight])

  /* Track Mouse Position for Parallax Effect */
  useEffect(() => {
    // Animation specific debugging
    // console.log(scrollLoc)
    document.onmousemove = (loc) => {
      const { x, y } = loc
      const xCalc = x - (window.innerWidth / 2)
      const yCalc = -(y - (window.innerHeight / 2))
      if (scrollLoc < 1300) { setCursorPos({ x: xCalc, y: yCalc }) }

      return () => document.removeEventListener('mousemove', () => { console.log('mousemove event removed') })
    }
  }, [scrollLoc])

  /* Get scroll position for animation */
  useEffect(() => {
    contentRef.current?.addEventListener('scroll', handleScroll, { passive: true })
    return () => { try { contentRef.current?.removeEventListener('scroll', handleScroll) } catch (e) {} }
  }, [])

  const handleScroll = async () => {
    const scrollLoc = contentRef.current?.scrollTop
    await setScrollLoc(scrollLoc)
  }

  const StageOne = () => {
    const enableMol = scrollLoc <= 1500

    return <>
      <StageOneContainer display={enableMol}>
        <div style={{
          zIndex: -1,
          position: 'fixed',
          display: scrollLoc > 400 ? 'none' : 'block',
          backgroundColor: 'rgb(15,17,22)',
          height: ((scrollLoc % 2000) + 40) * ((scrollLoc % 2000) / 100) + 50,
          width: ((scrollLoc % 2000) + 40) * ((scrollLoc % 2000) / 100) + 50,
          transform: `translateX(calc(-50% - ${(cursorPos.x / 30) * (scrollLoc / 200)}px)) translateY(calc(-50% + ${(cursorPos.y / 30) * (scrollLoc / 200)}px))`
        }}/>
        <EmblemLogo
          style={{
            zIndex: 1,
            display: scrollLoc < 450 ? 'block' : 'none',
            height: ((scrollLoc % 2000) + 40) * ((scrollLoc % 2000) / 100) + 50,
            transform: `translateX(calc(-50% - ${(cursorPos.x / 30) * (scrollLoc / 200)}px)) translateY(calc(-50% + ${(cursorPos.y / 30) * (scrollLoc / 200)}px))`
          }}
        />
        <EmblemLogo
          style={{
            zIndex: 2,
            display: scrollLoc < 500 ? 'block' : 'none',
            height: (scrollLoc % 2000) * ((scrollLoc % 2000) / 100) * 0.9,
            transform: `translateX(calc(-50% - ${(cursorPos.x / 50) * (scrollLoc / 200)}px)) translateY(calc(-50% + ${(cursorPos.y / 70) * (scrollLoc / 200)}px))`
          }}
        />
        <EmblemLogo
          style={{
            zIndex: 3,
            display: scrollLoc < 575 ? 'block' : 'none',
            height: (scrollLoc % 2000) * ((scrollLoc % 2000) / 100) * 0.6,
            transform: `translateX(calc(-50% - ${(cursorPos.x / 80) * (scrollLoc / 200)}px)) translateY(calc(-50% + ${(cursorPos.y / 110) * (scrollLoc / 200)}px))`
          }}
        />
        <EmblemLogo
          style={{
            zIndex: 4,
            display: scrollLoc < 1500 ? 'block' : 'none',
            height: (scrollLoc % 2000) * ((scrollLoc % 2000) / 100) * 0.4,
            transform: `translateX(calc(-50% - ${(cursorPos.x / 160) * (scrollLoc / 200)}px)) translateY(calc(-50% + ${(cursorPos.y / 160) * (scrollLoc / 200)}px))`
          }}
        />

      </StageOneContainer>

      <ScrollDownIndicator
        style={{
          opacity: 1 - scrollLoc / 200,
          display: (1 - scrollLoc / 200) > 0 ? 'grid' : 'none'
        }}
      >
        <KeyboardDoubleArrowDown/>
      </ScrollDownIndicator>

    </>
  }

  const StageTwo = () => {
    const displayTextRange = scrollLoc >= 245 && scrollLoc < 3800
    const displayAppRange = scrollLoc >= 0 && scrollLoc < 1200

    const determineTextSize = () => {
      if (scrollLoc >= 750) return 750
      else return scrollLoc
    }

    return (
      <StageTwoContainer
        displayText={displayTextRange}
        scrollLoc ={scrollLoc}
        style={{
          display: scrollLoc > 4000 ? 'none' : 'flex',
          placeContent: 'center',
          justifyItems: 'center',
          transform: `scale(${(scrollLoc / 20)})`,
          opacity: `${(determineTextSize())}%`
        }}
      >
        <>
          <div style={{
            position: 'fixed',
            transform: `
              scale(${(scrollLoc * scrollLoc / 400) / 900}) 
              translateX(${cursorPos.x / 150}px) 
              translateY(${-cursorPos.y / 150}px)
            `,
            display: displayAppRange ? 'block' : 'none',
            fontSize: 40
          }}>

            <div style={{ transform: `translateX(${((-determineTextSize() - 400) / 5) + (cursorPos.x / 25)}px) translateY(${-((determineTextSize() * (determineTextSize() / 400) + 400) / 3) - (cursorPos.y / 25)}px) scale(${(determineTextSize()) / 100})`, zIndex: 2 }}>
              🚀
            </div>

            <div>
              😎
            </div>

            <div style={{ transform: `translateX(${((determineTextSize() + 400) / 3) + (cursorPos.x / 20)}px) translateY(${(((determineTextSize() * (determineTextSize()) / 400) - 300) / 3) - (cursorPos.y / 20)}px) scale(${(determineTextSize() - 100) / 100})` }}>
              🤣
            </div>

            <div style={{ transform: `translateX(0px) translateY(${-(determineTextSize() + -10000) / 100}px) scale(80%)` }}>
              🤩
            </div>

            <div style={{ transform: `translateX(${(cursorPos.x / 200) + 100}px) translateY(${(-cursorPos.y / 200) - 190}px)` }}>
              🙄
            </div>

            <div style={{ transform: `scale(160%) translateX(${(cursorPos.x / 200) - 140}px) translateY(${(-cursorPos.y / 200) + 100}px)` }}>
              ⚛️
            </div>

            <div>
              🐋
            </div>

            <div style={{ transform: 'translateX(-300px) translateY(0px)' }}>
              🤘
            </div>

            <div style={{ transform: 'translateX(-250px) translateY(-100px)' }}>
              ❤️
            </div>
          </div>
        </>

        <h1 style={{
          position: 'fixed',
          transform: `scale(${((scrollLoc * (scrollLoc / 8)) / 500) / 50}) translateX(-9px) translateY(1.6px)`,
          zIndex: 10,
          width: 200,
          margin: 0,
          padding: 0
        }}>
          {/* Text can go here... */}
        </h1>
      </StageTwoContainer>
    )
  }

  const FinalStage = () => {
    return (
      <CardContainer style={{
        opacity: `${(scrollLoc / 2) - 500}%`
      }}>
        <h1 style={{ marginTop: 0, marginBottom: 25, color: 'white' }}>Getting Started!</h1>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          height: 'auto',
          backgroundColor: 'rgba(0,0,0,0)',
          width: 'auto'
        }}>
          <Emblem
            title={''}
            margin={'0 10px 0 0'}
            icon={<h1>1</h1>}
            colorOne={'red'}
            colorTwo={'red'}
          />
          <Form
            backgroundColor={ DARK_THEME.form.backgroundColor }
            borderColor={ 'red' }
            height={ '100px' }
            width={ '300px' }
            margin={'0'}
          >
            <p>
              <h3 style={{ margin: '0 0 10px 0' }}>Stop!</h3>
              Please read and review our Terms & Conditions!
            </p>
          </Form>
          <Link to={'legal'}>
            <Emblem
              title={''}
              margin={'0 0 0 10px'}
              icon={<ArrowForwardIosIcon style={{ height: 30, width: 30 }}/>}
              colorOne={ '#1f242e' }
              colorTwo={ '#2d3441' }
            />
          </Link>
        </div>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          height: 'auto',
          backgroundColor: 'rgba(0,0,0,0)',
          width: 'auto'
        }}>
          <Emblem
            title={''}
            margin={'0 10px 0 0'}
            icon={<h1>2</h1>}
            colorOne={'orange'}
            colorTwo={'orange'}
          />
          <Form
            backgroundColor={ DARK_THEME.form.backgroundColor }
            borderColor={ 'orange' }
            height={ '100px' }
            width={ '300px' }
            margin={'0'}
          >
            <p>
              <h3 style={{ margin: '0 0 10px 0' }}>Almost...</h3>
              Make your account then... (～￣▽￣)～
            </p>
          </Form>
          <Link to={'account-portal/sign-up'}>
            <Emblem
              title={''}
              margin={'0 0 0 10px'}
              icon={<ArrowForwardIosIcon style={{ height: 30, width: 30 }}/>}
              colorOne={ '#1f242e' }
              colorTwo={ '#2d3441' }
            />
          </Link>
        </div>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          height: 'auto',
          backgroundColor: 'rgba(0,0,0,0)',
          width: 'auto'
        }}>
          <Emblem
            title={''}
            margin={'0 10px 0 0'}
            icon={<h1>3</h1>}
            colorOne={'green'}
            colorTwo={'green'}
          />
          <Form
            backgroundColor={ DARK_THEME.form.backgroundColor }
            borderColor={ 'green' }
            height={ '100px' }
            width={ '300px' }
            margin={'0'}
          >
            <p>
              <h3 style={{ margin: '0 0 10px 0' }}>Go!</h3>
              Have fun! 👈(ﾟヮﾟ👈)
            </p>
          </Form>
          <Link to={'app'}>
            <Emblem
              title={''}
              margin={'0 0 0 10px'}
              icon={<ArrowForwardIosIcon style={{ height: 30, width: 30 }}/>}
              colorOne={ '#1f242e' }
              colorTwo={ '#2d3441' }
            />
          </Link>
        </div>
      </CardContainer>
    )
  }

  return (
    <>
      <StaryNightSVGStyled style={{
        transform: `scale(${100 + (scrollLoc / 100)}%)`,
        bottom: scrollLoc / 10000,
        display: scrollLoc < 400 ? 'block' : 'none'
      }}/>
      <IndexBody>

        <Content ref={ contentRef }>
          <div style={{
            height: 1200 + window.innerHeight,
            position: 'relative'
          }}>
            {FinalStage()}
          </div>
        </Content>

        <AbsoluteCenteredContainer>
          {StageOne()}
          {StageTwo()}
        </AbsoluteCenteredContainer>

      </IndexBody>
    </>
  )
}
