/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
import AllActions from '../redux/actions/index.js'
import { io } from 'socket.io-client'
import prettyConsoleLog from './prettyConsoleLog.js'

export default
class SocketClient {
  constructor (uri, options) {
    // Useful variables
    this.uri = uri
    this.options = options
    this.socket = io(this.uri, this.options)
    this.seconds = 4

    this.isConnected = false
    this.absoluteDisconnect = false

    // Establish the connection
    this.connectionManager()
  }

  on (event, func) { this.socket.on(event, func) }

  emit (ev, data, func = undefined) { this.socket.emit(ev, data, func) }

  disconnect () { this.socket.disconnect() }

  connectionManager () {
    try {
      /* Handle Reconnection Upon Close */
      const tryReconnect = () => {
        setTimeout(() => {
          this.seconds *= 2
          this.socket.io.open(err => { if (err) tryReconnect() })
        }, this.seconds * 1000)
      }

      /* Update Render State Upon Healthy Socket Connection */
      this.socket.on('connect', () => {
        prettyConsoleLog('success', `Established connection to ${this.uri}!`)
        this.isConnected = true
      })

      /* Re-Attempt Connection Upon Client-Side Connection Error */
      this.socket.on('connect_error', () => {
        prettyConsoleLog('error', `Failed to establish connection to ${this.uri}.`)

        if (!this.absoluteDisconnect) {
          prettyConsoleLog('info', `Retrying in ${this.seconds} seconds...`)
          this.isConnected = false
          tryReconnect()
        }
      })

      /* Received Disconnect from Server, reconnect depending on reason */
      this.socket.on('disconnect', () => {
        this.seconds = 4
        prettyConsoleLog('error', `Socket connection dropped with ${this.uri}!`)
        this.isConnected = false

        if (!this.absoluteDisconnect) { tryReconnect() }
      })
    } catch (e) {
      console.log(`Had trouble connecting to ${this.uri}!`)
      console.log(e)
    }
  }
}
