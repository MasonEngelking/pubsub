/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Slider
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const SliderContainer = styled.div`

  display: flex;
  align-items: center;

  margin: 12px 0 8px 0;
  padding: ${props => props.padding};
  
  & > input
  {
    flex:          6;
    width:         100%;
    height:        5px;
    border-radius: 5px;
    background:    #212121;

    -webkit-appearance: none;
    
    margin:  0;
    padding: 0;
    
    
    /* Track */
    &::-webkit-slider-runnable-track {
      
    }
    
    &::-moz-range-track {
      
    }
    
    &::-ms-track {
      
    }
    
    /* Thumb */
    &::-webkit-slider-thumb {

      margin:             0;
      width:              10px;
      height:             10px;
      border-radius:      2px;
      transform:          rotate(45deg);
      background-color:   white;
      cursor:             ew-resize;
      opacity:            ${props => props.enabled ? 1 : 0};
      -webkit-transition: .1s;
      transition:         opacity .1s;
      
      -webkit-appearance: none;
      appearance:         none;
      
    }
    
    &::-moz-range-thumb {
      
    }

    &::-ms-thumb {
      
    }
  }
`

export default
function Slider (props) {
  const [value, setValue] = useState(props.defaultValue)

  useEffect(() => {
    /* Passing state up */
    if (props.enabled) { props.stateMirror(value) }
  }, [value])

  return (
    <SliderContainer
      enabled={props.enabled}
    >
      <input
        type ={'range'}
        min ={0}
        max ={100}
        value ={value}
        onChange={e => setValue(e.target.value)}
        disabled={!props.enabled}
      />
    </SliderContainer>
  )
}

Slider.displayName = 'Slider'
Slider.propTypes = {
  enabled: PropTypes.bool,
  stateMirror: PropTypes.func,
  defaultValue: PropTypes.number,
  sliderRadius: PropTypes.string,
  sliderWidth: PropTypes.string,
  thumbRadius: PropTypes.string
}
Slider.defaultProps = {
  stateMirror: () => {},
  enabled: true,
  sliderRadius: '10px',
  sliderWidth: '100%',
  thumbRadius: '100%',
  defaultValue: 50
}
