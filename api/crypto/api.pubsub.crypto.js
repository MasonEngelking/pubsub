/**
 * Copyright 2022 Christian C. Larcomb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
 * to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Crypto.API
 *
 * @returns {Express} Express Node
 *
 * @author Christian C. Larcomb
 */

/* EXPRESS IMPORTS */
import express from 'express'
import cors    from 'cors'
import axios   from 'axios'

/* Local Functions */
import {
    generateJWTs,
    generateAccessKeys,
    generateSnowflakeID,
    generateStreamKey,
    preventInvalidContextType,
    preventInvalidJWTData,
    encryptUsingAES,
    encryptUsingSHA256,
    encryptUsingBcrypt,
    returnArray,
    preventInvalidEncryptionContextType,
    decryptUsingAES,
    decryptUsingBcrypt,
    preventInvalidDecryptionContextType,
    preventEmptyArrayRequest,
    decryptUsingJWT,
    authTokenProcessing
} from './middleware.js'

/* Global Functions */
import {
    handleHeathCheck,
    handleAppListen,
    isTrustedSource
} from './global.middleware.js'

/* OS IMPORTS */
import os      from 'os'
import cluster from 'cluster'

/* CONFIG & .ENV IMPORTS */
import dotenv from 'dotenv/config'

/* EXPRESS-APP CONFIGURATION */
const app = express()
app.use(cors())
app.use(express.json())
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin',  '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    return next()
})

/* ~~~ MASTER PROCESS ~~~ */
if (cluster.isMaster)
{
    console.log(`Master process <${process.pid}> is running.`)

    /* For Workers */
    for(let i = 0; i < os.cpus().length; i++)
        cluster.fork()

    cluster.on('exit', (worker, code, signal) => console.log(`Worker ${worker.process.pid} died.`))
}

/* WORKER PROCESSES -> HANDLE API CALLS */
else {

    /**
     * @description Tells the express application to begin listening on the configured network
     * @status      Operational
     */
    app.listen(process.env.API_PORT, handleAppListen)

    /**
     * @description Used to verify that the node is responding
     * @status      Operational
     */
    app.get('/api/v1/crypto/healthcheck',
        /* Security */
        isTrustedSource,
        /* Processing */
        handleHeathCheck)

    /**
     * @description Generates the requested JSON web-tokens
     * @status      Operational
     */
    app.post('/api/v1/crypto/generate',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidContextType,
        preventInvalidJWTData,
        /* Processing */
        authTokenProcessing,
        generateJWTs,
        generateSnowflakeID,
        generateAccessKeys,
        generateStreamKey)

    /**
     * @description Encrypts hashes per request
     * @status      Operational
     */
    app.post('/api/v1/crypto/encrypt',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidEncryptionContextType,
        preventEmptyArrayRequest,
        /* Processing */
        encryptUsingAES,
        encryptUsingSHA256,
        encryptUsingBcrypt,
        /* Return */
        returnArray)

    /**
     * @description Encrypts hashes per request
     * @status      Operational
     * @notes       This functions as Verify for Bcrypt but Decrypt for AES
     */
    app.post('/api/v1/crypto/decrypt',
        /* Security */
        isTrustedSource,
        /* Prevention */
        preventInvalidDecryptionContextType,
        preventEmptyArrayRequest,
        /* Processing */
        decryptUsingAES,
        decryptUsingBcrypt,
        decryptUsingJWT,
        /* Return */
        returnArray)

}