/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Selector_Account-Portal
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'

import { Link } from 'react-router-dom'
import { SECTION_COLORS } from '../../constants/themes.constants.js'
import Form from '../../components/core-components/form.component.jsx'
import Emblem from '../../components/core-components/emblem.component.jsx'

/* SVG Imports */
import { ReactComponent as HomeSVG } from '../../assets/svgs/home.svg'
import { ReactComponent as SignInSVG } from '../../assets/svgs/sign-in.svg'
import { ReactComponent as SignUpSVG } from '../../assets/svgs/sign-up.svg'
import { ReactComponent as GuestSVG } from '../../assets/svgs/guest.svg'
import { notify } from '../../components/core-components/notifications.component.jsx'

const HomeSVGStyled = styled(HomeSVG)`
  fill: white;
  height: 35px;
`
const SignInSVGStyled = styled(SignInSVG)`
  fill: white;
  height: 35px;
`
const SignUpSVGStyled = styled(SignUpSVG)`
  fill: white;
  height: 35px;
`
const GuestSVGStyled = styled(GuestSVG)`
  fill: white;
  height: 35px;
`

const FormContents = styled.div`
  
  color: white;
  
  /* SVG Wrapper Container */
  & > div:nth-child(1){
    
    display: flex;
    justify-content: center;
    
    /* The Logo Holder */
    & > div:nth-child(1){
      position:      relative;
      border-radius: 100%;
      background:    radial-gradient(${props => props.obj.colors[0]}, ${props => props.obj.colors[1]});
      height:        75px;
      width:         75px;
      margin-top:   -45px;
    }
  }
  
  /* Text Container */
  & > div:nth-child(2){
    width: 190px;
    & > div { 
      text-align: center;
      margin: 4px 0;
    }
    & > div:nth-child(1){font-size: 20px;}
    & > div:nth-child(2){font-size: 14px; opacity: 0.50}
  }
`

const SELECTIONS = [
  {
    title: 'Home',
    description: 'Go back to our homepage.',
    colors: [SECTION_COLORS.homepage[0], SECTION_COLORS.homepage[1]],
    linkTo: '/',
    svg: <HomeSVGStyled/>,
    callback: () => { }
  },
  {
    title: 'Sign In',
    description: "Let's get you back in the conversation!",
    colors: [SECTION_COLORS.signIn[0], SECTION_COLORS.signIn[1]],
    linkTo: '/account-portal/sign-in',
    svg: <SignInSVGStyled/>,
    callback: () => {}
  },
  {
    title: 'Sign Up',
    description: "Well, hello! Let's get you started.",
    colors: [SECTION_COLORS.signUp[0], SECTION_COLORS.signUp[1]],
    linkTo: '/account-portal/sign-up',
    svg: <SignUpSVGStyled/>,
    callback: () => {}
  },
  {
    title: 'Guest',
    description: 'Join in on the conversation as a guest.',
    colors: [SECTION_COLORS.guest[0], SECTION_COLORS.guest[1]],
    linkTo: '/account-portal/guest',
    svg: <GuestSVGStyled/>,
    callback: () => {}
  }
]

export default
function AccountSelector (props) {
  const theme = useContext(ThemeContext)

  return (
    SELECTIONS.map((obj, i) => {
      return (
        <Link
          key={i}
          to={obj.linkTo}
          style={{ textDecoration: 'none' }}
          onClick={obj.callback}
        >
          <Form
            backgroundColor={theme.form.backgroundColor}
            borderColor={obj.colors[1]}
            margin={'5px'}
            cursor={'pointer'}
          >
            <Emblem
              colorOne={obj.colors[0]}
              colorTwo={obj.colors[1]}
              title={obj.title}
              description={obj.description}
              margin={'-45px 0 0 0'}
              icon={obj.svg}
            />
          </Form>
        </Link>
      )
    })
  )
}
