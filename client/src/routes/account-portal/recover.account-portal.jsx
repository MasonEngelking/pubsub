/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Recover_Account-Portal
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useContext, useEffect, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { SECTION_COLORS } from '../../constants/themes.constants.js'

import Form from '../../components/core-components/form.component.jsx'
import Field from '../../components/core-components/field.component.jsx'
import Emblem from '../../components/core-components/emblem.component.jsx'
import Button from '../../components/core-components/button.component.jsx'
import { notify } from '../../components/core-components/notifications.component.jsx'

/* SVG Imports */
import { ReactComponent as SafetySVG } from '../../assets/svgs/safety.svg'

const SafetySVGStyled = styled(SafetySVG)`
  fill: white;
  height: 35px;
`

const FormContents = styled.div`
  
  color: white;
  min-width: 250px;
  
  /* SVG Wrapper Container */
  & > div:nth-child(1){
    
    display: flex;
    justify-content: center;
    
    /* The Logo Holder */
    & > div:nth-child(1){
      position:      relative;
      border-radius: 100%;
      background:    radial-gradient(${SECTION_COLORS.signIn[0]}, ${SECTION_COLORS.signIn[1]});
      height:        75px;
      width:         75px;
    }
  }
  
  /* Text Container */
  & > div:nth-child(2){
    min-width: 250px;
    & > div { 
      text-align: center;
      margin: 4px 0;
    }
    & > div:nth-child(1){font-size: 20px;}
  }
`

/* This is an Implementation Component */
export default
function RecoverAccountPortal (props) {
  const theme = useContext(ThemeContext)

  const [page, setPage] = useState(0)
  const [loginAttempts, setLoginAttempts] = useState(0)

  useEffect(() => {
    // TODO: Implement # of attempt(s) limitations/logic
  })

  return (
    <Form
      props ={props}
      borderColor ={SECTION_COLORS.recovery[1]}
      backgroundColor={theme.form.backgroundColor}
      minWidth ={250}
      padding ={10}
    >

      <Emblem
        props={props}
        colorOne={SECTION_COLORS.recovery[0]}
        colorTwo={SECTION_COLORS.recovery[1]}
        title={'Account Recovery'}
        icon={<SafetySVGStyled/>}
      />

      <div style={{ height: 3 }}/>

      <Field
        width ={255}
        maxCharCount ={50}
        enabled ={true}
        initText ="Email"
        required ={true}
        title ={'Email'}
        footnote ={'Please provide the email you last remember'}
        eBackgroundColor={theme.field.enabled.backgroundColor}
        eBorderColor ={theme.field.enabled.borderColor}
      />

      <div style={{ height: 15 }}/>

      <Button
        text={'Begin Recovery'}
        onClick={e => {
          notify('Woah!', "We're having trouble at the moment. Try back in a bit!", 'error')
          console.log('HI')
        }}
      />

      <Button
        text={'Cancel'}
        linkTo='/account-portal/sign-in'
      />

    </Form>
  )
}
