/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name BubbleVote
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Button from './button.component'

import { ReactComponent as Upvote } from './../../assets/svgs/like.svg'
import { ReactComponent as Misc } from './../../assets/svgs/dots.svg'
import image from '../../assets/testing/blurple-arch.jpg'

const BubbleVoteContainer = styled.div`
  display:          flex;
  width:            100%;
  color:            ${props => props.color};
  
  height:           170px;
  border-radius:    5px;
  overflow:         hidden;
  background-color: ${props => props.backgroundColor};
  border-color:     ${props => props.borderColor};
  border-width:     ${props => props.borderWidth};
  border-style:     ${props => props.borderStyle};

  /* Shifting Content Container */
  & > div:nth-child(1){
    display: flex;
    width: 100%;

    & > div:nth-child(1){
      transition: width 0.3s, opacity 0.3s;
      width: 200px;
      flex-grow: 1;
    }
    & > div:nth-child(2){
      width: 40px;
      flex-grow: 1;
    }
    
    &:hover{
      & > div:nth-child(1){
        width: 100px;
        opacity: 0.2;
      }
    }
  }
  
  & > div:nth-child(2){ width: 55px; }
`

export default
function BubbleVote ({
  backgroundColor,
  color,
  width,
  minWidth,
  maxWidth,
  height,
  minHeight,
  maxHeight,
  borderStyle,
  borderWidth,
  borderColor,
  borderRadius
}) {
  return (
    <BubbleVoteContainer
      backgroundColor={backgroundColor}
      color ={color}

      width ={width}
      minWidth ={minWidth}
      maxWidth ={maxWidth}

      height ={height}
      minHeight ={minHeight}
      maxHeight ={maxHeight}

      borderStyle ={borderStyle}
      borderWidth ={borderWidth}
      borderColor ={borderColor}
      borderRadius ={borderRadius}
    >
      {/* Shifting Content Container */}
      <div>
        {/* Text Box */}
        <div
          style={{
            margin: 8,
            overflow: 'hidden',
            display: 'flex',
            justifyContent: 'space-between',
            flexDirection: 'column'
          }}
        >
          <div>
            <div
              style={{
                fontSize: 16,
                marginBottom: 3,
                width: 205
              }}
            >
                            This is the title! LMFAO! Test.
            </div>
            <div
              style={{
                fontSize: 14,
                opacity: 0.5,
                width: 205
              }}
            >
                            This is a pretty cool description. I might need to limit the total number of characters. This is what 120 looks like!!!!
            </div>
          </div>
          <div>
            <div style={{ fontSize: 14 }}>
                            Tags
            </div>
            <div style={{ fontSize: 14 }}>
                            Tag 1, Tag 2
            </div>
          </div>
        </div>

        {/* Image Container */}
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden'
          }}
        >
          <img src={image} alt="Logo" style={{
            maxHeight: '100%'
          }} />
        </div>
      </div>

      {/* Interaction Bar */}
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center',
          margin: '2px 0'
        }}
      >
        <div
          style={{
            height: 35,
            fontSize: 13,
            fontWeight: 700,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div>
                        1.2K
          </div>

        </div>
        <div>
          <Button
            contents ={<Upvote style={{ fill: '#ffffff', height: 20, marginTop: '-1px' }}/>}
            borderRadius ={'6px'}
            height ={'35px'}
            width ={'40px'}
            borderStyle ={'none'}
            hoverColor ={'#ffffff'}
            backgroundColor ={backgroundColor}
          />
          <Button
            contents ={<Upvote style={{ fill: '#ffffff', height: 20, transform: 'rotate(180deg)', marginTop: '2px' }}/>}
            borderRadius ={'6px'}
            height ={'35px'}
            width ={'40px'}
            borderStyle ={'none'}
            backgroundColor ={backgroundColor}
          />
        </div>
        <Button
          contents ={<Misc style={{ fill: '#ffffff', width: 20, transform: 'rotate(180deg)', marginTop: '2px' }}/>}
          borderRadius ={'6px'}
          height ={'35px'}
          width ={'40px'}
          borderStyle ={'none'}
          backgroundColor ={backgroundColor}
        />
      </div>

    </BubbleVoteContainer>
  )
}

BubbleVote.displayName = 'Bubble Vote'
BubbleVote.propTypes = {
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,

  width: PropTypes.string,
  minWidth: PropTypes.string,
  maxWidth: PropTypes.string,

  height: PropTypes.string,
  minHeight: PropTypes.string,
  maxHeight: PropTypes.string,

  borderStyle: PropTypes.string,
  borderWidth: PropTypes.string,
  borderColor: PropTypes.string,
  borderRadius: PropTypes.string
}
BubbleVote.defaultProps = {
  backgroundColor: '#000000',
  color: '#ffffff',

  width: '',
  minWidth: '',
  maxWidth: '',

  height: '',
  minHeight: '',
  maxHeight: '',

  borderStyle: 'solid',
  borderWidth: '1px',
  borderColor: '#020202',
  borderRadius: '5px'
}
