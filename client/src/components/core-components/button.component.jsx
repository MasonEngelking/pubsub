/**
 * Copyright 2022 Christian C. Larcomb
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @name Button
 *
 * @returns {Element} React Component
 *
 * @author Christian C. Larcomb
 */

import React, { useState } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { usePopper } from 'react-popper'
import { Tooltip } from '@mui/material'

const ButtonContainer = styled.div`
  position: relative;
  display:  flex;
  height:   ${props => props.autoHeight ? '100%' : undefined};

  & > button {

    position: relative;

    display:         flex;
    justify-content: center;
    align-items:     center;

    flex-grow: ${props => props.flexGrow};

    width:     ${props => props.width};
    min-width: ${props => props.minWidth}px;
    max-width: ${props => props.maxWidth}px;

    height:     ${props => props.height};
    min-height: ${props => props.minHeight}px;
    max-height: ${props => props.maxHeight}px;

    border-style: ${props => props.borderStyle};
    border-width: ${props => props.borderWidth}px;
    border-color:     ${props => props.borderColor};
    background-color: ${props => props.backgroundColor};

    border-radius: ${props => props.borderRadius};
    color:         ${props => props.color};

    margin:  ${props => props.margin};
    padding: ${props => props.padding};

    font-size: ${props => props.fontSize}px;
    /* Prevent Text Selection */
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select:   none; /* Safari */
    -khtml-user-select:    none; /* Konqueror HTML */
    -moz-user-select:      none; /* Old versions of Firefox */
    -ms-user-select:       none; /* Internet Explorer/Edge */
    user-select:           none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */

    /* When hovering */
    &:hover {
      cursor: pointer;
      filter: brightness(275%);
    }

    /* When pressed */
    &:active {filter: brightness(90%);}
    /* Accessibly: Todo: in the future */
    // &:focus {
    //   background-color: $ {props => props.focusColor};
    //   outline:          none;
    //   box-shadow:       0 0 0 3px lightskyblue;
    // }
    /* RAINBOW-GLOW EFFECT */
    ${props => {
    if (props.effect === 'rainbow-glow') {
      return `
            &:before {
                content:         '';
                background:      linear-gradient(45deg, #ff0000, #ff7300, #fffb00, #48ff00, #00ffd5, #002bff, #7a00ff, #ff00c8, #ff0000);
                position:        absolute;
                top:             -2px;
                left:            -2px;
                background-size: 400%;
                z-index:         -1;
                filter:          blur(5px);
                width:           calc(100% + 4px);
                height:          calc(100% + 4px);
                animation:       glowing 50s linear infinite;
                opacity:         0;
                transition:      opacity .3s ease-in-out;
                border-radius:   ${props.borderRadius};
              }
            
              &:active {
                color: #000
              }
            
              &:hover:before {
                opacity: 1;
              }
            
              &:after {
                z-index:       -1;
                content:       '';
                position:      absolute;
                width:         100%;
                height:        100%;
                background:    #111;
                left:          0;
                top:           0;
                border-radius: ${props.borderRadius};
                filter:        brightness(40%);
              }
            
              @keyframes glowing {
                0%   { background-position: 0 0; }
                50%  { background-position: 400% 0; }
                100% { background-position: 0 0; }
              }
          `
    }
  }}
  }

`

export default
function Button (props) {
  const [displayMenu, setDisplayMenu] = useState(false)

  const [referenceElement, setReferenceElement] = useState(null)
  const [popperElement, setPopperElement] = useState(null)

  const { styles, attributes } = usePopper(referenceElement, popperElement, props.menuOptions)

  const handleButtonTypeRender = () => {
    if (typeof props.linkTo !== 'undefined') {
      return (
        <Link to={props.linkTo} style={{ textDecoration: 'none' }}>
          <Tooltip title={props.tooltipContents}>
            <ButtonContainer
              display ={props.display}

              backgroundColor={props.backgroundColor}
              borderColor ={props.borderColor}
              hoverColor ={props.hoverColor}
              focusColor ={props.focusColor}
              activeColor ={props.activeColor}
              color ={props.color}

              borderRadius ={props.borderRadius}
              borderWidth ={props.borderWidth}
              borderStyle ={props.borderStyle}
              height ={props.height}
              minHeight ={props.minHeight}
              maxHeight ={props.maxHeight}
              width ={props.width}
              minWidth ={props.minWidth}
              maxWidth ={props.maxWidth}

              margin ={props.margin}
              padding ={props.padding}
              fontSize ={props.fontSize}

              effect ={props.effect}

              flexGrow ={props.flexGrow}

              autoHeight ={props.autoHeight}
            >
              <button
                onClick={e => {
                  if (props.menuContents !== undefined) { setDisplayMenu(!displayMenu) }
                  props.onClick(e)
                }}
                ref={setReferenceElement}
              >
                {
                  typeof props.contents !== 'undefined'
                    ? props.contents
                    : props.text
                }
              </button>

            </ButtonContainer>
          </Tooltip>
        </Link>
      )
    } else {
      return (
        <Tooltip title={props.tooltipContents}>
          <ButtonContainer
            display ={props.display}

            backgroundColor={props.backgroundColor}
            borderColor ={props.borderColor}
            hoverColor ={props.hoverColor}
            focusColor ={props.focusColor}
            activeColor ={props.activeColor}
            color ={props.color}

            borderRadius ={props.borderRadius}
            borderWidth ={props.borderWidth}
            borderStyle ={props.borderStyle}
            height ={props.height}
            minHeight ={props.minHeight}
            maxHeight ={props.maxHeight}
            width ={props.width}
            minWidth ={props.minWidth}
            maxWidth ={props.maxWidth}

            margin ={props.margin}
            padding ={props.padding}
            fontSize ={props.fontSize}

            effect ={props.effect}

            flexGrow ={props.flexGrow}

            autoHeight ={props.autoHeight}
          >
            <button
              onClick={e => {
                if (props.menuContents !== undefined) { setDisplayMenu(!displayMenu) }
                props.onClick(e)
              }}
              ref={setReferenceElement}
            >
              {
                typeof props.contents !== 'undefined'
                  ? props.contents
                  : props.text
              }
            </button>

          </ButtonContainer>
        </Tooltip>
      )
    }
  }

  return handleButtonTypeRender()
}

Button.displayName = 'Button'
Button.propTypes = {
  onClick: PropTypes.func,
  linkTo: PropTypes.string,
  text: PropTypes.string,
  contents: PropTypes.element,

  tooltipContents: PropTypes.element,
  tooltipPlacement: PropTypes.oneOf(['top', 'right', 'bottom', 'left']),

  menuContents: PropTypes.element,
  menuOptions: PropTypes.object,

  popupContent: PropTypes.element,
  popupLocation: PropTypes.oneOf(['']),
  popupDistance: PropTypes.number,

  backgroundColor: PropTypes.string,
  borderColor: PropTypes.string,
  hoverColor: PropTypes.string,
  focusColor: PropTypes.string,
  activeColor: PropTypes.string,
  color: PropTypes.string,
  effect: PropTypes.oneOf(['rainbow-glow']),

  display: PropTypes.string,

  borderStyle: PropTypes.string,
  borderRadius: PropTypes.string,
  borderWidth: PropTypes.number,

  height: PropTypes.string,
  minHeight: PropTypes.number,
  maxHeight: PropTypes.number,

  width: PropTypes.string,
  minWidth: PropTypes.number,
  maxWidth: PropTypes.number,

  margin: PropTypes.string,
  padding: PropTypes.string,

  fontSize: PropTypes.number,
  flexGrow: PropTypes.number,

  autoHeight: PropTypes.bool
}
Button.defaultProps = {
  onClick: () => {},
  display: 'flex',
  text: 'Button',
  tooltipPlacement: 'top',
  backgroundColor: '#080808',
  borderColor: '#1d1d1d',
  hoverColor: '#111111',
  focusColor: '#111111',
  activeColor: '#080808',
  color: '#ffffff',
  borderStyle: 'solid',
  borderRadius: '5px',
  borderWidth: 1.5,
  height: '50px',
  width: '100%',
  margin: '3px 0'
}
